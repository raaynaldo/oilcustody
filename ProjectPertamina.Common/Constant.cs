﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.Common
{
    public class Constant
    {
        public enum Bulan
        {
            Januari = 1,
            Februari = 2,
            Maret = 3,
            April = 4,
            Mei = 5,
            Juni = 6,
            Juli = 7,
            Agustus = 8,
            September = 9,
            Oktober = 10,
            November = 11,
            Desember = 12,
        }

    }

    public class ConstantTipeProdukKode
    {
        public const string Oil = "Oil";
        public const string Hidrogen = "Hidrogen";
        public const string Electricity = "Listrik";
        public const string NSW = "NSW";
    }

    public class ConstantKonfigurasiParameter
    {
        public const string SupplyLossBeritaAcara = "SupplyLossBeritaAcara";
        public const string CustodyTransferBeritaAcara = "CustodyTransferBeritaAcara";
        public const string CustodyTransferReportExcel = "CustodyTransferExcel";
    }

    public class ConstantPerusahaan
    {
        public const string PerusahaanPertaminaKode = "P001";
        public const string PerusahaanPTSKKode = "P002";
    }
}