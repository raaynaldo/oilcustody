﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface ISupplyLossSupplyRepository : IGenericRepository<SupplyLossSupply>
    {
        List<string> GetListTrasportType();

        List<VMSupplyLossesSupply> getValueSupplyLossSupply(string PortDescription, string NamaProduk, string TransportType,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        IEnumerable<SupplyLossSupply> GetAllJoinData();
        List<SupplyLossSupply> SearchSupplyLossSupplyEdit(DateTime ? TanggalBL, DateTime ? TanggalBLSampai);
        List<SupplyLossSupply> SearchSupplyLossSupplyForSupplyLoss(decimal nilai, DateTime ? tanggal, DateTime ? tanggalSampai, string NamaKapal);
    }
}
