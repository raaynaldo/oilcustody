﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities.Models.ViewModel;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface IKKSupplyLossRepository : IGenericRepository<KKSupplyLoss>
    {
        List<string> GetListTrasportType();

        Tuple<List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>> getValueKKSupplyLoss(string LoadingPort, string Produk, string Vessel,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai);

        IEnumerable<KKSupplyLoss> GetAllJoinData();

        List<KKSupplyLoss> SearchKKSupplyLossEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);
    }
}
