﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface ISupplyLossDischargeRepository : IGenericRepository<SupplyLossDischarge>
    {
        List<string> GetListTrasportType();
        
        List<VMSupplyLossDischarge> getValueSupplyLossDischarge(string AlatAngkut, string LoadingPort, string Material,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        IEnumerable<SupplyLossDischarge> GetAllJoinData();
        IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);
        IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeForSupplyLoss(decimal nilai, DateTime? Tanggal, DateTime? TanggalSampai, string NamaKapal);
    }
}
