﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface IZmmSupplyLossRepository : IGenericRepository<ZmmSupplyLoss>
    {

    }
}
