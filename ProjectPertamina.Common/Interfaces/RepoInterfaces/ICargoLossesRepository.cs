﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.ViewModel;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface ICargoLossesRepository : IGenericRepository<CargoLosses>
    {
        List<VMChartCargoLosses> getValueCargoLosses(string Muatan, string Asal, string NamaKapal,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        IEnumerable<CargoLosses> GetAllJoinData();
        List<CargoLosses> SearchCargoLossesEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);

        //CargoLosses GetByIdJoinData(int Id);
    }
}
