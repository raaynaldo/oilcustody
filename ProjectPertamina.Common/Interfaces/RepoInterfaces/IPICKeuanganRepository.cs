﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface IPICKeuanganRepository : IGenericRepository<PICKeuangan>
    {
    }
}
