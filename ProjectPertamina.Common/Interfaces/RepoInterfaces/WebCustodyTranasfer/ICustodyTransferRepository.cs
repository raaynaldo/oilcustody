﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer
{
    public interface ICustodyTransferRepository : IGenericRepository<CustodyTransfer>
    {
        IEnumerable<CustodyTransfer> GetAllJoinData();
        IEnumerable<CustodyTransfer> GetAllJoinData(int bulan, int tahun);
        CustodyTransfer GetJoinAllDatabyId(int id);
    }
}
