﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer
{
    public interface IStreamRepository : IGenericRepository<Stream>
    {
        IEnumerable<Stream> GetAllJoinData();
        IEnumerable<Stream> GetAllJoinData(Expression<Func<Stream, bool>> predicate);
    }
}
