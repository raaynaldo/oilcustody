﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.RepoInterfaces
{
    public interface ISupplyLossRepository : IGenericRepository<SupplyLoss>
    {
        IEnumerable<SupplyLoss> SearchByDocDate(DateTime ? tanggal, DateTime ? tanggalSampai, string NamaKapal);
        IEnumerable<SupplyLoss> GetAllJoinData();
        SupplyLoss GetBySupplyLossSupply(int supplyLossSupplyId);
        SupplyLoss GetBySupplyLossDischarge(int supplyLossDischargeId);

    }
}
