﻿using ProjectPertamina.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Common.Interfaces.BaseInterfaces
{
    public interface IGenericRepository<T> where T : BaseEntities
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        T Get(object Id);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);

        void Update(T entity);

        void Remove(object Id);
        //void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}
