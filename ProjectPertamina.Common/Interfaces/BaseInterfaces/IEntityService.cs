﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities;
using System.Linq.Expressions;

namespace ProjectPertamina.Common.Interfaces.BaseInterfaces
{
    public interface IEntityService<T> 
    {        
        //void Delete(T entity);
        void Delete(int ID);
        void DeleteRange(IEnumerable<T> entities);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        T Get(object Id);        
        void Save(T entity);
        void AddRange(IEnumerable<T> entities);
        void UpdateRange(IEnumerable<T> entities);

    }
}
