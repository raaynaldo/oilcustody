﻿using DataTables.Mvc;
using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class BackgroundNoTelpController : BaseController
    {
        private IBackgroundNoTelpService _backgroundNoTelpService;
        public BackgroundNoTelpController(IBackgroundNoTelpService CampaignService)
        {
            _backgroundNoTelpService = CampaignService;
        }
        // GET: Admin/BackgroundNoTelp
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, string Telepon)
        {
            var obj = _backgroundNoTelpService.GetAll();

            var data = obj.Select(mainApp => new
            {
                Id = mainApp.Id,
                Gambar = mainApp.Gambar
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Count(), obj.Count()), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit(int id = 0)
        {
            var backgroundNoTelp = _backgroundNoTelpService.Get(id);

            return PartialView("_CreateEdit", backgroundNoTelp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(HttpPostedFileBase NamaFile, BackgroundNoTelp model)
        {
            if (NamaFile != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                if (NamaFile != null)
                {
                    string gambarLama = model.Gambar;
                    model.Gambar = SaveFile(NamaFile, ConstantKonfigurasiParameter.BackgroundNoTel);
                    if (!string.IsNullOrEmpty(gambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(gambarLama, ConstantKonfigurasiParameter.BackgroundNoTel);
                    }
                    _backgroundNoTelpService.Save(model);
                    return Content("success");
                }
                else
                {
                    ModelState["Gambar"].Errors.Add(new ModelError("The Gambar field is required."));
                    return View("_CreateEdit", model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }
    }
}