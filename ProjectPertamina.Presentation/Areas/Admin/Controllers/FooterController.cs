﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class FooterController : Controller
    {
        private IFooterService _footerService;
        public FooterController(IFooterService FooterService)
        {
            _footerService = FooterService;
        }

        // GET: Admin/Footer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Company, string Address, string Phone,
            string Email, string Copyright)
        {
            var obj = _footerService.GetAll();

            var data = obj.Select(camp => new
            {
                Id = camp.Id,
                Company = camp.Company,
                Address = camp.Address,
                Phone = camp.Phone,
                Email = camp.Email,
                Copyright = camp.Copyright
            });
            
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Count(), obj.Count()), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var footer = _footerService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", footer);
            return View();
        }

        [HttpPost]
        public ActionResult CreateEdit(Footer model)
        {
            if (!ModelState.IsValid)
                return View("_CreateEdit", model);
            try
            {

                _footerService.Save(model);
                //Success();
                return Content("success");
                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

    }
}