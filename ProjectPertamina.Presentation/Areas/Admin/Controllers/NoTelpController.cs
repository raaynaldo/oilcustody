﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class NoTelpController : BaseController
    {
        private INoTelpService _noTelpService;

        public NoTelpController(INoTelpService NoTelpService)
        {
            _noTelpService = NoTelpService;
        }

        // GET: Admin/NoTelp
        public ActionResult Index()
        {
            return View(new NoTelp());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, string Telepon)
        {
            var obj = _noTelpService.SearchNoTelp(requestModel.Start, requestModel.Length, Judul, Telepon);

            var data = obj.Item1.Select(berita => new
            {
                Id = berita.Id,
                Judul = berita.Judul,
                NoTelp = berita.Telp,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var notelp = _noTelpService.Get(id);
            ViewBag.Title = "Edit";
            if (notelp == null)
            {
                notelp = new NoTelp();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", notelp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(NoTelp model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                _noTelpService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var noTelp = _noTelpService.Get(Id);
            return PartialView("_Delete", noTelp);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _noTelpService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}