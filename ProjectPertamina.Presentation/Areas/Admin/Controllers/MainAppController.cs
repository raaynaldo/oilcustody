﻿using DataTables.Mvc;
using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class MainAppController : BaseController
    {
        private IMainAppService _mainAppService;

        public MainAppController(IMainAppService MainAppService)
        {
            _mainAppService = MainAppService;
        }
        // GET: Admin/MainApp
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, string Telepon)
        {
            var obj = _mainAppService.GetAll().OrderBy(i=>i.Urutan);

            var data = obj.Select(mainApp => new
            {
                Id = mainApp.Id,
                Link = mainApp.Link,
                Urutan = mainApp.Urutan
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Count(), obj.Count()), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit(int id = 0)
        {
            var notelp = _mainAppService.Get(id);
            ViewBag.Title = "Edit";
            if (notelp == null)
            {
                notelp = new MainApp();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", notelp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(HttpPostedFileBase NamaFile, MainApp model)
        {
            if (NamaFile != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                ViewBag.Title = "Add";
                return View("_CreateEdit", model);
            }
            try
            {
                if (!_mainAppService.CheckUrutan(model.Urutan))
                {
                    model.Gambar = SaveFile(NamaFile, ConstantKonfigurasiParameter.MainApp);
                    _mainAppService.Save(model);
                    return Content("success");
                }
                else
                {
                    ViewBag.Title = "Add";
                    ModelState.AddModelError("Urutan", "Urutan already exist");
                    return View("_CreateEdit", model);
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(HttpPostedFileBase NamaFile, MainApp mainApp)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Title = "Edit";
                return View("_CreateEdit", mainApp);
            }
            try
            {
                if (NamaFile != null)
                {
                    string gambarLama = mainApp.Gambar;
                    mainApp.Gambar = SaveFile(NamaFile, ConstantKonfigurasiParameter.MainApp);
                    if (!string.IsNullOrEmpty(gambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(gambarLama, ConstantKonfigurasiParameter.MainApp);
                    }
                }

                _mainAppService.ChangeUrutan(mainApp.Id, mainApp.Urutan);

                _mainAppService.Save(mainApp);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", mainApp);
            }
        }

        public ActionResult Delete(int Id)
        {
            var MainApp = _mainAppService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", MainApp);
            return View(MainApp);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _mainAppService.Get(id);
                string gambar = obj.Gambar;
                _mainAppService.Delete(id);

                if (!string.IsNullOrEmpty(gambar))
                    DeleteFile(gambar, ConstantKonfigurasiParameter.MainApp);

                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}