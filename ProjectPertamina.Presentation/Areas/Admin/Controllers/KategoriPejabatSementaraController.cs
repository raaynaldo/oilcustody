﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
        public class KategoriPejabatSementaraController : BaseController
    {
        private IKategoriPejabatSementaraService _kategoripejabatSementaraService;
        public KategoriPejabatSementaraController(IKategoriPejabatSementaraService KategoriPejabatSementaraService)
        {
            _kategoripejabatSementaraService = KategoriPejabatSementaraService;
        }

        public ActionResult Index()
        {
            return View(new KategoriPejabatSementara());
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Kategori)
        {
            var obj = _kategoripejabatSementaraService.SearchKategoriPejajabatSementara(requestModel.Start, requestModel.Length, Kategori);

            var data = obj.Item1.Select(Rapat => new
            {
                Id = Rapat.Id,
                Kategori = Rapat.Kategori,
                Status = Rapat.Status,
            });

            //var data = obj.Item1.Skip(requestModel.Start).Take(requestModel.Length);

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateEdit(int id = 0)
        {
            var kategoriBerita = _kategoripejabatSementaraService.Get(id);
            ViewBag.Title = "Edit";
            if (kategoriBerita == null)
            {
                kategoriBerita = new KategoriPejabatSementara();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", kategoriBerita);
        }

        // POST: Admin/PejabatSementara/Create
        [HttpPost]
        public ActionResult CreateEdit(KategoriPejabatSementara model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {

                _kategoripejabatSementaraService.Save(model);
                //Success();
                return Content("success");
                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var KategoriPejabatSementara = _kategoripejabatSementaraService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", KategoriPejabatSementara);
            return View(KategoriPejabatSementara);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _kategoripejabatSementaraService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }

}