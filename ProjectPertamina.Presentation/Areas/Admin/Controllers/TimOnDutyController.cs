﻿using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectPertamina.Entities.Models;
using DataTables.Mvc;
using System.IO;
using ProjectPertamina.Common;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class TimOnDutyController : BaseController
    {
        private ITimOnDutyService _timOnDutyService;

        public TimOnDutyController(ITimOnDutyService TimOnDutyService)
        {
            _timOnDutyService = TimOnDutyService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest mimiPeri, string Judul, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _timOnDutyService.SearchTimOnDuty(mimiPeri.Start, mimiPeri.Length, Judul, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(TimOnDuty => new
            {
                Id = TimOnDuty.Id,
                NamaTim = TimOnDuty.Judul,
                Attachment = TimOnDuty.Attachment,
                Tanggal = TimOnDuty.CreatedDate,
            });

            return Json(new DataTablesResponse(mimiPeri.Draw, data, obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var TimOnDuty = _timOnDutyService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", TimOnDuty);
            return View();
        }
        public ActionResult Create()
        {
            ViewBag.Title = "Add";
            TimOnDuty cp = new TimOnDuty();
            return PartialView("_CreateEdit", cp);
        }

        // POST: Admin/TimOnDuty/Create
        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult CreateEdit(HttpPostedFileBase InputGambar, HttpPostedFileBase InputAttachment, TimOnDuty model, bool Deleted)
        {
            if (InputGambar != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                if (InputGambar != null)
                {
                    string gambarLama = model.Gambar;
                    model.Gambar = SaveFile(InputGambar, ConstantKonfigurasiParameter.TeamOnDuty);
                    if (!string.IsNullOrEmpty(gambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(gambarLama, ConstantKonfigurasiParameter.TeamOnDuty);
                    }
                }

                string attachmentLama = model.Attachment;
                if (InputAttachment != null)
                {
                    model.Attachment = SaveFile(InputAttachment, ConstantKonfigurasiParameter.TeamOnDuty);
                    _timOnDutyService.Save(model);
                    if (!string.IsNullOrEmpty(attachmentLama)) //check ada gambar lama?
                    {
                        DeleteFile(attachmentLama, ConstantKonfigurasiParameter.TeamOnDuty);
                    }
                }

                if (Deleted)//kalo apus file attachment
                {
                    model.Attachment = "";
                    _timOnDutyService.Save(model);
                    DeleteFile(attachmentLama, ConstantKonfigurasiParameter.TeamOnDuty);
                }

                _timOnDutyService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var timOnDuty = _timOnDutyService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", timOnDuty);
            return View(timOnDuty);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _timOnDutyService.Get(id);
                string gambar = obj.Gambar;
                string attachment = obj.Attachment;
                _timOnDutyService.Delete(id);

                if (!string.IsNullOrEmpty(gambar))
                    DeleteFile(gambar, ConstantKonfigurasiParameter.TeamOnDuty);

                if (!string.IsNullOrEmpty(attachment))
                    DeleteFile(attachment, ConstantKonfigurasiParameter.TeamOnDuty);

                //SuccessDelete();
                return Content("success");
            }
            catch
            {
                //Danger();
                //return RedirectToAction("Index");
                return Content("error");
            }
        }
    }
}