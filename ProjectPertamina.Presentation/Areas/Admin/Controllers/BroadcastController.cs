﻿using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectPertamina.Entities.Models;
using DataTables.Mvc;
using System.IO;
using ProjectPertamina.Common;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class BroadcastController : BaseController
    {
        private IBroadcastService _broadcastService;
        public BroadcastController(IBroadcastService BroadcastService)
        {
            _broadcastService = BroadcastService;
        }

        public ActionResult Index()
        {
            return View(new Broadcast());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _broadcastService.SearchBroadcast(requestModel.Start, requestModel.Length, Judul, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(broadcast => new
            {
                Id = broadcast.Id,
                Judul = broadcast.Judul,
                CreatedDate = broadcast.CreatedDate
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var broadcast = _broadcastService.Get(Id);
            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", broadcast);
            return View(broadcast);
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Add";
            Broadcast cp = new Broadcast();
            return PartialView("_CreateEdit", cp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(HttpPostedFileBase inputFile, Broadcast model)
        {
            if (inputFile != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                //Checking file is available to save.  
                if (inputFile != null)
                {
                    string GambarLama = model.Gambar;
                    model.Gambar = SaveFile(inputFile, ConstantKonfigurasiParameter.Broadcast);
                    if (!string.IsNullOrEmpty(GambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(GambarLama, ConstantKonfigurasiParameter.Broadcast);
                    }
                }
                _broadcastService.Save(model);

                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }

        }

        public ActionResult Delete(int Id)
        {
            var broadcast = _broadcastService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", broadcast);
            return View(broadcast);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _broadcastService.Get(id);
                string Gambar = obj.Gambar;
                if (!string.IsNullOrEmpty(Gambar)) //check ada gambar lama?
                {
                    DeleteFile(Gambar, ConstantKonfigurasiParameter.Broadcast);
                }
                _broadcastService.Delete(id);
                //SuccessDelete();
                return Content("success");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}