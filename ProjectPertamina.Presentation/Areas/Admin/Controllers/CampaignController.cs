﻿using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectPertamina.Entities.Models;
using DataTables.Mvc;
using System.IO;
using ProjectPertamina.Common;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class CampaignController : BaseController
    {
        private ICampaignService _campaignService;
        public CampaignController(ICampaignService CampaignService)
        {
            _campaignService = CampaignService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Tipe, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _campaignService.SearchCampaign(requestModel.Start, requestModel.Length, Tipe, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Tipe = camp.Tipe,
                NamaFile = camp.NamaFile
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        // public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
        //string Tipe, DateTime? TanggalDari, DateTime? TanggalSampai)
        // {
        //     var obj = _campaignService.SearchCampaign(requestModel.Start, requestModel.Length, Tipe, TanggalDari, TanggalSampai);

        //     //var data = obj.Item1.Select(campaign => new
        //     //{
        //     //    Id = campaign.Id,
        //     //    Tipe = campaign.Tipe,
        //     //    Gambar = campaign.Gambar,
        //     //    VideoLink = campaign.VideoLink
        //     //});

        //     #region Sorting
        //     //var sortedColumns = requestModel.Columns.GetSortedColumns();
        //     //var orderByString = String.Empty;

        //     //foreach (var column in sortedColumns)
        //     //{
        //     //    orderByString += orderByString != String.Empty ? "," : "";
        //     //    orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
        //     //}
        //     //data = data.OrderBy(orderByString == string.Empty ? "Kategori asc" : orderByString);
        //     #endregion

        //     // Paging
        //     //data = data.Skip(requestModel.Start).Take(requestModel.Length);

        //     return Json(new DataTablesResponse(requestModel.Draw, obj.Item1, obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        // }


        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var campaign = _campaignService.Get(Id);
            //ViewBag.Gambar = campaign.NamaFile;
            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", campaign);
            return View(campaign);
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Add";
            Campaign cp = new Campaign();
            return PartialView("_CreateEdit", cp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(HttpPostedFileBase NamaFile, Campaign model)
        {
            if (NamaFile != null && model.Tipe == "Gambar")
            {
                ModelState["NamaFile"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            //return RedirectToAction("CreateEdit", new { id = campaign.Id });
            try
            {
                //Checking file is available to save.  
                if (NamaFile != null)
                {
                    string gambarLama = model.NamaFile;
                    model.NamaFile = SaveFile(NamaFile, ConstantKonfigurasiParameter.Campaign);
                    if (!string.IsNullOrEmpty(gambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(gambarLama, ConstantKonfigurasiParameter.Campaign);
                    }
                }
                else if (model.Tipe == "Gambar")
                {
                    ModelState["NamaFile"].Errors.Add(new ModelError("The Nama File field is required."));
                    return View("_CreateEdit", model);
                }

                _campaignService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }

        public ActionResult Delete(int Id)
        {
            var campaign = _campaignService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", campaign);
            return View(campaign);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _campaignService.Get(id);
                string namaFile = obj.NamaFile;
                if (obj.Tipe == "Gambar")
                {
                    if (!string.IsNullOrEmpty(namaFile)) //check ada gambar lama?
                    {
                        DeleteFile(namaFile, ConstantKonfigurasiParameter.Campaign);
                    }
                }
                _campaignService.Delete(id);

                //SuccessDelete();
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}