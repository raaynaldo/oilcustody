﻿using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        private IKonfigurasiParameterService konfigurasiParameterService = DependencyResolver.Current.GetService<IKonfigurasiParameterService>();

        public void Success()
        {
            AddAlert(AlertStyles.Success, "Sukses !", "Data berhasil disimpan");
        }

        public void SuccessDelete()
        {
            AddAlert(AlertStyles.Success, "Sukses !", "Data berhasil dihapus");
        }

        public void Danger()
        {
            AddAlert(AlertStyles.Danger, "Gagal !", "Terjadi kesalahan");
        }

        private void AddAlert(string alertStyle, string info, string message)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Info = info,
                Message = message
            });

            TempData[Alert.TempDataKey] = alerts;
        }

        #region Function
        protected string SaveFile(HttpPostedFileBase InputGambar, string KeyKonfigParam)
        {
            string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;

            var InputFileName = Path.GetFileName(InputGambar.FileName);
            var ServerSavePath = Path.Combine(Url + InputFileName);
            if (System.IO.File.Exists(ServerSavePath))
            {
                var InputFileNameWithoutExt = Path.GetFileNameWithoutExtension(InputGambar.FileName);
                var InputFileExtName = Path.GetExtension(InputGambar.FileName);
                InputFileName = InputFileNameWithoutExt + "_" + DateTime.Now.ToString("ddmmyyyyhhmmss") + InputFileExtName;
                ServerSavePath = Path.Combine(Url + InputFileName);
            }
            InputGambar.SaveAs(ServerSavePath);

            return InputFileName;
        }

        protected string SaveFile(HttpPostedFileBase InputGambar)
        {
            var InputFileName = Path.GetFileName(InputGambar.FileName);
            var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + InputFileName);
            if (System.IO.File.Exists(ServerSavePath))
            {
                var InputFileNameWithoutExt = Path.GetFileNameWithoutExtension(InputGambar.FileName);
                var InputFileExtName = Path.GetExtension(InputGambar.FileName);
                InputFileName = InputFileNameWithoutExt + "_" + DateTime.Now.ToString("ddmmyyyyhhmmss") + InputFileExtName;
                ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + InputFileName);
            }
            InputGambar.SaveAs(ServerSavePath);

            return InputFileName;
        }

        protected void DeleteFile(string NamaFile, string KeyKonfigParam)
        {
            string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;

            var ServerSavePath = Path.Combine(Url + NamaFile);
            if (System.IO.File.Exists(ServerSavePath))
            {
                System.IO.File.Delete(ServerSavePath);
            }

            return;
        }

        public async Task<ActionResult> imagerRDP(string KeyKonfigParam, string ImageName)
        {
            //string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;
            var Url = await Search(KeyKonfigParam);

            if (string.IsNullOrEmpty(ImageName))
                return null;
            var path = Path.Combine(Url, ImageName);
            path = Path.GetFullPath(path);
            if (!path.StartsWith(Url))
            {
                // Ensure that we are serving file only inside the root folder
                // and block requests outside like "../web.config"
                throw new HttpException(403, "Forbidden");
            }
            
            return File(path, "image/*");
        }

        public async Task<string> Search(string KeyKonfigParam)
        {
            string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;
            return Url;
        } 
        #endregion
    }
}