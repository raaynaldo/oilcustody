﻿using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectPertamina.Entities.Models;
using DataTables.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class RapatController : BaseController
    {
        private IRapatService _rapatService;
        public RapatController(IRapatService RapatService)
        {
            _rapatService = RapatService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _rapatService.SearchRapat(requestModel.Start, requestModel.Length, Judul, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(Rapat => new
            {
                Id= Rapat.Id,
                JudulRapat = Rapat.Judul,
                TanggalRapat = Rapat.TanggalRapat,
                LinkRapat = Rapat.Link
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var Rapat = _rapatService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", Rapat);
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Add";
            Rapat cp = new Rapat() { TanggalRapat = DateTime.Now};
            return PartialView("_CreateEdit", cp);
        }

        // POST: Admin/Rapat/Create
        [HttpPost]
        public ActionResult CreateEdit(Rapat model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                // TODO: Add insert logic here
                _rapatService.Save(model);
                return Content("success");
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var Rapat = _rapatService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", Rapat);
            return View(Rapat);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                _rapatService.Delete(id);
                if (Request.IsAjaxRequest())
                {
                    message = "success";
                }
            }
            catch
            {
                message = "Error";
            }
            return Content(message);
        }

    }
}