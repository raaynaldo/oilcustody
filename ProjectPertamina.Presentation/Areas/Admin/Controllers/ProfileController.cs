﻿using DataTables.Mvc;
using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class ProfileController : BaseController
    {
        private IProfileService _profileService;

        public ProfileController(IProfileService ProfileService)
        {
            _profileService = ProfileService;
        }

        // GET: Admin/Profile
        public ActionResult Index()
        {
            var profile = _profileService.GetAll().ToList();
            return View(new Profile());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul)
        {
            var obj = _profileService.SearchProfile(requestModel.Start, requestModel.Length, Judul);

            var data = obj.Item1.Select(profile => new
            {
                Id = profile.Id,
                Judul = profile.Judul,
                Urutan = profile.Urutan
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit(int id = 0)
        {
            var notelp = _profileService.Get(id);
            ViewBag.Title = "Edit";
            if (notelp == null)
            {
                notelp = new Profile();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", notelp);
        }

        // POST: Admin/PejabatSementara/Create
        [HttpPost]
        public ActionResult CreateEdit(HttpPostedFileBase inputFile, Profile model, bool Deleted)
        {
            if (inputFile != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                if (inputFile != null)
                {
                    string GambarLama = model.Gambar;
                    model.Gambar = SaveFile(inputFile, ConstantKonfigurasiParameter.Profile);
                    if (!string.IsNullOrEmpty(GambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(GambarLama, ConstantKonfigurasiParameter.Profile);
                    }
                    _profileService.Save(model);
                }

                if (Deleted)//kalo apus file attachment
                {
                    string gambarLama = model.Gambar;
                    model.Gambar = "";
                    _profileService.Save(model);
                    DeleteFile(gambarLama, ConstantKonfigurasiParameter.Profile);
                }

                _profileService.Save(model);
                string a = null;
                a.ToString();
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var Profile = _profileService.Get(Id);
            return PartialView("_Delete", Profile);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _profileService.Get(id);
                string Gambar = obj.Gambar;
                if (!string.IsNullOrEmpty(Gambar)) //check ada gambar lama?
                {
                    DeleteFile(Gambar, ConstantKonfigurasiParameter.Profile);
                }
                _profileService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}