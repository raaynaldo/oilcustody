﻿using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectPertamina.Entities.Models;
using DataTables.Mvc;
using System.IO;
using ProjectPertamina.Common;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class SuratEdaranController : BaseController
    {
        private ISuratEdaranService _suratEdaranService;
        public SuratEdaranController(ISuratEdaranService SuratEdaranService)
        {
            _suratEdaranService = SuratEdaranService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _suratEdaranService.SearchSuratEdaran(requestModel.Start, requestModel.Length, Judul, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(suratEdaran => new
            {
                Id = suratEdaran.Id,
                JudulSurat = suratEdaran.Judul,
                Attachment = suratEdaran.Attachment,
                Sumber = suratEdaran.Sumber
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int Id)
        {
            ViewBag.Title = "Edit";
            var SuratEdaran = _suratEdaranService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", SuratEdaran);
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Add";
            SuratEdaran cp = new SuratEdaran();
            return PartialView("_CreateEdit", cp);
        }

        // POST: Admin/SuratEdaran/Create
        [HttpPost]
        public ActionResult CreateEdit(HttpPostedFileBase InputGambar, HttpPostedFileBase InputAttachment, SuratEdaran model, bool Deleted)
        {
            if (InputGambar != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                if (InputGambar != null)
                {
                    string gambarLama = model.Gambar;
                    model.Gambar = SaveFile(InputGambar, ConstantKonfigurasiParameter.SuratEdaran);
                    if (!string.IsNullOrEmpty(gambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(gambarLama, ConstantKonfigurasiParameter.SuratEdaran);
                    }
                }

                string attachmentLama = model.Attachment;
                if (InputAttachment != null)
                {
                    model.Attachment = SaveFile(InputAttachment, ConstantKonfigurasiParameter.SuratEdaran);
                    _suratEdaranService.Save(model);
                    if (!string.IsNullOrEmpty(attachmentLama)) //check ada gambar lama?
                    {
                        DeleteFile(attachmentLama,ConstantKonfigurasiParameter.SuratEdaran);
                    }
                }

                if (Deleted)//kalo apus file attachment
                {
                    model.Attachment = "";
                    _suratEdaranService.Save(model);
                    DeleteFile(attachmentLama, ConstantKonfigurasiParameter.SuratEdaran);
                }

                _suratEdaranService.Save(model);
                return Content("success");
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var SuratEdaran = _suratEdaranService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", SuratEdaran);
            return View(SuratEdaran);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _suratEdaranService.Get(id);
                string gambar = obj.Gambar;
                string attachment = obj.Attachment;
                _suratEdaranService.Delete(id);

                if (!string.IsNullOrEmpty(gambar))
                    DeleteFile(gambar, ConstantKonfigurasiParameter.SuratEdaran);

                if (!string.IsNullOrEmpty(attachment))
                    DeleteFile(attachment, ConstantKonfigurasiParameter.SuratEdaran);

                return Content("success");
            }
            catch
            {
                return Content("error");
            }
        }
    }
}