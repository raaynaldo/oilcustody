﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class PejabatSementaraController : Controller
    {
        private IPejabatSementaraService _pejabatSementaraService;
        public PejabatSementaraController(IPejabatSementaraService PejabatSementaraService)
        {
            _pejabatSementaraService = PejabatSementaraService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string kosong)
        {
            IEnumerable<PejabatSementara> query = _pejabatSementaraService.GetAll();
            var totalCount = query.Count();

            var filteredCount = query.Count();

            // Paging
            query = query.Skip(requestModel.Start).Take(requestModel.Length);
            var data = query.ToList();

            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int Id)
        {
            var PejabatSementara = _pejabatSementaraService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_CreateEdit", PejabatSementara);
            return View();
        }

        public ActionResult Create()
        {
            PejabatSementara cp = new PejabatSementara();
            return PartialView("_CreateEdit", cp);
        }

        // POST: Admin/PejabatSementara/Create
        [HttpPost]
        public ActionResult CreateEdit(PejabatSementara model)
        {
            if (!ModelState.IsValid)
                return View("_CreateEdit", model);
            try
            {
                // TODO: Add insert logic here

                _pejabatSementaraService.Save(model);
                return Content("success");
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var PejabatSementara = _pejabatSementaraService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", PejabatSementara);
            return View(PejabatSementara);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int Id)
        {
            string message = string.Empty;
            try
            {
                _pejabatSementaraService.Delete(Id);
                if (Request.IsAjaxRequest())
                {
                    message = "success";
                }
            }
            catch
            {
                message = "Error";

            }
            return Content(message);
        }

    }

}