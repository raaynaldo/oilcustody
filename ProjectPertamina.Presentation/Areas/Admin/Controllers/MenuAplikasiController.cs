﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class MenuAplikasiController : Controller
    {
        private IMenuAplikasiService _menuAplikasiService;
        private IKategoriMenuAplikasiService _katMenuAplikasiService;
        public MenuAplikasiController(IMenuAplikasiService MenuAplikasiService, IKategoriMenuAplikasiService KategoriMenuAplikasiService)
        {
            _menuAplikasiService = MenuAplikasiService;
            _katMenuAplikasiService = KategoriMenuAplikasiService;
        }
        public ActionResult Index()
        {
            var Kategori = _katMenuAplikasiService.GetAll().ToList();
            Kategori.Insert(0, new KategoriMenuAplikasi() { Kategori = "All", Id = 0 });
            ViewBag.Kategori = Kategori;
            return View(new MenuAplikasi());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
        int Kategori, string MenuName, string UrlFile)
        {
            var obj = _menuAplikasiService.SearchMenuAplikasi(requestModel.Start, requestModel.Length, Kategori, MenuName, UrlFile);

            var data = obj.Item1.Select(menuaplikasi => new
            {
                Id = menuaplikasi.Id,
                MenuName= menuaplikasi.MenuName,
                Kategori = menuaplikasi.KategoriMenuAplikasiId != 0 ? menuaplikasi.KategoriMenuAplikasi.Kategori : "Tidak Ada Kategori",
                Parent = menuaplikasi.Parent,
                UrlFile = menuaplikasi.UrlFile,
                Orders = menuaplikasi.Orders,
                ModuleDescr = menuaplikasi.ModuleDescr,
                Status = menuaplikasi.Status
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var menuAplikasi = _menuAplikasiService.Get(id);
            var Kategori = _katMenuAplikasiService.GetAll().ToList();
            Kategori.Insert(0, new KategoriMenuAplikasi() { Kategori = "--Pilih Kategori--", Id = 0 });
            ViewBag.Kategori = Kategori;
            ViewBag.Title = "Edit";
            if (menuAplikasi == null)
            {
                menuAplikasi = new MenuAplikasi();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", menuAplikasi);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(MenuAplikasi model)
        {
            if (!ModelState.IsValid)
            {
                var Kategori = _katMenuAplikasiService.GetAll().ToList();
                Kategori.Insert(0, new KategoriMenuAplikasi() { Kategori = "--Pilih Kategori--", Id = 0 });
                ViewBag.Kategori = Kategori;
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                _menuAplikasiService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var menuAplikasi = _menuAplikasiService.Get(Id);
            return PartialView("_Delete", menuAplikasi);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _menuAplikasiService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}