﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class KategoriMenuAplikasiController : Controller
    {
        private IKategoriMenuAplikasiService _kategoriMenuAplikasiService;
        public KategoriMenuAplikasiController(IKategoriMenuAplikasiService KategoriBeritaService)
        {
            _kategoriMenuAplikasiService = KategoriBeritaService;
        }

        // GET: Admin/KategoriBerita
        public ActionResult Index()
        {
            return View(new KategoriMenuAplikasi());
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Kategori)
        {
            var obj = _kategoriMenuAplikasiService.SearchKategoriMenuAplikasi(requestModel.Start, requestModel.Length, Kategori);

            var data = obj.Item1.Select(KategoriMenuAplikasi => new
            {
                Id = KategoriMenuAplikasi.Id,
                Kategori = KategoriMenuAplikasi.Kategori,
                Status = KategoriMenuAplikasi.Status,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateEdit(int id = 0)
        {
            var menuAplikasi = _kategoriMenuAplikasiService.Get(id);
            ViewBag.Title = "Edit";
            if (menuAplikasi == null)
            {
                menuAplikasi = new KategoriMenuAplikasi();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", menuAplikasi);
        }

        [HttpPost]
        public ActionResult CreateEdit(KategoriMenuAplikasi model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                _kategoriMenuAplikasiService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var KategoriMenuAplikasi = _kategoriMenuAplikasiService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", KategoriMenuAplikasi);
            return View(KategoriMenuAplikasi);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _kategoriMenuAplikasiService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}