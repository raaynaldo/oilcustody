﻿using DataTables.Mvc;
using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class ProdukController : BaseController
    {
        private IProdukService _produkService;

        public ProdukController(IProdukService ProdukService)
        {
            _produkService = ProdukService;
        }

        // GET: Admin/Produk
        public ActionResult Index()
        {
            return View(new Produk());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Judul)
        {
            var obj = _produkService.SearchProduk(requestModel.Start, requestModel.Length, Judul);
            var data = obj.Item1.Select(produk => new
            {
                Id = produk.Id,
                Judul = produk.Judul,
                Deskripsi = produk.Deskripsi.Count() > 25 ? produk.Deskripsi.Substring(0, 25) + "..." : produk.Deskripsi
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit(int id = 0)
        {
            var notelp = _produkService.Get(id);
            ViewBag.Title = "Edit";
            if (notelp == null)
            {
                notelp = new Produk();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", notelp);
        }

        // POST: Admin/PejabatSementara/Create
        [HttpPost]
        public ActionResult CreateEdit(HttpPostedFileBase inputFile, Produk model, bool Deleted)
        {
            if (inputFile != null)
            {
                ModelState["Gambar"].Errors.Clear();
            }
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {
                if (inputFile != null)
                {
                    string GambarLama = model.Gambar;
                    model.Gambar = SaveFile(inputFile, ConstantKonfigurasiParameter.Produk);
                    if (!string.IsNullOrEmpty(GambarLama)) //check ada gambar lama?
                    {
                        DeleteFile(GambarLama, ConstantKonfigurasiParameter.Produk);
                    }

                    _produkService.Save(model);

                    return Content("success");
                }

                if (Deleted)//kalo apus file attachment
                {
                    string gambarLama = model.Gambar;
                    model.Gambar = "";
                    DeleteFile(gambarLama, ConstantKonfigurasiParameter.Produk);

                    _produkService.Save(model);

                    return Content("success");
                }

                _produkService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var produk = _produkService.Get(Id);
            return PartialView("_Delete", produk);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var obj = _produkService.Get(id);
                string Gambar = obj.Gambar;
                if (!string.IsNullOrEmpty(Gambar)) //check ada gambar lama?
                {
                    DeleteFile(Gambar, ConstantKonfigurasiParameter.Produk);
                }

                _produkService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}