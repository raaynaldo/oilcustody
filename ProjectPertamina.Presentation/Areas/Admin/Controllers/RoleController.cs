﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
//using ProjectPertamina.Presentation.Models;
using System.Net;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class RoleController : Controller
    {
        IRoleService _roleService;

        public RoleController(IRoleService RoleService)
        {
            _roleService = RoleService;
        }
        // GET: Admin/Role
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Role/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Role/Create
        public ActionResult Create()
        {
            Role rl = new Role();
            return View("_CreatePartial", rl);
        }

        // POST: Admin/Role/Create
        [HttpPost]
        public ActionResult Create(Role role)
        {
            if (!ModelState.IsValid)
                return View("_CreatePartial", role);
            try
            {
                // TODO: Add insert logic here

                _roleService.Save(role);
                return Content("success");
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreatePartial", role);
            }
        }

        // GET: Admin/Role/Edit/5
        public ActionResult Edit(int Id)
        {
            var Role = _roleService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_EditPartial", Role);
            return View();
        }

        // POST: Admin/Role/Edit/5
        [HttpPost]
        public ActionResult Edit(Role role)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return View(Request.IsAjaxRequest() ? "_EditPartial" : "Edit", role);
            }

            try
            {
                // TODO: Add insert logic here

                _roleService.Save(role);
                return Content("success");
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_EditPartial", role);
            }  
        }

        // GET: Admin/Role/Delete/5
        public ActionResult Delete(int Id)
        {
            var role = _roleService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", role);
            return View(role);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                _roleService.Delete(id);
                if (Request.IsAjaxRequest())
                {
                    message = "success";
                }
            }
            catch
            {
                message = "Error";

            }
            return Content(message);
        }

        //Region Datasource

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,  string Description)
        {
            IEnumerable<Role> query = _roleService.GetAll();
            var totalCount = query.Count();

            // searching and sorting
            var queryFilter = _roleService.SearchRole(requestModel.Start, requestModel.Length, Description);//SearchRole(requestModel, txtDescription, query);
            var filteredCount = _roleService.SearchRoleCount(Description);//query.Count();

            // Paging
            //query = query.Skip(requestModel.Start).Take(requestModel.Length);


            //var sortedColumns = requestModel.Columns.GetSortedColumns();
            //var orderByString = String.Empty;

            //foreach (var column in sortedColumns)
            //{
            //    orderByString += orderByString != String.Empty ? "," : "";
            //    orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            //}

            //queryFilter = queryFilter.OrderBy(orderByString == string.Empty ? "RoleName asc" : orderByString);

            var data = queryFilter.ToList();

            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);

        }
        
    }
}
