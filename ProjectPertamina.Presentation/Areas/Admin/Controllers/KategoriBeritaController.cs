﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class KategoriBeritaController : Controller
    {
        private IKategoriBeritaService _kategoriBeritaService;
        public KategoriBeritaController(IKategoriBeritaService KategoriBeritaService)
        {
            _kategoriBeritaService = KategoriBeritaService;
        }

        // GET: Admin/KategoriBerita
        public ActionResult Index()
        {
            return View(new KategoriBerita());
        }
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Kategori)
        {
            var obj = _kategoriBeritaService.SearchKategoriBerita(requestModel.Start, requestModel.Length, Kategori);

            var data = obj.Item1.Select(KategoriBerita => new
            {
                Id = KategoriBerita.Id,
                Kategori = KategoriBerita.Kategori,
                Status = KategoriBerita.Status,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateEdit(int id = 0)
        {
            var kategoriBerita = _kategoriBeritaService.Get(id);
            ViewBag.Title = "Edit";
            if (kategoriBerita == null)
            {
                kategoriBerita = new KategoriBerita();
                ViewBag.Title = "Add";
            }

            return PartialView("_CreateEdit", kategoriBerita);
        }

        // POST: Admin/PejabatSementara/Create
        [HttpPost]
        public ActionResult CreateEdit(KategoriBerita model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                    ViewBag.Title = "Add";
                else
                    ViewBag.Title = "Edit";
                return View("_CreateEdit", model);
            }
            try
            {

                _kategoriBeritaService.Save(model);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var KategoriBerita = _kategoriBeritaService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_Delete", KategoriBerita);
            return View(KategoriBerita);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _kategoriBeritaService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}