﻿using DataTables.Mvc;
using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.Admin.Controllers
{
    public class BeritaController : BaseController
    {
        private IBeritaService _beritaService;
        private IBeritaDetailService _beritaDetailService;
        private IKategoriBeritaService _kategoriBeritaService;

        public BeritaController(IBeritaService BeritaService, IBeritaDetailService BeritaDetailService, IKategoriBeritaService KategoriBeritaService)
        {
            _beritaService = BeritaService;
            _beritaDetailService = BeritaDetailService;
            _kategoriBeritaService = KategoriBeritaService;
        }


        public ActionResult Index()
        {
            var Kategori = _kategoriBeritaService.GetAll().ToList();
            Kategori.Insert(0, new KategoriBerita() { Kategori = "All", Id = 0 });
            ViewBag.Kategori = Kategori;
            return View(new Berita());
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
        int Kategori, string JudulBerita,
        DateTime? Tanggal, DateTime? Sampai, string Penulis, string Sumber, string IsiBerita)
        {
            var obj = _beritaService.SearchBerita(requestModel.Start, requestModel.Length, Kategori, JudulBerita, Tanggal, Sampai, Penulis, Sumber, IsiBerita);
            //var obj = _beritaService.SearchBerita(requestModel.Start, requestModel.Length, Kategori, "", DateTime.Now, DateTime.Now, "", "", "");


            var data = obj.Item1.Select(berita => new
            {
                Id = berita.Id,
                Kategori = berita.KategoriBerita.Kategori,
                Judul = berita.Judul,
                Tanggal = berita.Tanggal,
                Penulis = berita.Penulis,
                Sumber = berita.Sumber,
                Isi = berita.IsiBerita.Count() > 25 ? berita.IsiBerita.Substring(0, 25) + "..." : berita.IsiBerita
            });

            #region Sorting
            //var sortedColumns = requestModel.Columns.GetSortedColumns();
            //var orderByString = String.Empty;

            //foreach (var column in sortedColumns)
            //{
            //    orderByString += orderByString != String.Empty ? "," : "";
            //    orderByString += (column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
            //}
            //data = data.OrderBy(orderByString == string.Empty ? "Kategori asc" : orderByString);
            #endregion

            // Paging
            //data = data.Skip(requestModel.Start).Take(requestModel.Length);

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var role = _beritaService.Get(Id);

            return PartialView("_Delete", role);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                var berita = _beritaService.Get(id);
                foreach (var item in berita.BeritaDetails)
                {
                    DeleteFile(item.Gambar, ConstantKonfigurasiParameter.Berita);
                }
                _beritaService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }


        public ActionResult IndexDetail(int id = 0)
        {
            var Kategori = _kategoriBeritaService.Find(i => i.Status);
            ViewBag.Kategori = Kategori;

            List<SelectListItem> Status = new List<SelectListItem>();
            Status.Add(new SelectListItem() { Text = "Yes", Value = "True" });
            Status.Add(new SelectListItem() { Text = "No", Value = "False" });
            ViewBag.Status = Status;

            if (id == 0)
            {
                ViewBag.Title = "Add Berita";
                return View(new Berita() { Tanggal = DateTime.Now, Status = true });
            }
            else
            {
                ViewBag.Title = "Edit Berita";
                return View(_beritaService.Get(id));
            }
        }

        public ActionResult GetDetail([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int BeritaId = 0)
        {
            //var obj = _beritaService.SearchBerita(requestModel.Start, requestModel.Length, Kategori, JudulBerita, Tanggal, Sampai, Penulis, Sumber, IsiBerita);
            var obj = _beritaService.Get(BeritaId);
            if (obj == null)
                obj = new Berita();
            var count = obj.BeritaDetails.Count();
            var data = obj.BeritaDetails.Select(beritaDetail => new
            {
                Id = beritaDetail.Id,
                Gambar = beritaDetail.Gambar,
                Date = beritaDetail.CreatedDate
            });

            // Paging
            data = data.Skip(requestModel.Start).Take(requestModel.Length);

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), count, count), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(HttpPostedFileBase[] files, Berita berita)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("IndexDetail", new { id = berita.Id });
            try
            {
                //beritaDetail.Gambar = ConvertToBytes(file);
                _beritaService.Save(berita);
                foreach (HttpPostedFileBase file in files)
                {
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        _beritaDetailService.Save(new BeritaDetail() { Gambar = SaveFile(file, ConstantKonfigurasiParameter.Berita), BeritaId = berita.Id });
                    }
                }
                Success();
                return RedirectToAction("IndexDetail", new { id = berita.Id });
            }
            catch (Exception)
            {
                Danger();
                return RedirectToAction("IndexDetail", new { id = berita.Id });
            }

        }

        public ActionResult DeleteDetail(int Id)
        {
            var role = _beritaDetailService.Get(Id);

            if (Request.IsAjaxRequest())
                return PartialView("_DeleteDetail", role);
            return View(role);
        }

        [HttpPost, ActionName("DeleteDetail")]
        public ActionResult DeleteDetailAction(int id)
        {
            try
            {
                var beritaDetail = _beritaDetailService.Get(id);
                string Gambar = beritaDetail.Gambar;
                if (!string.IsNullOrEmpty(Gambar)) //check ada gambar lama?
                {
                    DeleteFile(Gambar, ConstantKonfigurasiParameter.Berita);
                }
                _beritaDetailService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit");
            }
        }
    }
}