﻿using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class BaseController : Controller
    {
        private ICampaignService _campaignService;
        private IPejabatSementaraService _pejabatSementaraService;
        private ITimOnDutyService _timOnDutyService;
        private IBroadcastService _broadcastService;
        private IRapatService _rapatService;
        private ISuratEdaranService _suratEdaranService;
        private INoTelpService _noTelpService;
        private IKategoriBeritaService _kategoriBerita;
        private IProdukService _produkService;
        private IProfileService _profileService;
        private IMainAppService _mainAppService;
        private IBackgroundNoTelpService _backgorundNoTelpService;
        private IMenuAplikasiService _menuAplikasiService;
        private IKategoriMenuAplikasiService _katMenuAplikasiService;
        private IFooterService _footerService;
        private IKonfigurasiParameterService konfigurasiParameterService = DependencyResolver.Current.GetService<IKonfigurasiParameterService>();

        public BaseController(ICampaignService CampaignService, IPejabatSementaraService PejabatSementaraService, ITimOnDutyService TimOnDutyService,
             IBroadcastService BroadcastService, IRapatService RapatService, ISuratEdaranService SuratEdaranService, INoTelpService NoTelpService,
             IKategoriBeritaService KategoriBeritaService, IProdukService ProdukService, IProfileService ProfileService, IMainAppService MainAppService, IBackgroundNoTelpService BackgroundNoTelpService,
             IMenuAplikasiService MenuAplikasiService, IKategoriMenuAplikasiService KategoriMenuAplikai, IFooterService FooterService)
        {
            _campaignService = CampaignService;
            _pejabatSementaraService = PejabatSementaraService;
            _timOnDutyService = TimOnDutyService;
            _broadcastService = BroadcastService;
            _rapatService = RapatService;
            _suratEdaranService = SuratEdaranService;
            _noTelpService = NoTelpService;
            _kategoriBerita = KategoriBeritaService;
            _produkService = ProdukService;
            _profileService = ProfileService;
            _mainAppService = MainAppService;
            _backgorundNoTelpService = BackgroundNoTelpService;
            _menuAplikasiService = MenuAplikasiService;
            _katMenuAplikasiService = KategoriMenuAplikai;
            _footerService = FooterService;
        }
        // GET: User/Home
        public ActionResult MenuPartial()
        {
            ViewData["KategoriBerita"] = _kategoriBerita.GetAll();
            ViewData["Produk"] = _produkService.GetAll();
            ViewData["Profile"] = _profileService.GetAll();
            var menu = _menuAplikasiService.GetAll();
            ViewData["MenuAplikasi"] = menu;
            return PartialView("_Menu");
        }

        public ActionResult LinkAplikasiPartial()
        {
            var berita = _katMenuAplikasiService.GetAll().Where(m => m.Kategori != "-").ToList();
            ViewData["MenuAplikasi"] = _menuAplikasiService.GetAll();
            ViewData["KatMenuAplikasi"] = berita;
            ViewBag.MaxTanggal = berita.Select(m => m.CreatedDate).OrderByDescending(m => m.Value).FirstOrDefault();
            return PartialView("_MenuAplikasi");
        }

        public ActionResult CampaignPartial()
        {
            var campaign = _campaignService.GetAll().ToList();
            var newList = campaign.Where(x => x.Tipe.Contains("Video")).ToList();
            if(newList == null || newList.Count == 0)
            {
                ViewData["Campaign"] = _campaignService.GetAll();
                ViewBag.Count = campaign.Count();
                return PartialView("_CampaignImage");
            }
            else
            {
                ViewData["Campaign"] = newList;
                ViewBag.Count = campaign.Count();
                return PartialView("_CampaignVideo");
            }
        }

        public ActionResult MainAppPartial()
        {
            ViewData["MainApp"] = _mainAppService.GetAll();
            return PartialView("_MainApp");
        }

        public ActionResult TimOnDutyPartial()
        {
            ViewData["TimOnDuty"] = _timOnDutyService.GetAll();
            return PartialView("_TimOnDuty");
        }
        public ActionResult BroadcastPartial()
        {
            var broadcast = _broadcastService.GetAll();
            ViewData["Broadcast"] = broadcast.ToList();
            ViewBag.Count = broadcast.Count();
            return PartialView("_Broadcast");
        }
        public ActionResult RapatPartial()
        {
            IEnumerable<Rapat> rapat = _rapatService.GetAll();
            ViewData["RapatHariIni"] = rapat.Where(m => m.TanggalRapat.Date == DateTime.Now.Date).ToList();
            ViewData["RapatBesok"] = rapat.Where(m => m.TanggalRapat.Date == (DateTime.Now.Date).AddDays(1)).ToList();

            return PartialView("_Rapat");
        }
        public ActionResult SuratEdaranPartial()
        {
            ViewData["SuratEdaran"] = _suratEdaranService.GetAll();
            return PartialView("_SuratEdaran");
        }
        public ActionResult NoTelpPartial()
        {
            ViewData["NoTelp"] = _noTelpService.GetAll();
            ViewData["BgNoTelp"] = _backgorundNoTelpService.GetAll();
            return PartialView("_NoTelp");
        }

        public ActionResult FooterPartial()
        {
            ViewData["Footer"] = _footerService.GetAll();
            return PartialView("_Footer");
        }

        public async Task<ActionResult> imagerRDP(string KeyKonfigParam, string ImageName)
        {
            //string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;
            var Url = await Search(KeyKonfigParam);

            if (string.IsNullOrEmpty(ImageName))
                return null;
            var path = Path.Combine(Url, ImageName);
            path = Path.GetFullPath(path);
            if (!path.StartsWith(Url))
            {
                // Ensure that we are serving file only inside the root folder
                // and block requests outside like "../web.config"
                throw new HttpException(403, "Forbidden");
            }

            return File(path, "image/*");
        }

        public async Task<string> Search(string KeyKonfigParam)
        {
            string Url = @konfigurasiParameterService.Find(i => i.Key == KeyKonfigParam).FirstOrDefault().Value;
            return Url;
        }
    }
}