﻿using ProjectPertamina.Services;
using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class ProfileController : Controller
    {
        private IProfileService _profileService;

        public ProfileController(IProfileService ProfileService)
        {
            _profileService = ProfileService;
        }
        // GET: User/Profile
        public ActionResult Index(int id)
        {
            IEnumerable<Profile> model = _profileService.GetAll();
            var profile = model.Where(m => m.Id == id);
            return View(profile);
        }
    }
}