﻿using PagedList;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class BroadcastController : Controller
    {
        private IBroadcastService _broadcastService;

        public BroadcastController(IBroadcastService BroadcastService)
        {
            _broadcastService = BroadcastService;
        }
        // GET: User/Broadcast
        public ActionResult Index(int? page)
        {
            IEnumerable<Broadcast> broadcast = _broadcastService.GetAll();
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            return View(broadcast.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Detail(int Id)
        {
            IEnumerable<Broadcast> list = _broadcastService.GetAll();
            IEnumerable<Broadcast> broadcast = list.Where(m => m.Id == Id);
            return View("Detail",broadcast);
        }
    }
}