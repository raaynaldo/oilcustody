﻿using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class ProdukController : Controller
    {
        private IProdukService _produkService;

        public ProdukController(IProdukService ProdukService)
        {
            _produkService = ProdukService;
        }
        // GET: User/Produk
        public ActionResult Index(int id)
        {
            IEnumerable<Produk> model = _produkService.GetAll();
            var produk = model.Where(m => m.Id == id);
            return View(produk);
        }
    }
}