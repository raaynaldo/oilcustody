﻿using PagedList;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.ViewModel;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class HomeController : Controller
    {
        private IBeritaService _beritaService;
        private IBeritaDetailService _beritaDetailService;

        public HomeController(IBeritaService BeritaService, IBeritaDetailService BeritaDetailService)
        {
            _beritaService = BeritaService;
            _beritaDetailService = BeritaDetailService;
        }
        // GET: User/Home

        public ActionResult Index(int? page)
        {
            IEnumerable<Berita> model = _beritaService.GetAll();
            var berita = model.Select(x => new BeritaViewModel
            {
                Id = x.Id,
                KategoriBeritaId = x.KategoriBeritaId,
                IsiBerita = StripHTML(x.IsiBerita),
                Judul = x.Judul,
                Penulis = x.Penulis,
                Sumber = x.Sumber,
                Tanggal = x.Tanggal
            });
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            //var BeritaID = _beritaService.GetAll().Select(m => m.Id);
            ViewData["BeritaDetail"] = _beritaDetailService.GetAll().ToList();
            return View(berita.ToPagedList(pageNumber, pageSize));
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }


    }
}