﻿using PagedList;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.ViewModel;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class BeritaController : Controller
    {
        private IBeritaService _beritaService;
        private IBeritaDetailService _beritaDetailService;
        private IKategoriBeritaService _kategoriBeritaService;

        public BeritaController(IBeritaService BeritaService, IBeritaDetailService BeritaDetailService, IKategoriBeritaService KategoriBeritaService)
        {
            _beritaService = BeritaService;
            _beritaDetailService = BeritaDetailService;
            _kategoriBeritaService = KategoriBeritaService;
        }
        // GET: User/Berita
        //public ActionResult Index(int id, int? page)
        //{
        //    IEnumerable<Berita> berita = _beritaService.GetAll();
        //    int pageSize = 12;
        //    int pageNumber = (page ?? 1);
        //    var BeritaID = _beritaService.GetAll().Select(m => m.Id);
        //    ViewData["BeritaDetail"] = _beritaDetailService.GetAll();

        //    var kat = _kategoriBeritaService.Get(id);
        //    ViewBag.KategoriBerita = kat.Kategori;

        //    return View(berita.ToPagedList(pageNumber, pageSize));

        //}
        public ActionResult Index(int id, int? page)
        {
            IEnumerable<Berita> model = _beritaService.Find(m => m.KategoriBeritaId == id);//GetAll().Where(m => m.Id == Id);
            var berita = model.Select(x => new BeritaViewModel
            {
                Id = x.Id,
                KategoriBeritaId = x.KategoriBeritaId,
                IsiBerita = x.IsiBerita.Substring(0, 500),
                Judul = x.Judul,
                Penulis = x.Penulis,
                Sumber = x.Sumber,
                Tanggal = x.Tanggal
            });
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            ViewData["BeritaDetail"] = _beritaDetailService.GetAll();
            var kat = _kategoriBeritaService.Get(id);
            ViewBag.KategoriBerita = kat.Kategori;
            return View(berita.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Detail(int Id)
        {
            IEnumerable<Berita> model = _beritaService.GetAll();
            var berita = model.Where(m => m.Id == Id);
            var jd = _beritaService.Get(Id);
            ViewBag.JudulBerita = jd.Judul;
            ViewBag.IdKat = jd.KategoriBeritaId;
            var idKat = jd.KategoriBeritaId;
            var kat = _kategoriBeritaService.Get(idKat);
            ViewBag.KategoriBerita = kat.Kategori;
            ViewData["BeritaDetail"] = _beritaDetailService.GetAll().Where(m => m.BeritaId == Id).ToList();
            return View(berita);
        }
    }
}