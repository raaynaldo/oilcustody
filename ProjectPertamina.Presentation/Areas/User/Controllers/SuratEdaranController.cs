﻿using ProjectPertamina.Common;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.ViewModel;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class SuratEdaranController : Controller
    {
        // GET: User/SuratEdaran
        private ISuratEdaranService _suratEdaranService;

        public SuratEdaranController(ISuratEdaranService SuratEdaranService)
        {
            _suratEdaranService = SuratEdaranService;
        }
        // GET: User/Home
        public ActionResult Index()
        {
            IEnumerable<SuratEdaranViewModel> periode = _suratEdaranService.GetPeriodSuratEdaran();
            ViewData["Surat"] = _suratEdaranService.GetAll();
            return View(periode);
        }
        public ActionResult Detail(int Id)
        {
            IEnumerable<SuratEdaran> list = _suratEdaranService.GetAll();
            IEnumerable<SuratEdaran> suratedaran = list.Where(m => m.Id == Id);
            return View(suratedaran);
        }

        public ActionResult Downloads()
        {
            var dir = new System.IO.DirectoryInfo(Server.MapPath(ConstantKonfigurasiParameter.SuratEdaran));
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*"); List<string> items = new List<string>();
            foreach (var file in fileNames)
            {
                items.Add(file.Name);
            }
            return View(items);
        }

        public FileResult Download(string file)
        {
            var FileVirtualPath = ConstantKonfigurasiParameter.SuratEdaran + file;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }
    }
}