﻿using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Areas.User.Controllers
{
    public class TimOnDutyController : Controller
    {
        private ITimOnDutyService _timOnDutyService;

        public TimOnDutyController(ITimOnDutyService TimOnDutyService)
        {
            _timOnDutyService = TimOnDutyService;
        }
        // GET: User/Home
        public ActionResult Index(int Id)
        {
            IEnumerable<TimOnDuty> list = _timOnDutyService.GetAll();
            IEnumerable<TimOnDuty> timonduty = list.Where(m => m.Id == Id);
            return View(timonduty);

        }

        public ActionResult Downloads()
        {
            var dir = new System.IO.DirectoryInfo(Server.MapPath("~/UploadedFiles/"));
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*"); List<string> items = new List<string>();
            foreach (var file in fileNames)
            {
                items.Add(file.Name);
            }
            return View(items);
        }

        public FileResult Download(string file)
        {

            var FileVirtualPath = "~/UploadedFiles/" + file;
            return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
        }
    }
}