﻿using System.Web;
using System.Web.Optimization;

namespace ProjectPertamina.Presentation
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.12.4.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/chosen.jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/unobtrusiveajax").Include(
                        "~/Scripts/jquery.unobtrusive*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Scripts/jquery.dataTables.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/bootstrap-datetimepicker.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/script.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/dataTables.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/style.css",
                      "~/Content/chosen.css"));

            // jquery datatables js file
            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/Scripts/DataTables/jquery.dataTables.min.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.js",
                         "~/Scripts/DataTables/dataTables.rowsGroup.js"));


            // jquery datatables css file
            bundles.Add(new StyleBundle("~/Content/datatables").Include(
                      "~/Content/DataTables/css/dataTables.bootstrap.css",
                      "~/Content/DataTables/css/fixedColumns.dataTables.min.css",
                      "~/Content/DataTables/css/fixedColumns.bootstrap.css"
                      ));

            // datetimepicker js files
            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                "~/Scripts/moment.min.js",
                "~/Scripts/bootstrap-datetimepicker.js"));

            // datetimepicker css file
            bundles.Add(new StyleBundle("~/Content/datetimepicker").Include(
                "~/Content/bootstrap-datetimepicker.css"));

            // datejs js file
            bundles.Add(new ScriptBundle("~/bundles/datejs").Include(
                "~/Scripts/datejs.js"));

            // accounting js file
            bundles.Add(new ScriptBundle("~/bundles/accountingjs").Include(
    "~/Scripts/accounting.min.js"));

            // sweetalert css file
            bundles.Add(new StyleBundle("~/Content/sweetalert").Include(
                "~/Content/sweetalert.css"));

            // sweetalert js file
            bundles.Add(new StyleBundle("~/bundles/sweetalert").Include(
                "~/Scripts/sweetalert-dev.js"));

            // jquerymask js file
            bundles.Add(new StyleBundle("~/bundles/jquerymask").Include(
                "~/Scripts/jquery.mask.min.js"));

        }
    }
}
