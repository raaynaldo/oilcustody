﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectPertamina.Presentation
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    "Default", // Route name
            //    "{controller}/{action}/{id}", // URL with parameters
            //    new { area = "User", controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
            //    null,
            //    new[] { "ProjectPertamina.Presentation.Areas.User.Controllers" }
            //).DataTokens.Add("area", "User");

            //routes.MapRoute(
            //    "Default", // Route name
            //    "{controller}/{action}/{id}", // URL with parameters
            //    new { area = "Admin", controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
            //    null,
            //    new[] { "ProjectPertamina.Presentation.Areas.Admin.Controllers" }
            //).DataTokens.Add("area", "Admin");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { area = "", controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
