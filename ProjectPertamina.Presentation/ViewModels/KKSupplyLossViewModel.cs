﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class KKSupplyLossViewModel
    {
        [Display(Name = "Tanggal BL")]
        public DateTime? TanggalBl { get; set; }

        [Display(Name = "Sampai")]
        public DateTime? TanggalBlSampai { get; set; }

        [Display(Name = "Tanggal CQD")]
        public DateTime? TanggalCQD { get; set; }

        [Display(Name = "Sampai")]
        public DateTime? TanggalCQDSampai { get; set; }

        //[Required(ErrorMessage = "Kode Opr harus di isi")]
        [Display(Name = "Tipe Transportasi")]
        public string TipeTransportasi { get; set; }

        public IEnumerable<string> DataTipeTransportasi { get; set; }

        [Display(Name = "Loading Port")]
        public string LoadingPort { get; set; }

        [Display(Name = "Nama Kapal")]
        public string NamaKapal { get; set; }

        [Display(Name = "No BL")]
        public string NoBL { get; set; }
    }
}