﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportRankViewModel
    {
        public string TipeSupplyLoss { get; set; }
        public string TransportName { get; set; }

        [Display(Name="Transport Type")]
        public string TransportType { get; set; }

        public string NamaProduk { get; set; }
        public decimal R1 { get; set; }
        public decimal PercR1 { get; set; }
        public decimal R2 { get; set; }
        public decimal PercR2 { get; set; }
        public decimal R3 { get; set; }
        public decimal PercR3 { get; set; }
        public decimal R4Gross { get; set; }
        public decimal PercGross { get; set; }
        public decimal R4Nett { get; set; }
        public decimal PercNett { get; set; }
        public DateTime BlDate { get; set; }

        //Untuk Tampung nilai View
        public string TampilR { get; set; }
        public DateTime? TanggalBL { get; set; }
        public DateTime? TanggalBLSampai { get; set; }

        //Untuk Nampung nilai Chart
        public DateTime TanggalXaxis { get; set; }
        public decimal NilaiYaxis { get; set; }
    }
}