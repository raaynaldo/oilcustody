﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class RoasViewModel
    {
        [Display(Name = "Tanggal Dari")]
        public DateTime? TanggalDari { get; set; }

        [Display(Name = "Sampai")]
        public DateTime? TanggalSampai { get; set; }

        //[Required(ErrorMessage = "Kode Opr harus di isi")]
        [Display(Name = "Kode Opr")]
        public string KodeOprSelected { get; set; }

        public IEnumerable<String> KodeOpr { get; set; }

        //[Required(ErrorMessage = "Tipe Transport harus di isi")]
        [Display(Name = "Tipe Transport")]
        public string TipeTransportSelected { get; set; }

        public IEnumerable<String> TipeTransport { get; set; }

        [Display(Name = "Transport")]
        public string TransportSelected { get; set; }

        public IEnumerable<String> Transport { get; set; }

        [Display(Name = "Asal Tujuan")]
        public string AsalTujuanSelected { get; set; }

        public IEnumerable<String> AsalTujuan { get; set; }

        [Display(Name = "Produk")]
        public string ProdukSelected { get; set; }

        public IEnumerable<String> Produk { get; set; }
    }
}