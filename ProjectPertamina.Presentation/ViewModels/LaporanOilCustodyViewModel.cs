﻿using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class LaporanOilCustodyViewModel
    {
        [Required(ErrorMessage = "Tanggal harus di isi")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? Tanggal { get; set; }

        //[Required(ErrorMessage = "Shift harus di isi")]
        [Display(Name = "Shift")]
        public int ? ShiftId { get; set; }

        public IEnumerable<Shift> Shifts { get; set; }

        //[Required(ErrorMessage = "PIC Keuangan harus di isi")]
        [Display(Name = "PIC Keuangan")]
        public int ? PICKeuanganId { get; set; }

        public IEnumerable<PICKeuangan> PICKeuangans { get; set; }

    }

    public class LaporanOilCustodyReportViewModel
    {
        public int No { get; set; }
        public DateTime ? Tanggal { get; set; }
        public string OnCall { get; set; }
        public string Status { get; set; }
        public string Nama { get; set; }
        public string Aktivitas { get; set; }
        public string TipeAktivitas { get; set; }
        public string Catatan { get; set; }
    }

    public class LaporanOilCustodyGroup
    {
        public DateTime Tanggal { get; set; }
        public string OnCall { get; set; }
        public string Status { get; set; }
        public string Nama { get; set; }
        public List<SubGroup> Aktivitas { get; set; }
    }
    public class SubGroup
    {
        public string TipeAktivitas { get; set; }
        public List<string> Aktivitas { get; set; }
    }
}