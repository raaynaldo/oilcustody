﻿using ProjectPertamina.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class SupplyLossViewModel
    {
        [Display(Name = "Tanggal")]
        public DateTime? TanggalDari { get; set; }

        [Display(Name = "Sampai")]
        public DateTime? TanggalSampai { get; set; }

        [Display(Name = "Loading Master")]
        public int LoadingMaster { get; set; }

        public List<Loading> LoadingMasterList { get; set; }

        [Display(Name = "Keterangan")]
        public string Keterangan { get; set; }
    }
}