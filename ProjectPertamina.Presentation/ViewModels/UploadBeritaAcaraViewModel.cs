﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class UploadBeritaAcaraViewModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Bulan harus diisi")]
        public int Bulan { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Tahun harus diisi")]
        public int Tahun { get; set; }
        public string NamaDokumen { get; set; }

        //Untuk Keperluan Display di Modal Delete
        public string NamaBulan { get; set; }
        public int Id { get; set; }
    }
}