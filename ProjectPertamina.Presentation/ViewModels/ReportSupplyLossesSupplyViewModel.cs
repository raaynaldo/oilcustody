﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportSupplyLossesSupplyViewModel
    {
        public string AlatAngkut { get; set; }
        public string DischargePort { get; set; }
        public string Material { get; set; }
        public DateTime? TanggalBL { get; set; }
        public DateTime? TanggalBLSampai { get; set; }
        public DateTime? TanggalBongkar { get; set; }
        public DateTime? TanggalBongkarSampai { get; set; }
    }
}