﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class TableReportRekonsiliasiViewModel
    {
        //================================
        public string Tanggal { get; set; }
        public string RUUII { get; set; }
        public string PTSK { get; set; }
        public string DELTA { get; set; }
        public string PerusahaanKode { get; set; }
    }
}