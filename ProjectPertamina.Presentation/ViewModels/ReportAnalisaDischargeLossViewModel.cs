﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportAnalisaDischargeLossViewModel
    {
        //Untuk Input dari UI
        public DateTime? TanggalBL { get; set; }
        public DateTime? TanggalBLSampai { get; set; }
        public string ShipmentNo { get; set; }
        public string Material { get; set; }
        //====
        public DateTime? BlDate { get; set; }
        public string RegionData { get; set; }
        public string UoM { get; set; }
        public string LastDiscPort { get; set; }
        public decimal? BL_SAP { get; set; }
        public decimal? SFAL_SAP { get; set; }
        public decimal? SFBD_SAP { get; set; }
        public decimal? AR_SAP { get; set; }
        public decimal? SFAD_SAP { get; set; }
        public decimal? BL_Manual { get; set; }
        public decimal? SFAL_Manual { get; set; }
        public decimal? SFBD_Manual { get; set; }
        public decimal? AR_Manual { get; set; }
        public decimal? SFAD_Manual { get; set; }
        public decimal? Selisih_BL { get; set; }
        public decimal? Selisih_SFAL { get; set; }
        public decimal? Selisih_SFBD { get; set; }
        public decimal? Selisih_AR { get; set; }
        public decimal? Selisih_SFAD { get; set; }
        public string Catatan { get; set; }

    }
}