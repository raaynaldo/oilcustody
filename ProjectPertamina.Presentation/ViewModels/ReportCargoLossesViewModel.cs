﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportCargoLossesViewModel
    {
        public string Muatan { get; set; }
        public string Asal { get; set; }
        public string NamaKapal { get; set; }
        public DateTime? TanggalBL { get; set; }
        public DateTime? TanggalBLSampai { get; set; }
        public DateTime? TanggalBongkar { get; set; }
        public DateTime? TanggalBongkarSampai { get; set; }
    }
}