﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ListTableReportRekonsiliasiViewModel
    {
        public IEnumerable<TableReportRekonsiliasiViewModel> TableReportNonOil { get; set; }
        //=================================
        public IEnumerable<TableReportRekonsiliasiOilViewModel> TableReportOil { get; set; }
    }
}