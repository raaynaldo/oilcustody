﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class FinancePipaDetailViewModel
    {
        public string FromTo { get; set; }
        public string NamaCargo { get; set; }
        public string Qty { get; set; }
        public string Start { get; set; }
        public string Stop { get; set; }
    }
}