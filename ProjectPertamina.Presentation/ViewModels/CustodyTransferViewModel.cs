﻿using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class CustodyTransferViewModel
    {
        public int Id { get; set; }
        //public Perusahaan Perusahaan { get; set; }
        public string PerusahaanKode { get; set; }
        [Display(Name = "Perusahaan")]
        public string PerusahaanName { get; set; }

        public Stream Stream { get; set; }

        //[Display(Name = "Perusahaan")]
        //public int PerusahaanId { get; set; }

        [Display(Name = "Stream")]
        [Range(1, int.MaxValue, ErrorMessage = "Harap Masukan Stream")]
        public int StreamId { get; set; }

        [Display(Name = "Tanggal"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        [Display(Name = "MT")]
        [Required(ErrorMessage = "Harap Masukan MT")]
        [Range(0.00001, double.MaxValue, ErrorMessage = "Harap Masukan MT")]
        public decimal MT { get; set; }

        [Display(Name = "Specific Gravity (SG)")]
        [Required(ErrorMessage = "Harap Masukan Specific Gravity (SG)")]
        [Range(0.000000000000001, double.MaxValue, ErrorMessage = "Harap Masukan Specific Gravity (SG)")]
        public decimal SG { get; set; }

        [Display(Name = "Kilowatt Hour (Kwh)")]
        [Range(1, int.MaxValue, ErrorMessage = "Harap Masukan Kilowatt Hour (Kwh)")]
        [Required(ErrorMessage = "Harap Masukan Kilowatt Hour (Kwh)")]
        public decimal Kwh { get; set; }
    }

    public class CustodyTransferExcelViewModel
    {
        public string StreamName { get; set; }
        public string KodeTipeProduk { get; set; }
        public List<CustodyTransferTableExcelViewModel> Table { get; set; }
    }

    public class CustodyTransferTableExcelViewModel
    {
        public DateTime Tanggal { get; set; }
        public CustodyTransfer CTRU2 { get; set; }
        public CustodyTransfer CTPTSK { get; set; }
        public decimal DensityRU2 { get; set; }
        public decimal SGRU2 { get; set; }
        public decimal DensityPTSK { get; set; }
        public decimal SGPTSK { get; set; }
        public decimal MtRU2 { get; set; }
        public decimal MtPTSK { get; set; }
        public decimal LiterRU2 { get; set; }
        public decimal LiterPTSK { get; set; }
        public decimal BB60RU2 { get; set; }
        public decimal BB60PTSK { get; set; }
    }

    public class CustodyTransferSGTableViewModel
    {
        public DateTime Tanggal { get; set; }
        public decimal SG { get; set; }
    }
    public class CustodyTransferSGViewModel
    {
        public string StreamName { get; set; }
        public List<CustodyTransferSGTableViewModel> Table { get; set; }
    }
}