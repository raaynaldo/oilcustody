﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class VMReportRank
    {
        //Untuk Nampung nilai Chart
        public DateTime TanggalXaxis { get; set; }
        public decimal NilaiYaxis { get; set; }
    }
}