﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportKKSuplyLossViewModel
    {
        public string LoadingPort { get; set; }
        public string Produk { get; set; }
        public string Vessel { get; set; }
        public DateTime? TanggalBL { get; set; }
        public DateTime? TanggalBLSampai { get; set; }
        public DateTime? TanggalCQD { get; set; }
        public DateTime? TanggalCQDSampai { get; set; }
    }
}