﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class dropDownListViewModel
    {
        public string Text { get; set; }
        public int? Value { get; set; }
    }
}