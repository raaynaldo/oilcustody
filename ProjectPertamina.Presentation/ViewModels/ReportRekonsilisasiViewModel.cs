﻿using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportRekonsilisasiViewModel
    {
        //Input dari UI
        public int? Bulan { get; set; }
        public int? Tahun { get; set; }
        public string kodeProduk { get; set; }
        public string Stream { get; set; }
        public string TagNumber { get; set; }
        //=========================
        public bool isTableEmpty { get; set; }
        //================================
        public IEnumerable<TableReportRekonsiliasiViewModel> TableReport { get; set; }
        //=================================
        public IEnumerable<TableReportRekonsiliasiOilViewModel> TableReportOil { get; set; }
        //=================================
        public IEnumerable<Stream>StreamList { get; set;}

    }
}