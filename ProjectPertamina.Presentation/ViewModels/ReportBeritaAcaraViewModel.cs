﻿using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportBeritaAcaraViewModel
    {
        public Stream Stream { get; set; }
        public string TagNo { get; set; }
        public decimal MtTon { get; set; }
        public decimal Density { get; set; }
        public decimal M3 { get; set; }
        public decimal NM3 { get; set; }
        public decimal Barrels { get; set; }
        public decimal USGallon { get; set; }
        public decimal Kwh { get; set; }

    }

    public class PDFReportBeritaAcaraViewModel
    {
        public List<ReportBeritaAcaraViewModel> listmodel { get; set; }
        public string bulan { get; set; }
        public int tahun { get; set; }
        //===
        public int bulanInput { get; set; }
        //=====
        
        public string nama1 { get; set; }
        public string jabatan1 { get; set; }
        public string nama2 { get; set; }
        public string jabatan2 { get; set; }
        public string nama3 { get; set; }
        public string jabatan3 { get; set; }
        public string nama4 { get; set; }
        public string jabatan4 { get; set; }
        public string nama5 { get; set; }
        public string jabatan5 { get; set; }
        public string nama6 { get; set; }
        public string jabatan6 { get; set; }
        public string nama7 { get; set; }
        public string jabatan7 { get; set; }
        public string nama8 { get; set; }
        public string jabatan8 { get; set; }
    }
}