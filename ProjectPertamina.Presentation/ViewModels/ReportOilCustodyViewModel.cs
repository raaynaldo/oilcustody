﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class ReportOilCustodyViewModel
    {
        //Untuk Data Jetty
        public IEnumerable<FinanceJettyDetailViewModel> Jetty { get; set; }

        //Untuk Data Pipa
        public IEnumerable<FinancePipaDetailViewModel> Pipa { get; set; }

        //Untuk Data Tangki
        public IEnumerable<FinanceTangkiDetailViewModel> Tangki { get; set; }

        //Untuk Tampung Input User
        public DateTime? Tanggal { get; set; }
        public int? ShiftId { get; set; }
        public int? PICKeuanganId { get; set; }

    }
}