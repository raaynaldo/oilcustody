﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class TableReportRekonsiliasiOilViewModel
    {
        public string Tanggal { get; set; }
        public string RUUII_Liter15 { get; set; }
        public string PTSK_Liter15 { get; set; }
        public string DELTA_Liter15 { get; set; }
        public string RUUII_BB60 { get; set; }
        public string PTSK_BB60 { get; set; }
        public string DELTA_BB60 { get; set; }
        public string PerusaahKode { get; set; }
    }
}