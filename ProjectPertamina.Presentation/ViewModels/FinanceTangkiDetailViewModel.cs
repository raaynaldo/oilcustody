﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.ViewModels
{
    public class FinanceTangkiDetailViewModel
    {
        public string FromTo { get; set; }
        public string Qty { get; set; }
    }
}