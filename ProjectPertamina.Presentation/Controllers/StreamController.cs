﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class StreamController : Controller
    {
        private IStreamService _streamService;
        private ITipeProdukService _tipeProdukService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public StreamController(IStreamService StreamService, ITipeProdukService TipeProdukService)
        {
            _streamService = StreamService;
            _tipeProdukService = TipeProdukService;
        }
        // GET: MasterStream
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var tipeProduk = _tipeProdukService.GetAll().ToList();
            tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
            ViewBag.TipeProduk = tipeProduk;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Stream, string TagNumber, int TipeProduk)
        {
            var obj = _streamService.SearchStream(requestModel.Start, requestModel.Length, Stream, TagNumber, TipeProduk);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Kode = camp.Kode,
                Stream = camp.StreamName,
                TagNumber = camp.TagNumber,
                TipeProduk = camp.TipeProduk.Deskripsi
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var stream = _streamService.Get(id);
            ViewBag.Title = "EDIT";
            if (stream == null)
            {
                stream = new Stream();
                ViewBag.Title = "ADD";
            }

            var tipeProduk = _tipeProdukService.GetAll().ToList();
            tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
            ViewBag.TipeProduk = tipeProduk;

            return PartialView("_CreateEdit", stream);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Stream model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                return View("_CreateEdit", model);
            }
            try
            {
                model.Kode = model.Kode.Trim();
                model.TagNumber = model.TagNumber == null ? "" : model.TagNumber;
                _streamService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                ModelState.AddModelError("Kode", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var stream = _streamService.Get(Id);
            return PartialView("_Delete", stream);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _streamService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var stream = _streamService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", stream);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", stream);
            }
            catch (Exception ex)
            {
                var stream = _streamService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", stream);
            }
        }
    }
}