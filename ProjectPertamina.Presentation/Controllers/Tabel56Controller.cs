﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class Tabel56Controller : Controller
    {
        private ITabel56Service _tabel56Service;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public Tabel56Controller(ITabel56Service Tabel56Service)
        {
            _tabel56Service = Tabel56Service;
        }
        // GET: MasterTabel56
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, decimal Density, decimal Liter)
        {
            var obj = _tabel56Service.SearchTabel56(requestModel.Start, requestModel.Length, Density, Liter);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Density = camp.Density15,
                Liter = camp.LiterMetricTon,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            List<Tabel56> Tabel56List = new List<Tabel56>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        List<Tabel56> ListTabel56 = _tabel56Service.GetAll().ToList();
                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "Density 15" ||
                                workSheet.Cells[1, 2].Value.ToString() != "Liter/Metric Ton" ||
                                noOfCol > 2)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Table 56.";
                                return View("_Upload");
                            }
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new Tabel56();
                                obj.Density15 = decimal.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                                if (ListTabel56.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null || Tabel56List.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null)
                                {
                                    ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Density15 baris ke {rowIterator} gagal, karena data sudah ada.";
                                    if (rowIterator == 2)
                                        ViewBag.Error = $"Density15 baris ke {rowIterator} gagal, karena data sudah ada.<br>Tidak ada data yang terupload.";

                                    _tabel56Service.AddRange(Tabel56List);

                                    return View("_Upload");
                                }

                                obj.LiterMetricTon = decimal.Parse(workSheet.Cells[rowIterator, 2].Value.ToString());

                                Tabel56List.Add(obj);
                            }

                            _tabel56Service.AddRange(Tabel56List);
                        }
                    }
                }
                return Content("success");
            }
            //catch (DbUpdateException ex)
            //{
            //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
            //    return View("_Upload");
            //}
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Terdapat data kosong pada baris ke {rowIterator}";
                _tabel56Service.AddRange(Tabel56List);
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"Input Gagal, tidak ada data yang diinput.";
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }

        }

        public ActionResult Create(int id = 0)
        {
            var tabel56 = _tabel56Service.Get(id);
            ViewBag.Title = "EDIT";
            if (tabel56 == null)
            {
                tabel56 = new Tabel56();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", tabel56);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Tabel56 model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                _tabel56Service.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Density15", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var tabel56 = _tabel56Service.Get(Id);
            return PartialView("_Delete", tabel56);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _tabel56Service.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var tabel56 = _tabel56Service.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", tabel56);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel56);
            }
            catch (Exception ex)
            {
                var tabel56 = _tabel56Service.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel56);
            }
        }

        public JsonResult GetLiter15FromTabel56(decimal mt, decimal density)
        {
            decimal d1 = Math.Truncate(density * 1000) / 1000;
            decimal d2 = d1 + (decimal)0.001;
            decimal d3 = d1 + (decimal)0.001;
            decimal d4 = d2 + (decimal)0.001;
            Tabel56 d5Obj = _tabel56Service.Find(i => i.Density15 == d1).FirstOrDefault();
            Tabel56 d6Obj = _tabel56Service.Find(i => i.Density15 == d2).FirstOrDefault();
            if (d5Obj == null)
                return Json(new { Liter15 = "Master Tabel56 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            if (d6Obj == null)
                return Json(new { Liter15 = "Master Tabel56 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            decimal d5 = d5Obj.LiterMetricTon;
            decimal d6 = d6Obj.LiterMetricTon;

            decimal Liter15 = mt * (d5 + ((density - d1) / (decimal)0.001 * (d6 - d5)));
            //Liter15 = Math.Round(Liter15, 0);
            return Json(new { Liter15 = Liter15, val = true }, JsonRequestBehavior.AllowGet);
        }
    }
}