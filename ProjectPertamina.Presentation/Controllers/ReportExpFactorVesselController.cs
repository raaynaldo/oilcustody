﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using NinjaNye.SearchExtensions;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportExpFactorVesselController : Controller
    {
        private ISupplyLossDischargeService _supplyLossDischargeService;
        private ISupplyLossSupplyService _supplyLossesSupplyService;

        public ReportExpFactorVesselController(ISupplyLossDischargeService SupplyLossDischarge, ISupplyLossSupplyService SupplyLossSupply)
        {
            _supplyLossDischargeService = SupplyLossDischarge;
            _supplyLossesSupplyService = SupplyLossSupply;
        }
        // GET: ReportExpFactorVessel
        public ActionResult Index()
        {
            string[] Kapal = null;
            var vKapal = getReportQuery(0, 0, Kapal, null, null, false);
            var vddlKapal = vKapal.Item1.OrderBy(x => x.TransportName).GroupBy(x => x.TransportName).ToList();
            //vddlKapal.Insert(0, "");
            ViewBag.ddlKapal = vddlKapal;

            return View();
        }

        public JsonResult barchart(string TampilanR, string[] Kapal, DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            TampilanR = TampilanR == null ? "" : TampilanR;
            var obj = getReportQuery(0, 0, Kapal, TanggalBL, TanggalBLSampai, false);
            var qResulst = obj.Item1;
            List<ReportExpFactorVesselViewModel> chartSupply = new List<ReportExpFactorVesselViewModel>();


            if (TampilanR == "R1")
            { 
                chartSupply = qResulst.GroupBy(x => new { x.BlDate, x.TransportName }).Select(y => new ReportExpFactorVesselViewModel
                {
                    TanggalXaxis = y.Key.BlDate,
                    NamaKapal = y.Key.TransportName,
                    NilaiYaxis = y.Sum(z => z.R1)
                }).OrderByDescending(x => x.NilaiYaxis).Take(10).ToList();
            }
            else if (TampilanR == "R2")
            {
                chartSupply = qResulst.GroupBy(x => new { x.BlDate, x.TransportName }).Select(y => new ReportExpFactorVesselViewModel
                {
                    TanggalXaxis = y.Key.BlDate,
                    NamaKapal = y.Key.TransportName,
                    NilaiYaxis = y.Sum(z => z.R2)
                }).OrderByDescending(x => x.NilaiYaxis).Take(10).ToList();

            }
            else if (TampilanR == "R3")
            {
                chartSupply = qResulst.GroupBy(x => new { x.BlDate, x.TransportName }).Select(y => new ReportExpFactorVesselViewModel
                {
                    TanggalXaxis = y.Key.BlDate,
                    NamaKapal = y.Key.TransportName,
                    NilaiYaxis = y.Sum(z => z.R3)
                }).OrderByDescending(x => x.NilaiYaxis).Take(10).ToList();

            }
            else if (TampilanR == "R4Nett")
            {
                chartSupply = qResulst.GroupBy(x => new { x.BlDate, x.TransportName }).Select(y => new ReportExpFactorVesselViewModel
                {
                    TanggalXaxis = y.Key.BlDate,
                    NamaKapal = y.Key.TransportName,
                    NilaiYaxis = y.Sum(z => z.R4Nett)
                }).OrderByDescending(x => x.NilaiYaxis).Take(10).ToList();

            };

            string chartTitle = "Experience Factor Vessel";

            var ListTanggal = chartSupply.OrderBy(x => x.TanggalXaxis).Select(x => x.TanggalXaxis);
            string chartSubtitle = "Periode " + ListTanggal.ElementAt(0).ToString("dd MMM yyyy") + " - " + ListTanggal.ElementAt(ListTanggal.Count()-1).ToString("dd MMM yyyy");

            //var Xaxis = chartSupply.Select(x => x.TanggalXaxis).ToArray();
            //var Yaxis = chartSupply.Select(x => x.NilaiYaxis).ToArray();
            var NamaKapal = chartSupply.Select(x => x.NamaKapal).Distinct();

            var JumlahKapal = NamaKapal.Count();
            List<LineDateSeriesModel> LineSeries = new List<LineDateSeriesModel>();
            for (int i = 0; i < JumlahKapal; i++)
            {
                var query2 = chartSupply.Where(x => x.NamaKapal.ToLower().Contains(NamaKapal.ElementAt(i).ToLower())).OrderBy(x => x.TanggalXaxis);
                var nilaiX = query2.Select(x => x.TanggalXaxis).ToArray();
                var nilaiY = query2.Select(x => x.NilaiYaxis).ToArray();

                var JumlahBaris1 = query2.Count();
                var JumlahKolom1 = 2;
                double[][] Datas = new double[JumlahBaris1][];
                
                for (int x = 0; x < JumlahBaris1; x++)
                {
                    Datas[x] = new double[JumlahKolom1];
                    for (int z = 0; z < JumlahKolom1; z++)
                    {
                        //Kolom Pertama Diisi dengan TanggalUTC untuk barchart
                        if (z == 0)
                        {
                            Datas[x][z] = UTCJavaScript(nilaiX[x]);
                        }

                        //Kolom Kedua Diisi dengan NilaiYaxis untuk barchart
                        if (z == 1)
                        {
                            Datas[x][z] = Convert.ToDouble(nilaiY[x]);
                        }
                    }
                }
                               
                //Masukin ke List
                LineSeries.Add(new LineDateSeriesModel { name = NamaKapal.ElementAt(i).Trim(), data = Datas });
            }
                                               
            return Json(new { LineSeries, chartTitle, chartSubtitle }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TampilanR, string[] Kapal, DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            var obj = getReportQuery(requestModel.Start, requestModel.Length, Kapal, TanggalBL, TanggalBLSampai, true);

            var data = obj.Item1.Select(item => new
            {
                TipeSupplyLoss = item.TipeSupplyLoss,
                BLDate = item.BlDate,
                TransportName = item.TransportName,
                MatirialDesc = item.NamaProduk,
                R1 = item.R1,
                PercR1 = item.PercR1,
                R2 = item.R2,
                PercR2 = item.PercR2,
                R3 = item.R3,
                PercR3 = item.PercR3,
                R4 = item.R4Gross
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportExpFactorVesselViewModel input)
        {
            var getReport = getReportQuery(0, 0, input.Kapal, input.TanggalBL, input.TanggalBLSampai, false);
            var listLaporan = getReport.Item1;

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("Laporan_Experience_Factor_Vessel");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells["A1:A2"].Merge = true;
                workSheet.Cells["A1:A2"].Value = "Tipe Supply Loss";
                workSheet.Cells["B1:B2"].Merge = true;
                workSheet.Cells["B1:B2"].Value = "Tanggal BL";
                workSheet.Cells["C1:C2"].Merge = true;
                workSheet.Cells["C1:C2"].Value = "Transport Name";
                workSheet.Cells["D1:D2"].Merge = true;
                workSheet.Cells["D1:D2"].Value = "Material Desc";
                workSheet.Cells["E1:F1"].Merge = true;
                workSheet.Cells["E1:F1"].Value = "R1";
                workSheet.Cells["E2"].Value = "Nilai";
                workSheet.Cells["F2"].Value = "%";
                workSheet.Cells["G1:H1"].Merge = true;
                workSheet.Cells["G1:H1"].Value = "R2";
                workSheet.Cells["G2"].Value = "Nilai";
                workSheet.Cells["H2"].Value = "%";
                workSheet.Cells["I1:J1"].Merge = true;
                workSheet.Cells["I1:J1"].Value = "R3";
                workSheet.Cells["I2"].Value = "Nilai";
                workSheet.Cells["J2"].Value = "%";
                workSheet.Cells["K1:L1"].Merge = true;
                workSheet.Cells["K1:L1"].Value = "R4 Gross";
                workSheet.Cells["K2"].Value = "Nilai";
                workSheet.Cells["L2"].Value = "%";
                workSheet.Cells["M1:N1"].Merge = true;
                workSheet.Cells["M1:N1"].Value = "R4 Nett";
                workSheet.Cells["M2"].Value = "Nilai";
                workSheet.Cells["N2"].Value = "%";


                int recordIndex = 3;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = item.TipeSupplyLoss;
                    workSheet.Cells[recordIndex, 2].Value = item.BlDate.ToString("dd-MM-yyyy");
                    workSheet.Cells[recordIndex, 3].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 4].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 5].Value = item.R1;
                    workSheet.Cells[recordIndex, 6].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 7].Value = item.R2;
                    workSheet.Cells[recordIndex, 8].Value = item.PercR2;
                    workSheet.Cells[recordIndex, 9].Value = item.R3;
                    workSheet.Cells[recordIndex, 10].Value = item.PercR3;
                    workSheet.Cells[recordIndex, 11].Value = item.R4Gross;
                    workSheet.Cells[recordIndex, 12].Value = item.PercGross;
                    workSheet.Cells[recordIndex, 13].Value = item.R4Nett;
                    workSheet.Cells[recordIndex, 14].Value = item.PercNett;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 15; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 14])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "Laporan_Experience_Factor_Vessel" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }

        public Tuple<IEnumerable<ReportExpFactorVesselViewModel>, int, int> getReportQuery(int Skip, int length, string[] Kapal, DateTime? TanggalBL, DateTime? TanggalBLSampai, bool isDataTable)
        {
            var query1 = _supplyLossesSupplyService.GetAllJoinData().Where(i => i.TransportType.ToLower().Contains("vessel"));
            var query2 = _supplyLossDischargeService.GetAllJoinData().Where(i => i.TransportType.ToLower().Contains("vessel"));

            IEnumerable<ReportExpFactorVesselViewModel> UnionResult = query1.Select(c => new ReportExpFactorVesselViewModel
            {
                TipeSupplyLoss = "Supply",
                BlDate = c.BlDate,
                R1 = c.R1,
                PercR1 = c.PercR1,
                R2 = Convert.ToDecimal(0),
                PercR2 = Convert.ToDecimal(0),
                R3 = Convert.ToDecimal(0),
                PercR3 = Convert.ToDecimal(0),
                R4Gross = Convert.ToDecimal(0),
                PercGross = Convert.ToDecimal(0),
                R4Nett = Convert.ToDecimal(0),
                PercNett = Convert.ToDecimal(0),
                TransportName = c.TransportName,
                NamaProduk = c.NamaProduk

            }).Union(
                query2.Select(d => new ReportExpFactorVesselViewModel
                {
                    TipeSupplyLoss = "Discharge",
                    BlDate = d.BlDate,
                    R1 = d.R1,
                    PercR1 = d.PercR1,
                    R2 = d.R2,
                    PercR2 = d.PercR2,
                    R3 = d.R3,
                    PercR3 = d.PercR3,
                    R4Gross = d.R4Gross,
                    PercGross = d.PercGross,
                    R4Nett = d.R4Nett,
                    PercNett = d.PercNett,
                    TransportName = d.TransportName,
                    NamaProduk = d.NamaProduk
                })
            );

            var TotalCount = UnionResult.Count();
            if (Kapal != null)
            {
                for (int i = 0; i < Kapal.Count(); i++)
                {
                    if (Kapal[i] == null || Kapal[i] == "null")
                    {
                        Kapal[i] = "";
                    }
                }
                // Query Search multiple dari package ninjanye Search Extension
                UnionResult = UnionResult.Search(x => x.TransportName).Containing(Kapal);
            }

            if (TanggalBL != null)
                UnionResult = UnionResult.Where(i => i.BlDate >= TanggalBL);
            if (TanggalBLSampai != null)
                UnionResult = UnionResult.Where(i => i.BlDate <= TanggalBLSampai);

            var filteredCount = UnionResult.Count();

            if (isDataTable)
            {
                UnionResult = UnionResult.Skip(Skip).Take(length).ToList();
            }

            return new Tuple<IEnumerable<ReportExpFactorVesselViewModel>, int, int>(UnionResult, TotalCount, filteredCount);
        }

        public double UTCJavaScript(DateTime TheDate)
        {            
            DateTime d1 = new DateTime(1970, 1, 1);
            // DateTime d2 = TheDate1.ToUniversalTime();
            TimeSpan ts = new TimeSpan(TheDate.Ticks - d1.Ticks);

            return ts.TotalMilliseconds;
        }
    }
}