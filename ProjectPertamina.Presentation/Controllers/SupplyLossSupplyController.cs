﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class SupplyLossSupplyController : Controller
    {
        private ISupplyLossSupplyService _supplyLossSupplyService;
        private IRoasService _roasService;
        private IPortService _portService;

        private CurrentUser user = CurrentUser.GetCurrentUser();
        public SupplyLossSupplyController(ISupplyLossSupplyService SupplyLossSupplyService, IRoasService RoasService, IPortService PortService)
        {
            _supplyLossSupplyService = SupplyLossSupplyService;
            _roasService = RoasService;
            _portService = PortService;
        }
        // GET: CargoLosses
        public ActionResult Index()
        {
            ViewBag.Process = user.ActionCreate || user.ActionUpdate;

            var TransportTypeList = _supplyLossSupplyService.GetListTransportType();
            TransportTypeList.Insert(0, "");
            ViewBag.TipeTransportasi = TransportTypeList;

            return View(new SupplyLossSupply());
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeTransportasi, string DischargePort,
            string NamaKapal, string NoBl, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossSupplyService.SearchSupplyLossSupply(requestModel.Start, requestModel.Length,
                TipeTransportasi, DischargePort, NamaKapal, NoBl, TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                TanggalBl = item.BlDate,
                TipeTransport = item.TransportType,
                NamaKapal = item.TransportName,
                Muatan = item.NamaProduk,
                DischargePort = item.DischargePortDesc,
                NoBl = item.Roas.BlAsal,
                TanggalBongkar = item.TanggalBongkar,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit()
        {
            if (user.ActionCreate || user.ActionUpdate)
            {
                ViewBag.Create = user.ActionCreate;
                ViewBag.Edit = user.ActionUpdate;
                return View();
            }
            else
                return RedirectToAction("Index", "UnAuthorized");
        }

        [HttpPost]
        public ActionResult GetSupplyLossSupplyRoas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeProses, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        //DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            if (TipeProses == "Edit")
            {
                //var objEdit = _supplyLossSupplyService.Find(i => !i.Deleted && i.BlDate >= TanggalBongkar && i.BlDate <= TanggalBongkarSampai).ToList();
                var objEdit = _supplyLossSupplyService.SearchSupplyLossSupplyEdit(TanggalBongkar, TanggalBongkarSampai);

                var dataEdit = objEdit.Select(item => new
                {
                    Id = item.Id,
                    DocDate = item.DocDate,
                    DocNo = item.DocNo,
                    TanggalBl = item.BlDate,
                    TipeTransport = item.TransportType,
                    NamaKapal = item.TransportName,
                    Muatan = item.NamaProduk,
                    DischargePort = item.DischargePortDesc,
                    NoBl = item.Roas.BlAsal,
                    TanggalBongkar = item.TanggalBongkar,
                });

                return Json(new DataTablesResponse(requestModel.Draw, dataEdit.ToList(), dataEdit.Count(), dataEdit.Count()), JsonRequestBehavior.AllowGet);
            }

            var obj = _roasService.GetRoasForSupplyLossSupply();

            var data = obj.Select(item => new
            {
                Id = item.Id,
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                TanggalBl = item.BlDate,
                TipeTransport = item.TransportType,
                NamaKapal = item.TransportName,
                Muatan = item.NamaProduk,
                DischargePort = item.Port.Description,
                NoBl = item.BlAsal,
                TanggalBongkar = DateTime.Now,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), data.Count(), data.Count()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSupplyLossSupply(List<SupplyLossSupply> Item, string TipeProses)
        {
            try
            {

                if (TipeProses == "Baru")
                {
                    List<SupplyLossSupply> SupplyLossSupplyList = new List<SupplyLossSupply>();
                    var DataSupplyLossSupply = _supplyLossSupplyService.GetAllJoinData().ToList();

                    foreach (var item in Item)
                    {
                        SupplyLossSupply CL = new SupplyLossSupply();
                        Roas RoasObj = _roasService.Get(item.Id);
                        RoasObj.Port = _portService.Get(RoasObj.PortId);
                        CL.ROASId = item.Id;
                        CL.TanggalBongkar = item.TanggalBongkar;

                        CL.TransportType = RoasObj.TransportType;
                        CL.LoadingPortCode = "R201";
                        CL.LoadingPortDesc = "RU II DUMAI";
                        CL.DischargePortCode = RoasObj.Port.Code;
                        CL.DischargePortDesc = RoasObj.Port.Description;
                        CL.DocDate = RoasObj.DocDate;
                        CL.KdMaterial = RoasObj.KdMaterial;
                        CL.NamaProduk = RoasObj.NamaProduk;
                        CL.Uom = "BB6";
                        CL.DocNo = RoasObj.DocNo;
                        CL.BlDate = RoasObj.BlDate;
                        CL.BarrelsPtm = RoasObj.BarrelsPtm;
                        CL.SFAL = RoasObj.TransportType.Trim() == "TRUCK" || RoasObj.TransportType.Trim() == "PIPA" ?
                            RoasObj.BarrelsPtm : RoasObj.BarrelsAl;
                        CL.R1 = RoasObj.BarrelsPtm - CL.SFAL;
                        CL.PercR1 = CL.R1 / RoasObj.BarrelsPtm * 100;
                        CL.ShipmentNumber = RoasObj.ShipmentNumber;
                        CL.TransportName = RoasObj.TransportName;
                        CL.Deleted = false;
                        CL.Roas = RoasObj;

                        var check = SupplyLossSupplyList.FindIndex(i => i.Roas.DocDate == RoasObj.DocDate && i.Roas.DocNo == RoasObj.DocNo && i.Roas.BlAsal == RoasObj.BlAsal && !i.Deleted);
                        if (check > -1)
                        {

                            SupplyLossSupplyList[check].Deleted = true;
                        }

                        SupplyLossSupplyList.Add(CL);

                    }

                    _supplyLossSupplyService.AddRange(SupplyLossSupplyList);

                    foreach (var item in SupplyLossSupplyList.Where(i => !i.Deleted))
                    {
                        var check = DataSupplyLossSupply.FindIndex(i => i.Roas.DocDate == item.Roas.DocDate && i.Roas.DocNo == item.Roas.DocNo && i.Roas.BlAsal == item.Roas.BlAsal && !i.Deleted);
                        if (check > -1)
                        {
                            DataSupplyLossSupply[check].Deleted = true;
                        }
                    }

                    _supplyLossSupplyService.UpdateRange(DataSupplyLossSupply);

                }
                else if (TipeProses == "Edit")
                {
                    var list = new List<SupplyLossSupply>();
                    foreach (var item in Item)
                    {
                        var SupplyLossSupply = _supplyLossSupplyService.Get(item.Id);
                        SupplyLossSupply.TanggalBongkar = item.TanggalBongkar;

                        list.Add(SupplyLossSupply);
                        //_supplyLossSupplyService.Save(SupplyLossSupply);
                    }
                    _supplyLossSupplyService.UpdateRange(list);
                }

                return Content("Success");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }
        }

    }
}