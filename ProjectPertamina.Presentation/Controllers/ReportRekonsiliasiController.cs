﻿using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services.WebCustodyTransfer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportRekonsiliasiController : Controller
    {
        private ITabel21Service _tabel21Service;
        private ITabel56Service _tabel56Service;
        private ICustodyTransferService _custodyTransferService;
        private ITabel52Service _tabel52Service;
        private ITipeProdukService _tipeProdukService;
        private IStreamService _streamService;

        public ReportRekonsiliasiController(ITabel21Service Tabel21Service,
            ICustodyTransferService CustodyTransferService,
            ITabel56Service Tabel56Service,
            ITabel52Service Tabel52Service,
            ITipeProdukService TipeProdukService,
            IStreamService StreamService)
        {
            _tabel21Service = Tabel21Service;
            _custodyTransferService = CustodyTransferService;
            _tabel56Service = Tabel56Service;
            _tabel52Service = Tabel52Service;
            _tipeProdukService = TipeProdukService;
            _streamService = StreamService;
        }

        // GET: ReportRekonsiliasi
        public ActionResult Index()
        {
            var model = new ReportRekonsilisasiViewModel();

            var ddlTahun = GetddlTahun();
            ViewBag.ddlTahun = ddlTahun;
            //select value for ddlTahun
            model.Tahun = DateTime.Now.Year;

            var ddlBulan = GetddlBulan();
            ViewBag.ddlBulan = ddlBulan;
            //select value for ddlBulan
            model.Bulan = DateTime.Now.Month;

            var ddlTipeProduk = _tipeProdukService.GetAll().ToList();
            //ddlTipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "All" });
            ViewBag.ddlTipeProduk = ddlTipeProduk;
            //select value for ddlTipeProduk
            model.kodeProduk = ConstantTipeProdukKode.Oil;

            int TipeProdukId = _tipeProdukService.Find(i => i.Kode == model.kodeProduk).FirstOrDefault().Id;

            return View("Index", model);
        }

        public ActionResult Get(ReportRekonsilisasiViewModel input)
        {
            var model = new ReportRekonsilisasiViewModel();

            var ddlTahun = GetddlTahun();
            ViewBag.ddlTahun = ddlTahun;
            model.Tahun = input.Tahun;

            var ddlBulan = GetddlBulan();
            ViewBag.ddlBulan = ddlBulan;
            model.Bulan = input.Bulan;

            var ddlTipeProduk = _tipeProdukService.GetAll().ToList();
            
            ViewBag.ddlTipeProduk = ddlTipeProduk;
            model.kodeProduk = input.kodeProduk;

            int TipeProdukId = _tipeProdukService.Find(i => i.Kode == model.kodeProduk).FirstOrDefault().Id;

            input.Stream = input.Stream == null ? "" : input.Stream;
            var ListStream = _streamService.GetAll().Where(x => x.TipeProdukId == TipeProdukId).Select(x => new Stream { Id = x.Id, StreamName = x.StreamName });
            ListStream = ListStream.Where(x => x.StreamName.ToLower().Contains(input.Stream.ToLower()));
            model.StreamList = ListStream;


            if(model.StreamList.Count() > 0)
            {
                var TableReport = getReportQuery(input.Bulan, input.Tahun, input.kodeProduk, ListStream.ElementAt(0).Id, input.TagNumber);
                model.TableReport = TableReport;

                if (input.kodeProduk == ConstantTipeProdukKode.Oil)
                {
                    var TableReportOil = getReportQueryOil(input.Bulan, input.Tahun, ListStream.ElementAt(0).Id, input.TagNumber);
                    model.TableReportOil = TableReportOil;
                }
            }

            model.isTableEmpty= checkTableReportEmpty(model.TableReport);

            return View("Index", model);
        }

        public ActionResult RenderPartial(int? Bulan, int? Tahun, string KodeProduk, int Streamid, string TagNumber)
        {
            string PartialPage = "";
            if (KodeProduk == ConstantTipeProdukKode.Oil)
            {
                PartialPage = "_TableRekonsiliasiOil";
            }
            else
            {
                PartialPage = "_TableRekonsiliasiNonOil";
            }

            var model = new ReportRekonsilisasiViewModel();
            var TableReport = getReportQuery(Bulan, Tahun, KodeProduk, Streamid, TagNumber);
            model.TableReport = TableReport;

            if (KodeProduk == ConstantTipeProdukKode.Oil)
            {
                var TableReportOil = getReportQueryOil(Bulan, Tahun, Streamid, TagNumber);
                model.TableReportOil = TableReportOil;
            }

            return PartialView(PartialPage, model);
        }
       
        public IEnumerable<TableReportRekonsiliasiOilViewModel> getReportQueryOil (int? bulan, int? Tahun, int Streamid, string TagNumber)
        {
            var connectionStringDB = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            //var connectionStringUsMan = ConfigurationManager.ConnectionStrings["UserManagerConnectionString"].ConnectionString;
            var DatabaseDB = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionStringDB).InitialCatalog;
            //var DatabaseUsMan = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionStringUsMan).InitialCatalog;
            DataTable dt = new DataTable();
            List<TableReportRekonsiliasiOilViewModel> Result = new List<TableReportRekonsiliasiOilViewModel>();

            string query = " SELECT TanggalReport, SUM(SG) AS SG, SUM(MT) AS MT, StreamId, PerusahaanKode" +
                          " INTO #tempRekon1" +
                          " FROM (" +
                                " SELECT" +
                                " CONVERT(date, A.Tanggal) AS TanggalReport," +
                                " A.SG," +
                                " A.MT," +
                                " A.StreamId," +
                                " A.PerusahaanKode" +
                                $" FROM[{DatabaseDB}].[dbo].[CustodyTransfer] A" +
                                $" LEFT JOIN[{DatabaseDB}].[dbo].[Stream] B ON A.StreamId = B.Id" +
                                //" WHERE A.PerusahaanId = 1" +
                                $" WHERE A.PerusahaanKode = '{ConstantPerusahaan.PerusahaanPertaminaKode}'" +
                                " AND B.Id ='" + Streamid + "'";
                 if (TagNumber != "" && TagNumber != "null")
                            query = query + " AND B.TagNumber LIKE '%" + TagNumber + "%'";
            query = query + " ) TblRUUII" +
                            " GROUP BY TanggalReport, StreamId, PerusahaanKode" +

                            " SELECT TanggalReport, SUM(SG) AS SG, SUM (MT) AS MT,StreamId, PerusahaanKode" +
                            " INTO #tempRekon2" +
                            " FROM (" +
                                " SELECT" +
                                " CONVERT(date, A.Tanggal) AS TanggalReport," +
                                " A.SG," +
                                " A.MT," +
                                " A.StreamId," +
                                 " A.PerusahaanKode" +
                                $" FROM[{DatabaseDB}].[dbo].[CustodyTransfer] A" +
                                $" INNER JOIN[{DatabaseDB}].[dbo].[Stream] B ON A.StreamId = B.Id" +
                                $" WHERE A.PerusahaanKode = '{ConstantPerusahaan.PerusahaanPTSKKode}'" +
                                //" WHERE A.PerusahaanId = 2" +
                                " AND B.Id = '" + Streamid + "'";
                 if (TagNumber != "" && TagNumber != "null")
                    query = query + " AND B.TagNumber LIKE '%" + TagNumber + "%'";
            query = query + " ) TblPTSK" +
                         "  GROUP BY TanggalReport, StreamId, PerusahaanKode" +

                         " SELECT [Tanggals] = CASE WHEN A.TanggalReport = B.TanggalReport THEN A.TanggalReport" +
                         " WHEN A.TanggalReport IS NOT NULL AND B.TanggalReport IS NULL THEN A.TanggalReport" +
                         " WHEN A.TanggalReport IS NULL AND B.TanggalReport IS NOT NULL THEN B.TanggalReport ELSE NULL END," +
                        " [RUUI_SG] = ISNULL(A.SG,0)," +
                        " [RUUI_MT] = ISNULL(A.MT,0)," +
                        " [PTSK_SG] = ISNULL(B.SG,0)," +
                        " [PTSK_MT] = ISNULL(B.MT,0)," +
                        " A.PerusahaanKode" +
                        " into #tempRekon3" +
                        " FROM #tempRekon1  A" +
                        " FULL OUTER JOIN  #tempRekon2 B ON A.TanggalReport = B.TanggalReport";

            query = query + " DECLARE @RepMonth as datetime" +
            " SET @RepMonth = '" + bulan + "/01/" + Tahun + "'" +
            " ; WITH DayList (DayDate) AS" +
            " (" +
                " SELECT @RepMonth" +
                " UNION ALL" +
                " SELECT DATEADD(d, 1, DayDate)" +
                " FROM DayList" +
                " WHERE(DayDate < DATEADD(d, -1, DATEADD(m, 1, @RepMonth)))" +
            " )" +

            " SELECT" +
                " d.Tanggal," +
                " d.Liter15RUUI," +
                " d.Liter15PTSK," +
                " [DeltaLiter15] = ABS(d.Liter15RUUI - d.Liter15PTSK)," +
                $" [BB60RUUI] = {DatabaseDB}.dbo.CountBarrel60F(d.RUUI_MT,d.Liter15RUUI,d.CfBarrelRUUI)," +
                $" [BB60PTSK] = {DatabaseDB}.dbo.CountBarrel60F(d.PTSK_MT,d.Liter15PTSK,d.CFBarrelPTSK)," +
                " [DeltaBB60] = ABS(d.Liter15RUUI - d.Liter15PTSK)," +
                " d.PerusahaanKode" +
            " FROM" +
            " (" +
                " SELECT" +
                " c.Tanggal," +
                " c.RUUI_SG," +
                " c.RUUI_MT," +
                " c.PTSK_SG," +
                " c.PTSK_MT," +
                " c.Density_RUUI," +
                " c.Density_PTSK," +
                " c.Liter15RUUI," +
                " c.[Liter15PTSK]," +
                $" [CfBarrelRUUI] = {DatabaseDB}.dbo.CountCfBarrel(c.RUUI_SG, c.Density_RUUI)," +
                $" [CFBarrelPTSK] = {DatabaseDB}.dbo.CountCfBarrel(c.PTSK_SG, c.Density_PTSK)," +
                " c.PerusahaanKode" +
                " FROM (" +
                            " SELECT" +
                            " b.Tanggal," +
                            " b.RUUI_SG," +
                            " b.RUUI_MT," +
                            " b.PTSK_SG," +
                            " b.PTSK_MT," +
                            " b.Density_RUUI," +
                            " b.Density_PTSK," +
                            $" [Liter15RUUI] = {DatabaseDB}.dbo.CountLiter15(b.RUUI_MT, b.Density_RUUI)," +
                            $" [Liter15PTSK] = {DatabaseDB}.dbo.CountLiter15(b.PTSK_MT, b.Density_PTSK)," +
                            " b.PerusahaanKode" +

                            " FROM (" +

                                    " SELECT[Tanggal]  = CONVERT(date, t1.DayDate)," +
                                    " t2.RUUI_SG," +
                                    " t2.RUUI_MT," +
                                    " t2.PTSK_SG," +
                                    " t2.PTSK_MT," +
                                    $" [Density_RUUI] = {DatabaseDB}.dbo.CountDensity(t2.RUUI_SG)," +
                                    $" [Density_PTSK] = {DatabaseDB}.dbo.CountDensity(t2.PTSK_SG)," +
                                    " t2.PerusahaanKode" +
                                    " FROM DayList t1" +
                                    " left join #tempRekon3 t2 on t1.DayDate=Tanggals" +
                            " ) b" +
                     " ) c" +

            " )d";
            query = query + " DROP TABLE #tempRekon1, #tempRekon2, #tempRekon3";

            dt = ExecuteQuery(query);

            if(dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Result.Add(new TableReportRekonsiliasiOilViewModel
                    {
                        Tanggal = DateTime.Parse(dt.Rows[i]["Tanggal"].ToString()).ToString("dd MMM yyyy"),
                        RUUII_Liter15 = dt.Rows[i]["Liter15RUUI"].ToString(),
                        PTSK_Liter15 = dt.Rows[i]["Liter15PTSK"].ToString(),
                        DELTA_Liter15 = dt.Rows[i]["DeltaLiter15"].ToString(),
                        RUUII_BB60 = dt.Rows[i]["BB60RUUI"].ToString(),
                        PTSK_BB60 = dt.Rows[i]["BB60PTSK"].ToString(),
                        DELTA_BB60 = dt.Rows[i]["DeltaBB60"].ToString(),
                        PerusaahKode = dt.Rows[i]["PerusahaanKode"].ToString()
                    });
                }
            }

            return Result;
        }
        public IEnumerable<TableReportRekonsiliasiViewModel> getReportQuery(int? bulan, int? Tahun, string idTipeProduk, int Streamid, string TagNumber)
        {
            var connectionStringDB = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            var DatabaseDB = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionStringDB).InitialCatalog;

            DataTable dt = new DataTable();
            List<TableReportRekonsiliasiViewModel> Result = new List<TableReportRekonsiliasiViewModel>();

            string fieldquery = "MT";
            if (idTipeProduk == ConstantTipeProdukKode.Electricity)
            {
                fieldquery = "Kwh";
            }
            else if (idTipeProduk == ConstantTipeProdukKode.Oil || idTipeProduk == ConstantTipeProdukKode.Hidrogen || idTipeProduk == ConstantTipeProdukKode.NSW)
            {
                fieldquery = "MT";
            }

            string query = "SELECT TanggalReport, SUM(" + fieldquery + ") AS RUUII, StreamId, PerusahaanKode " +
                     " INTO #tempRekon1" +
                     " FROM ( " +
                           " SELECT" +
                           " CONVERT(date, A.Tanggal) AS TanggalReport," +
                           " A." + fieldquery + "," +
                           " A.StreamId," +
                           " A.PerusahaanKode" +
                           $" FROM[{DatabaseDB}].[dbo].[CustodyTransfer] A" +
                           $" LEFT JOIN [{DatabaseDB}].[dbo].[Stream] B ON A.StreamId = B.Id" +
                            $" WHERE A.PerusahaanKode = '{ConstantPerusahaan.PerusahaanPertaminaKode}'" +
                           //" WHERE A.PerusahaanId = 1 " +
                           " AND B.Id ='" + Streamid + "'";
            if (TagNumber != "" && TagNumber != "null")
                query = query + " AND B.TagNumber LIKE '%" + TagNumber + "%'";
            query = query + " ) TblRUUII" +
                            " GROUP BY TanggalReport, StreamId, PerusahaanKode" +

                            " SELECT TanggalReport, SUM(" + fieldquery + ")AS PTSK, StreamId, PerusahaanKode" +
                            "  INTO #tempRekon2" +
                            " FROM( " +
                                   " SELECT" +
                                   " CONVERT(date, A.Tanggal) AS TanggalReport," +
                                   " A." + fieldquery + "," +
                                   " A.StreamId," +
                                   " A.PerusahaanKode" +
                                   $" FROM[{DatabaseDB}].[dbo].[CustodyTransfer] A" +
                                   $" LEFT JOIN [{DatabaseDB}].[dbo].[Stream] B ON A.StreamId = B.Id" +
                        $" WHERE A.PerusahaanKode = '{ConstantPerusahaan.PerusahaanPTSKKode}'" +
                                   //" WHERE A.PerusahaanId = 2 " +
                                   " AND B.Id ='" + Streamid + "'";
            if (TagNumber != "" && TagNumber != "null")
                query = query + " AND B.TagNumber LIKE '%" + TagNumber + "%'";
            query = query + " ) TblPTSK" +
                            " GROUP BY TanggalReport, StreamId, PerusahaanKode";

            query = query + " SELECT [Tanggals] = CASE WHEN A.TanggalReport = B.TanggalReport THEN A.TanggalReport" +
                         " WHEN A.TanggalReport IS NOT NULL AND B.TanggalReport IS NULL THEN A.TanggalReport" +
                         " WHEN A.TanggalReport IS NULL AND B.TanggalReport IS NOT NULL THEN B.TanggalReport ELSE NULL END," +
                         "  ISNULL(A.[RUUII], 0) AS RUUII," +
                         "  ISNULL(B.[PTSK], 0) AS PTSK," +
                         "   ABS(ISNULL(A.[RUUII], 0) - ISNULL(B.[PTSK], 0)) AS DELTA," +
                         " A.PerusahaanKode" +
                         "   into #tempRekon3" +
                         "   FROM #tempRekon1 A" +
                         "   FULL OUTER JOIN #tempRekon2 B ON A.TanggalReport = B.TanggalReport";

            query = query + " DECLARE @RepMonth as datetime" +
            " SET @RepMonth = '" + bulan + "/01/" + Tahun + "'" +

            " ; WITH DayList (DayDate) AS" +
            " (" +
                 " SELECT @RepMonth" +
                 " UNION ALL" +
                 " SELECT DATEADD(d, 1, DayDate)" +
                 " FROM DayList" +
                 " WHERE(DayDate < DATEADD(d, -1, DATEADD(m, 1, @RepMonth)))" +
             " )" +
             " SELECT[Tanggal] = CONVERT(date, t1.DayDate)," +
             " t2.RUUII," +
             " t2.PTSK," +
             " t2.DELTA," +
             " t2.PerusahaanKode" +
             " FROM DayList t1" +
             " LEFT JOIN #tempRekon3 t2 on t1.DayDate=Tanggals ";

            query = query + " DROP TABLE #tempRekon1, #tempRekon2, #tempRekon3";

            dt = ExecuteQuery(query);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Result.Add(new TableReportRekonsiliasiViewModel
                    {
                        Tanggal = DateTime.Parse(dt.Rows[i]["Tanggal"].ToString()).ToString("dd MMM yyyy"),
                        RUUII = dt.Rows[i]["RUUII"].ToString(),
                        PTSK = dt.Rows[i]["PTSK"].ToString(),
                        DELTA = dt.Rows[i]["DELTA"].ToString(),
                        PerusahaanKode = dt.Rows[i]["PerusahaanKode"].ToString()
                    });
                }
            }

            return Result;
        }

        List<dropDownListViewModel> GetddlTahun()
        {
            //ddl Year
            var YearNow = DateTime.Now.Year;
            List<dropDownListViewModel> ddlTahun = new List<dropDownListViewModel>();
            int row = 0;
            for (int i = YearNow - 20; i <= YearNow; i++)
            {
                ddlTahun.Insert(row, new dropDownListViewModel { Text = i.ToString(), Value = i });
                row = row + 1;
            }

            return ddlTahun;
        }

        bool checkTableReportEmpty(IEnumerable<TableReportRekonsiliasiViewModel> input)
        {
            bool isEmpty = true;
            if(input != null)
            {
                for (int i = 0; i < input.Count(); i++)
                {
                    if (input.ElementAt(i).DELTA != "")
                    {
                        isEmpty = false;
                    }
                }
            }
            
            return isEmpty;
        }

        List<dropDownListViewModel> GetddlBulan()
        {
            //ddl Bulan
            List<dropDownListViewModel> ddlBulan = new List<dropDownListViewModel>();
            ddlBulan.Add(new dropDownListViewModel { Text = "Januari", Value = 1 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Februari", Value = 2 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Maret", Value = 3 });
            ddlBulan.Add(new dropDownListViewModel { Text = "April", Value = 4 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Mei", Value = 5 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juni", Value = 6 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juli", Value = 7 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Agustus", Value = 8 });
            ddlBulan.Add(new dropDownListViewModel { Text = "September", Value = 9 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Oktober", Value = 10 });
            ddlBulan.Add(new dropDownListViewModel { Text = "November", Value = 11 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Desember", Value = 12 });

            return ddlBulan;
        }

        //tupple Item1 = GetLiter15, Item 2 = Barrel 60
        Tuple<decimal, decimal> GetNilaiRumus(decimal SG, decimal MT)
        {
            decimal density = 0;
            decimal Liter15 = 0;
            decimal cfBarrel = 0;
            decimal barrel60f = 0;
            var sg = SG;
            if (sg == 0)
            {
                density = 0;
            }
            else
            {
                decimal sg1 = (Math.Truncate(sg * 1000) / 1000);
                decimal sg2 = sg1 + (decimal)0.001;
                Tabel21 sg3obj = _tabel21Service.Find(i => i.SG == sg1).FirstOrDefault();
                Tabel21 sg4obj = _tabel21Service.Find(i => i.SG == sg2).FirstOrDefault();
                decimal sg3 = sg3obj.Density15;
                decimal sg4 = sg4obj.Density15;

                density = sg3 + ((sg - sg1) / (decimal)0.001 * (sg4 - sg3));
            }



            var mt = MT;
            if (mt == 0)
            {
                Liter15 = 0;
            }
            else
            {
                decimal d1 = Math.Truncate(density * 1000) / 1000;
                decimal d2 = d1 + (decimal)0.001;
                decimal d3 = d1 + (decimal)0.001;
                decimal d4 = d2 + (decimal)0.001;
                Tabel56 d5Obj = _tabel56Service.Find(i => i.Density15 == d1).FirstOrDefault();
                Tabel56 d6Obj = _tabel56Service.Find(i => i.Density15 == d2).FirstOrDefault();

                decimal d5 = d5Obj.LiterMetricTon;
                decimal d6 = d6Obj.LiterMetricTon;

                Liter15 = mt * (d5 + ((density - d3) / (decimal)0.001 * (d6 - d5)));
            }

            if (sg == 0)
            {
                cfBarrel = 0;
            }
            else
            {
                Tabel52 querycf = _tabel52Service.Find(i => i.Density15 == density).FirstOrDefault();
                cfBarrel = querycf.Barrel60F;
            }

            if (mt == 0)
            {
                barrel60f = 0;
            }
            else
            {
                barrel60f = (Liter15 * cfBarrel) / 1000;
            }

            return new Tuple<decimal, decimal>(Liter15, barrel60f);
        }

        public DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable();
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    //sql data adapter
                    SqlDataAdapter sqlD = new SqlDataAdapter(cmd);
                    sqlD.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

        }
    }
}