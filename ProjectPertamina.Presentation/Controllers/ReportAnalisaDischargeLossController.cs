﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Repository.BaseRepository;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NinjaNye.SearchExtensions;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportAnalisaDischargeLossController : Controller
    {
        private ISupplyLossDischargeService _supplyLossDischargeService;
        private IZmmSupplyLossService _zmmSupplyLossService;
        private BaseContext dbCtx = new BaseContext();
        public ReportAnalisaDischargeLossController(ISupplyLossDischargeService SupplyLossDischarge, IZmmSupplyLossService ZmmSupplyLoss)
        {
            _supplyLossDischargeService = SupplyLossDischarge;
            _zmmSupplyLossService = ZmmSupplyLoss;
        }
        // GET: ReportAnalisaDischargeLoss
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }


        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            List<ZmmSupplyLoss> ZmmSupplyLossList = new List<ZmmSupplyLoss>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = 36;
                            //var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            string[] Shipment_No = new string[noOfRow - 1];

                            for (int i = 2; i <= noOfRow; i++)
                            {
                                for (int x = 1; x <= noOfCol; x++)
                                {
                                    if (workSheet.Cells[i, x].Value == null || workSheet.Cells[i, x].Value.ToString() == "")
                                    {
                                        noOfRow = i - 1;
                                        break;
                                    };

                                }
                            }

                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }

                            for (int i = 2; i <= noOfRow; i++)
                            {
                                Shipment_No[i - 2] = workSheet.Cells[i, 1].Value.ToString();
                            }
                            //Chek Data if There is same ShipmentNumber id Database

                            //var getQuery = getReportQuery(0, 0, null, null,"", "", false);
                            //var GetData = getQuery.Item1;
                            //GetData = GetData.Search(x => x.ShipmentNo).Containing(Shipment_No);

                            var queryCheckShipmentNo = _zmmSupplyLossService.GetAll();
                            queryCheckShipmentNo = queryCheckShipmentNo.Search(x => x.ShipmentNo).Containing(Shipment_No);

                            //if Data there is no ShipmentNumber conflict then save to DB
                            if (queryCheckShipmentNo.Count() < 1)
                            {
                                for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                                {

                                    var obj = new ZmmSupplyLoss();

                                    obj.ShipmentNo = workSheet.Cells[rowIterator, 1].Value.ToString();
                                    obj.ShipmentStatus = workSheet.Cells[rowIterator, 2].Value.ToString();
                                    obj.Date = DateTime.Parse(workSheet.Cells[rowIterator, 3].Value.ToString());
                                    obj.LoadingPort = workSheet.Cells[rowIterator, 4].Value.ToString();
                                    obj.LoadingPortDesc = workSheet.Cells[rowIterator, 5].Value.ToString();
                                    obj.LoadingDate = DateTime.Parse(workSheet.Cells[rowIterator, 6].Value.ToString());
                                    obj.DischargePort = workSheet.Cells[rowIterator, 7].Value.ToString();
                                    obj.DischargePortDesc = workSheet.Cells[rowIterator, 8].Value.ToString();
                                    obj.DischargeDate = DateTime.Parse(workSheet.Cells[rowIterator, 9].Value.ToString());
                                    obj.Type = workSheet.Cells[rowIterator, 10].Value.ToString();
                                    obj.Vessel = workSheet.Cells[rowIterator, 11].Value.ToString();
                                    obj.VesselName = workSheet.Cells[rowIterator, 12].Value.ToString();
                                    obj.BINumber = workSheet.Cells[rowIterator, 13].Value.ToString();
                                    obj.MaterialNo = workSheet.Cells[rowIterator, 14].Value.ToString();
                                    obj.MaterialName = workSheet.Cells[rowIterator, 15].Value.ToString();
                                    obj.Uom = workSheet.Cells[rowIterator, 16].Value.ToString();
                                    obj.Tipe = workSheet.Cells[rowIterator, 17].Value.ToString();
                                    obj.BlGross = decimal.Parse(workSheet.Cells[rowIterator, 18].Value.ToString());
                                    obj.Bl = decimal.Parse(workSheet.Cells[rowIterator, 19].Value.ToString());
                                    obj.Sfal = decimal.Parse(workSheet.Cells[rowIterator, 20].Value.ToString());
                                    obj.Sfbd = decimal.Parse(workSheet.Cells[rowIterator, 21].Value.ToString());
                                    obj.ArGross = decimal.Parse(workSheet.Cells[rowIterator, 22].Value.ToString());
                                    obj.Ar = decimal.Parse(workSheet.Cells[rowIterator, 23].Value.ToString());
                                    obj.NewBl = decimal.Parse(workSheet.Cells[rowIterator, 24].Value.ToString());
                                    obj.Sfad = decimal.Parse(workSheet.Cells[rowIterator, 25].Value.ToString());
                                    obj.R1 = decimal.Parse(workSheet.Cells[rowIterator, 26].Value.ToString());
                                    obj.R2 = decimal.Parse(workSheet.Cells[rowIterator, 27].Value.ToString());
                                    obj.R3 = decimal.Parse(workSheet.Cells[rowIterator, 28].Value.ToString());
                                    obj.R4 = decimal.Parse(workSheet.Cells[rowIterator, 29].Value.ToString());
                                    obj.R1Perc = decimal.Parse(workSheet.Cells[rowIterator, 30].Value.ToString());
                                    obj.R2Perc = decimal.Parse(workSheet.Cells[rowIterator, 31].Value.ToString());
                                    obj.R3Perc = decimal.Parse(workSheet.Cells[rowIterator, 32].Value.ToString());
                                    obj.R4PercGross = decimal.Parse(workSheet.Cells[rowIterator, 33].Value.ToString());
                                    obj.R4Perc = decimal.Parse(workSheet.Cells[rowIterator, 34].Value.ToString());
                                    obj.DocType = workSheet.Cells[rowIterator, 35].Value.ToString();
                                    obj.R4Gross = decimal.Parse(workSheet.Cells[rowIterator, 36].Value.ToString());

                                    ZmmSupplyLossList.Add(obj);

                                }
                                _zmmSupplyLossService.AddRange(ZmmSupplyLossList);
                            }
                            else
                            {
                                return View("_UploadFailed");
                            }

                        }
                    }
                }
                return Content("success");
            }
            catch (Exception ex)
            {
                string Error = "";
                Error = ex.Message + "Upload Failed " + ex.Message;
                ViewBag.Error = Error;
                return View("_Upload");
            }
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string vShipmentNumber, string vMaterial,
             DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            var obj = getReportQuery(requestModel.Start, requestModel.Length, TanggalBL, TanggalBLSampai, vShipmentNumber, vMaterial, true);
            var data = obj.Item1.Select(item => new
            {
                NoShipment = item.ShipmentNo,
                Region = item.RegionData,
                LastDiscPort = item.LastDiscPort,
                MaterialNumber = item.Material,
                UoM = item.UoM,
                BL_SAP = item.BL_SAP,
                SFAL_SAP = item.SFAL_SAP,
                SFBD_SAP = item.SFBD_SAP
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportAnalisaDischargeLossViewModel input)
        {
            var getQuery = getReportQuery(0, 0, input.TanggalBL, input.TanggalBLSampai, input.ShipmentNo, input.Material, false);
            var listLaporan = getQuery.Item1;

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanAnalisaDischargeLoss");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = "No.";
                workSheet.Cells[1, 2].Value = "No.Shipment";
                workSheet.Cells[1, 3].Value = "Region";
                workSheet.Cells[1, 4].Value = "Last Disc. Port";
                workSheet.Cells[1, 5].Value = "Material Number";
                workSheet.Cells[1, 6].Value = "UoM";
                workSheet.Cells[1, 7].Value = "B/L SAP";
                workSheet.Cells[1, 8].Value = "SFAL SAP";
                workSheet.Cells[1, 9].Value = "SFBD SAP";
                workSheet.Cells[1, 10].Value = "AR SAP";
                workSheet.Cells[1, 11].Value = "SFAD SAP";
                workSheet.Cells[1, 12].Value = "B/L Manual";
                workSheet.Cells[1, 13].Value = "SFAL Manual";
                workSheet.Cells[1, 14].Value = "SFBD Manual";
                workSheet.Cells[1, 15].Value = "AR Manual ";
                workSheet.Cells[1, 16].Value = "SFAD Manual";
                workSheet.Cells[1, 17].Value = "Selisih B/L";
                workSheet.Cells[1, 18].Value = "Selisih SFAL";
                workSheet.Cells[1, 19].Value = "Selisih SFBD";
                workSheet.Cells[1, 20].Value = "Selisih AR";
                workSheet.Cells[1, 21].Value = "Selisih SFAD";
                workSheet.Cells[1, 22].Value = "Catatan";


                int recordIndex = 2;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.ShipmentNo;
                    workSheet.Cells[recordIndex, 3].Value = item.RegionData;
                    workSheet.Cells[recordIndex, 4].Value = item.LastDiscPort;
                    workSheet.Cells[recordIndex, 5].Value = item.Material;
                    workSheet.Cells[recordIndex, 6].Value = item.UoM;
                    workSheet.Cells[recordIndex, 7].Value = item.BL_SAP;
                    workSheet.Cells[recordIndex, 8].Value = item.SFAL_SAP;
                    workSheet.Cells[recordIndex, 9].Value = item.SFBD_SAP;
                    workSheet.Cells[recordIndex, 10].Value = item.AR_SAP;
                    workSheet.Cells[recordIndex, 11].Value = item.SFAD_SAP;
                    workSheet.Cells[recordIndex, 12].Value = item.BL_Manual;
                    workSheet.Cells[recordIndex, 13].Value = item.SFAL_Manual;
                    workSheet.Cells[recordIndex, 14].Value = item.SFBD_Manual;
                    workSheet.Cells[recordIndex, 15].Value = item.AR_Manual;
                    workSheet.Cells[recordIndex, 16].Value = item.SFAD_Manual;
                    workSheet.Cells[recordIndex, 17].Value = item.Selisih_BL;
                    workSheet.Cells[recordIndex, 18].Value = item.Selisih_SFAL;
                    workSheet.Cells[recordIndex, 19].Value = item.Selisih_SFBD;
                    workSheet.Cells[recordIndex, 20].Value = item.Selisih_AR;
                    workSheet.Cells[recordIndex, 21].Value = item.Selisih_SFAD;
                    workSheet.Cells[recordIndex, 22].Value = item.Catatan;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 23; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 22].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 22].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 22].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 22].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 22])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanAnalisaDischargeLoss" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }

        public Tuple<IEnumerable<ReportAnalisaDischargeLossViewModel>, int, int> getReportQuery(int Skip, int length, DateTime? TanggalBL, DateTime? TanggalBLSampai, string vShipmentNumber, string vMaterial, bool isDataTable)
        {
            vShipmentNumber = vShipmentNumber == null ? "" : vShipmentNumber;
            vMaterial = vMaterial == null ? "" : vMaterial;

            IEnumerable<ReportAnalisaDischargeLossViewModel> query = null;
            if (vShipmentNumber != "" && vMaterial != "")
            {
                //Linq untuk menghasilkan SQL RAW Pure INNER JOIN
                query =
                (from a in dbCtx.SupplyLossDischarge.Where(x => !x.Deleted && !string.IsNullOrEmpty(x.ShipmentNumber))
                 from b in dbCtx.ZmmSupplyLoss
                 .Where(v => v.ShipmentNo == a.ShipmentNumber && v.MaterialNo == a.KdMaterial)
                 select new ReportAnalisaDischargeLossViewModel
                 {
                     BlDate = a.BlDate,
                     ShipmentNo = a.ShipmentNumber,
                     RegionData = "RU II DUMAI",
                     LastDiscPort = a.LoadingPortDesc,
                     Material = a.KdMaterial,
                     UoM = "BB6",
                     BL_SAP = b.Bl,
                     SFAL_SAP = b.Sfal,
                     SFBD_SAP = b.Sfbd,
                     AR_SAP = b.Ar,
                     SFAD_SAP = b.Sfad,
                     BL_Manual = a.BarrelsPtm,
                     SFAL_Manual = a.BarrelsAl,
                     SFBD_Manual = a.BarrelsBd,
                     AR_Manual = a.Barrels,
                     SFAD_Manual = a.Sfad,
                     Selisih_BL = b.Bl - a.BarrelsPtm,
                     Selisih_SFAL = b.Sfal - a.BarrelsAl,
                     Selisih_SFBD = b.Sfbd - a.BarrelsBd,
                     Selisih_AR = b.Ar - a.Barrels,
                     Selisih_SFAD = b.Sfad - a.Sfad,
                     Catatan = "-"
                 }).ToList();
            }
            else
            {
                //Linq untuk menghasilkan SQL RAW Pure LEFT JOIN 
                query =
                          (from a in dbCtx.SupplyLossDischarge.Where(x => !x.Deleted && !string.IsNullOrEmpty(x.ShipmentNumber))
                           from b in dbCtx.ZmmSupplyLoss
                           .Where(v => v.ShipmentNo == a.ShipmentNumber || v.MaterialNo == a.KdMaterial)
                           .DefaultIfEmpty()
                           select new ReportAnalisaDischargeLossViewModel
                           {
                               BlDate = a.BlDate,
                               ShipmentNo = a.ShipmentNumber,
                               RegionData = "RU II DUMAI",
                               LastDiscPort = a.LoadingPortDesc,
                               Material = a.KdMaterial,
                               UoM = "BB6",
                               BL_SAP = b.Bl,
                               SFAL_SAP = b.Sfal,
                               SFBD_SAP = b.Sfbd,
                               AR_SAP = b.Ar,
                               SFAD_SAP = b.Sfad,
                               BL_Manual = a.BarrelsPtm,
                               SFAL_Manual = a.BarrelsAl,
                               SFBD_Manual = a.BarrelsBd,
                               AR_Manual = a.Barrels,
                               SFAD_Manual = a.Sfad,
                               Selisih_BL = b.Bl - a.BarrelsPtm,
                               Selisih_SFAL = b.Sfal - a.BarrelsAl,
                               Selisih_SFBD = b.Sfbd - a.BarrelsBd,
                               Selisih_AR = b.Ar - a.Barrels,
                               Selisih_SFAD = b.Sfad - a.Sfad,
                               Catatan = "-"
                           }).ToList();
            }


            var TotalCount = query.Count();

            query = query.Where(x => x.ShipmentNo.ToLower().Contains(vShipmentNumber.ToLower())
                            && x.Material.ToLower().Contains(vMaterial.ToLower()));

            if (TanggalBL != null)
                query = query.Where(i => i.BlDate >= TanggalBL);
            if (TanggalBLSampai != null)
                query = query.Where(i => i.BlDate <= TanggalBLSampai);

            var filteredCount = query.Count();

            if (isDataTable)
            {
                query = query.Skip(Skip).Take(length).ToList();
            }
            return new Tuple<IEnumerable<ReportAnalisaDischargeLossViewModel>, int, int>(query, TotalCount, filteredCount);
        }
    }
}