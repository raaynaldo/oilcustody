﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectPertamina.Services;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class UploadBeritaAcaraController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IBeritaAcaraService _beritaAcaraService;
        private IKonfigurasiParameterService _konfigurasiParamaterService;

        public UploadBeritaAcaraController(IBeritaAcaraService BeritaAcaraService, IKonfigurasiParameterService KonfigurasiParameterService)
        {
            _beritaAcaraService = BeritaAcaraService;
            _konfigurasiParamaterService = KonfigurasiParameterService;
        }
        // GET: UploadBeritaAcara
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Delete = user.ActionDelete;

            var model = new UploadBeritaAcaraViewModel();

            var ddlTahun = GetddlTahun();
            ddlTahun.Insert(0, new dropDownListViewModel() { Value = 0, Text = "" });
            ViewBag.ddlTahun = ddlTahun;
            //select value for ddlTahun
            //model.Tahun = null;

            var ddlBulan = GetddlBulan();
            ddlBulan.Insert(0, new dropDownListViewModel() { Value = 0, Text = "" });
            ViewBag.ddlBulan = ddlBulan;
            //select value for ddlBulan
            //model.Bulan = null;

            return View();
        }

        public JsonResult GetbyId(long id)
        {
            UploadBeritaAcaraViewModel result = new UploadBeritaAcaraViewModel();
            var getResult = _beritaAcaraService.Get(id);
            result.Tahun = getResult.Tahun;
            result.NamaBulan = getNamaBulan(getResult.Bulan);
            result.Id = getResult.Id;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
       int vBulan, int vTahun)
        {
            //var model = new UploadBeritaAcaraViewModel();

            //var ddlTahun = GetddlTahun();
            //ddlTahun.Insert(0, new dropDownListViewModel() { Value = null, Text = "" });
            //ViewBag.ddlTahun = ddlTahun;
            ////select value for ddlTahun
            //model.Tahun = vTahun;

            //var ddlBulan = GetddlBulan();
            //ddlBulan.Insert(0, new dropDownListViewModel() { Value = null, Text = "" });
            //ViewBag.ddlBulan = ddlBulan;
            ////select value for ddlBulan
            //model.Bulan = vBulan;

            Tuple<IEnumerable<BeritaAcara>, int, int> getBeritaAcara = _beritaAcaraService.SearchBeritaAcara(requestModel.Start, requestModel.Length, vBulan, vTahun);
            IEnumerable<UploadBeritaAcaraViewModel> data = getBeritaAcara.Item1.Select(x => new UploadBeritaAcaraViewModel
            {
                NamaBulan = getNamaBulan(x.Bulan),
                Tahun = x.Tahun,
                Id = x.Id,
                NamaDokumen = x.Dokumen
            });

            var filteredCount = getBeritaAcara.Item2;
            var totalCount = getBeritaAcara.Item3;

            return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            var model = new UploadBeritaAcaraViewModel();

            var ddlTahun = GetddlTahun();
            ddlTahun.Insert(0, new dropDownListViewModel() { Value = 0, Text = "" });
            ViewBag.ddlTahun = ddlTahun;
            //select value for ddlTahun
            //model.Tahun = null;

            var ddlBulan = GetddlBulan();
            ddlBulan.Insert(0, new dropDownListViewModel() { Value = 0, Text = "" });
            ViewBag.ddlBulan = ddlBulan;
            //select value for ddlBulan
            //model.Bulan = null;

            return PartialView("_Upload");
        }

        [HttpPost]
        public JsonResult SaveBeritaAcara(HttpPostedFileBase UploadBeritaAcara, int Bulan, int Tahun)
        {
            var message = "";
            var ChekData = _beritaAcaraService.GetAll();
            ChekData = ChekData.Where(x => x.Bulan == Bulan && x.Tahun == Tahun);

            if (ChekData.Count() < 1)
            {
                var InputFileName = Path.GetFileName(UploadBeritaAcara != null ? UploadBeritaAcara.FileName : "");
                if (UploadBeritaAcara != null)
                {
                    string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.CustodyTransferBeritaAcara).FirstOrDefault().Value;
                    string fullPath = System.IO.Path.Combine(Url + InputFileName);
                    //string fullPath = Server.MapPath("~/Upload/BeritaAcara/" +  InputFileName);
                    if (System.IO.File.Exists(fullPath))
                    {
                        var InputFileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadBeritaAcara.FileName);
                        var InputFileExtName = Path.GetExtension(UploadBeritaAcara.FileName);
                        InputFileName = InputFileNameWithoutExt + "_" + DateTime.Now.ToString("ddmmyyyyhhmmss") + InputFileExtName;
                        fullPath = System.IO.Path.Combine(Url + InputFileName);
                        //fullPath = Server.MapPath("~/Upload/BeritaAcara/" + InputFileName);
                    }
                    UploadBeritaAcara.SaveAs(fullPath);


                    BeritaAcara beritaAcara = new BeritaAcara
                    {
                        Tahun = Tahun,
                        Bulan = Bulan,
                        Dokumen = InputFileName
                    };
                    _beritaAcaraService.Save(beritaAcara);


                    message = "Success";
                }
            }
            else
            {
                message = "Save Failed, Data already exist";
            }

            return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var beritaAcara = _beritaAcaraService.Get(Id);
            ViewBag.Bulan = getNamaBulan(beritaAcara.Bulan);
            return PartialView("_Delete", beritaAcara);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                string NamaFile = _beritaAcaraService.Get(id).Dokumen;
                _beritaAcaraService.Delete(id);
                string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.CustodyTransferBeritaAcara).FirstOrDefault().Value;

                var ServerSavePath = Path.Combine(Url + NamaFile);
                if (System.IO.File.Exists(ServerSavePath))
                {
                    System.IO.File.Delete(ServerSavePath);
                }
                return Content("success");
            }
            //catch (DbUpdateException ex)
            //{
            //    var sqlException = ex.GetBaseException() as SqlException;
            //    var tabel52 = _tabel52Service.Get(id);

            //    if (sqlException != null)
            //    {
            //        var number = sqlException.Number;

            //        if (number == 547)
            //        {
            //            ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
            //            return View("_Delete", tabel52);
            //        }
            //    }
            //    ModelState.AddModelError("", ex.Message);
            //    return View("_Delete", tabel52);
            //}
            catch (Exception ex)
            {
                var beritaAcara = _beritaAcaraService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", beritaAcara);
            }
        }

        List<dropDownListViewModel> GetddlTahun()
        {
            //ddl Year
            var YearNow = DateTime.Now.Year;
            List<dropDownListViewModel> ddlTahun = new List<dropDownListViewModel>();
            int row = 0;
            for (int i = YearNow - 20; i <= YearNow + 10; i++)
            {
                ddlTahun.Insert(row, new dropDownListViewModel { Text = i.ToString(), Value = i });
                row = row + 1;
            }

            return ddlTahun;
        }


        List<dropDownListViewModel> GetddlBulan()
        {
            //ddl Bulan
            List<dropDownListViewModel> ddlBulan = new List<dropDownListViewModel>();
            ddlBulan.Add(new dropDownListViewModel { Text = "Januari", Value = 1 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Februari", Value = 2 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Maret", Value = 3 });
            ddlBulan.Add(new dropDownListViewModel { Text = "April", Value = 4 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Mei", Value = 5 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juni", Value = 6 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juli", Value = 7 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Agustus", Value = 8 });
            ddlBulan.Add(new dropDownListViewModel { Text = "September", Value = 9 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Oktober", Value = 10 });
            ddlBulan.Add(new dropDownListViewModel { Text = "November", Value = 11 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Desember", Value = 12 });

            return ddlBulan;
        }

        string getNamaBulan(int Bulan)
        {
            string namaBulan = "";

            if (Bulan == 1)
                namaBulan = "Januari";
            if (Bulan == 2)
                namaBulan = "Februari";
            if (Bulan == 3)
                namaBulan = "Maret";
            if (Bulan == 4)
                namaBulan = "April";
            if (Bulan == 5)
                namaBulan = "Mei";
            if (Bulan == 6)
                namaBulan = "Juni";
            if (Bulan == 7)
                namaBulan = "Juli";
            if (Bulan == 8)
                namaBulan = "Agustus";
            if (Bulan == 9)
                namaBulan = "September";
            if (Bulan == 10)
                namaBulan = "Oktober";
            if (Bulan == 11)
                namaBulan = "November";
            if (Bulan == 12)
                namaBulan = "Desember";

            return namaBulan;

        }

        public FileStreamResult GetPDF(string NamaFile)
        {
            string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.CustodyTransferBeritaAcara).FirstOrDefault().Value;
            string ServerSavePath = System.IO.Path.Combine(Url + NamaFile);


            System.IO.FileStream fs = new System.IO.FileStream(ServerSavePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //System.IO.FileStream fs = new System.IO.FileStream(Server.MapPath($"~/Upload/SupplyLoss/{NamaFile}"), System.IO.FileMode.Open, System.IO.FileAccess.Read);

            return File(fs, "application/pdf");
        }
    }
}