﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportCargoLossesController : Controller
    {
        private ICargoLossesService _cargoLossesService;
        public ReportCargoLossesController(ICargoLossesService CargoLossesService)
        {
            _cargoLossesService = CargoLossesService;
        }
        // GET: ReportCargoLosses
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult barchart(string Muatan, string Asal, string NamaKapal, DateTime? TanggalBL, DateTime? TanggalBLSampai, 
            DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _cargoLossesService.getChart(Muatan, Asal, NamaKapal,  TanggalBL, TanggalBLSampai,
                TanggalBongkar, TanggalBongkarSampai);
            var barChartModel = new ChartModel<decimal>
            {
                Title = "Cargo Losses",
                //Subtitle = "Cargo Losses",
                XAxisCategories = obj.Item1,//new string[] { "Africa", "America", "Asia", "Europe", "Oceania" },
                XAxisTitle = "",
                YAxisTitle = "Values",
                //YAxisTooltipValueSuffix = " millions",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "Supply Loss R4(BBls)", data = obj.Item3},//new decimal[]{ 107, 31, 635, 203, 2 }},
                    new SeriesModel<decimal>{ name = "R4 (%)", data = obj.Item2}
                }
            };
            return Json(barChartModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Muatan, string Asal,
            string NamaKapal, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _cargoLossesService.SearchCargoLosses(requestModel.Start, requestModel.Length,
                Muatan, Asal, NamaKapal, "", TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                TglBL = item.BlDate,
                NamaKapal = item.TransportName,
                Muatan = item.NamaProduk,
                Asal = item.PortDescription,
                NoBl = item.BlAsal,
                TanggalBongkar = item.TanggalBongkar,
                KursBeli = item.KursBeli,
                PriceRef = item.PriceRef
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        
        public ActionResult ExportExcel(ReportCargoLossesViewModel input)
        {
            var listLaporan = _cargoLossesService.queryExportToExcel(input.Muatan, input.Asal, input.NamaKapal, input.TanggalBL, input.TanggalBLSampai, input.TanggalBongkar, input.TanggalBongkarSampai);
            
            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanCargoLosses");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = "Periode";
                workSheet.Cells[1, 2].Value = "";
                workSheet.Cells[1, 3].Value = "No";
                workSheet.Cells[1, 4].Value = "Tanggal BL";
                workSheet.Cells[1, 5].Value = "Tanggal Bongkar";
                workSheet.Cells[1, 6].Value = "Kurs Beli";
                workSheet.Cells[1, 7].Value = "Price Ref";
                workSheet.Cells[1, 8].Value = "Nama Kapal";
                workSheet.Cells[1, 9].Value = "Muatan";
                workSheet.Cells[1, 10].Value = "Asal";
                workSheet.Cells[1, 11].Value = "No BL";
                workSheet.Cells[1, 12].Value = "B/L (Bbls)";
                workSheet.Cells[1, 13].Value = "SFAL (Bbls)";
                workSheet.Cells[1, 14].Value = "SFBD (Bbls)";
                workSheet.Cells[1, 15].Value = "A/R - CQD (Bbls)";   
                workSheet.Cells[1, 16].Value = "Loading Loss(R1) (Bbls)";
                workSheet.Cells[1, 17].Value = "Loading Loss(R1) %";
                workSheet.Cells[1, 18].Value = "Loading Loss(R1) >0.3%";
                workSheet.Cells[1, 19].Value = "Transportation Loss(R2) (Bbls)";
                workSheet.Cells[1, 20].Value = "Transportation Loss(R2) %";
                workSheet.Cells[1, 21].Value = "Transportation Loss(R2) >0.15%";
                workSheet.Cells[1, 22].Value = "Discharge Loss(R3) (Bbls)";
                workSheet.Cells[1, 23].Value = "Discharge Loss(R3) %";
                workSheet.Cells[1, 24].Value = "Discharge Loss(R3) >0.3%";
                workSheet.Cells[1, 25].Value = "Supply Loss(R4) (Bbls)";
                workSheet.Cells[1, 26].Value = "Supply Loss(R4) %";
                workSheet.Cells[1, 27].Value = "Supply Loss(R4) >0.3%";
                workSheet.Cells[1, 28].Value = "Supply Losses Value";

                int recordIndex = 2;
                int no = 1;
                int noPeriode = 0;
                string periode = string.Empty;
                foreach (var item in listLaporan)
                {
                    if (periode != item.TanggalBongkar.ToString("MMM"))
                    {
                        noPeriode = 1;
                    }
                    workSheet.Cells[recordIndex, 1].Value = item.TanggalBongkar.ToString("MMM"); 
                    workSheet.Cells[recordIndex, 2].Value = no;
                    workSheet.Cells[recordIndex, 3].Value = noPeriode;
                    workSheet.Cells[recordIndex, 4].Value = item.BlDate.ToString("dd MMM yyyy");
                    workSheet.Cells[recordIndex, 5].Value = item.TanggalBongkar.ToString("dd MMM yyyy");
                    workSheet.Cells[recordIndex, 6].Value = item.KursBeli;
                    workSheet.Cells[recordIndex, 7].Value = item.PriceRef;
                    workSheet.Cells[recordIndex, 8].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 9].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 10].Value = item.PortDescription;
                    workSheet.Cells[recordIndex, 11].Value = item.BlAsal;
                    workSheet.Cells[recordIndex, 12].Value = item.BarrelsPtm;
                    workSheet.Cells[recordIndex, 13].Value = item.BarrelsAl;
                    workSheet.Cells[recordIndex, 14].Value = item.BarrelsBd;
                    workSheet.Cells[recordIndex, 15].Value = item.Barrels;
                    workSheet.Cells[recordIndex, 16].Value = item.R1;
                    workSheet.Cells[recordIndex, 17].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 18].Value = item.R103;
                    workSheet.Cells[recordIndex, 19].Value = item.R2;
                    workSheet.Cells[recordIndex, 20].Value = item.PercR2;
                    workSheet.Cells[recordIndex, 21].Value = item.R2015;
                    workSheet.Cells[recordIndex, 22].Value = item.R3;
                    workSheet.Cells[recordIndex, 23].Value = item.PercR3;
                    workSheet.Cells[recordIndex, 24].Value = item.R303;
                    workSheet.Cells[recordIndex, 25].Value = item.R4;
                    workSheet.Cells[recordIndex, 26].Value = item.PercR4;
                    workSheet.Cells[recordIndex, 27].Value = item.R403;
                    workSheet.Cells[recordIndex, 28].Value = item.SupplyLossesValue;

                    recordIndex++;
                    no++;
                    noPeriode++;
                    periode = item.TanggalBongkar.ToString("MMM");
                }

                for (int i = 1; i < 29; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 28].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 28].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 28].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 28].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 28])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanCargoLosses" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }
    }
}