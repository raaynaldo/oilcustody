﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Repository.BaseRepository;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportOilCustodyController : Controller
    {
        private IShiftService _shiftService;
        private IPICKeuanganService _PICKeuanganService;
        private BaseContext dbCtx = new BaseContext();

        public ReportOilCustodyController(
            IShiftService ShiftService,
            IPICKeuanganService PICKeuanganService)
        {
            _shiftService = ShiftService;
            _PICKeuanganService = PICKeuanganService;
        }
        // GET: ReportOilCustody
        public ActionResult Index()
        {
            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "All", Id = 0 });
            ViewBag.shift = shift;
            ViewBag.SelectShfit = 0;

            var picKeuangan = _PICKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "All", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;
            ViewBag.SelectPic = 0;

            var model = getReportQuery(DateTime.Now, 0, 0);
            return View("Index",model);
        }

        public ActionResult Get (DateTime? Tanggal,int? ShiftId,int? PICKeuanganId )
       {
            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "All", Id = 0 });
            ViewBag.shift = shift;
            ViewBag.SelectShfit = ShiftId;

            var picKeuangan = _PICKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "All", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;
            ViewBag.SelectPic = PICKeuanganId;

            var model = getReportQuery(Tanggal, ShiftId, PICKeuanganId);
            return View("Index", model);
        }

        public ActionResult ExportExcel(ReportOilCustodyViewModel input)
        {
            var getReport = getReportQuery(input.Tanggal, input.ShiftId, input.PICKeuanganId);
            var listLaporan = getReport;

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("Laporan Oil Custody");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells["A1"].Value = "No.";
                workSheet.Cells["B1"].Value = "Tanggal";
                workSheet.Cells["C1"].Value = "OnCall / Standby";
                workSheet.Cells["D1"].Value = "Status";
                workSheet.Cells["E1"].Value = "Nama";
                workSheet.Cells["F1:G1"].Merge = true;
                workSheet.Cells["F1:G1"].Value = "Aktivitas";

                workSheet.Cells["A1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells["F1:G1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                workSheet.Cells[2, 1].Value = 1;
                workSheet.Cells[2, 2].Value = Convert.ToDateTime(input.Tanggal).ToString("dd MMM yyyy");
                workSheet.Cells[2, 3].Value = "On Call";
                if (input.ShiftId == 0)
                { workSheet.Cells[2, 4].Value = "All";  }
                else
                { workSheet.Cells[2, 4].Value = _shiftService.Get(input.ShiftId).Description.ToString(); }
                if (input.PICKeuanganId == 0)
                { workSheet.Cells[2, 5].Value = "All";  }
                else
                { workSheet.Cells[2, 5].Value = _PICKeuanganService.Get(input.PICKeuanganId).Nama.ToString(); }
                workSheet.Cells[2, 6, 2, 7].Merge = true;
                workSheet.Cells[2, 6, 2, 7].Value = "Jetty/Dermaga";

                workSheet.Cells[2, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[2, 6, 2, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                int rowJetty = 3;
                foreach (var item in listLaporan.Jetty)
                {
                    workSheet.Cells[rowJetty, 1].Value = "";
                    workSheet.Cells[rowJetty, 2].Value = "";
                    workSheet.Cells[rowJetty, 3].Value = "";
                    workSheet.Cells[rowJetty, 4].Value = "";
                    workSheet.Cells[rowJetty, 5].Value = "";
                    workSheet.Cells[rowJetty, 6].Value = "";
                    workSheet.Cells[rowJetty, 7].Value =
                        item.NamaJetty + "; "
                        + item.NamaKapal + "; "
                        + item.NamaCargo + "; "
                        + item.Qty + "; "
                        + item.Start + "; "
                        + item.Rate + "; "
                        + item.Stop;

                    workSheet.Cells[rowJetty, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowJetty, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    rowJetty++;
                }
                int PipaHeaderPos = rowJetty;

                workSheet.Cells[PipaHeaderPos, 1].Value = "";
                workSheet.Cells[PipaHeaderPos, 2].Value = "";
                workSheet.Cells[PipaHeaderPos, 3].Value = "";
                workSheet.Cells[PipaHeaderPos, 4].Value = "";
                workSheet.Cells[PipaHeaderPos, 5].Value = "";
                workSheet.Cells[PipaHeaderPos, 6, PipaHeaderPos, 7].Merge = true;
                workSheet.Cells[PipaHeaderPos, 6, PipaHeaderPos, 7].Value = "Pipa";

                workSheet.Cells[PipaHeaderPos, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[ PipaHeaderPos, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                int rowPipa = PipaHeaderPos + 1;
                foreach (var item in listLaporan.Pipa)
                {
                    workSheet.Cells[rowPipa, 1].Value = "";
                    workSheet.Cells[rowPipa, 2].Value = "";
                    workSheet.Cells[rowPipa, 3].Value = "";
                    workSheet.Cells[rowPipa, 4].Value = "";
                    workSheet.Cells[rowPipa, 5].Value = "";
                    workSheet.Cells[rowPipa, 6].Value = "";
                    workSheet.Cells[rowPipa, 7].Value =
                        item.FromTo + " "
                        + item.NamaCargo + " "
                        + item.Qty + " "
                        + item.Start + " "
                        + item.Stop;

                    workSheet.Cells[rowPipa, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowPipa, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    rowPipa++;
                }

                int TangkiHeaderPos = rowPipa;

                workSheet.Cells[TangkiHeaderPos, 1].Value = "";
                workSheet.Cells[TangkiHeaderPos, 2].Value = "";
                workSheet.Cells[TangkiHeaderPos, 3].Value = "";
                workSheet.Cells[TangkiHeaderPos, 4].Value = "";
                workSheet.Cells[TangkiHeaderPos, 5].Value = "";
                workSheet.Cells[TangkiHeaderPos, 6, TangkiHeaderPos, 7].Merge = true;
                workSheet.Cells[TangkiHeaderPos, 6, TangkiHeaderPos, 7].Value = "Mobil Tangki";

                workSheet.Cells[TangkiHeaderPos, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[TangkiHeaderPos, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                int rowTangki = TangkiHeaderPos + 1;
                foreach (var item in listLaporan.Tangki)
                {
                    workSheet.Cells[rowTangki, 1].Value = "";
                    workSheet.Cells[rowTangki, 2].Value = "";
                    workSheet.Cells[rowTangki, 3].Value = "";
                    workSheet.Cells[rowTangki, 4].Value = "";
                    workSheet.Cells[rowTangki, 5].Value = "";
                    workSheet.Cells[rowTangki, 6].Value = "";
                    workSheet.Cells[rowTangki, 7].Value =
                        item.FromTo + " "
                        + item.Qty;

                    workSheet.Cells[rowTangki, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[rowTangki, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rowTangki++;
                    workSheet.Cells[rowTangki, 1, rowTangki, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                }

                for (int i = 1; i < 8; i++)
                {
                    workSheet.Column(i).AutoFit();
                }
                using (var range = workSheet.Cells[1, 1, 1, 7])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "Laporan_Oil_Custody" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }

        public ReportOilCustodyViewModel getReportQuery(DateTime? Tanggal, int? IdShift, int? IdPICKeuangan)
        {
            ReportOilCustodyViewModel Result = new ReportOilCustodyViewModel();

            //Get For Finance Jetty
            DataTable dt = new DataTable();
            List<FinanceJettyDetailViewModel> Jetty = new List<FinanceJettyDetailViewModel>();
            string query = " SELECT A.[Jetty], D.[Description] AS TangkiDesc, C.[Description] AS CargoDesc, A.[Qty],A.[RealisasiStart],A.[PumpRate],A.[RealisasiStop]" +
                                  " FROM [OilCustody].[dbo].[FinanceJetty] A" +
                                  " LEFT JOIN [OilCustody].[dbo].[FinanceJettyDetail] B ON A.Id = B.FinanceJettyId" +
                                  " LEFT JOIN [OilCustody].[dbo].[Cargo] C ON B.CargoId = C.Id" +
                                  " LEFT JOIN [OilCustody].[dbo].[Tangki] D ON B.TangkiId = D.id" +
                                  " LEFT JOIN [OilCustody].[dbo].[Shift] E ON E.Id = A.ShiftId" +
                                  " LEFT JOIN [OilCustody].[dbo].[PICKeuangan] F ON F.Id = A.PICKeuanganId" +
                                  " WHERE 1 = 1";
            if (Tanggal != null)
                query = query + " AND A.[Tanggal] ='" + Tanggal + "'";
            if (IdPICKeuangan != null && IdPICKeuangan != 0)
                query = query + " AND A.[PICKeuanganId] =" + IdPICKeuangan.ToString();
            if (IdShift != null && IdShift != 0)
                query = query + " AND A.[ShiftId] =" + IdShift.ToString();

            dt = ExecuteQuery(query);
            if (dt.Rows.Count > 0)
            {
                for(int i = 0;i < dt.Rows.Count; i++)
                {
                    Jetty.Add(new FinanceJettyDetailViewModel
                    {
                        NamaJetty = dt.Rows[i]["Jetty"].ToString(),
                        NamaKapal = dt.Rows[i]["TangkiDesc"].ToString(),
                        NamaCargo = dt.Rows[i]["CargoDesc"].ToString(),
                        Qty = dt.Rows[i]["Qty"].ToString(),
                        Start = dt.Rows[i]["RealisasiStart"].ToString(),
                        Rate = dt.Rows[i]["PumpRate"].ToString(),
                        Stop = dt.Rows[i]["RealisasiStop"].ToString()
                    });
                }
            }

            //Get Data for Pipa
            DataTable dt1 = new DataTable();
            List<FinancePipaDetailViewModel> Pipa = new List<FinancePipaDetailViewModel>();
            string  query1 =      "SELECT A.[FromTo],B.[Description] AS CargoDesc,A.[Qty],A.[EstimasiStart],A.[EstimasiStop]" +
                                  " FROM [OilCustody].[dbo].[FinancePipa] A" +
                                  " LEFT JOIN [OilCustody].[dbo].[Cargo] B ON A.CargoId = B.Id" +
                                  " WHERE 1 = 1";
            if (Tanggal != null)
                query1 = query1 + " AND A.[Tanggal] ='" + Tanggal + "'";
            if (IdPICKeuangan != null && IdPICKeuangan != 0)
                query1 = query1 + " AND A.[PICKeuanganId] =" + IdPICKeuangan.ToString();
            if (IdShift != null && IdShift != 0)
                query1 = query1 + " AND A.[ShiftId] =" + IdShift.ToString();

            dt1 = ExecuteQuery(query1);
            if (dt1.Rows.Count > 0)
            {
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    Pipa.Add(new FinancePipaDetailViewModel
                    {
                        FromTo = dt1.Rows[i]["FromTo"].ToString(),
                        NamaCargo = dt1.Rows[i]["CargoDesc"].ToString(),
                        Qty = dt1.Rows[i]["Qty"].ToString(),
                        Start = dt1.Rows[i]["EstimasiStart"].ToString(),
                        Stop = dt1.Rows[i]["EstimasiStop"].ToString()
                    });
                }
            }

            //Get Data for Mobil Tangki
            DataTable dt2 = new DataTable();
            List<FinanceTangkiDetailViewModel> Tangki = new List<FinanceTangkiDetailViewModel>();
            string query2 = "SELECT FromTo,Qty" +
                             " FROM [OilCustody].[dbo].[FinanceTangki]" +
                             " WHERE 1 = 1";
            if (Tanggal != null)
                query2 = query2 + " AND [Tanggal] ='" + Tanggal + "'";
            if (IdPICKeuangan != null && IdPICKeuangan != 0)
                query2 = query2 + " AND [PICKeuanganId] =" + IdPICKeuangan.ToString();
            if (IdShift != null && IdPICKeuangan != 0)
                query2 = query2 + " AND [ShiftId] =" + IdShift.ToString();

            dt2 = ExecuteQuery(query2);
            if (dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    Tangki.Add(new FinanceTangkiDetailViewModel
                    {
                        FromTo = dt2.Rows[i]["FromTo"].ToString(),
                        Qty = dt2.Rows[i]["Qty"].ToString()
                    });
                }
            }
            Result.Jetty = Jetty;
            Result.Pipa = Pipa;
            Result.Tangki = Tangki; 

            return Result;
        }

        public DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable();
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    //sql data adapter
                    SqlDataAdapter sqlD = new SqlDataAdapter(cmd);
                    sqlD.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

        }
    }
}