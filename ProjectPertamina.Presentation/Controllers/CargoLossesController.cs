﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class CargoLossesController : Controller
    {
        private ICargoLossesService _cargoLossesService;
        private IRoasService _roasService;
        private IPortService _portService;

        private CurrentUser user = CurrentUser.GetCurrentUser();
        public CargoLossesController(ICargoLossesService CargoLossesService, IRoasService RoasService, IPortService PortService)
        {
            _cargoLossesService = CargoLossesService;
            _roasService = RoasService;
            _portService = PortService;

        }
        // GET: CargoLosses
        public ActionResult Index()
        {
            ViewBag.Process = user.ActionCreate || user.ActionUpdate;

            return View(new CargoLosses());
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Muatan, string Asal,
            string NamaKapal, string NoBl, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _cargoLossesService.SearchCargoLosses(requestModel.Start, requestModel.Length,
                Muatan, Asal, NamaKapal, NoBl, TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                DocDate = item.Roas.DocDate,
                DocNo = item.Roas.DocNo,
                TanggalBl = item.BlDate,
                NamaKapal = item.TransportName,
                Muatan = item.NamaProduk,
                Asal = item.PortDescription,
                NoBl = item.BlAsal,
                TanggalBongkar = item.TanggalBongkar,
                KursBeli = item.KursBeli,
                PriceRef = item.PriceRef
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit()
        {
            if (user.ActionCreate || user.ActionUpdate)
            {
                ViewBag.Create = user.ActionCreate;
                ViewBag.Edit = user.ActionUpdate;
                return View();
            }
            else
                return RedirectToAction("Index", "UnAuthorized");
        }

        [HttpPost]
        public ActionResult GetCargoLossesRoas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeProses, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        //DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            if (TipeProses == "Edit")
            {
                var objEdit = _cargoLossesService.SearchCargoLossesEdit(TanggalBongkar, TanggalBongkarSampai);

                var dataEdit = objEdit.Select(item => new
                {
                    Id = item.Id,
                    DocDate = item.Roas.DocDate,
                    DocNo = item.Roas.DocNo,
                    TanggalBl = item.BlDate,
                    NamaKapal = item.TransportName,
                    Muatan = item.NamaProduk,
                    Asal = item.PortDescription,
                    NoBl = item.BlAsal,
                    TanggalBongkar = item.TanggalBongkar,
                    KursBeli = item.KursBeli,
                    PriceRef = item.PriceRef
                });

                return Json(new DataTablesResponse(requestModel.Draw, dataEdit.ToList(), dataEdit.Count(), dataEdit.Count()), JsonRequestBehavior.AllowGet);
            }

            var obj = _roasService.GetRoasForCargoLosses();

            var data = obj.Select(item => new
            {
                Id = item.Id,
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                TanggalBl = item.BlDate,
                NamaKapal = item.TransportName,
                Muatan = item.NamaProduk,
                Asal = item.Port.Description,
                NoBl = item.BlAsal,
                TanggalBongkar = DateTime.Now,
                KursBeli = 0,
                PriceRef = 0
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), data.Count(), data.Count()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCargoLosses(List<CargoLosses> Item, string TipeProses)
        {
            try
            {

                if (TipeProses == "Baru")
                {
                    List<CargoLosses> CargoLossesList = new List<CargoLosses>();
                    var DataCargoLosses = _cargoLossesService.GetAllJoinData().Where(i => !i.Deleted).ToList();

                    foreach (var item in Item)
                    {
                        CargoLosses CL = new CargoLosses();
                        Roas RoasObj = _roasService.Get(item.Id);
                        RoasObj.Port = _portService.Get(RoasObj.PortId);
                        CL.ROASId = item.Id;
                        CL.TanggalBongkar = item.TanggalBongkar;
                        CL.PriceRef = item.PriceRef;
                        CL.KursBeli = item.KursBeli;

                        CL.BlDate = RoasObj.BlDate;
                        CL.TransportName = RoasObj.TransportName;
                        CL.NamaProduk = RoasObj.NamaProduk;
                        CL.PortDescription = RoasObj.Port.Description;
                        CL.BlAsal = RoasObj.BlAsal;
                        CL.BarrelsPtm = RoasObj.BarrelsPtm;
                        CL.BarrelsAl = RoasObj.BarrelsAl;
                        CL.BarrelsBd = RoasObj.BarrelsBd;
                        CL.Barrels = RoasObj.Barrels;
                        CL.R1 = RoasObj.BarrelsPtm - RoasObj.BarrelsAl;
                        CL.PercR1 = CL.R1 * 100 / RoasObj.BarrelsPtm;
                        CL.R103 = CL.PercR1 > (decimal)0.3 ? "Qry" : "-";
                        CL.R2 = RoasObj.BarrelsAl - RoasObj.BarrelsBd;
                        CL.PercR2 = CL.R2 * 100 / RoasObj.BarrelsPtm;
                        CL.R2015 = CL.PercR2 > (decimal)0.3 ? "Qry" : "-";
                        CL.R3 = CL.BarrelsBd - CL.Barrels;
                        CL.PercR3 = CL.R3 * 100 / RoasObj.BarrelsPtm;
                        CL.R303 = CL.PercR3 > (decimal)0.3 ? "Qry" : "-";
                        CL.R4 = RoasObj.BarrelsPtm - RoasObj.Barrels;
                        CL.PercR4 = CL.R4 * 100 / RoasObj.BarrelsPtm;
                        CL.R403 = CL.PercR4 > (decimal)0.3 ? "Qry" : "-";
                        CL.SupplyLossesValue = CL.R4 * CL.KursBeli * CL.PriceRef;
                        CL.Deleted = false;
                        CL.Roas = RoasObj;

                        var check = CargoLossesList.FindIndex(i => i.Roas.DocDate == RoasObj.DocDate && i.Roas.DocNo == RoasObj.DocNo && i.Roas.BlAsal == RoasObj.BlAsal && !i.Deleted);
                        if (check > -1)
                        {
                            CargoLossesList[check].Deleted = true;
                        }

                        CargoLossesList.Add(CL);

                        //var check = _cargoLossesService.Find(i => i.Roas.DocDate == RoasObj.DocDate && i.Roas.DocNo == RoasObj.DocNo && i.Roas.BlAsal == RoasObj.BlAsal && !i.Deleted);
                        //if (check.Count() > 0)
                        //{
                        //    var CargoLama = check.FirstOrDefault();
                        //    CargoLama.Deleted = true;
                        //    _cargoLossesService.Save(CargoLama);
                        //}

                        //_cargoLossesService.Save(CL);
                    }

                    _cargoLossesService.AddRange(CargoLossesList);

                    foreach (var item in CargoLossesList.Where(i => !i.Deleted))
                    {
                        var check = DataCargoLosses.FindIndex(i => i.Roas.DocDate == item.Roas.DocDate && i.Roas.DocNo == item.Roas.DocNo && i.Roas.BlAsal == item.Roas.BlAsal && !i.Deleted);
                        if (check > -1)
                        {
                            DataCargoLosses[check].Deleted = true;
                        }

                        //var check = DataCargoLosses.Where(i => i.Roas.DocDate == item.Roas.DocDate && i.Roas.DocNo == item.Roas.DocNo && i.Roas.BlAsal == item.Roas.BlAsal);
                        //if (check.Count() > 0)
                        //{
                        //    var CargoLama = check.FirstOrDefault();
                        //    CargoLama.Deleted = true;
                        //    _cargoLossesService.Save(CargoLama);

                        //    DataCargoLosses.Remove(CargoLama);
                        //}
                    }

                    _cargoLossesService.UpdateRange(DataCargoLosses);

                }
                else if (TipeProses == "Edit")
                {
                    var list = new List<CargoLosses>();
                    foreach (var item in Item)
                    {
                        var CargoLosses = _cargoLossesService.Get(item.Id);
                        CargoLosses.TanggalBongkar = item.TanggalBongkar;
                        CargoLosses.PriceRef = item.PriceRef;
                        CargoLosses.KursBeli = item.KursBeli;

                        CargoLosses.SupplyLossesValue = CargoLosses.R4 * CargoLosses.KursBeli * CargoLosses.PriceRef;

                        list.Add(CargoLosses);
                        //_cargoLossesService.Save(CargoLosses);
                    }
                    _cargoLossesService.UpdateRange(list);
                }
                //_financeJettyDetailService.Save(model);
                return Content("Success");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }
        }

    }
}