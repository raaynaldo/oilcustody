﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class Tabel21Controller : Controller
    {
        private ITabel21Service _tabel21Service;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public Tabel21Controller(ITabel21Service Tabel21Service)
        {
            _tabel21Service = Tabel21Service;
        }
        // GET: MasterTabel21
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, decimal SG, decimal Density)
        {
            var obj = _tabel21Service.SearchTabel21(requestModel.Start, requestModel.Length, SG, Density);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                SG = camp.SG,
                Density = camp.Density15,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            List<Tabel21> Tabel21List = new List<Tabel21>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        List<Tabel21> ListTabel21 = _tabel21Service.GetAll().ToList();
                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "SG" ||
                                workSheet.Cells[1, 2].Value.ToString() != "Density 15" ||
                                noOfCol > 2)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Table 21.";
                                return View("_Upload");
                            }
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new Tabel21();
                                obj.SG = decimal.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                                if (ListTabel21.Where(i => i.SG == obj.SG).FirstOrDefault() != null || Tabel21List.Where(i => i.SG == obj.SG).FirstOrDefault() != null)
                                {
                                    ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>SG baris ke {rowIterator} gagal, karena data sudah ada.";
                                    if (rowIterator == 2)
                                        ViewBag.Error = $"SG baris ke {rowIterator} gagal, karena data sudah ada.<br>Tidak ada data yang terupload.";

                                    _tabel21Service.AddRange(Tabel21List);

                                    return View("_Upload");
                                }

                                obj.Density15 = decimal.Parse(workSheet.Cells[rowIterator, 2].Value.ToString());

                                Tabel21List.Add(obj);
                            }

                            _tabel21Service.AddRange(Tabel21List);
                        }
                    }
                }
                return Content("success");
            }
            //catch (DbUpdateException ex)
            //{
            //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
            //    return View("_Upload");
            //}
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Terdapat data kosong pada baris ke {rowIterator}";
                _tabel21Service.AddRange(Tabel21List);
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"Input Gagal, tidak ada data yang diinput.";
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }

        }

        public ActionResult Create(int id = 0)
        {
            var tabel21 = _tabel21Service.Get(id);
            ViewBag.Title = "EDIT";
            if (tabel21 == null)
            {
                tabel21 = new Tabel21();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", tabel21);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Tabel21 model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                _tabel21Service.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("SG", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var tabel21 = _tabel21Service.Get(Id);
            return PartialView("_Delete", tabel21);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _tabel21Service.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var tabel21 = _tabel21Service.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", tabel21);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel21);
            }
            catch (Exception ex)
            {
                var tabel21 = _tabel21Service.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel21);
            }
        }

        public JsonResult GetDensityFromTabel21(decimal sg)
        {
            decimal sg1 = (Math.Truncate(sg * 1000) / 1000);
            decimal sg2 = sg1 + (decimal)0.001;
            Tabel21 sg3obj = _tabel21Service.Find(i => i.SG == sg1).FirstOrDefault();
            Tabel21 sg4obj = _tabel21Service.Find(i => i.SG == sg2).FirstOrDefault();
            if (sg3obj == null)
                return Json(new { density = "Master Tabel21 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            if (sg4obj == null)
                return Json(new { density = "Master Tabel21 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            decimal sg3 = sg3obj.Density15;
            decimal sg4 = sg4obj.Density15;

            var density = sg3 + ((sg - sg1) / (decimal)0.001 * (sg4 - sg3));
            //density = Math.Round(density,4);
            return Json(new { density = density, val = true }, JsonRequestBehavior.AllowGet);
        }

    }
}