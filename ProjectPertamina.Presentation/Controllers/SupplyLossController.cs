﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    public class SupplyLossTemp
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string NamaKapal { get; set; }
        public DateTime Tanggal { get; set; }
    }

    [PertaminaAuthorize]
    public class SupplyLossController : Controller
    {
        private ISupplyLossSupplyService _supplyLossSupplyService;
        private INilaiService _nilaiService;
        private ISupplyLossDischargeService _supplyLossDischargeService;
        private ISupplyLossService _supplyLossService;
        private ILoadingService _loadingService;
        private IKonfigurasiParameterService _konfigurasiParamaterService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public SupplyLossController(ISupplyLossService SupplyLossService, ILoadingService LoadingService, ISupplyLossSupplyService SupplyLossSupply, ISupplyLossDischargeService SupplyLossDischarge, INilaiService Nilai, IKonfigurasiParameterService KonfigurasiParameter)
        {
            _nilaiService = Nilai;
            _supplyLossSupplyService = SupplyLossSupply;
            _supplyLossDischargeService = SupplyLossDischarge;
            _supplyLossService = SupplyLossService;
            _loadingService = LoadingService;
            _konfigurasiParamaterService = KonfigurasiParameter;
        }

        // GET: SupplyLoss
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var model = new SupplyLossViewModel();
            model.LoadingMasterList = _loadingService.GetAll().ToList();
            model.LoadingMasterList.Insert(0, new Entities.Models.Loading() { Id = 0, Description = "" });


            return View(model);
        }

        //[HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? TanggalDari, DateTime? TanggalSampai, int LoadingId = 0, string Keterangan = "")
        {
            var obj = _supplyLossService.SearchSupplyLoss(requestModel.Start, requestModel.Length, Keterangan, TanggalDari, TanggalSampai, LoadingId);

            var data = obj.Item1.Select(lm => new
            {
                Id = lm.Id,
                Tanggal = lm.Tanggal,
                LoadingMaster = lm.Loading.Description,
                KeteranganLosses = lm.Keterangan,
                BeritaAcara = lm.BeritaAcara,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit()
        {
            if (user.ActionCreate)
            {
                List<string> NamaKapal = new List<string>();
                NamaKapal.AddRange(_supplyLossSupplyService.GetAllJoinData().Select(i => i.TransportName).ToList());
                NamaKapal.AddRange(_supplyLossDischargeService.GetAllJoinData().Select(i => i.TransportName).ToList());
                NamaKapal = NamaKapal.Distinct().ToList();
                NamaKapal.Insert(0, "");
                ViewBag.NamaKapal = NamaKapal;

                var Loading = _loadingService.GetAll().ToList();
                Loading.Insert(0, new Loading() { Id = 0, Description = "", Code = "" });
                ViewBag.Loading = Loading;

                return View();
            }
            else
                return RedirectToAction("Index", "UnAuthorized");
        }

        public ActionResult GetSupplyLoss([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? Tanggal, DateTime? TanggalSampai, string NamaKapal)
        {

            var R1 = _nilaiService.Find(i => i.Code.Trim() == "R1").FirstOrDefault();
            if (R1 == null)
                return Content("Data Nilai R1 tidak ada, harap masukan Master R1 di menu Nilai");
            var R4 = _nilaiService.Find(i => i.Code.Trim() == "R4").FirstOrDefault();
            if (R4 == null)
                return Content("Data Nilai R4 tidak ada, harap masukan Master R4 di menu Nilai");
            var SupplyLoss = _supplyLossService.SearchByDocDate(Tanggal, TanggalSampai, NamaKapal).ToList();
            var SLSList = from list in _supplyLossSupplyService.SearchSupplyLossSupplyForSupplyLoss(R1.Value, Tanggal, TanggalSampai, NamaKapal).ToList()
                          join SL in SupplyLoss
                          on list.Id equals SL.SupplyLossSupplyId into a
                          from SLS in a.DefaultIfEmpty()
                          select new
                          {
                              Id = list.Id,
                              DocDate = list.DocDate,
                              DocNo = list.DocNo,
                              NoBL = list.Roas.BlAsal,
                              NamaKapal = list.TransportName,
                              NamaKargo = list.NamaProduk,
                              NilaiToleransi = R1.Value,
                              BeritaAcara = SLS != null ? SLS.BeritaAcara : "",
                              Type = "Supply"
                          };

            var SLDList = from list in _supplyLossDischargeService.SearchSupplyLossDischargeForSupplyLoss(R4.Value, Tanggal, TanggalSampai, NamaKapal).ToList()
                          join SL in SupplyLoss
                          on list.Id equals SL.SupplyLossSupplyId into a
                          from SLS in a.DefaultIfEmpty()
                          select new
                          {
                              Id = list.Id,
                              DocDate = list.DocDate,
                              DocNo = list.DocNo,
                              NoBL = list.Roas.BlAsal,
                              NamaKapal = list.TransportName,
                              NamaKargo = list.NamaProduk,
                              NilaiToleransi = R4.Value,
                              BeritaAcara = SLS != null ? SLS.BeritaAcara : "",
                              Type = "Discharge"
                          };

            var data = SLSList.Union(SLDList).ToList();


            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), 0, 0), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveSupplyLoss(HttpPostedFileBase InputBeritaAcara, int Loading, string Keterangan, List<SupplyLossTemp> SupplyLoss)
        {

            var InputFileName = Path.GetFileName(InputBeritaAcara != null ? InputBeritaAcara.FileName : "");
            if (InputBeritaAcara != null)
            {
                //var ServerSavePath = Path.Combine("D:/Upload/Campaign/" + InputFileName);
                string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.SupplyLossBeritaAcara).FirstOrDefault().Value;
                string ServerSavePath = System.IO.Path.Combine(Url + InputFileName);
                //string ServerSavePath = Server.MapPath("~/Upload/SupplyLoss/" + InputFileName);
                if (System.IO.File.Exists(ServerSavePath))
                {
                    var InputFileNameWithoutExt = Path.GetFileNameWithoutExtension(InputBeritaAcara.FileName);
                    var InputFileExtName = Path.GetExtension(InputBeritaAcara.FileName);
                    InputFileName = InputFileNameWithoutExt + "_" + DateTime.Now.ToString("ddmmyyyyhhmmss") + InputFileExtName;
                    //ServerSavePath = Path.Combine("D:/Upload/Campaign/" + InputFileName);
                    ServerSavePath = System.IO.Path.Combine(Url + InputFileName);
                }
                InputBeritaAcara.SaveAs(ServerSavePath);
            }

            List<SupplyLoss> SupplyLossList = new List<SupplyLoss>();
            List<SupplyLoss> SupplyLossUpdateList = new List<SupplyLoss>();
            foreach (var item in SupplyLoss)
            {


                SupplyLoss SL = new SupplyLoss();
                SL.Tanggal = item.Tanggal;
                SL.NamaKapal = item.NamaKapal;
                SL.LoadingId = Loading;
                SL.Keterangan = Keterangan;
                SL.BeritaAcara = InputFileName;
                if (item.Type == "Supply")
                {
                    SL.SupplyLossSupplyId = item.Id;
                    SL.SupplyLossDischargeId = null;
                    var obj = _supplyLossService.GetBySupplyLossSupply(item.Id);
                    if (obj == null)
                        SupplyLossList.Add(SL);
                    else
                    {
                        SL.Id = obj.Id;
                        SupplyLossUpdateList.Add(SL);
                    }
                }
                else
                {
                    SL.SupplyLossSupplyId = null;
                    SL.SupplyLossDischargeId = item.Id;
                    var obj = _supplyLossService.GetBySupplyLossDischarge(item.Id);
                    if (obj == null)
                        SupplyLossList.Add(SL);
                    else
                    {
                        SL.Id = obj.Id;
                        SupplyLossUpdateList.Add(SL);
                    }
                }

            }
            if (SupplyLossList.Count() > 0)
                _supplyLossService.AddRange(SupplyLossList);

            if (SupplyLossUpdateList.Count() > 0)
                _supplyLossService.UpdateRange(SupplyLossUpdateList);

            return Content("Success");
        }

        public ActionResult Edit(int id)
        {
            var Loading = _loadingService.GetAll().ToList();
            //Loading.Insert(0, new Loading() { Id = 0, Description = "", Code = "" });
            ViewBag.Loading = Loading;

            var SL = _supplyLossService.Get(id);

            return PartialView("_Edit", SL);
        }

        [HttpPost, ActionName("Edit")]
        public ActionResult EditAction(SupplyLoss model, HttpPostedFileBase InputBeritaAcara, bool Deleted)
        {
            try
            {
                var SL = _supplyLossService.Get(model.Id);

                var InputFileName = Path.GetFileName(InputBeritaAcara != null ? InputBeritaAcara.FileName : "");
                if (InputBeritaAcara != null)
                {
                    string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.SupplyLossBeritaAcara).FirstOrDefault().Value;
                    string ServerSavePath = System.IO.Path.Combine(Url + InputFileName);
                    //var ServerSavePath = Server.MapPath("~/Upload/SupplyLoss/" + InputFileName);
                    if (System.IO.File.Exists(ServerSavePath))
                    {
                        var InputFileNameWithoutExt = Path.GetFileNameWithoutExtension(InputBeritaAcara.FileName);
                        var InputFileExtName = Path.GetExtension(InputBeritaAcara.FileName);
                        InputFileName = InputFileNameWithoutExt + "_" + DateTime.Now.ToString("ddmmyyyyhhmmss") + InputFileExtName;
                        ServerSavePath = ServerSavePath = System.IO.Path.Combine(Url + InputFileName);
                    }

                    SL.BeritaAcara = InputFileName;
                    InputBeritaAcara.SaveAs(ServerSavePath);
                }

                if (Deleted)
                    SL.BeritaAcara = "";


                SL.LoadingId = model.LoadingId;
                SL.Keterangan = model.Keterangan != null ? model.Keterangan : "";

                _supplyLossService.Save(SL);


                return Content("success");

                //return PartialView("_Edit", SL);
            }
            catch
            {
                ModelState.AddModelError("", "Unable to add the Asset");
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var supplyLoss = _supplyLossService.Get(Id);
            return PartialView("_Delete", supplyLoss);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _supplyLossService.Delete(id);
                return Content("success");
            }
            catch (Exception ex)
            {
                var supplyLoss = _supplyLossService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", supplyLoss);
            }
        }

        //public ActionResult PDFPreview(string NamaFile)
        //{
        //    return View("_PDFPreview", NamaFile);
        //}
        public FileStreamResult GetPDF(string NamaFile)
        {
            string Url = @_konfigurasiParamaterService.Find(i => i.Key == ConstantKonfigurasiParameter.SupplyLossBeritaAcara).FirstOrDefault().Value;
            string ServerSavePath = System.IO.Path.Combine(Url + NamaFile);


            System.IO.FileStream fs = new System.IO.FileStream(ServerSavePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //System.IO.FileStream fs = new System.IO.FileStream(Server.MapPath($"~/Upload/SupplyLoss/{NamaFile}"), System.IO.FileMode.Open, System.IO.FileAccess.Read);

            return File(fs, "application/pdf");
        }
    }
}