﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using DataTables.Mvc;
using ProjectPertamina.Services;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.UserManager;
using Rotativa.Options;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportJurnalAvturController : Controller
    {
        private IOilMovementService _oilMovementService;
        private CurrentUser user = CurrentUser.GetCurrentUser();

        public ReportJurnalAvturController(IOilMovementService OilMovementService)
        {
            _oilMovementService = OilMovementService;
        }
        // GET: ReportJurnalAvtur
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult ReportGenerate()
        {

            return View();
        }

        [AllowAnonymous]
        public ActionResult HeaderReport()
        {

            return View();
        }

        [AllowAnonymous]
        public ActionResult FooterReport()
        {
            ViewBag.User = user.DisplayName;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Print(string Nopol, string Tujuan, DateTime? TanggalDari, DateTime? TanggalSampai)
        {

            List<OilMovement> listModel = _oilMovementService.reportJurnal(TanggalDari, TanggalSampai, Tujuan, Nopol);
            ViewBag.User = user.DisplayName;
            string header = Server.MapPath("~/Views/Shared/Header.html");
            //string footer = Server.MapPath("~/Views/Shared/Footer.html");
            string footer = string.Format("--footer-right \"[Oil Custody] Printed on: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + " by " + user.DisplayName + "\"" + " --footer-line --footer-font-size \"10\" --footer-spacing 6 --footer-font-name \"Times New Roman\"");

            //string header = Url.Action("HeaderReport", "ReportJurnalAvtur", new { Areas = "" }, Request.Url.Scheme);
            //string footer = Url.Action("FooterReport", "ReportJurnalAvtur", new { Areas = "" }, Request.Url.Scheme);
            //string customSwitches = string.Format("--print-media-type --allow {0} --header-html {0} --header-spacing 10 --footer-html {1} --header-spacing 10",
            //                        header, footer);
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                                   "--header-spacing \"0\" " +
                                   "--footer-html \"{1}\" " +
                                   "--footer-spacing \"10\" " +
                                   "--footer-font-size \"10\" " +
                                   "--header-font-size \"10\" ", header, footer);
            //Url.Action("HeaderReport", "ReportJurnalAvtur", null, "http"),
            //                       Url.Action("FooterReport", "ReportJurnalAvtur", null, "http"));
            //return View("ReportGenerate", listModel);

                return new ViewAsPdf("ReportGenerate", listModel)
            {
                //FileName = "PdfFileName.pdf",
                PageSize = Size.A4,
                //PageOrientation = Orientation.Landscape,
                CustomSwitches = footer
            };
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? TanggalDari, DateTime? TanggalSampai
            , string Tujuan, string Nopol)
        {
            var obj = _oilMovementService.SearchOilMovementReport(requestModel.Start, requestModel.Length, TanggalDari, TanggalSampai, Tujuan, Nopol);

            var data = obj.Item1.Select(olm => new
            {
                Id = olm.Id,
                JamAwal = olm.JamMulai,
                Ket = olm.JamSelesai,
                Nopol = olm.NoPolisi,
                Tujuan = olm.Tujuan,
                MeterAwal = olm.MeterAwal,
                MeterAkhir = olm.MeterAkhir,
                Bridger = olm.KapBridger
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
    }
}