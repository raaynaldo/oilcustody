﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class SupplyLossDischargeController : Controller
    {
        private ISupplyLossDischargeService _supplyLossDischargeService;
        private IRoasService _roasService;
        private IPortService _portService;

        private CurrentUser user = CurrentUser.GetCurrentUser();
        public SupplyLossDischargeController(ISupplyLossDischargeService SupplyLossDischargeService, IRoasService RoasService, IPortService PortService)
        {
            _supplyLossDischargeService = SupplyLossDischargeService;
            _roasService = RoasService;
            _portService = PortService;

        }
        // GET: CargoLosses
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;

            var TransportTypeList = _supplyLossDischargeService.GetListTransportType();
            TransportTypeList.Insert(0, "");
            ViewBag.TipeTransportasi = TransportTypeList;

            return View(new SupplyLossDischarge());
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeTransportasi, string LoadingPort,
            string NamaKapal, string NoBl, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossDischargeService.SearchSupplyLossDischarge(requestModel.Start, requestModel.Length,
                TipeTransportasi, LoadingPort, NamaKapal, NoBl, TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                TipeTransport = item.TransportType,
                Muatan = item.NamaProduk,
                NoBl = item.Roas.BlAsal,
                BLGross = item.BlGross,
                ARGross = item.ARGross,
                TanggalBl = item.BlDate,
                TanggalBongkar = item.TanggalBongkar,
                AccountingDoc = item.AccountingDoc,
                Amount1 = item.Amount1,
                Amount2 = item.Amount2,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "Document Date" ||
                                workSheet.Cells[1, 2].Value.ToString() != "Document No" ||
                                workSheet.Cells[1, 3].Value.ToString() != "BL Asal" ||
                                workSheet.Cells[1, 4].Value.ToString() != "Accounting Doc" ||
                                workSheet.Cells[1, 5].Value.ToString() != "Amount 1" ||
                                workSheet.Cells[1, 6].Value.ToString() != "Amount 2" ||
                                noOfCol > 7)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Format Upload Accounting Doc.";
                                return View("_Upload");
                            }
                            if (noOfRow == 2)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            var list = new List<SupplyLossDischarge>();
                            for (rowIterator = 3; rowIterator <= noOfRow; rowIterator++)
                            {
                                DateTime DocDate;
                                var CheckDate = DateTime.TryParse(workSheet.Cells[rowIterator, 1].Value.ToString(), out DocDate);
                                if (!CheckDate)
                                    DocDate = DateTime.FromOADate((double)workSheet.Cells[rowIterator, 1].Value);

                                var DocNo = workSheet.Cells[rowIterator, 2].Value.ToString().Trim();
                                var BLAsal = workSheet.Cells[rowIterator, 3].Value.ToString().Trim();
                                var AccountingDoc = workSheet.Cells[rowIterator, 4].Value.ToString().Trim();
                                var Amount1 = decimal.Parse(workSheet.Cells[rowIterator, 5].Value.ToString());
                                var Amount2 = decimal.Parse(workSheet.Cells[rowIterator, 6].Value.ToString());

                                var obj = _supplyLossDischargeService.Find(
                                    i => i.DocDate == DocDate &&
                                    i.DocNo == DocNo &&
                                    i.Roas.BlAsal == BLAsal && !i.Deleted).FirstOrDefault();

                                if (obj == null)
                                {
                                    ViewBag.Error = $"Data pada baris ke {rowIterator} tidak ditemukan.";
                                    return View("_Upload");
                                }

                                obj.AccountingDoc = AccountingDoc;
                                obj.Amount1 = Amount1;
                                obj.Amount2 = Amount2;

                                list.Add(obj);
                                //_supplyLossDischargeService.Save(obj);
                            }

                            _supplyLossDischargeService.UpdateRange(list);
                        }
                    }
                }
                return Content("success");
            }
            catch (DbEntityValidationException ex)
            {

                ViewBag.Error = $"baris ke {rowIterator} :";
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        ViewBag.Error += $"<br>{validationError.ErrorMessage}";
                        //Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }
                return View("_Upload");
            }
            //catch (DbUpdateException)
            //{
            //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
            //    return View("_Upload");
            //}
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Terdapat data kosong pada baris ke {rowIterator}";
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }
        }

        public ActionResult CreateEdit()
        {
            if (user.ActionCreate || user.ActionUpdate)
            {
                ViewBag.Create = user.ActionCreate;
                ViewBag.Edit = user.ActionUpdate;
                return View();
            }
            else
                return RedirectToAction("Index", "UnAuthorized");
        }

        [HttpPost]
        public ActionResult GetSupplyLossDischargeRoas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeProses, DateTime? TanggalBL, DateTime? TanggalBLSampai)
        //DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            if (TipeProses == "Edit")
            {
                var objEdit = _supplyLossDischargeService.SearchSupplyLossDischargeEdit(TanggalBL, TanggalBLSampai);

                var dataEdit = objEdit.Select(item => new
                {
                    Id = item.Id,
                    DocDate = item.DocDate,
                    DocNo = item.DocNo,
                    NoBl = item.Roas.BlAsal,
                    BLGross = item.BlGross,
                    ARGross = item.ARGross,
                    TanggalBongkar = item.TanggalBongkar,
                    AccountingDoc = item.AccountingDoc,
                    Amount1 = item.Amount1,
                    Amount2 = item.Amount2,
                });

                return Json(new DataTablesResponse(requestModel.Draw, dataEdit.ToList(), dataEdit.Count(), dataEdit.Count()), JsonRequestBehavior.AllowGet);
            }

            var obj = _roasService.GetRoasForSupplyLossDischarge();

            var data = obj.Select(item => new
            {
                Id = item.Id,
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                NoBl = item.BlAsal,
                BLGross = 0,
                ARGross = 0,
                TanggalBongkar = DateTime.Now,
                AccountingDoc = "",
                Amount1 = 0,
                Amount2 = 0,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), data.Count(), data.Count()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSupplyLossDischarge(List<SupplyLossDischarge> Item, string TipeProses)
        {
            try
            {

                if (TipeProses == "Baru")
                {
                    List<SupplyLossDischarge> SupplyLossDischargeList = new List<SupplyLossDischarge>();
                    //var DataSupplyLossDischarge = _supplyLossDischargeService.Find(i => !i.Deleted).ToList();
                    var DataSupplyLossDischarge = _supplyLossDischargeService.GetAllJoinData().ToList();


                    foreach (var item in Item)
                    {
                        SupplyLossDischarge CL = new SupplyLossDischarge();
                        Roas RoasObj = _roasService.Get(item.Id);
                        RoasObj.Port = _portService.Get(RoasObj.PortId);
                        CL.ROASId = item.Id;
                        CL.TanggalBongkar = item.TanggalBongkar;
                        CL.BlGross = item.BlGross;
                        CL.ARGross = item.ARGross;
                        CL.AccountingDoc = string.IsNullOrEmpty(item.AccountingDoc) ? "" : item.AccountingDoc;
                        CL.Amount1 = item.Amount1;
                        CL.Amount2 = item.Amount2;

                        CL.TransportType = RoasObj.TransportType;
                        CL.LoadingPortCode = RoasObj.Port.Code;
                        CL.LoadingPortDesc = RoasObj.Port.Description;
                        CL.DischargePortCode = "R201";
                        CL.DischargePortDesc = "RU II DUMAI";
                        CL.DocDate = RoasObj.DocDate;
                        CL.KdMaterial = RoasObj.KdMaterial;
                        CL.NamaProduk = RoasObj.NamaProduk;
                        CL.Uom = "BB6";
                        CL.DocNo = RoasObj.DocNo;
                        CL.BlDate = RoasObj.BlDate;
                        CL.BarrelsPtm = RoasObj.BarrelsPtm;
                        CL.BarrelsAl = RoasObj.BarrelsAl;
                        CL.R1 = RoasObj.BarrelsPtm - RoasObj.BarrelsAl;
                        CL.PercR1 = CL.R1 / CL.BarrelsPtm * 100;
                        CL.BarrelsBd = RoasObj.BarrelsBd;
                        CL.R2 = CL.BarrelsAl - CL.BarrelsBd;
                        CL.PercR2 = CL.R2 / CL.BarrelsPtm * 100;
                        CL.Barrels = RoasObj.Barrels;
                        CL.R3 = CL.BarrelsBd - CL.Barrels;
                        CL.PercR3 = CL.R3 / CL.BarrelsPtm * 100;
                        CL.R4Gross = CL.BlGross - CL.ARGross;
                        CL.R4Nett = CL.BarrelsPtm - CL.Barrels;
                        CL.PercNett = CL.R4Nett / CL.BarrelsPtm * 100;

                        CL.ShipmentNumber = RoasObj.ShipmentNumber;
                        CL.TransportName = RoasObj.TransportName;
                        CL.SapMaterialDoc = RoasObj.SAPMaterialDoc;
                        CL.Deleted = false;
                        CL.Roas = RoasObj;

                        var check = SupplyLossDischargeList.FindIndex(i => i.Roas.DocDate == RoasObj.DocDate && i.Roas.DocNo == RoasObj.DocNo && i.Roas.BlAsal == RoasObj.BlAsal && !i.Deleted);
                        if (check > -1)
                        {

                            SupplyLossDischargeList[check].Deleted = true;
                        }

                        SupplyLossDischargeList.Add(CL);

                    }


                    _supplyLossDischargeService.AddRange(SupplyLossDischargeList);

                    foreach (var item in SupplyLossDischargeList.Where(i => !i.Deleted))
                    {
                        var check = DataSupplyLossDischarge.FindIndex(i => i.Roas.DocDate == item.Roas.DocDate && i.Roas.DocNo == item.Roas.DocNo && i.Roas.BlAsal == item.Roas.BlAsal && !i.Deleted);
                        if (check > -1)
                        {
                            DataSupplyLossDischarge[check].Deleted = true;
                        }
                    }

                    _supplyLossDischargeService.UpdateRange(DataSupplyLossDischarge);

                }
                else if (TipeProses == "Edit")
                {
                    var List = new List<SupplyLossDischarge>();
                    foreach (var item in Item)
                    {
                        var SupplyLossDischarge = _supplyLossDischargeService.Get(item.Id);
                        SupplyLossDischarge.TanggalBongkar = item.TanggalBongkar;
                        SupplyLossDischarge.BlGross = item.BlGross;
                        SupplyLossDischarge.ARGross = item.ARGross;
                        SupplyLossDischarge.AccountingDoc = item.AccountingDoc;
                        SupplyLossDischarge.Amount1 = item.Amount1;
                        SupplyLossDischarge.Amount2 = item.Amount2;

                        SupplyLossDischarge.R4Gross = SupplyLossDischarge.BlGross - SupplyLossDischarge.ARGross;

                        List.Add(SupplyLossDischarge);
                        //_supplyLossDischargeService.Save(SupplyLossDischarge);
                    }
                    _supplyLossDischargeService.UpdateRange(List);
                }

                return Content("Success");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }
        }

    }
}