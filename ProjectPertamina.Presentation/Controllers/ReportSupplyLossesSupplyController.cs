﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportSupplyLossesSupplyController : Controller
    {
        private ISupplyLossSupplyService _supplyLossesSupplyService;
        public ReportSupplyLossesSupplyController(ISupplyLossSupplyService SuppplyLossesSupplyService)
        {
            _supplyLossesSupplyService = SuppplyLossesSupplyService;
        }
        // GET: ReportSupplyLossesSupply
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult barchart(string AlatAngkut, string DischargePort, string Material,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossesSupplyService.getChart(DischargePort, Material, AlatAngkut, TanggalBL, TanggalBLSampai,
                TanggalBongkar, TanggalBongkarSampai);
            
            var barChartModel = new ChartModel<decimal>
            {
                Title = "Supply Loss Supply",
                //Subtitle = "Cargo Losses",
                XAxisCategories = obj.Item1,
                XAxisTitle = "",
                YAxisTitle = "Values",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "R1", data = obj.Item2},
                    new SeriesModel<decimal>{ name = "% R1", data = obj.Item3}
                }
            };
            return Json(barChartModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string AlatAngkut, string DischargePort,
            string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossesSupplyService.SearchSupplyLossSupply(requestModel.Start, requestModel.Length,
                AlatAngkut, DischargePort, Material, "", TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                TipeTransportasi = item.TransportType,
                LoadingPort = item.LoadingPortCode,
                LoadingPortDesc = item.LoadingPortDesc,
                DischargePort = item.DischargePortCode,
                DischargePortDesc = item.DischargePortDesc,
                TanggalBongkar = item.TanggalBongkar,
                PostingDate = item.DocDate,
                MaterialNo = item.KdMaterial
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportSupplyLossesSupplyViewModel input)
        {
            var listLaporan = _supplyLossesSupplyService.queryExportToExcel(input.AlatAngkut, input.DischargePort, input.Material, input.TanggalBL, input.TanggalBLSampai, input.TanggalBongkar, input.TanggalBongkarSampai);

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanSupplyLosses(Supply)");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = "No.";
                workSheet.Cells[1, 2].Value = "Mode of Transport (Alat Angkut)";
                workSheet.Cells[1, 3].Value = "Loading Port (Plant Code)";
                workSheet.Cells[1, 4].Value = "Loading Port (Description)";
                workSheet.Cells[1, 5].Value = "Discharge Port/ Tujuan (Plant Code)";
                workSheet.Cells[1, 6].Value = "Discharge Port (Description)";
                workSheet.Cells[1, 7].Value = "Tanggal Bongkar";
                workSheet.Cells[1, 8].Value = "Posting Date";
                workSheet.Cells[1, 9].Value = "Material No. (SAP)";
                workSheet.Cells[1, 10].Value = "Material Desc. (SAP)";
                workSheet.Cells[1, 11].Value = "UoM (SAP)";
                workSheet.Cells[1, 12].Value = "NO. BL";
                workSheet.Cells[1, 13].Value = "Tanggal BL";
                workSheet.Cells[1, 14].Value = "BL";
                workSheet.Cells[1, 15].Value = "SFAL";
                workSheet.Cells[1, 16].Value = "R1";
                workSheet.Cells[1, 17].Value = "%";
                workSheet.Cells[1, 18].Value = "No KN (BL Original)";
                workSheet.Cells[1, 19].Value = "Shipment No.";
                workSheet.Cells[1, 20].Value = "Nama Kapal";
                workSheet.Cells[1, 21].Value = "Keterangan";


                int recordIndex = 2;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.TransportType;
                    workSheet.Cells[recordIndex, 3].Value = item.LoadingPortCode;
                    workSheet.Cells[recordIndex, 4].Value = item.LoadingPortDesc;
                    workSheet.Cells[recordIndex, 5].Value = item.DischargePortCode;
                    workSheet.Cells[recordIndex, 6].Value = item.DischargePortDesc;
                    workSheet.Cells[recordIndex, 7].Value = item.TanggalBongkar;
                    workSheet.Cells[recordIndex, 8].Value = item.DocDate;
                    workSheet.Cells[recordIndex, 9].Value = item.KdMaterial;
                    workSheet.Cells[recordIndex, 10].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 11].Value = item.Uom;
                    workSheet.Cells[recordIndex, 12].Value = item.DocNo;
                    workSheet.Cells[recordIndex, 13].Value = item.BlDate;
                    workSheet.Cells[recordIndex, 14].Value = item.BarrelsPtm;
                    workSheet.Cells[recordIndex, 15].Value = item.SFAL;
                    workSheet.Cells[recordIndex, 16].Value = item.R1;
                    workSheet.Cells[recordIndex, 17].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 18].Value = "";
                    workSheet.Cells[recordIndex, 19].Value = item.ShipmentNumber;
                    workSheet.Cells[recordIndex, 20].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 21].Value = "";

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 22; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 21].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 21].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 21].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 21].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 21])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanSupplyLosses(Supply)" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }
    }
}