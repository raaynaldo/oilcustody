﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class NilaiController : Controller
    {
        private INilaiService _nilaiService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public NilaiController(INilaiService NilaiService)
        {
            _nilaiService = NilaiService;
        }
        // GET: Nilai
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;
            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Code, string Desc)
        {
            var obj = _nilaiService.SearchNilai(requestModel.Start, requestModel.Length, Code, Desc);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Code = camp.Code,
                Desc = camp.Description,
                Value = camp.Value
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var nilai = _nilaiService.Get(id);
            ViewBag.Title = "EDIT";
            if (nilai == null)
            {
                nilai = new Nilai();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", nilai);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Nilai model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                model.Code = model.Code.Trim();
                _nilaiService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Code", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }



        public ActionResult Delete(int Id)
        {
            var noTelp = _nilaiService.Get(Id);
            return PartialView("_Delete", noTelp);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _nilaiService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var cargo = _nilaiService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", cargo);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
            catch (Exception ex)
            {
                var cargo = _nilaiService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
        }
    }
}