﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportKKSupplyLossController : Controller
    {
        private IKKSupplyLossService _kkSupplyService;
        private ISupplyLossDischargeService _supplyLossDischargeService;
        public ReportKKSupplyLossController(IKKSupplyLossService KKSupplyService, ISupplyLossDischargeService SupplyLossDischargeService)
        {
            _kkSupplyService = KKSupplyService;
            _supplyLossDischargeService = SupplyLossDischargeService;
        }
        // GET: ReportKKSupplyLoss
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult barchart(string LoadingPort, string Produk, string Vessel,
        DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            var obj = _kkSupplyService.getChart(LoadingPort, Produk, Vessel, TanggalBL, TanggalBLSampai,
                TanggalCQD, TanggalCQDSampai);

            string[] XAxisPipa = obj.Item1.Select(x => x.BlDate.ToString("dd MMM yyyy")).ToArray();
            string [] TransportNamePipa = obj.Item1.Select(x => x.TransportName).ToArray();
            for (int i = 0; i < XAxisPipa.Count(); i ++)
            {
                XAxisPipa[i] = Convert.ToString(XAxisPipa[i]) + " <br /> " + Convert.ToString(TransportNamePipa[i]);
            }

            string[] XAxisVessel = obj.Item2.Select(x => x.BlDate.ToString("dd MMM yyyy")).ToArray();
            string[] TransportNameVessel = obj.Item2.Select(x => x.TransportName).ToArray();
            for (int i = 0; i < XAxisVessel.Count(); i++)
            {
                XAxisVessel[i] = Convert.ToString(XAxisVessel[i]) + " <br /> " + Convert.ToString(TransportNameVessel[i]);
            }

            var barChartModel1 = new ChartModel<decimal>
            {
                
                Title = "Laporan KK Supply Loss Via Pipa",
                //Subtitle = "Cargo Losses",
                XAxisCategories = XAxisPipa,
                XAxisTitle = "",
                YAxisTitle = "Values",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "BS & W B/L", data = obj.Item1.Select(x => x.D1QualityBl).ToArray()},
                    new SeriesModel<decimal>{ name = "BS & W AR Tanki", data = obj.Item1.Select(x => x.D1QualitiTanki).ToArray()}
                }
            };

            var barChartModel2 = new ChartModel<decimal>
            {

                Title = "Laporan KK Supply Loss Via Vessel",
                //Subtitle = "Cargo Losses",
                XAxisCategories = XAxisVessel,
                XAxisTitle = "",
                YAxisTitle = "Values",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "OBQ Load Port", data = obj.Item2.Select(x => x.D2ObqAwal).ToArray()},
                    new SeriesModel<decimal>{ name = "OBQ Disch Port", data = obj.Item2.Select(x => x.D2ObqAwal).ToArray()},
                    new SeriesModel<decimal>{ name = "ROB Disch Port", data = obj.Item2.Select(x => x.D2BarrelsRob).ToArray()}
                }
            };
            return Json(new { chart1 = barChartModel1, chart2 = barChartModel2 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string LoadingPort, string Produk,
           string Vessel, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            var obj = _kkSupplyService.SearchKertasKerjaSupplyLossReport(requestModel.Start, requestModel.Length,
                LoadingPort, Produk, Vessel, TanggalBL, TanggalBLSampai, TanggalCQD, TanggalCQDSampai);

            var data = obj.Item1.Select(item => new
            {
                Produk = item.NamaProduk,
                Vessel = item.TransportName,
                BLDate = item.BlDate,
                CQDDate = item.DocDate,
                LoadPort = item.LoadingPort,
                Dischport = item.DischargePort,
                PoMySAP = item.PONumber,
                DONumberInbound = item.DelOrderNumber
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportKKSuplyLossViewModel input)
        {
            var listLaporan = _kkSupplyService.queryExportToExcel(input.LoadingPort, input.Produk, input.Vessel, input.TanggalBL, input.TanggalBLSampai, input.TanggalCQD, input.TanggalCQDSampai);

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanKKSupplyLosses");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                
                workSheet.Cells["A1:A3"].Merge = true;
                workSheet.Cells["A1:A3"].Value = "No.";
                workSheet.Cells["B1:B3"].Merge = true;
                workSheet.Cells["B1:B3"].Value = "Produk";
                workSheet.Cells["C1:C3"].Merge = true;
                workSheet.Cells["C1:C3"].Value = "Vessel.";
                workSheet.Cells["D1:D3"].Merge = true;
                workSheet.Cells["D1:D3"].Value = "B/L Date";
                workSheet.Cells["E1:E3"].Merge = true;
                workSheet.Cells["E1:E3"].Value = "CQD Date";
                workSheet.Cells["F1:F3"].Merge = true;
                workSheet.Cells["F1:F3"].Value = "Loadport";
                workSheet.Cells["G1:G3"].Merge = true;
                workSheet.Cells["G1:G3"].Value = "Dischport";
                workSheet.Cells["H1:H3"].Merge = true;
                workSheet.Cells["H1:H3"].Value = "PO MySAP";
                workSheet.Cells["I1:I3"].Merge = true;
                workSheet.Cells["I1:I3"].Value = "DO Number / INBOUND";
                workSheet.Cells["J1:J3"].Merge = true;
                workSheet.Cells["J1:J3"].Value = "Shipment Number";
                workSheet.Cells["K1:L2"].Merge = true;
                workSheet.Cells["K1:L2"].Value = "Bill of Lading (B/L)";
                workSheet.Cells["K3"].Value = "Gross";
                workSheet.Cells["L3"].Value = "Nett";
                workSheet.Cells["M1:M2"].Merge = true;
                workSheet.Cells["M1:M2"].Value = "SFAL";
                workSheet.Cells["M3"].Value = "Gross";
                workSheet.Cells["N1:N2"].Merge = true;
                workSheet.Cells["N1:N2"].Value = "SFBD";
                workSheet.Cells["N3"].Value = "Gross";
                workSheet.Cells["O1:P2"].Merge = true;
                workSheet.Cells["O1:P2"].Value = "AR";
                workSheet.Cells["O3"].Value = "Gross";
                workSheet.Cells["P3"].Value = "Nett";
                workSheet.Cells["Q1:R1"].Merge = true;
                workSheet.Cells["Q1:R1"].Value = "R1";
                workSheet.Cells["Q2:R2"].Merge = true;
                workSheet.Cells["Q2:R2"].Value = "Gross";
                workSheet.Cells["Q3"].Value = "Selisih (bbls)";
                workSheet.Cells["R3"].Value = "%";
                workSheet.Cells["S1:T1"].Merge = true;
                workSheet.Cells["S1:T1"].Value = "R2";
                workSheet.Cells["S2:T2"].Merge = true;
                workSheet.Cells["S2:T2"].Value = "Gross";
                workSheet.Cells["S3"].Value = "Selisih (bbls)";
                workSheet.Cells["T3"].Value = "%";
                workSheet.Cells["U1:V1"].Merge = true;
                workSheet.Cells["U1:V1"].Value = "R3";
                workSheet.Cells["U2:V2"].Merge = true;
                workSheet.Cells["U2:V2"].Value = "Gross";
                workSheet.Cells["U3"].Value = "Selisih (bbls)";
                workSheet.Cells["V3"].Value = "%";
                workSheet.Cells["W1:Z1"].Merge = true;
                workSheet.Cells["W1:Z1"].Value = "R4";
                workSheet.Cells["W2:X2"].Merge = true;
                workSheet.Cells["W2:X2"].Value = "Gross";
                workSheet.Cells["Y2:Z2"].Merge = true;
                workSheet.Cells["Y2:Z2"].Value = "Nett";
                workSheet.Cells["W3"].Value = "Selisih (bbls)";
                workSheet.Cells["X3"].Value = "%";
                workSheet.Cells["Y3"].Value = "Selisih (bbls)";
                workSheet.Cells["Z3"].Value = "%";
                workSheet.Cells["AA1:AB1"].Merge = true;
                workSheet.Cells["AA1:AB1"].Value = "OBQ";
                workSheet.Cells["AA2"].Value = "AWAL";
                workSheet.Cells["AB2"].Value = "AKHIR";
                workSheet.Cells["AA3"].Value = "Loading Port";
                workSheet.Cells["AB3"].Value = "Discharging Port";
                workSheet.Cells["AC1:AC2"].Merge = true;
                workSheet.Cells["AC1:AC2"].Value = "ROB";
                workSheet.Cells["AC3"].Value = "Discharging Port";
                workSheet.Cells["AD1:AH1"].Merge = true;
                workSheet.Cells["AD1:AH1"].Value = "Quality";
                workSheet.Cells["AD2:AH2"].Merge = true;
                workSheet.Cells["AD2:AH2"].Value = "Basic Sediment & Water (BS&W)";
                workSheet.Cells["AD3"].Value = "B/L (COQ)";
                workSheet.Cells["AE3"].Value = "Master Sample";
                workSheet.Cells["AF3"].Value = "Ship Comp Aft Loading";
                workSheet.Cells["AG3"].Value = "Ship Comp Bef Disch";
                workSheet.Cells["AH3"].Value = "Tanki";
                workSheet.Cells["AI1:AK2"].Merge = true;
                workSheet.Cells["AI1:AK2"].Value = "Free Water";
                workSheet.Cells["AI3"].Value = "SFAL";
                workSheet.Cells["AJ3"].Value = "SFBD";
                workSheet.Cells["AK3"].Value = "AR";

                int recordIndex = 4;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 3].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 4].Value = item.BlDate;
                    workSheet.Cells[recordIndex, 5].Value = item.DocDate;
                    workSheet.Cells[recordIndex, 6].Value = item.LoadingPort;
                    workSheet.Cells[recordIndex, 7].Value = item.DischargePort;
                    workSheet.Cells[recordIndex, 8].Value = item.PONumber;
                    workSheet.Cells[recordIndex, 9].Value = item.DelOrderNumber;
                    workSheet.Cells[recordIndex, 10].Value = item.ShipmentNumber;
                    workSheet.Cells[recordIndex, 11].Value = item.BlGross;
                    workSheet.Cells[recordIndex, 12].Value = item.BarrelsPtm;
                    workSheet.Cells[recordIndex, 13].Value = item.BarrelsAl;
                    workSheet.Cells[recordIndex, 14].Value = item.BarrelsBd;
                    workSheet.Cells[recordIndex, 15].Value = item.ArGross;
                    workSheet.Cells[recordIndex, 16].Value = item.Barrels;
                    workSheet.Cells[recordIndex, 17].Value = item.R1;
                    workSheet.Cells[recordIndex, 18].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 19].Value = item.R2;
                    workSheet.Cells[recordIndex, 20].Value = item.PercR2;
                    workSheet.Cells[recordIndex, 21].Value = item.R3;
                    workSheet.Cells[recordIndex, 22].Value = item.PercR3;
                    workSheet.Cells[recordIndex, 23].Value = item.R4Gross;
                    workSheet.Cells[recordIndex, 24].Value = item.PercGross;
                    workSheet.Cells[recordIndex, 25].Value = item.R4Nett;
                    workSheet.Cells[recordIndex, 26].Value = item.PercNett;
                    workSheet.Cells[recordIndex, 27].Value = item.ObqAwal;
                    workSheet.Cells[recordIndex, 28].Value = item.ObqAkhir;
                    workSheet.Cells[recordIndex, 29].Value = item.BarrelsRob;
                    workSheet.Cells[recordIndex, 30].Value = item.QualityBl;
                    workSheet.Cells[recordIndex, 31].Value = item.QualitySample;
                    workSheet.Cells[recordIndex, 32].Value = item.QualityAft;
                    workSheet.Cells[recordIndex, 33].Value = item.QualityBef;
                    workSheet.Cells[recordIndex, 34].Value = item.QualityTanki;
                    workSheet.Cells[recordIndex, 35].Value = item.FreeWaterSfal;
                    workSheet.Cells[recordIndex, 36].Value = item.FreeWaterSfbd;
                    workSheet.Cells[recordIndex, 37].Value = item.FreeWaterAr;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 38; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 37].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 37].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 37].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 37].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 37])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanKKSupplyLosses" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }
    }
}