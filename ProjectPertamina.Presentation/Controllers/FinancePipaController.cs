﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class FinancePipaController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IFinancePipaService _financePipaService;
        private IFinancePipaDetailService _financePipaDetailService;
        private IShiftService _shiftService;
        private IPICKeuanganService _picKeuanganService;
        private ITangkiService _tangkiService;
        private ICargoService _cargoService;


        public FinancePipaController(IFinancePipaService FinancePipaService, IFinancePipaDetailService FinancePipaDetailService, IShiftService Shift, IPICKeuanganService PICKeuangan, ITangkiService Tangki, ICargoService Cargo)
        {
            _financePipaService = FinancePipaService;
            _financePipaDetailService = FinancePipaDetailService;
            _shiftService = Shift;
            _picKeuanganService = PICKeuangan;
            _tangkiService = Tangki;
            _cargoService = Cargo;
        }
        // GET: FinancePipa
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "All", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "All", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            return View();
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? TanggalDari, DateTime? TanggalSampai, int Shift = 0, int PICKeuangan = 0)
        {
            var obj = _financePipaService.SearchFinancePipa(requestModel.Start, requestModel.Length, TanggalDari, TanggalSampai, PICKeuangan, Shift);

            var data = obj.Item1.Select(fP => new
            {
                Id = fP.Id,
                Tanggal = fP.Tanggal,
                Shift = fP.Shift.Description,
                PICKeuangan = fP.PICKeuangan.Nama,
                JumlahTangki = fP.JumlahTangki,
                NamaCargo = fP.Cargo.Description,
                Quantity = fP.Qty,
                EstimasiStart = fP.EstimasiStart,
                EstimasiStop = fP.EstimasiStop
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexDetail(int Id = 0)
        {
            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            var tangki = _tangkiService.GetAll().ToList();
            tangki.Insert(0, new Tangki() { Description = "", Id = 0 });
            ViewBag.tangki = tangki;

            var cargo = _cargoService.GetAll().ToList();
            cargo.Insert(0, new Cargo() { Description = "", Id = 0 });
            ViewBag.cargo = cargo;

            if (Id == 0)
            {
                ViewBag.Title = "ADD";
                return View(new FinancePipa());
            }
            else
            {
                ViewBag.Title = "EDIT";
                var financePipa = _financePipaService.Get(Id);
                financePipa.FinancePipaDetailSelect = financePipa.FinancePipaDetails.Select(i => i.TangkiId).ToList();

                return View(financePipa);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(FinancePipa model)
        {
            //if (model.JumlahTangki != model.FinancePipaDetailSelect.Count())
            //{
            //    ModelState.AddModelError("FinancePipaDetailSelect", "Nomor Tangki kurang dari Jumlah Tangki");
            //    return RedirectToAction("IndexDetail", model);
            //}
            if (!ModelState.IsValid)
            {
                var shift = _shiftService.GetAll().ToList();
                shift.Insert(0, new Shift() { Description = "", Id = 0 });
                ViewBag.shift = shift;

                var picKeuangan = _picKeuanganService.GetAll().ToList();
                picKeuangan.Insert(0, new PICKeuangan() { Nama = "", Id = 0 });
                ViewBag.picKeuangan = picKeuangan;

                var tangki = _tangkiService.GetAll().ToList();
                tangki.Insert(0, new Tangki() { Description = "", Id = 0 });
                ViewBag.tangki = tangki;

                var cargo = _cargoService.GetAll().ToList();
                cargo.Insert(0, new Cargo() { Description = "", Id = 0 });
                ViewBag.cargo = cargo;

                if (model.Id == 0)
                {
                    ViewBag.Title = "ADD";
                }
                else
                {
                    ViewBag.Title = "EDIT";
                }

                return View("IndexDetail", model);
            }
            try
            {
                _financePipaService.Save(model);
                IEnumerable<FinancePipaDetail> financedetail = _financePipaDetailService.Find(i => i.FinancePipaId == model.Id);
                _financePipaDetailService.DeleteRange(financedetail);

                foreach (var item in model.FinancePipaDetailSelect)
                {
                    _financePipaDetailService.Save(new FinancePipaDetail() { TangkiId = item, FinancePipaId = model.Id });
                }
                TempData["MessageSucces"] = "Data berhasil disimpan !";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return RedirectToAction("IndexDetail", model.Id);
            }

        }

        public ActionResult Delete(int Id)
        {
            var financePipa = _financePipaService.Get(Id);

            return PartialView("_Delete", financePipa);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                var berita = _financePipaService.Get(id);

                _financePipaService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}