﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportSupplyLossDischargeController : Controller
    {
        private ISupplyLossDischargeService _supplyLossDischargeService;
        public ReportSupplyLossDischargeController(ISupplyLossDischargeService SupplyLossDischargeService)
        {
            _supplyLossDischargeService = SupplyLossDischargeService;
        }
        // GET: ReportSupplyLossDischarge
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult barchart(string AlatAngkut, string LoadingPort, string Material,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossDischargeService.getChart(AlatAngkut, LoadingPort, Material, TanggalBL, TanggalBLSampai,
                TanggalBongkar, TanggalBongkarSampai);

            var barChartModel = new ChartModel<decimal>
            {
                Title = "Supply Loss Discharge",
                //Subtitle = "Cargo Losses",
                XAxisCategories = obj.Item1,
                XAxisTitle = "",
                YAxisTitle = "Values",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "Qty R4 Nett (BBls)", data = obj.Item2},
                    new SeriesModel<decimal>{ name = "% Nett", data = obj.Item3}
                }
            };
            return Json(barChartModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string AlatAngkut, string LoadingPort,
            string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var obj = _supplyLossDischargeService.SearchSupplyLossDischargeReport(requestModel.Start, requestModel.Length,
                AlatAngkut, LoadingPort, Material, "", TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);

            var data = obj.Item1.Select(item => new
            {
                TipeTransportasi = item.TransportType,
                LoadingPort = item.LoadingPortCode,
                LoadingPortDesc = item.LoadingPortDesc,
                DischargePort = item.DischargePortCode,
                DischargePortDesc = item.DischargePortDesc,
                TanggalBongkar = item.TanggalBongkar,
                PostingDate = item.DocDate,
                MaterialNo = item.KdMaterial
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportSupplyLossDischargeViewModel input)
        {
            var listLaporan = _supplyLossDischargeService.queryExportToExcel(input.AlatAngkut, input.LoadingPort, input.Material, input.TanggalBL, input.TanggalBLSampai, input.TanggalBongkar, input.TanggalBongkarSampai);

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanSupplyLosses(Discharge)");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = "No.";
                workSheet.Cells[1, 2].Value = "Mode of Transport (Alat Angkut)";
                workSheet.Cells[1, 3].Value = "Loading Port (Plant Code)";
                workSheet.Cells[1, 4].Value = "Loading Port (Description)";
                workSheet.Cells[1, 5].Value = "Discharge Port (Plant Code)";
                workSheet.Cells[1, 6].Value = "Discharge Port (Description)";
                workSheet.Cells[1, 7].Value = "Tanggal Bongkar";
                workSheet.Cells[1, 8].Value = "Posting Date";
                workSheet.Cells[1, 9].Value = "Material No. (SAP)";
                workSheet.Cells[1, 10].Value = "Material Desc. (SAP)";
                workSheet.Cells[1, 11].Value = "UoM (SAP)";
                workSheet.Cells[1, 12].Value = "NO. BL";
                workSheet.Cells[1, 13].Value = "Tanggal BL";
                workSheet.Cells[1, 14].Value = "BL GROSS";
                workSheet.Cells[1, 15].Value = "BL NETT ";
                workSheet.Cells[1, 16].Value = "SFAL ";
                workSheet.Cells[1, 17].Value = "R1";
                workSheet.Cells[1, 18].Value = "%";
                workSheet.Cells[1, 19].Value = "SFBD ";
                workSheet.Cells[1, 20].Value = "R2";
                workSheet.Cells[1, 21].Value = "%";
                workSheet.Cells[1, 22].Value = "AR GROSS";
                workSheet.Cells[1, 23].Value = "AR NETT";
                workSheet.Cells[1, 24].Value = "SFAD";
                workSheet.Cells[1, 25].Value = "New B/L (Bila ada)";
                workSheet.Cells[1, 26].Value = "R3";
                workSheet.Cells[1, 27].Value = "%";
                workSheet.Cells[1, 28].Value = "R4 GROSS";
                workSheet.Cells[1, 29].Value = "% GROSS";
                workSheet.Cells[1, 30].Value = "R4 NETT";
                workSheet.Cells[1, 31].Value = "% NETT";
                workSheet.Cells[1, 32].Value = "Shipment No.";
                workSheet.Cells[1, 33].Value = "Nama Kapal";
                workSheet.Cells[1, 34].Value = "Material Document (SAP)";
                workSheet.Cells[1, 35].Value = "Accounting Document (SAP)";
                workSheet.Cells[1, 36].Value = "Amount (USD)";


                int recordIndex = 2;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.TransportType;
                    workSheet.Cells[recordIndex, 3].Value = item.LoadingPortCode;
                    workSheet.Cells[recordIndex, 4].Value = item.LoadingPortDesc;
                    workSheet.Cells[recordIndex, 5].Value = item.DischargePortCode;
                    workSheet.Cells[recordIndex, 6].Value = item.DischargePortDesc;
                    workSheet.Cells[recordIndex, 7].Value = item.TanggalBongkar;
                    workSheet.Cells[recordIndex, 8].Value = item.DocDate;
                    workSheet.Cells[recordIndex, 9].Value = item.KdMaterial;
                    workSheet.Cells[recordIndex, 10].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 11].Value = item.Uom;
                    workSheet.Cells[recordIndex, 12].Value = item.DocNo;
                    workSheet.Cells[recordIndex, 13].Value = item.BlDate;
                    workSheet.Cells[recordIndex, 14].Value = item.BlGross;
                    workSheet.Cells[recordIndex, 15].Value = item.BarrelsPtm;
                    workSheet.Cells[recordIndex, 16].Value = item.BarrelsAl;
                    workSheet.Cells[recordIndex, 17].Value = item.R1;
                    workSheet.Cells[recordIndex, 18].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 19].Value = item.BarrelsBd;
                    workSheet.Cells[recordIndex, 20].Value = item.R2;
                    workSheet.Cells[recordIndex, 21].Value = item.PercR2;
                    workSheet.Cells[recordIndex, 22].Value = item.ARGross;
                    workSheet.Cells[recordIndex, 23].Value = item.Barrels;
                    workSheet.Cells[recordIndex, 24].Value = "";
                    workSheet.Cells[recordIndex, 25].Value = "";
                    workSheet.Cells[recordIndex, 26].Value = item.R3;
                    workSheet.Cells[recordIndex, 27].Value = item.PercR3;
                    workSheet.Cells[recordIndex, 28].Value = item.R4Gross;
                    workSheet.Cells[recordIndex, 29].Value = item.PercGross;
                    workSheet.Cells[recordIndex, 30].Value = item.R4Nett;
                    workSheet.Cells[recordIndex, 31].Value = item.PercNett;
                    workSheet.Cells[recordIndex, 32].Value = item.ShipmentNumber;
                    workSheet.Cells[recordIndex, 33].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 34].Value = item.SapMaterialDoc;
                    workSheet.Cells[recordIndex, 35].Value = item.AccountingDoc;
                    workSheet.Cells[recordIndex, 36].Value = item.Amount1;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 37; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 36].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 36].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 36].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 36].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 36])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanSupplyLosses(Discharge)" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }
    }
}