﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class Tabel52Controller : Controller
    {
        private ITabel52Service _tabel52Service;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public Tabel52Controller(ITabel52Service Tabel52Service)
        {
            _tabel52Service = Tabel52Service;
        }
        // GET: MasterTabel52
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, decimal Density, decimal Gallon, decimal Barrel)
        {
            var obj = _tabel52Service.SearchTabel52(requestModel.Start, requestModel.Length, Density, Gallon, Barrel);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Density = camp.Density15,
                Gallon = camp.UsGallon60F,
                Barrel = camp.Barrel60F
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            List<Tabel52> Tabel52List = new List<Tabel52>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        List<Tabel52> ListTabel52 = _tabel52Service.GetAll().ToList();
                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "Density 15" ||
                                workSheet.Cells[1, 2].Value.ToString() != "UsGallon60F" ||
                                workSheet.Cells[1, 3].Value.ToString() != "Barrel60F" ||
                                noOfCol > 3)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Table 52.";
                                return View("_Upload");
                            }
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new Tabel52();
                                obj.Density15 = decimal.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                                if (ListTabel52.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null || Tabel52List.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null)
                                {
                                    ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Density15 baris ke {rowIterator} gagal, karena data sudah ada.";
                                    if (rowIterator == 2)
                                        ViewBag.Error = $"Density15 baris ke {rowIterator} gagal, karena data sudah ada.<br>Tidak ada data yang terupload.";

                                    _tabel52Service.AddRange(Tabel52List);

                                    return View("_Upload");
                                }

                                obj.UsGallon60F = decimal.Parse(workSheet.Cells[rowIterator, 2].Value.ToString());
                                obj.Barrel60F = decimal.Parse(workSheet.Cells[rowIterator, 3].Value.ToString());

                                Tabel52List.Add(obj);
                            }

                            _tabel52Service.AddRange(Tabel52List);
                        }
                    }
                }
                return Content("success");
            }
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Terdapat data kosong pada baris ke {rowIterator}";
                _tabel52Service.AddRange(Tabel52List);
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"Input Gagal, tidak ada data yang diinput.";
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }

        }

        public ActionResult Create(int id = 0)
        {
            var tabel52 = _tabel52Service.Get(id);
            ViewBag.Title = "EDIT";
            if (tabel52 == null)
            {
                tabel52 = new Tabel52();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", tabel52);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Tabel52 model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                _tabel52Service.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Density15", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var tabel52 = _tabel52Service.Get(Id);
            return PartialView("_Delete", tabel52);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _tabel52Service.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var tabel52 = _tabel52Service.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", tabel52);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel52);
            }
            catch (Exception ex)
            {
                var tabel52 = _tabel52Service.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel52);
            }
        }

        public JsonResult GetCFBarrelFromTabel52(decimal sg, decimal density)
        {
            density = Math.Truncate(density * 1000) / 1000;

            Tabel52 cf = _tabel52Service.Find(i => i.Density15 == density).FirstOrDefault();
            if (cf == null)
                return Json(new { cf = "Master Tabel52 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            return Json(new { cf = cf.Barrel60F, val = true }, JsonRequestBehavior.AllowGet);
        }
    }
}