﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectPertamina.Presentation.Models.ViewModel.HigcharrtViewModel;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.ViewModels;
using NinjaNye.SearchExtensions;
using ProjectPertamina.Presentation.Common;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportRankController : Controller
    {
        private ISupplyLossDischargeService _supplyLossDischargeService;
        private ISupplyLossSupplyService _supplyLossesSupplyService;

        public ReportRankController(ISupplyLossDischargeService SupplyLossDischarge, ISupplyLossSupplyService SupplyLossSupply)
        {
            _supplyLossDischargeService = SupplyLossDischarge;
            _supplyLossesSupplyService = SupplyLossSupply;
        }
        // GET: ReportRank
        public ActionResult Index()
        {
            var vTransportType = getReportQuery(0, 0, "", "", "", null, null, false,"");
            var vddlTransportType = vTransportType.Item1.OrderBy(x => x.TransportType).GroupBy(x => x.TransportType);
            ViewBag.ddlTransportType = vddlTransportType;
            return View();
        }

        public JsonResult barchart(string TampilanR, string TipeSupplyLoss, string TransportType, string Material,
           DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            var obj = getReportQuery(0, 0, TipeSupplyLoss, TransportType, Material, TanggalBL, TanggalBLSampai, false, TampilanR);
            var qResulst = obj.Item1;
            List<ReportRankViewModel> chartSupply = new List<ReportRankViewModel>();

            if (TampilanR == "R1")
            {
                var countResult = qResulst.Count();
                if (countResult > 10)
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercR1).Take(10);
                }
                else
                {
                    qResulst =  qResulst.OrderByDescending(x => x.PercR1);
                }
                qResulst = qResulst.OrderBy(x => x.BlDate);
                chartSupply = qResulst.Select(y => new ReportRankViewModel
                {
                    TanggalXaxis = y.BlDate,
                    NilaiYaxis = y.R1
                }).ToList();
            }
            else if (TampilanR == "R2")
            {
                var countResult = qResulst.Count();
                if (countResult > 10)
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercR2).Take(10);
                }
                else
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercR2);
                }
                qResulst = qResulst.OrderBy(x => x.BlDate);
                chartSupply = qResulst.Select(y => new ReportRankViewModel
                {
                    TanggalXaxis = y.BlDate,
                    NilaiYaxis = y.R2
                }).ToList();

            }
            else if (TampilanR == "R3")
            {
                var countResult = qResulst.Count();
                if(countResult > 10 )
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercR3).Take(10);
                }
                else
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercR3);
                }
                qResulst = qResulst.OrderBy(x => x.BlDate);
                chartSupply = qResulst.Select(y => new ReportRankViewModel
                {
                    TanggalXaxis = y.BlDate,
                    NilaiYaxis = y.R3
                }).ToList();

            }
            else if (TampilanR == "R4Nett")
            {
                var countResult = qResulst.Count();
                if(countResult > 10)
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercNett).Take(10);
                }
                else
                {
                    qResulst = qResulst.OrderByDescending(x => x.PercNett);
                }
                qResulst = qResulst.OrderBy(x => x.BlDate);
                chartSupply = qResulst.Select(y => new ReportRankViewModel
                {
                    TanggalXaxis = y.BlDate,
                    NilaiYaxis = y.R4Nett
                }).ToList();

            };

            var Xaxis = chartSupply.Select(x => x.TanggalXaxis.ToString("dd MMM yyyy")).ToArray();
            var Yaxis = chartSupply.Select(x => x.NilaiYaxis).ToArray();


            var barChartModel = new ChartModel<decimal>
            {
                Title = "Nilai Rank Losses " + TampilanR + " Periode " + Xaxis[0] + " - " + Xaxis[(Xaxis.Count() - 1)],
                //Subtitle = "Cargo Losses",
                XAxisCategories = Xaxis,
                XAxisTitle = "",
                YAxisTitle = "Values",
                Series = new List<SeriesModel<decimal>> {
                    new SeriesModel<decimal>{ name = "Rank", data = Yaxis}
                }
            };
            return Json(barChartModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TampilanR, string TipeSupplyLoss, string TransportType, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            var obj = getReportQuery(requestModel.Start, requestModel.Length, TipeSupplyLoss, TransportType, Material, TanggalBL, TanggalBLSampai, true, TampilanR);

            var data1 = obj.Item1;
            var data = data1.Select(item => new
            {
                TipeSupplyLoss = item.TipeSupplyLoss,
                BLDate = item.BlDate,
                TransportName = item.TransportName,
                MatirialDesc = item.NamaProduk,
                R1 = item.R1,
                PercR1 = item.PercR1,
                R2 = item.R2,
                PercR2 = item.PercR2,
                R3 = item.R3,
                PercR3 = item.PercR3,
                R4 = item.R4Gross
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportExcel(ReportRankViewModel input)
        {
            var getReport = getReportQuery(0, 0, input.TipeSupplyLoss, input.TransportType, input.NamaProduk, input.TanggalBL, input.TanggalBLSampai, false, input.TampilR);
            var listLaporan = getReport.Item1;

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("Laporan_Rank");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells["A1:A2"].Merge = true;
                workSheet.Cells["A1:A2"].Value = "Tipe Supply Loss";
                workSheet.Cells["B1:B2"].Merge = true;
                workSheet.Cells["B1:B2"].Value = "Tanggal BL";
                workSheet.Cells["C1:C2"].Merge = true;
                workSheet.Cells["C1:C2"].Value = "Transport Name";
                workSheet.Cells["D1:D2"].Merge = true;
                workSheet.Cells["D1:D2"].Value = "Material Desc";
                workSheet.Cells["E1:F1"].Merge = true;
                workSheet.Cells["E1:F1"].Value = "R1";
                workSheet.Cells["E2"].Value = "Nilai";
                workSheet.Cells["F2"].Value = "%";
                workSheet.Cells["G1:H1"].Merge = true;
                workSheet.Cells["G1:H1"].Value = "R2";
                workSheet.Cells["G2"].Value = "Nilai";
                workSheet.Cells["H2"].Value = "%";
                workSheet.Cells["I1:J1"].Merge = true;
                workSheet.Cells["I1:J1"].Value = "R3";
                workSheet.Cells["I2"].Value = "Nilai";
                workSheet.Cells["J2"].Value = "%";
                workSheet.Cells["K1:L1"].Merge = true;
                workSheet.Cells["K1:L1"].Value = "R4 Gross";
                workSheet.Cells["K2"].Value = "Nilai";
                workSheet.Cells["L2"].Value = "%";
                workSheet.Cells["M1:N1"].Merge = true;
                workSheet.Cells["M1:N1"].Value = "R4 Nett";
                workSheet.Cells["M2"].Value = "Nilai";
                workSheet.Cells["N2"].Value = "%";


                int recordIndex = 3;
                int no = 1;
                foreach (var item in listLaporan)
                {

                    workSheet.Cells[recordIndex, 1].Value = item.TipeSupplyLoss;
                    workSheet.Cells[recordIndex, 2].Value = item.BlDate.ToString("dd-MM-yyyy");
                    workSheet.Cells[recordIndex, 3].Value = item.TransportName;
                    workSheet.Cells[recordIndex, 4].Value = item.NamaProduk;
                    workSheet.Cells[recordIndex, 5].Value = item.R1;
                    workSheet.Cells[recordIndex, 6].Value = item.PercR1;
                    workSheet.Cells[recordIndex, 7].Value = item.R2;
                    workSheet.Cells[recordIndex, 8].Value = item.PercR2;
                    workSheet.Cells[recordIndex, 9].Value = item.R3;
                    workSheet.Cells[recordIndex, 10].Value = item.PercR3;
                    workSheet.Cells[recordIndex, 11].Value = item.R4Gross;
                    workSheet.Cells[recordIndex, 12].Value = item.PercGross;
                    workSheet.Cells[recordIndex, 13].Value = item.R4Nett;
                    workSheet.Cells[recordIndex, 14].Value = item.PercNett;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 15; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 14])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "Laporan_Rank" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");
        }

        public Tuple<IEnumerable<ReportRankViewModel>, int, int> getReportQuery(int Skip, int length, string TipeSupplyLoss, string TransportType, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai, bool isDataTable, string TampilanR)
        {
            TransportType = TransportType == null ? "" : TransportType;
            Material = Material == null ? "" : Material;
            TipeSupplyLoss = TipeSupplyLoss == null ? "" : TipeSupplyLoss;
                   
            IEnumerable<ReportRankViewModel> UnionResult;

            if (TampilanR =="R1")
            {
                var query1 = _supplyLossesSupplyService.GetAllJoinData();
                var query2 = _supplyLossDischargeService.GetAllJoinData();

                UnionResult = query1.Select(c => new ReportRankViewModel
                    {
                        TipeSupplyLoss = "Supply",
                        BlDate = c.BlDate,
                        R1 = c.R1,
                        PercR1 = c.PercR1,
                        TransportType = c.TransportType,
                        R2 = Convert.ToDecimal(0),
                        PercR2 = Convert.ToDecimal(0),
                        R3 = Convert.ToDecimal(0),
                        PercR3 = Convert.ToDecimal(0),
                        R4Gross = Convert.ToDecimal(0),
                        PercGross = Convert.ToDecimal(0),
                        R4Nett = Convert.ToDecimal(0),
                        PercNett = Convert.ToDecimal(0),
                        TransportName = c.TransportName,
                        NamaProduk = c.NamaProduk

                    }).Union(
                   query2.Select(d => new ReportRankViewModel
                   {
                       TipeSupplyLoss = "Discharge",
                       BlDate = d.BlDate,
                       R1 = d.R1,
                       PercR1 = d.PercR1,
                       TransportType = d.TransportType,
                       R2 = d.R2,
                       PercR2 = d.PercR2,
                       R3 = d.R3,
                       PercR3 = d.PercR3,
                       R4Gross = d.R4Gross,
                       PercGross = d.PercGross,
                       R4Nett = d.R4Nett,
                       PercNett = d.PercNett,
                       TransportName = d.TransportName,
                       NamaProduk = d.NamaProduk
                   })
               );
            }
            else
            {
                var query2 = _supplyLossDischargeService.GetAllJoinData();
                UnionResult = query2.Select(d => new ReportRankViewModel
                {
                    TipeSupplyLoss = "Discharge",
                    BlDate = d.BlDate,
                    R1 = d.R1,
                    PercR1 = d.PercR1,
                    TransportType = d.TransportType,
                    R2 = d.R2,
                    PercR2 = d.PercR2,
                    R3 = d.R3,
                    PercR3 = d.PercR3,
                    R4Gross = d.R4Gross,
                    PercGross = d.PercGross,
                    R4Nett = d.R4Nett,
                    PercNett = d.PercNett,
                    TransportName = d.TransportName,
                    NamaProduk = d.NamaProduk
                });
            }

           

            var TotalCount = UnionResult.Count();
            UnionResult = UnionResult.Where(x => x.TransportType.ToLower().Contains(TransportType.ToLower()) &&
                            x.NamaProduk.ToLower().Contains(Material.ToLower()) &&
                            x.TipeSupplyLoss.ToLower().Contains(TipeSupplyLoss.ToLower()));

            if (TanggalBL != null)
                UnionResult = UnionResult.Where(i => i.BlDate >= TanggalBL);
            if (TanggalBLSampai != null)
                UnionResult = UnionResult.Where(i => i.BlDate <= TanggalBLSampai);

            var filteredCount = UnionResult.Count();

            if (isDataTable)
            {
                UnionResult = UnionResult.Skip(Skip).Take(length).ToList();
            }

            return new Tuple<IEnumerable<ReportRankViewModel>, int, int>(UnionResult, TotalCount, filteredCount);
        }
    }
}