﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class RoasController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IRoasService _roasService;
        private IPortService _portService;

        public RoasController(IRoasService RoasService, IPortService PortService)
        {
            _roasService = RoasService;
            _portService = PortService;
        }

        // GET: Roas
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var RoasList = _roasService.GetAll();

            var ViewModel = new RoasViewModel();
            ViewModel.KodeOpr = RoasList.Select(i => i.KodeOpr).Distinct();
            ViewModel.TipeTransport = RoasList.Select(i => i.TransportType).Distinct();
            ViewModel.Transport = RoasList.Select(i => i.TransportName).Distinct();
            ViewModel.AsalTujuan = RoasList.Select(i => i.AsalTujuan).Distinct();
            ViewModel.Produk = RoasList.Select(i => i.NamaProduk).Distinct();

            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string KodeOpr, string TipeTransport,
            string TransportName, string AsalTujuan, string NamaProduk, DateTime? TanggalDari, DateTime? TanggalSampai)
        {

            var obj = _roasService.SearchRoas(requestModel.Start, requestModel.Length, KodeOpr, TipeTransport, TransportName,
                AsalTujuan, NamaProduk, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(r => new Roas()
            {
                Id = r.Id,
                DocDate = r.DocDate,
                DocNo = r.DocNo,
                BlAsal = r.BlAsal,
                BlDate = r.BlDate,
                Plant = r.Plant,
                RefineryName = r.RefineryName,
                KodeProduk = r.KodeProduk,
                KdMaterial = r.KdMaterial,
                NamaProduk = r.NamaProduk,
                KodeFmlProduk = r.KodeFmlProduk,
                NamaFmlProduk = r.NamaFmlProduk,
                KodeOpr = r.KodeOpr,
                TransportType = r.TransportType,
                TransportName = r.TransportName,
                AsalTujuan = r.AsalTujuan,
                PauInfo = r.PauInfo,
                LiterObs = r.LiterObs,
                Liter15 = r.Liter15,
                Mton = r.Mton,
                Barrels = r.Barrels,
                Lton = r.Lton,
                LiterObsPtm = r.LiterObsPtm,
                Liter15Ptm = r.Liter15Ptm,
                MtonPtm = r.MtonPtm,
                BarrelsPtm = r.BarrelsPtm,
                LtonPtm = r.LtonPtm,
                LiterObsAl = r.LiterObsAl,
                Liter15Al = r.Liter15Al,
                MtonAl = r.MtonAl,
                BarrelsAl = r.BarrelsAl,
                LtonAl = r.LtonAl,
                LiterObsBd = r.LiterObsBd,
                Liter15Bd = r.Liter15Bd,
                MtonBd = r.MtonBd,
                BarrelsBd = r.BarrelsBd,
                LtonBd = r.LtonBd,
                LiterObsRob = r.LiterObsRob,
                MtonRob = r.MtonRob,
                BarrelsRob = r.BarrelsRob,
                LtonRob = r.LtonRob,
                PoNumber = r.PoNumber,
                DelOrderNumber = r.DelOrderNumber,
                ShipmentNumber = r.ShipmentNumber,
                PebNumber = r.PebNumber,
                SAPMaterialDoc = r.SAPMaterialDoc,
                Interface = r.Interface,
                RoasCreatedBy = r.RoasCreatedBy,
                RoasCreatedOn = r.RoasCreatedOn,
                RoasUpdatedBy = r.RoasUpdatedBy,
                RoasUpdatedOn = r.RoasUpdatedOn,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            List<Roas> RoasList = new List<Roas>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }

                            var PortList = _portService.GetAll().ToList();
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                string astuj = workSheet.Cells[rowIterator, 16].Value.ToString();
                                string a = astuj.Trim();
                                var port = PortList.Where(i => i.RoasAsalTujuan.Trim().ToLower() == astuj.Trim().ToLower()).FirstOrDefault();
                                if (port == null)
                                {
                                    _roasService.AddRange(RoasList);
                                    string Error = "";
                                    if (rowIterator > 2)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"Maaf Port <b>({ workSheet.Cells[rowIterator, 16].Value.ToString()})</b> tidak ditemukan";
                                    ViewBag.Error = Error;

                                    return View("_Upload");
                                }
                                var obj = new Roas();
                                obj.PortId = port.Id;
                                obj.No = Int32.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                                if (workSheet.Cells[rowIterator, 2].Value == null)
                                {
                                    _roasService.AddRange(RoasList);
                                    string Error = "";
                                    if (rowIterator > 2)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"doc_Date baris ke {rowIterator} tidak boleh kosong.";
                                    ViewBag.Error = Error;

                                    return View("_Upload");
                                }
                                obj.DocDate = (DateTime)workSheet.Cells[rowIterator, 2].Value;
                                if (workSheet.Cells[rowIterator, 3].Value == null)
                                {
                                    _roasService.AddRange(RoasList);
                                    string Error = "";
                                    if (rowIterator > 2)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"doc_No baris ke {rowIterator} tidak boleh kosong.";
                                    ViewBag.Error = Error;

                                    return View("_Upload");
                                }
                                obj.DocNo = workSheet.Cells[rowIterator, 3].Value.ToString();
                                obj.BlAsal = workSheet.Cells[rowIterator, 4].Value.ToString();
                                if (workSheet.Cells[rowIterator, 5].Value == null)
                                {
                                    _roasService.AddRange(RoasList);
                                    string Error = "";
                                    if (rowIterator > 2)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"bl_date baris ke {rowIterator} tidak boleh kosong.";
                                    ViewBag.Error = Error;

                                    return View("_Upload");
                                }
                                obj.BlDate = (DateTime)workSheet.Cells[rowIterator, 5].Value;
                                obj.Plant = workSheet.Cells[rowIterator, 6].Value.ToString();
                                obj.RefineryName = workSheet.Cells[rowIterator, 7].Value.ToString();
                                obj.KodeProduk = workSheet.Cells[rowIterator, 8].Value.ToString();
                                obj.NamaProduk = workSheet.Cells[rowIterator, 9].Value.ToString();
                                obj.KdMaterial = workSheet.Cells[rowIterator, 10].Value.ToString();
                                obj.KodeFmlProduk = workSheet.Cells[rowIterator, 11].Value.ToString();
                                obj.NamaFmlProduk = workSheet.Cells[rowIterator, 12].Value.ToString();
                                obj.KodeOpr = workSheet.Cells[rowIterator, 13].Value.ToString();
                                obj.TransportType = workSheet.Cells[rowIterator, 14].Value.ToString();
                                obj.TransportName = workSheet.Cells[rowIterator, 15].Value.ToString();
                                obj.AsalTujuan = workSheet.Cells[rowIterator, 16].Value.ToString();
                                obj.PauInfo = workSheet.Cells[rowIterator, 17].Value.ToString();
                                obj.LiterObs = decimal.Parse(workSheet.Cells[rowIterator, 18].Value.ToString());
                                obj.Liter15 = decimal.Parse(workSheet.Cells[rowIterator, 19].Value.ToString());
                                obj.Mton = decimal.Parse(workSheet.Cells[rowIterator, 20].Value.ToString());
                                obj.Barrels = decimal.Parse(workSheet.Cells[rowIterator, 21].Value.ToString());
                                obj.Lton = decimal.Parse(workSheet.Cells[rowIterator, 22].Value.ToString());
                                obj.LiterObsPtm = decimal.Parse(workSheet.Cells[rowIterator, 23].Value.ToString());
                                obj.Liter15Ptm = decimal.Parse(workSheet.Cells[rowIterator, 24].Value.ToString());
                                obj.MtonPtm = decimal.Parse(workSheet.Cells[rowIterator, 25].Value.ToString());
                                if (workSheet.Cells[rowIterator, 26].Value == null || workSheet.Cells[rowIterator, 26].Value == "0")
                                {
                                    _roasService.AddRange(RoasList);
                                    string Error = "";
                                    if (rowIterator > 2)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"BarrelsPtm baris ke { rowIterator} tidak boleh 0.";
                                    ViewBag.Error = Error;

                                    return View("_Upload");
                                }
                                obj.BarrelsPtm = decimal.Parse(workSheet.Cells[rowIterator, 26].Value.ToString());
                                obj.LtonPtm = decimal.Parse(workSheet.Cells[rowIterator, 27].Value.ToString());
                                obj.LiterObsAl = decimal.Parse(workSheet.Cells[rowIterator, 28].Value.ToString());
                                obj.Liter15Al = decimal.Parse(workSheet.Cells[rowIterator, 29].Value.ToString());
                                obj.MtonAl = decimal.Parse(workSheet.Cells[rowIterator, 30].Value.ToString());
                                //if (workSheet.Cells[rowIterator, 31].Value == null || workSheet.Cells[rowIterator, 31].Value == "0")
                                //{
                                //    _roasService.AddRange(RoasList);
                                //    string Error = "";
                                //    if (rowIterator > 2)
                                //        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                //    Error += $"BarrelsAl baris ke { rowIterator} tidak boleh 0.";
                                //    ViewBag.Error = Error;

                                //    return View("_Upload");
                                //}
                                obj.BarrelsAl = decimal.Parse(workSheet.Cells[rowIterator, 31].Value.ToString());
                                obj.LtonAl = decimal.Parse(workSheet.Cells[rowIterator, 32].Value.ToString());
                                obj.LiterObsBd = decimal.Parse(workSheet.Cells[rowIterator, 33].Value.ToString());
                                obj.Liter15Bd = decimal.Parse(workSheet.Cells[rowIterator, 34].Value.ToString());
                                obj.MtonBd = decimal.Parse(workSheet.Cells[rowIterator, 35].Value.ToString());
                                obj.BarrelsBd = decimal.Parse(workSheet.Cells[rowIterator, 36].Value.ToString());
                                obj.LtonBd = decimal.Parse(workSheet.Cells[rowIterator, 37].Value.ToString());
                                obj.LiterObsRob = decimal.Parse(workSheet.Cells[rowIterator, 38].Value.ToString());
                                obj.Liter15Rob = decimal.Parse(workSheet.Cells[rowIterator, 39].Value.ToString());
                                obj.MtonRob = decimal.Parse(workSheet.Cells[rowIterator, 40].Value.ToString());
                                obj.BarrelsRob = decimal.Parse(workSheet.Cells[rowIterator, 41].Value.ToString());
                                obj.LtonRob = decimal.Parse(workSheet.Cells[rowIterator, 42].Value.ToString());
                                obj.PoNumber = workSheet.Cells[rowIterator, 43].Value.ToString();
                                obj.DelOrderNumber = workSheet.Cells[rowIterator, 44].Value.ToString();
                                obj.ShipmentNumber = workSheet.Cells[rowIterator, 45].Value.ToString();
                                obj.PebNumber = workSheet.Cells[rowIterator, 46].Value.ToString();
                                obj.SAPMaterialDoc = workSheet.Cells[rowIterator, 47].Value.ToString();
                                obj.Interface = workSheet.Cells[rowIterator, 48].Value.ToString();
                                obj.RoasCreatedBy = workSheet.Cells[rowIterator, 49].Value.ToString();
                                if (workSheet.Cells[rowIterator, 50].Value.ToString().Trim() != "")
                                    obj.RoasCreatedOn = DateTime.Parse(workSheet.Cells[rowIterator, 50].Value.ToString());
                                obj.RoasUpdatedBy = workSheet.Cells[rowIterator, 51].Value.ToString();
                                if (workSheet.Cells[rowIterator, 52].Value.ToString().Trim() != "")
                                    obj.RoasCreatedOn = DateTime.Parse(workSheet.Cells[rowIterator, 52].Value.ToString());

                                RoasList.Add(obj);
                                //_roasService.Save(obj);
                                //_roasService.SaveRoas(obj);
                            }

                            //_roasService.RoasCommit();
                            _roasService.AddRange(RoasList);
                        }
                    }
                }
                return Content("success");
            }
            //catch (DbEntityValidationException ex)
            //{

            //    ViewBag.Error = $"baris ke {rowIterator} gagal :";
            //    foreach (var entityValidationErrors in ex.EntityValidationErrors)
            //    {
            //        foreach (var validationError in entityValidationErrors.ValidationErrors)
            //        {
            //            ViewBag.Error += $"<br>{validationError.ErrorMessage}";
            //        }
            //    }
            //    return View("_Upload");
            //}
            catch (Exception ex)
            {
                _roasService.AddRange(RoasList);
                string Error = "";
                if (rowIterator > 2)
                    Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                Error += $"Baris ke { rowIterator} gagal. <br>" + ex;
                ViewBag.Error = Error;

                return View("_Upload");
            }
        }

        public ActionResult Delete(int Id)
        {
            var roas = _roasService.Get(Id);
            return PartialView("_Delete", roas);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _roasService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var roas = _roasService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", roas);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", roas);
            }
            catch (Exception ex)
            {
                var roas = _roasService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", roas);
            }
        }
    }
}