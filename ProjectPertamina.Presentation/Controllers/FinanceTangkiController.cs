﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class FinanceTangkiController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IFinanceTangkiService _financeTangkiService;
        private IFinanceTangkiDetailService _financeTangkiDetailService;
        private IShiftService _shiftService;
        private IPICKeuanganService _picKeuanganService;
        private ITangkiService _tangkiService;
        private ICargoService _cargoService;


        public FinanceTangkiController(IFinanceTangkiService FinanceTangkiService, IFinanceTangkiDetailService FinanceTangkiDetailService, IShiftService Shift, IPICKeuanganService PICKeuangan, ITangkiService Tangki, ICargoService Cargo)
        {
            _financeTangkiService = FinanceTangkiService;
            _financeTangkiDetailService = FinanceTangkiDetailService;
            _shiftService = Shift;
            _picKeuanganService = PICKeuangan;
            _tangkiService = Tangki;
            _cargoService = Cargo;
        }
        // GET: FinanceTangki
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "All", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "All", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            var nomorTangki = _tangkiService.GetAll().ToList();
            nomorTangki.Insert(0, new Tangki() { Description = "All", Id = 0 });
            ViewBag.nomorTangki = nomorTangki;

            var namaCargo = _cargoService.GetAll().ToList();
            namaCargo.Insert(0, new Cargo() { Description = "All", Id = 0 });
            ViewBag.namaCargo = namaCargo;

            return View();
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? TanggalDari, DateTime? TanggalSampai, int PICKeuanganId = 0, int ShiftId = 0, int TangkiId = 0, int CargoId = 0)
        {
            var obj = _financeTangkiService.SearchFinanceTangki(requestModel.Start, requestModel.Length, TanggalDari, TanggalSampai, PICKeuanganId, ShiftId, TangkiId, CargoId);

            var data = obj.Item1.Select(fP => new
            {
                Id = fP.Id,
                Tanggal = fP.Tanggal,
                Shift = fP.Shift.Description,
                PICKeuangan = fP.PICKeuangan.Nama,
                NomorTangki = fP.Tangki.Description,
                NamaCargo = fP.Cargo.Description,
                Quantity = fP.Qty,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexDetail(int Id = 0)
        {
            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            var tangki = _tangkiService.GetAll().ToList();
            tangki.Insert(0, new Tangki() { Description = "", Id = 0 });
            ViewBag.tangki = tangki;

            var cargo = _cargoService.GetAll().ToList();
            cargo.Insert(0, new Cargo() { Description = "", Id = 0 });
            ViewBag.cargo = cargo;

            if (Id == 0)
            {
                ViewBag.Title = "ADD";
                return View(new FinanceTangki());
            }
            else
            {
                ViewBag.Title = "EDIT";
                var financeTangki = _financeTangkiService.Get(Id);
                return View(financeTangki);
            }
        }

        public ActionResult Delete(int Id)
        {
            var financeTangki = _financeTangkiService.Get(Id);

            return PartialView("_Delete", financeTangki);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                var berita = _financeTangkiService.Get(id);

                _financeTangkiService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult GetDetail([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int Id)
        {
            var obj = _financeTangkiDetailService.Find(i => i.FinanceTangkiId == Id);

            var data = obj.Select(fP => new
            {
                Id = fP.Id,
                NoPolisi = fP.NoPolisi,
                JumlahKL = fP.JumlahKl,
                NoDO = fP.NoDo,
                NoShipment = fP.NoShipment,
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), 0, 0), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(FinanceTangki model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("IndexDetail", model);
            try
            {
                _financeTangkiService.Save(model);
                TempData["MessageSucces"] = "Data berhasil disimpan !";

                return RedirectToAction("IndexDetail", new { Id = model.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return RedirectToAction("IndexDetail", model.Id);
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveDetail(FinanceTangkiDetail Model)
        {
            if (!ModelState.IsValid)
                return new HttpStatusCodeResult(500, "Model State Invalid");
            try
            {
                _financeTangkiDetailService.Save(Model);
                return Content("asd");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }
        }

        public ActionResult DeleteDetail(int Id)
        {
            var financeTangkiDetail = _financeTangkiDetailService.Get(Id);

            return PartialView("_DeleteDetail", financeTangkiDetail);
        }

        [HttpPost, ActionName("DeleteDetail")]
        public ActionResult DeleteDetailAction(int id)
        {
            try
            {
                _financeTangkiDetailService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }


    }
}