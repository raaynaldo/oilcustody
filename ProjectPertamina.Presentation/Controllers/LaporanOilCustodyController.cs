﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class LaporanOilCustodyController : Controller
    {
        private IShiftService _shiftService;
        private IPICKeuanganService _picKeuanganService;
        private IFinanceJettyDetailService _financeJettyDetailService;
        private IFinancePipaService _financePipaService;
        //private IFinanceJettyDetailService _financePipaDetailService;
        private IFinanceTangkiService _financeTangkiService;
        //private IFinanceTangkiDetailService _financeTangkiDetailService;



        public LaporanOilCustodyController(IShiftService ShiftService, IPICKeuanganService PICKeuanganService,
            IFinanceJettyDetailService FinanceJettyDetailService,
            IFinancePipaService FinancePipaService,
            IFinanceJettyDetailService FinancePipaDetailService,
            IFinanceTangkiService FinanceTangkiService,
            IFinanceTangkiDetailService FinanceTangkiDetailService
            )
        {
            _shiftService = ShiftService;
            _picKeuanganService = PICKeuanganService;
            _financeJettyDetailService = FinanceJettyDetailService;
            _financePipaService = FinancePipaService;
            _financeTangkiService = FinanceTangkiService;
        }

        // GET: LaporanOilCustody
        public ActionResult Index()
        {
            var model = new LaporanOilCustodyViewModel();

            model.Shifts = _shiftService.GetAll();
            model.PICKeuangans = _picKeuanganService.GetAll();

            return View(model);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? Tanggal, int ShiftId = 0, int PICKeuanganId = 0)
        {
            if (Tanggal == null && ShiftId == 0 && PICKeuanganId == 0)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<LaporanOilCustodyReportViewModel>(), 0, 0), JsonRequestBehavior.AllowGet);
            }

            #region temp
            //var listLaporan = new List<LaporanOilCustodyReportViewModel>();

            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            //{
            //    using (SqlCommand cmd = new SqlCommand("SP_LaporanOilCustody", con))
            //    {
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.Add("@tanggal", SqlDbType.DateTime).Value = Tanggal.Value.Date.ToString("yyyy-MM-dd");
            //        cmd.Parameters.Add("@Shift", SqlDbType.Int).Value = ShiftId;
            //        cmd.Parameters.Add("@PICKeuangan", SqlDbType.Int).Value = PICKeuanganId;

            //        con.Open();

            //        using (SqlDataReader oReader = cmd.ExecuteReader())
            //        {
            //            while (oReader.Read())
            //            {
            //                var obj = new LaporanOilCustodyReportViewModel();
            //                obj.Tanggal = DateTime.Parse(oReader["Tanggal"].ToString());
            //                obj.OnCall = oReader["OnCall/Standby"].ToString();
            //                obj.Status = oReader["Status"].ToString();
            //                obj.Nama = oReader["Nama"].ToString();
            //                obj.Aktivitas = oReader["Aktivitas"].ToString();
            //                obj.TipeAktivitas = oReader["TipeAktivitas"].ToString();
            //                listLaporan.Add(obj);
            //            }
            //            con.Close();
            //        }
            //    }
            //}
            #endregion

            #region  GetData

            var financeJettyDetail = _financeJettyDetailService.Find(i => i.FinanceJetty.Tanggal == Tanggal).ToList();
            var financePipa = _financePipaService.Find(i => i.Tanggal == Tanggal).ToList();
            var fincanceTangki = _financeTangkiService.Find(i => i.Tanggal == Tanggal).ToList();

            var financeJettyDetailNew = from list in financeJettyDetail
                                        select new
                                        {
                                            Tanggal = list.FinanceJetty.Tanggal,
                                            OnCall = "On Call",
                                            Shift = list.FinanceJetty.Shift,
                                            PIC = list.FinanceJetty.PICKeuangan,
                                            TipeAktivitas = "Jetty/Dermaga",
                                            order = 1,
                                            Aktivitas = $"{list.FinanceJetty.Jetty}; {list.Tangki.Description}; {list.Cargo.Description}; {list.FinanceJetty.Qty}; {list.FinanceJetty.RealisasiStart}; {list.FinanceJetty.PumpRate}; {list.FinanceJetty.RealisasiStop}",
                                            Catatan = list.FinanceJetty.Catatan
                                        };

            var financePipaNew = from list in financePipa
                                 select new
                                 {
                                     Tanggal = list.Tanggal,
                                     OnCall = "On Call",
                                     Shift = list.Shift,
                                     PIC = list.PICKeuangan,
                                     TipeAktivitas = "Pipa",
                                     order = 2,
                                     Aktivitas = $"{list.FromTo} {list.Cargo.Description} {list.Qty} {list.EstimasiStart} {list.EstimasiStop}",
                                     Catatan = list.Catatan
                                 };


            var financeTangkiNew = from list in fincanceTangki
                                   select new
                                   {
                                       Tanggal = list.Tanggal,
                                       OnCall = "On Call",
                                       Shift = list.Shift,
                                       PIC = list.PICKeuangan,
                                       TipeAktivitas = "Mobil Tangki",
                                       order = 3,
                                       Aktivitas = $"{list.FromTo} {list.Qty}",
                                       Catatan = list.Catatan
                                   };

            var ListLaporan = financeJettyDetailNew.Union(financePipaNew).Union(financeTangkiNew).ToList();

            if (ShiftId != 0)
                ListLaporan = ListLaporan.Where(i => i.Shift.Id == ShiftId).ToList();

            if (PICKeuanganId != 0)
                ListLaporan = ListLaporan.Where(i => i.PIC.Id == PICKeuanganId).ToList();

            ListLaporan = ListLaporan.OrderBy(i => i.Tanggal).ThenBy(i => i.order).ToList();

            #endregion

            int no = 1;
            var data = new List<LaporanOilCustodyReportViewModel>();
            foreach (var item in ListLaporan)
            {
                var LOCReport = new LaporanOilCustodyReportViewModel();
                LOCReport.No = no;
                LOCReport.Tanggal = item.Tanggal;
                LOCReport.OnCall = item.OnCall;
                LOCReport.Status = item.Shift.Description;
                LOCReport.Nama = item.PIC.Nama;
                LOCReport.TipeAktivitas = item.TipeAktivitas;
                LOCReport.Aktivitas = item.Aktivitas;
                LOCReport.Catatan = item.Catatan;
                data.Add(LOCReport);
                no++;
            }

            #region temp
            //var List = ListLaporan.GroupBy(x => new { x.Tanggal, x.PIC, x.OnCall, x.Shift })
            //.Select(grp => new LaporanOilCustodyGroup
            //{
            //    Nama = grp.Key.PIC.Nama,
            //    OnCall = grp.Key.OnCall,
            //    Tanggal = grp.Key.Tanggal,
            //    Status = grp.Key.Shift.Description,
            //    Aktivitas = grp.GroupBy(x => x.TipeAktivitas).Select(sgrp => new SubGroup
            //    {
            //        TipeAktivitas = sgrp.Key,
            //        Aktivitas = sgrp.Select(i => i.Aktivitas).ToList()
            //    }).ToList()
            //}).ToList();

            //int no = 1;
            //var data = new List<LaporanOilCustodyReportViewModel>();
            //foreach (var item in List)
            //{
            //    var LOCReport = new LaporanOilCustodyReportViewModel();
            //    LOCReport.No = no;
            //    LOCReport.Tanggal = item.Tanggal;
            //    LOCReport.OnCall = item.OnCall;
            //    LOCReport.Status = item.Status;
            //    LOCReport.Nama = item.Nama;
            //    for (int i = 0; i < item.Aktivitas.Count(); i++)
            //    {
            //        var LOCReportKosong = new LaporanOilCustodyReportViewModel();
            //        if (i == 0)
            //        {
            //            LOCReport.Aktivitas = item.Aktivitas[i].TipeAktivitas;
            //            data.Add(LOCReport);
            //        }
            //        else
            //        {
            //            LOCReportKosong = new LaporanOilCustodyReportViewModel();
            //            LOCReportKosong.Tanggal = null;
            //            LOCReportKosong.OnCall = "";
            //            LOCReportKosong.Status = "";
            //            LOCReportKosong.Aktivitas = item.Aktivitas[i].TipeAktivitas;
            //            data.Add(LOCReportKosong);
            //        }

            //        for (int j = 0; j < item.Aktivitas[i].Aktivitas.Count(); j++)
            //        {
            //            LOCReportKosong = new LaporanOilCustodyReportViewModel();
            //            LOCReportKosong.Tanggal = null;
            //            LOCReportKosong.OnCall = "";
            //            LOCReportKosong.Status = "";
            //            LOCReportKosong.Aktivitas = item.Aktivitas[i].Aktivitas[j];
            //            data.Add(LOCReportKosong);
            //        }
            //    }
            //    no++;
            #endregion

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), 0, 0), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get1([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? Tanggal, int ShiftId = 0, int PICKeuanganId = 0)
        {
            //if (Tanggal == null)
            //{
            //    return Json(new DataTablesResponse(requestModel.Draw, new List<LaporanOilCustodyReportViewModel>(), 0, 0), JsonRequestBehavior.AllowGet);
            //}

            var financeJettyDetail = _financeJettyDetailService.GetAll();
            var financePipa = _financePipaService.GetAll();
            var fincanceTangki = _financeTangkiService.GetAll();

            var financeJettyDetailNew = from list in financeJettyDetail
                                        select new
                                        {
                                            Tanggal = list.FinanceJetty.Tanggal,
                                            OnCall = "On Call",
                                            Shift = list.FinanceJetty.Shift,
                                            PIC = list.FinanceJetty.PICKeuangan,
                                            TipeAktivitas = "Jetty/Dermaga",
                                            order = 1,
                                            Aktivitas = $"{list.FinanceJetty.Jetty}; {list.Tangki.Description}; {list.Cargo.Description}; {list.FinanceJetty.Qty}; {list.FinanceJetty.RealisasiStart}; {list.FinanceJetty.PumpRate}; {list.FinanceJetty.RealisasiStop}"
                                        };

            var financePipaNew = from list in financePipa
                                 select new
                                 {
                                     Tanggal = list.Tanggal,
                                     OnCall = "On Call",
                                     Shift = list.Shift,
                                     PIC = list.PICKeuangan,
                                     TipeAktivitas = "Pipa",
                                     order = 2,
                                     Aktivitas = $"{list.FromTo} {list.Cargo.Description} {list.Qty} {list.EstimasiStart} {list.EstimasiStop}"
                                 };


            var financeTangkiNew = from list in fincanceTangki
                                   select new
                                   {
                                       Tanggal = list.Tanggal,
                                       OnCall = "On Call",
                                       Shift = list.Shift,
                                       PIC = list.PICKeuangan,
                                       TipeAktivitas = "Mobil Tangki",
                                       order = 3,
                                       Aktivitas = $"{list.FromTo} {list.Qty}"
                                   };

            var data = financeJettyDetailNew.Union(financePipaNew).Union(financeTangkiNew);
            data = data.OrderBy(i => i.Tanggal).ThenBy(i => i.order);


            var List = data.GroupBy(x => new { x.Tanggal, x.PIC, x.OnCall, x.Shift })
            .Select(grp => new LaporanOilCustodyGroup
            {
                Nama = grp.Key.PIC.Nama,
                OnCall = grp.Key.OnCall,
                Tanggal = grp.Key.Tanggal,
                Status = grp.Key.Shift.Description,
                Aktivitas = grp.GroupBy(x => x.TipeAktivitas).Select(sgrp => new SubGroup
                {
                    TipeAktivitas = sgrp.Key,
                    Aktivitas = sgrp.Select(i => i.Aktivitas).ToList()
                }).ToList()
            }).ToList();

            //return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);

            return null;
        }


        public ActionResult ExportExcel(DateTime Tanggal, int ShiftId = 0, int PICKeuanganId = 0)
        {
            //var tgl = Tanggal.Date.ToString("yyyy-MM-dd");
            var listLaporan = new List<LaporanOilCustodyReportViewModel>();

            #region temp
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            //{
            //    using (SqlCommand cmd = new SqlCommand("SP_LaporanOilCustody", con))
            //    {
            //        cmd.CommandType = CommandType.StoredProcedure;

            //        cmd.Parameters.Add("@tanggal", SqlDbType.DateTime).Value = tgl;
            //        cmd.Parameters.Add("@Shift", SqlDbType.Int).Value = ShiftId;
            //        cmd.Parameters.Add("@PICKeuangan", SqlDbType.Int).Value = PICKeuanganId;

            //        con.Open();

            //        using (SqlDataReader oReader = cmd.ExecuteReader())
            //        {
            //            while (oReader.Read())
            //            {
            //                var obj = new LaporanOilCustodyReportViewModel();
            //                obj.Tanggal = DateTime.Parse(oReader["Tanggal"].ToString());
            //                obj.OnCall = oReader["OnCall/Standby"].ToString();
            //                obj.Status = oReader["Status"].ToString();
            //                obj.Nama = oReader["Nama"].ToString();
            //                obj.Aktivitas = oReader["Aktivitas"].ToString();
            //                obj.TipeAktivitas = oReader["TipeAktivitas"].ToString();
            //                listLaporan.Add(obj);
            //            }
            //            con.Close();
            //        }
            //    }
            //}
            #endregion

            #region  GetData

            var financeJettyDetail = _financeJettyDetailService.Find(i => i.FinanceJetty.Tanggal == Tanggal).ToList();
            var financePipa = _financePipaService.Find(i => i.Tanggal == Tanggal).ToList();
            var fincanceTangki = _financeTangkiService.Find(i => i.Tanggal == Tanggal).ToList();

            var financeJettyDetailNew = from list in financeJettyDetail
                                        select new
                                        {
                                            Tanggal = list.FinanceJetty.Tanggal,
                                            OnCall = "On Call",
                                            Shift = list.FinanceJetty.Shift,
                                            PIC = list.FinanceJetty.PICKeuangan,
                                            TipeAktivitas = "Jetty/Dermaga",
                                            order = 1,
                                            Aktivitas = $"{list.FinanceJetty.Jetty}; {list.Tangki.Description}; {list.Cargo.Description}; {list.FinanceJetty.Qty}; {list.FinanceJetty.RealisasiStart}; {list.FinanceJetty.PumpRate}; {list.FinanceJetty.RealisasiStop}",
                                            Catatan = list.FinanceJetty.Catatan
                                        };

            var financePipaNew = from list in financePipa
                                 select new
                                 {
                                     Tanggal = list.Tanggal,
                                     OnCall = "On Call",
                                     Shift = list.Shift,
                                     PIC = list.PICKeuangan,
                                     TipeAktivitas = "Pipa",
                                     order = 2,
                                     Aktivitas = $"{list.FromTo} {list.Cargo.Description} {list.Qty} {list.EstimasiStart} {list.EstimasiStop}",
                                     Catatan = list.Catatan
                                 };


            var financeTangkiNew = from list in fincanceTangki
                                   select new
                                   {
                                       Tanggal = list.Tanggal,
                                       OnCall = "On Call",
                                       Shift = list.Shift,
                                       PIC = list.PICKeuangan,
                                       TipeAktivitas = "Mobil Tangki",
                                       order = 3,
                                       Aktivitas = $"{list.FromTo} {list.Qty}",
                                       Catatan = list.Catatan
                                   };

            var ListLaporan = financeJettyDetailNew.Union(financePipaNew).Union(financeTangkiNew).ToList();

            if (ShiftId != 0)
                ListLaporan = ListLaporan.Where(i => i.Shift.Id == ShiftId).ToList();

            if (PICKeuanganId != 0)
                ListLaporan = ListLaporan.Where(i => i.PIC.Id == PICKeuanganId).ToList();

            ListLaporan = ListLaporan.OrderBy(i => i.Tanggal).ThenBy(i => i.order).ToList();

            #endregion

            //var List = ListLaporan.GroupBy(x => new { x.Tanggal, x.PIC, x.OnCall, x.Shift })
            //.Select(grp => new LaporanOilCustodyGroup
            //{
            //    Nama = grp.Key.PIC.Nama,
            //    OnCall = grp.Key.OnCall,
            //    Tanggal = grp.Key.Tanggal,
            //    Status = grp.Key.Shift.Description,
            //    Aktivitas = grp.GroupBy(x => x.TipeAktivitas).Select(sgrp => new SubGroup
            //    {
            //        TipeAktivitas = sgrp.Key,
            //        Aktivitas = sgrp.Select(i => i.Aktivitas).ToList()
            //    }).ToList()
            //}).ToList();


            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("LaporanOilCustody");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  
                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;
                workSheet.Cells[1, 1].Value = "No";
                workSheet.Cells[1, 2].Value = "Tanggal";
                workSheet.Cells[1, 3].Value = "onCall/Standby";
                workSheet.Cells[1, 4].Value = "Status";
                workSheet.Cells[1, 5].Value = "Nama";
                workSheet.Cells[1, 6].Value = "Tipe Aktivitas";
                workSheet.Cells[1, 7].Value = "Aktivitas";
                workSheet.Cells[1, 8].Value = "Catatan";

                //Body of table  

                //int recordIndex = 2;
                //int no = 1;
                //foreach (var item in List)
                //{
                //    workSheet.Cells[recordIndex, 1].Value = no;
                //    workSheet.Cells[recordIndex, 2].Value = item.Tanggal.ToString("dd MMM yyyy");
                //    workSheet.Cells[recordIndex, 3].Value = item.OnCall;
                //    workSheet.Cells[recordIndex, 4].Value = item.Status;
                //    workSheet.Cells[recordIndex, 5].Value = item.Nama;
                //    foreach (var item2 in item.Aktivitas)
                //    {
                //        workSheet.Cells[recordIndex, 6].Value = item2.TipeAktivitas;
                //        workSheet.Cells[recordIndex, 6].Style.Font.Bold = true;
                //        recordIndex++;
                //        foreach (var item3 in item2.Aktivitas)
                //        {
                //            workSheet.Cells[recordIndex, 6].Value = item3;
                //            recordIndex++;
                //        }
                //    }
                //    recordIndex++;
                //    no++;
                //}

                int recordIndex = 2;
                int no = 1;
                foreach (var item in ListLaporan)
                {
                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.Tanggal.ToString("dd MMM yyyy");
                    workSheet.Cells[recordIndex, 3].Value = item.OnCall;
                    workSheet.Cells[recordIndex, 4].Value = item.Shift.Description;
                    workSheet.Cells[recordIndex, 5].Value = item.PIC.Nama;
                    workSheet.Cells[recordIndex, 6].Value = item.TipeAktivitas;
                    workSheet.Cells[recordIndex, 7].Value = item.Aktivitas;
                    workSheet.Cells[recordIndex, 8].Value = item.Catatan;

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 9; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[1, 1, recordIndex, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[1, 1, recordIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[1, 1, 1, 8])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "LaporanOilCustody" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }

            return Content("ok");
        }
    }
}