﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class CargoController : Controller
    {
        private ICargoService _cargoService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public CargoController(ICargoService CargoService)
        {
            _cargoService = CargoService;
        }
        // GET: MasterCargo
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Code, string Desc)
        {
            var obj = _cargoService.SearchCargo(requestModel.Start, requestModel.Length, Code, Desc);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Code = camp.Code,
                Desc = camp.Description
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new Cargo();
                                obj.Code = workSheet.Cells[rowIterator, 1].Value.ToString().Trim();
                                //var asd = _cargoService.Find(i => i.Code == obj.Code).FirstOrDefault();
                                //if (_cargoService.Find(i => i.Code == obj.Code).FirstOrDefault() != null)
                                //{
                                //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
                                //    return View("_Upload");
                                //}
                                obj.Description = workSheet.Cells[rowIterator, 2].Value.ToString();

                                _cargoService.Save(obj);
                            }
                        }
                    }
                }
                return Content("success");
            }
            catch (DbEntityValidationException ex)
            {

                ViewBag.Error = $"baris ke {rowIterator} :";
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        ViewBag.Error += $"<br>{validationError.ErrorMessage}";
                        //Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }
                return View("_Upload");
            }
            catch (DbUpdateException)
            {
                ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
                return View("_Upload");
            }
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Terdapat data kosong pada baris ke {rowIterator}";
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }

        }

        public ActionResult Create(int id = 0)
        {
            var cargo = _cargoService.Get(id);
            ViewBag.Title = "EDIT";
            if (cargo == null)
            {
                cargo = new Cargo();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", cargo);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Cargo model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                model.Code = model.Code.Trim();
                _cargoService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Code", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var cargo = _cargoService.Get(Id);
            return PartialView("_Delete", cargo);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _cargoService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var cargo = _cargoService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", cargo);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
            catch (Exception ex)
            {
                var cargo = _cargoService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
        }
    }
}