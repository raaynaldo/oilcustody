﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
//using Rotativa;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class LoadingController : Controller
    {
        private ILoadingService _loadingService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public LoadingController(ILoadingService LoadingService)
        {
            _loadingService = LoadingService;
        }
        // GET: Loading
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;
            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Code, string Desc)
        {
            var obj = _loadingService.SearchLoading(requestModel.Start, requestModel.Length, Code, Desc);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Code = camp.Code,
                Desc = camp.Description
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            if(id > 0)
            {
                if (user.ActionUpdate == false)
                {
                    return RedirectToAction("Index", "UnAuthorized");
                }
            }
            else
            {
                if (user.ActionCreate == false)
                {
                    return RedirectToAction("Index", "UnAuthorized");
                }
            }
            var loading = _loadingService.Get(id);
            ViewBag.Title = "EDIT";
            if (loading == null)
            {
                loading = new Loading();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", loading);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Loading model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                model.Code = model.Code.Trim();
                _loadingService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Code", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var loading = _loadingService.Get(Id);
            return PartialView("_Delete", loading);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _loadingService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var cargo = _loadingService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", cargo);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
            catch (Exception ex)
            {
                var cargo = _loadingService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", cargo);
            }
        }
    }
}