﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class KKSupplyLossController : Controller
    {
        private IKKSupplyLossService _kertasKerjaSupplyLossService;
        private IRoasService _roasService;
        private IPortService _portService;

        private CurrentUser user = CurrentUser.GetCurrentUser();
        public KKSupplyLossController(IKKSupplyLossService KertasKerjaSupplyLossService, IRoasService RoasService, IPortService PortService)
        {
            _kertasKerjaSupplyLossService = KertasKerjaSupplyLossService;
            _roasService = RoasService;
            _portService = PortService;

        }
        // GET: CargoLosses
        public ActionResult Index()
        {
            ViewBag.Process = user.ActionCreate || user.ActionUpdate;

            var Model = new KKSupplyLossViewModel();
            var TransportTypeList = _kertasKerjaSupplyLossService.GetListTransportType();
            TransportTypeList.Insert(0, "");
            Model.DataTipeTransportasi = TransportTypeList;

            return View(Model);
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeTransportasi, string LoadingPort,
            string NamaKapal, string NoBl, DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            var obj = _kertasKerjaSupplyLossService.SearchKertasKerjaSupplyLoss(requestModel.Start, requestModel.Length,
                TipeTransportasi, LoadingPort, NamaKapal, NoBl, TanggalBL, TanggalBLSampai, TanggalCQD, TanggalCQDSampai);

            var data = obj.Item1.Select(item => new
            {
                DocDate = item.DocDate,
                DocNo = item.Roas.DocNo,
                TipeTransport = item.Roas.TransportType,
                TanggalBl = item.BlDate,
                PONumber = item.PONumber,
                NoBl = item.Roas.BlAsal,
                Muatan = item.NamaProduk,
                BLGross = item.BlGross,
                ARGross = item.ArGross,
                OBQAwal = item.ObqAwal,
                OBQAkhir = item.ObqAkhir
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateEdit()
        {
            if (user.ActionCreate || user.ActionUpdate)
            {
                ViewBag.Create = user.ActionCreate;
                ViewBag.Edit = user.ActionUpdate;

                return View();
            }
            else
                return RedirectToAction("Index", "UnAuthorized");
        }

        [HttpPost]
        public ActionResult GetKKSupplyLossRoas([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string TipeProses, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            if (TipeProses == "Edit")
            {
                //var objEdit = _kertasKerjaSupplyLossService.Find(i => !i.Deleted && i.BlDate >= TanggalBongkar && i.BlDate <= TanggalBongkarSampai).ToList();
                var objEdit = _kertasKerjaSupplyLossService.SearchKKSupplyLossEdit(TanggalBongkar, TanggalBongkarSampai);

                var dataEdit = objEdit.Select(item => new
                {
                    Id = item.Id,
                    DocDate = item.DocDate,
                    DocNo = item.Roas.DocNo,
                    NoBl = item.Roas.BlAsal,
                    BLGross = item.BlGross,
                    ARGross = item.ArGross,
                    OBQAwal = item.ObqAwal,
                    OBQAkhir = item.ObqAkhir,
                    BLCQD = item.QualityBl,
                    MasterSample = item.QualitySample,
                    ShipCompAfter = item.QualityAft,
                    ShipCompBefore = item.QualityBef,
                    Tanki = item.QualityTanki,
                    SFAL = item.FreeWaterSfal,
                    SFBD = item.FreeWaterSfbd
                });

                return Json(new DataTablesResponse(requestModel.Draw, dataEdit.ToList(), dataEdit.Count(), dataEdit.Count()), JsonRequestBehavior.AllowGet);
            }

            var obj = _roasService.GetRoasForKKSupplyLoss();

            var data = obj.Select(item => new
            {
                Id = item.Id,
                DocDate = item.DocDate,
                DocNo = item.DocNo,
                NoBl = item.BlAsal,
                BLGross = 0,
                ARGross = 0,
                OBQAwal = 0,
                OBQAkhir = 0,
                BLCQD = 0,
                MasterSample = 0,
                ShipCompAfter = 0,
                ShipCompBefore = 0,
                Tanki = 0,
                SFAL = 0,
                SFBD = 0
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), data.Count(), data.Count()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveKKSupplyLoss(List<KKSupplyLoss> Item, string TipeProses)
        {
            try
            {

                if (TipeProses == "Baru")
                {
                    List<KKSupplyLoss> KKSupplyLossList = new List<KKSupplyLoss>();
                    var DataKKSupplyLoss = _kertasKerjaSupplyLossService.Find(i => !i.Deleted).ToList();

                    foreach (var item in Item)
                    {
                        KKSupplyLoss CL = new KKSupplyLoss();
                        Roas RoasObj = _roasService.Get(item.Id);
                        RoasObj.Port = _portService.Get(RoasObj.PortId);
                        CL.ROASId = item.Id;
                        CL.BlGross = item.BlGross;
                        CL.ArGross = item.ArGross;
                        CL.ObqAkhir = item.ObqAkhir;
                        CL.ObqAwal = item.ObqAwal;
                        CL.QualityBl = item.QualityBl;
                        CL.QualitySample = item.QualitySample;
                        CL.QualityAft = item.QualityAft;
                        CL.QualityBef = item.QualityBef;
                        CL.QualityTanki = item.QualityTanki;
                        CL.FreeWaterSfal = item.FreeWaterSfal;
                        CL.FreeWaterSfbd = item.FreeWaterSfbd;

                        CL.NamaProduk = RoasObj.NamaProduk;
                        CL.LoadingPort = RoasObj.Port.Description;
                        CL.DischargePort = "RU II DUMAI";
                        CL.PONumber = RoasObj.PoNumber;
                        CL.DelOrderNumber = RoasObj.DelOrderNumber;
                        CL.ShipmentNumber = RoasObj.ShipmentNumber;
                        CL.BarrelsPtm = RoasObj.BarrelsPtm;
                        CL.BarrelsAl = RoasObj.BarrelsAl;
                        CL.BarrelsBd = RoasObj.BarrelsBd;
                        CL.Barrels = RoasObj.Barrels;
                        CL.R1 = CL.BlGross - CL.BarrelsAl;
                        CL.PercR1 = CL.R1 / CL.BlGross;
                        CL.R2 = CL.BarrelsAl - CL.BarrelsBd;
                        CL.PercR2 = CL.R2 / CL.BlGross;
                        CL.R3 = CL.BarrelsBd - CL.ArGross;
                        CL.PercR3 = CL.R3 / CL.BlGross;
                        CL.R4Gross = CL.BlGross - CL.ArGross;
                        CL.PercGross = CL.R4Gross / CL.BlGross;
                        CL.R4Nett = CL.BarrelsPtm - CL.Barrels;
                        CL.PercNett = CL.R4Nett / CL.BarrelsPtm;

                        CL.DocDate = RoasObj.DocDate;
                        CL.BlDate = RoasObj.BlDate;
                        CL.ShipmentNumber = RoasObj.ShipmentNumber;
                        CL.TransportName = RoasObj.TransportName;
                        CL.Deleted = false;
                        CL.Roas = RoasObj;

                        var check = KKSupplyLossList.FindIndex(i => i.Roas.DocDate == RoasObj.DocDate && i.Roas.DocNo == RoasObj.DocNo && i.Roas.BlAsal == RoasObj.BlAsal && !i.Deleted);
                        if (check > -1)
                        {

                            KKSupplyLossList[check].Deleted = true;
                        }

                        KKSupplyLossList.Add(CL);

                    }

                    _kertasKerjaSupplyLossService.AddRange(KKSupplyLossList);

                    foreach (var item in KKSupplyLossList.Where(i => !i.Deleted))
                    {
                        var check = DataKKSupplyLoss.FindIndex(i => i.Roas.DocDate == item.Roas.DocDate && i.Roas.DocNo == item.Roas.DocNo && i.Roas.BlAsal == item.Roas.BlAsal && !i.Deleted);
                        if (check > -1)
                        {
                            DataKKSupplyLoss[check].Deleted = true;
                        }
                    }

                    _kertasKerjaSupplyLossService.UpdateRange(DataKKSupplyLoss);

                }
                else if (TipeProses == "Edit")
                {
                    var list = new List<KKSupplyLoss>();
                    foreach (var item in Item)
                    {
                        var KKSupplyLoss = _kertasKerjaSupplyLossService.Get(item.Id);
                        KKSupplyLoss.BlGross = item.BlGross;
                        KKSupplyLoss.ArGross = item.ArGross;
                        KKSupplyLoss.ObqAkhir = item.ObqAkhir;
                        KKSupplyLoss.ObqAwal = item.ObqAwal;
                        KKSupplyLoss.QualityBl = item.QualityBl;
                        KKSupplyLoss.QualitySample = item.QualitySample;
                        KKSupplyLoss.QualityAft = item.QualityAft;
                        KKSupplyLoss.QualityBef = item.QualityBef;
                        KKSupplyLoss.QualityTanki = item.QualityTanki;
                        KKSupplyLoss.FreeWaterSfal = item.FreeWaterSfal;
                        KKSupplyLoss.FreeWaterSfbd = item.FreeWaterSfbd;

                        KKSupplyLoss.R1 = KKSupplyLoss.BlGross - KKSupplyLoss.BarrelsAl;
                        KKSupplyLoss.PercR1 = KKSupplyLoss.R1 / KKSupplyLoss.BlGross;
                        KKSupplyLoss.R2 = KKSupplyLoss.BarrelsAl - KKSupplyLoss.BarrelsBd;
                        KKSupplyLoss.PercR2 = KKSupplyLoss.R2 / KKSupplyLoss.BlGross;
                        KKSupplyLoss.R3 = KKSupplyLoss.BarrelsBd - KKSupplyLoss.ArGross;
                        KKSupplyLoss.PercR3 = KKSupplyLoss.R3 / KKSupplyLoss.BlGross;
                        KKSupplyLoss.R4Gross = KKSupplyLoss.BlGross - KKSupplyLoss.ArGross;
                        KKSupplyLoss.PercGross = KKSupplyLoss.R4Gross / KKSupplyLoss.Barrels;

                        list.Add(KKSupplyLoss);
                        //_kertasKerjaSupplyLossService.Save(KKSupplyLoss);
                    }
                    _kertasKerjaSupplyLossService.UpdateRange(list);
                }

                return Content("Success");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }
        }

    }
}