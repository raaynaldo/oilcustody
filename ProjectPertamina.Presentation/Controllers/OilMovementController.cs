﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class OilMovementController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IOilMovementService _oilMovementService;

        public OilMovementController(IOilMovementService OilMovementService)
        {
            _oilMovementService = OilMovementService;
        }

        // GET: OilMovement
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;
            return View();
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? TanggalDari, DateTime? TanggalSampai, string Tujuan)
        {
            var obj = _oilMovementService.SearchOilMovement(requestModel.Start, requestModel.Length, TanggalDari, TanggalSampai, Tujuan);

            var data = obj.Item1.Select(fP => new
            {
                Id = fP.Id,
                Tanggal = fP.Tanggal,
                NoPo = fP.NoPO,
                JamMulai = fP.JamMulai,
                JamSelesai = fP.JamSelesai,
                NoPolisi = fP.NoPolisi,
                Tujuan = fP.Tujuan,
                MeterAwal = fP.MeterAwal,
                MeterAkhir = fP.MeterAkhir,
                KapBridger = fP.KapBridger
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            var oilMovement = _oilMovementService.Get(Id);

            return PartialView("_Delete", oilMovement);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                var berita = _oilMovementService.Get(id);

                _oilMovementService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult IndexDetail(int Id = 0)
        {

            if (Id == 0)
            {
                ViewBag.Title = "ADD";
                return View(new OilMovement() { JamSelesai = TimeSpan.FromHours(23) });
            }
            else
            {
                ViewBag.Title = "EDIT";
                var oilMovement = _oilMovementService.Get(Id);
                return View(oilMovement);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(OilMovement model)
        {
            if (!ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    ViewBag.Title = "ADD";
                    return View("IndexDetail", new OilMovement() { JamSelesai = TimeSpan.FromHours(23) });
                }
                else
                {
                    ViewBag.Title = "EDIT";
                    var oilMovement = _oilMovementService.Get(model.Id);
                    return View("IndexDetail", model);
                }
            }
            try
            {
                _oilMovementService.Save(model);
                TempData["MessageSucces"] = "Data berhasil disimpan !";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return RedirectToAction("IndexDetail", model.Id);
            }

        }

    }
}