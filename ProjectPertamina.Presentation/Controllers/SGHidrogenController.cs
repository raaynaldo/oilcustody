﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class SGHidrogenController : Controller
    {
        private ISGHidrogenService _sgHidrogenService;
        private ITipeProdukService _tipeProdukService;
        private ICustodyTransferService _custodyTransferService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public SGHidrogenController(ISGHidrogenService SGHidrogenService, ITipeProdukService TipeProdukService, ICustodyTransferService CustodyTransferService)
        {
            _sgHidrogenService = SGHidrogenService;
            _tipeProdukService = TipeProdukService;
            _custodyTransferService = CustodyTransferService;
        }
        // GET: MasterSGHidrogen
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            ViewBag.BulanList = GetBulan();

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int Bulan = 0, int Tahun = 0)
        {
            var obj = _sgHidrogenService.SearchSGHidrogen(requestModel.Start, requestModel.Length, Bulan, Tahun);

            var ItemList = GetBulan();
            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Bulan = ItemList.Where(i => i.Item2 == camp.Bulan).FirstOrDefault().Item1,
                Tahun = camp.Tahun,
                NilaiSG = camp.NilaiSG
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var sgHidrogen = _sgHidrogenService.Get(id);
            ViewBag.Title = "EDIT";
            if (sgHidrogen == null)
            {
                sgHidrogen = new SGHidrogen();
                ViewBag.Title = "ADD";
            }

            var ItemList = GetBulan();

            ViewBag.BulanList = ItemList;
            return PartialView("_CreateEdit", sgHidrogen);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(SGHidrogen model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                ViewBag.BulanList = GetBulan();
                return View("_CreateEdit", model);
            }
            try
            {
                if (model.NilaiSG == 0)
                {
                    ModelState.AddModelError("NilaiSG", "Nilai SG tidak boleh 0");
                    return View("_CreateEdit", model);
                }
                _sgHidrogenService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ViewBag.BulanList = GetBulan();
                ModelState.AddModelError("Bulan", "Kombinasi Bulan dan Tahun sudah ada");
                ModelState.AddModelError("Tahun", "Kombinasi Bulan dan Tahun sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ViewBag.BulanList = GetBulan();
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var sgHidrogen = _sgHidrogenService.Get(Id);
            ViewBag.Bulan = GetBulan().Where(i => i.Item2 == sgHidrogen.Bulan).FirstOrDefault().Item1;
            return PartialView("_Delete", sgHidrogen);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                var SG = _sgHidrogenService.Get(id);
                var JumlahDiCustody = _custodyTransferService.Find(i => i.Tanggal.Month == SG.Bulan && i.Tanggal.Year == SG.Tahun).Count();
                if (JumlahDiCustody != 0)
                {
                    ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                    return View("_Delete", SG);
                }
                _sgHidrogenService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var sgHidrogen = _sgHidrogenService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", sgHidrogen);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", sgHidrogen);
            }
            catch (Exception ex)
            {
                var sgHidrogen = _sgHidrogenService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", sgHidrogen);
            }
        }

        private List<Tuple<string, int>> GetBulan()
        {
            List<Tuple<string, int>> ItemList = new List<Tuple<string, int>>();
            ItemList.Add(new Tuple<string, int>("", 0));
            ItemList.Add(new Tuple<string, int>("Januari", 1));
            ItemList.Add(new Tuple<string, int>("Febuari", 2));
            ItemList.Add(new Tuple<string, int>("Maret", 3));
            ItemList.Add(new Tuple<string, int>("April", 4));
            ItemList.Add(new Tuple<string, int>("Mei", 5));
            ItemList.Add(new Tuple<string, int>("Juni", 6));
            ItemList.Add(new Tuple<string, int>("Juli", 7));
            ItemList.Add(new Tuple<string, int>("Agustus", 8));
            ItemList.Add(new Tuple<string, int>("September", 9));
            ItemList.Add(new Tuple<string, int>("Oktober", 10));
            ItemList.Add(new Tuple<string, int>("November", 11));
            ItemList.Add(new Tuple<string, int>("Desember", 12));

            return ItemList;
        }

        public JsonResult getSGHidrogen(DateTime Tanggal)
        {
            var bulan = Tanggal.Month;
            var tahun = Tanggal.Year;
            var SGHidrogen = _sgHidrogenService.Find(i => i.Bulan == bulan && i.Tahun == tahun).FirstOrDefault();

            if (SGHidrogen == null)
            {
                var List = GetBulan();
                var Bulan = List.Where(i => i.Item2 == bulan).FirstOrDefault().Item1;
                return Json(new { SGHidrogen = $"SG Hidrogen {Bulan} {tahun} tidak ditemukan!", val = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { SGHidrogen = SGHidrogen.NilaiSG, val = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}