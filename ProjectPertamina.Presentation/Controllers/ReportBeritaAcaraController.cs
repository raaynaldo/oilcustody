﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class ReportBeritaAcaraController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();

        private ICustodyTransferService _custodyTransferService;
        private ISGHidrogenService _sgHidrogenService;
        private ITabel21Service _tabel21Service;
        private ITabel56Service _tabel56Service;
        private ITabel52Service _tabel52Service;

        public ReportBeritaAcaraController(ICustodyTransferService CustodyTransferService,
            ISGHidrogenService SGHidrogen,
            ITabel21Service Tabel21Service,
            ITabel56Service Tabel56Service,
            ITabel52Service Tabel52Service)
        {
            _custodyTransferService = CustodyTransferService;
            _sgHidrogenService = SGHidrogen;
            _tabel21Service = Tabel21Service;
            _tabel56Service = Tabel56Service;
            _tabel52Service = Tabel52Service;
        }

        // GET: LaporanOilCustody
        public ActionResult Index()
        {
            ViewBag.ddlBulan = GetddlBulan();
            ViewBag.ddlTahun = GetddlTahun();

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int bulan, int tahun)
        {
            if (bulan == 0 || tahun == 0)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<ReportBeritaAcaraViewModel>(), 0, 0), JsonRequestBehavior.AllowGet);
            }

            var CustodyTrfList = _custodyTransferService.SearchCustodyTransferForReportBeritaAcara(bulan, tahun).Where(i => i.PerusahaanKode == ConstantPerusahaan.PerusahaanPertaminaKode).ToList();

            var ModelList = new List<ReportBeritaAcaraViewModel>();

            var SGHidrogenList = _sgHidrogenService.GetAll().ToList();
            var Tabel21List = _tabel21Service.GetAll().ToList();
            var Tabel56List = _tabel56Service.GetAll().ToList();
            var Tabel52List = _tabel52Service.GetAll().ToList();
            //var CustodyTransferList = _custodyTransferService.GetAll().ToList();

            foreach (var item in CustodyTrfList)
            {
                var obj = new ReportBeritaAcaraViewModel();
                obj.Stream = item.Stream;
                obj.TagNo = item.Stream.TagNumber;
                obj.MtTon = item.MT;
                var sg = getSGHidrogen(item.Tanggal, SGHidrogenList);
                obj.Density = GetDensity(item.SG, Tabel21List);
                obj.M3 = GetLiter15(item.MT, obj.Density, Tabel56List);
                obj.NM3 = item.Stream.TipeProduk.Kode == ConstantTipeProdukKode.Hidrogen ? GetNM3(item.Stream.TipeProduk.Kode, sg, item.Tanggal, CustodyTrfList.ToList()) : 0;
                var CFBarrels = GetCFBarrel(item.SG, obj.Density, Tabel52List);
                obj.Barrels = (obj.M3 * CFBarrels) / 1000;
                obj.USGallon = (obj.M3 * CFBarrels) / 1000;
                obj.Kwh = item.Kwh;

                ModelList.Add(obj);
            }

            var GroupModelList = from list in ModelList
                                 group list by new
                                 {
                                     list.Stream,
                                     list.TagNo
                                 } into newGroup
                                 select new ReportBeritaAcaraViewModel()
                                 {
                                     Stream = newGroup.Key.Stream,
                                     TagNo = newGroup.Key.TagNo,
                                     MtTon = Math.Round(newGroup.Sum(i => i.MtTon), 3),
                                     Density = Math.Round(newGroup.Sum(i => i.Density), 4),
                                     M3 = Math.Round(newGroup.Sum(i => i.M3), 3),
                                     NM3 = Math.Round(newGroup.Sum(i => i.NM3), 3),
                                     Barrels = Math.Round(newGroup.Sum(i => i.Barrels), 3),
                                     USGallon = Math.Round(newGroup.Sum(i => i.USGallon), 3),
                                     Kwh = Math.Round(newGroup.Sum(i => i.Kwh), 3)
                                 };

            return Json(new DataTablesResponse(requestModel.Draw, GroupModelList.ToList(), 0, 0), JsonRequestBehavior.AllowGet);


        }

        [AllowAnonymous]
        public ActionResult Print(PDFReportBeritaAcaraViewModel input)
        {
            var CustodyTrfList = _custodyTransferService.SearchCustodyTransferForReportBeritaAcara(input.bulanInput, input.tahun).Where(i => i.PerusahaanKode == ConstantPerusahaan.PerusahaanPertaminaKode).ToList();

            var ModelList = new List<ReportBeritaAcaraViewModel>();

            var SGHidrogenList = _sgHidrogenService.GetAll().ToList();
            var Tabel21List = _tabel21Service.GetAll().ToList();
            var Tabel56List = _tabel56Service.GetAll().ToList();
            var Tabel52List = _tabel52Service.GetAll().ToList();

            foreach (var item in CustodyTrfList)
            {
                var obj = new ReportBeritaAcaraViewModel();
                obj.Stream = item.Stream;
                obj.TagNo = item.Stream.TagNumber;
                obj.MtTon = item.MT;
                var sg = getSGHidrogen(item.Tanggal, SGHidrogenList);
                obj.Density = GetDensity(item.SG, Tabel21List);
                obj.M3 = GetLiter15(item.MT, obj.Density, Tabel56List);
                obj.NM3 = item.Stream.TipeProduk.Kode == ConstantTipeProdukKode.Hidrogen ? GetNM3(item.Stream.TipeProduk.Kode, sg, item.Tanggal, CustodyTrfList.ToList()) : 0;
                var CFBarrels = GetCFBarrel(item.SG, obj.Density, Tabel52List);
                obj.Barrels = (obj.M3 * CFBarrels) / 1000;
                obj.USGallon = (obj.M3 * CFBarrels) / 1000;
                obj.Kwh = item.Kwh;

                ModelList.Add(obj);
            }

            var GroupModelList = from list in ModelList
                                 group list by new
                                 {
                                     list.Stream,
                                     list.TagNo
                                 } into newGroup
                                 select new ReportBeritaAcaraViewModel()
                                 {
                                     Stream = newGroup.Key.Stream,
                                     TagNo = newGroup.Key.TagNo,
                                     MtTon = Math.Round(newGroup.Sum(i => i.MtTon), 3),
                                     Density = Math.Round(newGroup.Sum(i => i.Density), 4),
                                     M3 = Math.Round(newGroup.Sum(i => i.M3), 3),
                                     NM3 = Math.Round(newGroup.Sum(i => i.NM3), 3),
                                     Barrels = Math.Round(newGroup.Sum(i => i.Barrels), 3),
                                     USGallon = Math.Round(newGroup.Sum(i => i.USGallon), 3),
                                     Kwh = Math.Round(newGroup.Sum(i => i.Kwh), 3)
                                 };

            //ViewBag.User = user.DisplayName;
            //string header = Server.MapPath("~/Views/Shared/Header.html");
            //string footer = string.Format("--footer-right \"[Oil Custody] Printed on: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + " by " + user.DisplayName + "\"" + " --footer-line --footer-font-size \"10\" --footer-spacing 6 --footer-font-name \"Times New Roman\"");


            //string customSwitches = string.Format("--header-html  \"{0}\" " +
            //                       "--header-spacing \"0\" " +
            //                       "--footer-html \"{1}\" " +
            //                       "--footer-spacing \"10\" " +
            //                       "--footer-font-size \"10\" " +
            //                       "--header-font-size \"10\" ", header, footer);


            //var model = new PDFReportBeritaAcaraViewModel();
            //model.listmodel = GroupModelList.ToList();
            //model.bulan = GetddlBulan().Where(i => i.Value == input.bulanInput).FirstOrDefault().Text;
            //model.tahun = input.tahun;
            //model.nama1 = input.nama1;
            //model.nama2 = input.nama2;
            //model.nama3 = input.nama3;
            //model.nama4 = input.nama4;
            //model.nama5 = input.nama5;
            //model.nama6 = input.nama6;
            //model.nama7 = input.nama7;
            //model.nama8 = input.nama8;
            //model.jabatan1 = input.jabatan1;
            //model.jabatan2 = input.jabatan2;
            //model.jabatan3 = input.jabatan3;
            //model.jabatan4 = input.jabatan4;
            //model.jabatan5 = input.jabatan5;
            //model.jabatan6 = input.jabatan6;
            //model.jabatan7 = input.jabatan7;
            //model.jabatan8 = input.jabatan8;


            //return new ViewAsPdf("ReportGenerate", model)
            //{
            //    //FileName = "PdfFileName.pdf",
            //    PageSize = Rotativa.Options.Size.A4,
            //    //PageOrientation = Orientation.Landscape,
            //    CustomSwitches = footer
            //};

            string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            FileInfo newFile = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(newFile))
            {
                var workSheet = excel.Workbook.Worksheets.Add("ReportBeritaAcara");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                //Header of table  
                //  

                workSheet.Cells[1, 1, 1, 10].Merge = true;
                workSheet.Cells[2, 1, 2, 10].Merge = true;
                workSheet.Cells[3, 1, 3, 10].Merge = true;
                workSheet.Cells[1, 1].Value = "CUSTODY TRANSFER";
                workSheet.Cells[2, 1].Value = "Between PATRA SK and PERTAMINA";
                workSheet.Cells[3, 1].Value = $"{GetddlBulan().Where(i => i.Value == input.bulanInput).FirstOrDefault().Text} {input.tahun}";
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                workSheet.Row(4).Height = 20;
                workSheet.Row(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(4).Style.Font.Bold = true;
                workSheet.Cells[4, 1].Value = "No";
                workSheet.Cells[4, 2].Value = "Stream";
                workSheet.Cells[4, 3].Value = "Tag No";
                workSheet.Cells[4, 4].Value = "Mt Ton";
                workSheet.Cells[4, 5].Value = "Density";
                workSheet.Cells[4, 6].Value = "M3";
                workSheet.Cells[4, 7].Value = "NM3";
                workSheet.Cells[4, 8].Value = "Barrels";
                workSheet.Cells[4, 9].Value = "US Gallon";
                workSheet.Cells[4, 10].Value = "Kwh";

                int recordIndex = 5;
                int no = 1;
                foreach (var item in GroupModelList)
                {
                    workSheet.Cells[recordIndex, 1].Value = no;
                    workSheet.Cells[recordIndex, 2].Value = item.Stream.StreamName;
                    workSheet.Cells[recordIndex, 3].Value = item.TagNo;
                    workSheet.Cells[recordIndex, 4].Value = item.MtTon != 0 ? string.Format("{0:n3}", item.MtTon) : "-";
                    workSheet.Cells[recordIndex, 5].Value = item.Density != 0 ? string.Format("{0:n4}", item.Density) : "-";
                    workSheet.Cells[recordIndex, 6].Value = item.M3 != 0 ? string.Format("{0:n3}", item.M3) : "-";
                    workSheet.Cells[recordIndex, 7].Value = item.NM3 != 0 ? string.Format("{0:n3}", item.NM3) : "-";
                    workSheet.Cells[recordIndex, 8].Value = item.Barrels != 0 ? string.Format("{0:n3}", item.Barrels) : "-";
                    workSheet.Cells[recordIndex, 9].Value = item.USGallon != 0 ? string.Format("{0:n3}", item.USGallon) : "-";
                    workSheet.Cells[recordIndex, 10].Value = item.Kwh != 0 ? string.Format("{0:n3}", item.Kwh) : "-";

                    recordIndex++;
                    no++;
                }

                for (int i = 1; i < 11; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

                if (recordIndex > 3)
                    recordIndex -= 1;
                workSheet.Cells[4, 1, recordIndex, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[4, 1, recordIndex, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[4, 1, recordIndex, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[4, 1, recordIndex, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var range = workSheet.Cells[4, 1, 4, 10])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                    range.Style.Font.Color.SetColor(Color.White);
                }


                workSheet.Cells[recordIndex + 1, 1].Value = "Remarks:";
                workSheet.Cells[recordIndex + 2, 1].Value = "1. Custodytransfer measurement user metering orifice and Coriolls which has been and certified by DITMET and withnessed by MIGAS";
                workSheet.Cells[recordIndex + 3, 1].Value = "2.	Conversion from MTon to Barrel use Table T-52 and T-56 (ASTM-IP)";
                workSheet.Cells[recordIndex + 4, 1].Value = "3.	This documents only raw data for OCC meeting (can’t use for commercial invoice and tax purpose)";
                using (var range = workSheet.Cells[recordIndex + 1, 1, recordIndex + 4, 1])
                {
                    range.Style.Font.Italic = true;
                    range.Style.Font.Size = 9;
                }

                workSheet.Cells[recordIndex + 8, 1].Value = $"Dumai, {DateTime.Now.ToString("dd MMMM yyyy")}";

                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 6].Merge = true;
                workSheet.Cells[recordIndex + 9, 7, recordIndex + 9, 10].Merge = true;
                workSheet.Cells[recordIndex + 9, 1].Value = "PT. Pertamina, Refenery Unit II";
                workSheet.Cells[recordIndex + 9, 7].Value = "PT. PATRA SK";

                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 10].Style.Font.Bold = true;
                workSheet.Row(recordIndex + 9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 1, recordIndex + 9, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 7, recordIndex + 9, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 7, recordIndex + 9, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 7, recordIndex + 9, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 9, 7, recordIndex + 9, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                workSheet.Cells[recordIndex + 10, 1, recordIndex + 10, 2].Merge = true;
                workSheet.Cells[recordIndex + 10, 1].Value = input.jabatan1;
                workSheet.Cells[recordIndex + 10, 3, recordIndex + 10, 4].Merge = true;
                workSheet.Cells[recordIndex + 10, 3].Value = input.jabatan2;
                workSheet.Cells[recordIndex + 10, 5, recordIndex + 10, 6].Merge = true;
                workSheet.Cells[recordIndex + 10, 5].Value = input.jabatan3;
                workSheet.Cells[recordIndex + 10, 7, recordIndex + 10, 10].Merge = true;
                workSheet.Cells[recordIndex + 10, 7].Value = input.jabatan4;
                workSheet.Row(recordIndex + 10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int jarak = 5;
                workSheet.Cells[recordIndex + 10 + jarak, 1, recordIndex + 10 + jarak, 2].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak, 1].Value = input.nama1;
                workSheet.Cells[recordIndex + 10 + jarak, 3, recordIndex + 10 + jarak, 4].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak, 3].Value = input.nama2;
                workSheet.Cells[recordIndex + 10 + jarak, 5, recordIndex + 10 + jarak, 6].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak, 5].Value = input.nama3;
                workSheet.Cells[recordIndex + 10 + jarak, 7, recordIndex + 10 + jarak, 10].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak, 7].Value = input.nama4;
                workSheet.Row(recordIndex + 10 + jarak).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int jarak2 = jarak + 2;
                workSheet.Cells[recordIndex + 10 + jarak2, 1, recordIndex + 10 + jarak2, 2].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak2, 1].Value = input.jabatan5;
                workSheet.Cells[recordIndex + 10 + jarak2, 3, recordIndex + 10 + jarak2, 4].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak2, 3].Value = input.jabatan6;
                workSheet.Cells[recordIndex + 10 + jarak2, 5, recordIndex + 10 + jarak2, 6].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak2, 5].Value = input.jabatan7;
                workSheet.Cells[recordIndex + 10 + jarak2, 7, recordIndex + 10 + jarak2, 10].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak2, 7].Value = input.jabatan8;
                workSheet.Row(recordIndex + 10 + jarak2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int jarak3 = jarak2 + jarak;
                workSheet.Cells[recordIndex + 10 + jarak3, 1, recordIndex + 10 + jarak3, 2].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak3, 1].Value = input.jabatan5;
                workSheet.Cells[recordIndex + 10 + jarak3, 3, recordIndex + 10 + jarak3, 4].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak3, 3].Value = input.jabatan6;
                workSheet.Cells[recordIndex + 10 + jarak3, 5, recordIndex + 10 + jarak3, 6].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak3, 5].Value = input.jabatan7;
                workSheet.Cells[recordIndex + 10 + jarak3, 7, recordIndex + 10 + jarak3, 10].Merge = true;
                workSheet.Cells[recordIndex + 10 + jarak3, 7].Value = input.jabatan8;
                workSheet.Row(recordIndex + 10 + jarak3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                workSheet.Cells[recordIndex + 10, 1, recordIndex + 10 + jarak3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 10, 7, recordIndex + 10 + jarak3, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells[recordIndex + 10, 10, recordIndex + 10 + jarak3, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                workSheet.Cells[recordIndex + 10 + jarak3, 1, recordIndex + 10 + jarak3, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=" + "ReportBeritaAcara" + ".xlsx");
                    excel.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
                excel.Save();
            }
            return Content("ok");

        }

        private decimal getSGHidrogen(DateTime Tanggal, List<SGHidrogen> SGHidrogenList)
        {
            var bulan = Tanggal.Month;
            var tahun = Tanggal.Year;
            var SGHidrogen = SGHidrogenList.Where(i => i.Bulan == bulan && i.Tahun == tahun).FirstOrDefault();

            if (SGHidrogen == null)
                return 0;
            else
                return SGHidrogen.NilaiSG;
        }

        private decimal GetDensity(decimal sg, List<Tabel21> Tabel21List)
        {
            if (sg == 0) //Tanda Kutip yang di DS
                return 0;

            decimal sg1 = (Math.Truncate(sg * 1000) / 1000);
            decimal sg2 = sg1 + (decimal)0.001;
            Tabel21 sg3obj = Tabel21List.Where(i => i.SG == sg1).FirstOrDefault();
            Tabel21 sg4obj = Tabel21List.Where(i => i.SG == sg2).FirstOrDefault();

            if (sg3obj == null)// Master Ga ada
                return 0;
            if (sg4obj == null)// Master Ga ada
                return 0;

            decimal sg3 = sg3obj.Density15;
            decimal sg4 = sg4obj.Density15;

            var density = sg3 + ((sg - sg1) / (decimal)0.001 * (sg4 - sg3));

            return density;
        }

        private decimal GetLiter15(decimal mt, decimal density, List<Tabel56> Tabel56List)
        {
            if (mt == 0) // Kutip di DS
                return 0;

            if (density == 0) // Kutip di DS
                return 0;

            decimal d1 = Math.Truncate(density * 1000) / 1000;
            decimal d2 = d1 + (decimal)0.001;
            decimal d3 = d1 + (decimal)0.001;
            decimal d4 = d2 + (decimal)0.001;
            Tabel56 d5Obj = Tabel56List.Where(i => i.Density15 == d1).FirstOrDefault();
            Tabel56 d6Obj = Tabel56List.Where(i => i.Density15 == d2).FirstOrDefault();
            if (d5Obj == null) // Master Ga ada
                return 0;

            if (d6Obj == null) // Master Ga ada
                return 0;

            decimal d5 = d5Obj.LiterMetricTon;
            decimal d6 = d6Obj.LiterMetricTon;

            decimal Liter15 = mt * (d5 + ((density - d1) / (decimal)0.001 * (d6 - d5)));

            return Liter15;
        }

        private decimal GetNM3(string tipeProduk, decimal sg, DateTime Tanggal, List<CustodyTransfer> CustodyTransferList)
        {
            if (tipeProduk != ConstantTipeProdukKode.Hidrogen)
            {
                return 0;
            }

            decimal bmh2 = sg * (decimal)28.84;
            //decimal berath2 = CustodyTransferList.Where(i => i.Tanggal.Month == Tanggal.Month && i.Tanggal.Year == Tanggal.Year)
            //                            .Sum(i => i.MT)
            //                            * 1000000;
            decimal berath2 = CustodyTransferList.Sum(i => i.MT) * 1000000;
            decimal grlh2 = berath2 / bmh2;
            decimal volh2 = grlh2 / bmh2;

            var NM3 = volh2 / 1000;

            return NM3;
        }

        private decimal GetCFBarrel(decimal sg, decimal density, List<Tabel52> Tabel52List)
        {
            if (sg == 0)
                return 0;

            density = Math.Truncate(density * 1000) / 1000;
            Tabel52 cf = Tabel52List.Where(i => i.Density15 == density).FirstOrDefault();
            if (cf == null)
                return 0;

            var CFBarrel = cf.Barrel60F;

            return CFBarrel;
        }

        private List<dropDownListViewModel> GetddlTahun()
        {
            //ddl Year
            var YearNow = DateTime.Now.Year;
            List<dropDownListViewModel> ddlTahun = new List<dropDownListViewModel>();
            int row = 0;
            for (int i = YearNow; i >= 2000; i--)
            {
                ddlTahun.Insert(row, new dropDownListViewModel { Text = i.ToString(), Value = i });
                row = row + 1;
            }

            ddlTahun.Insert(0, new dropDownListViewModel { Text = "Tahun", Value = 0 });

            return ddlTahun;
        }

        private List<dropDownListViewModel> GetddlBulan()
        {
            //ddl Bulan
            List<dropDownListViewModel> ddlBulan = new List<dropDownListViewModel>();
            ddlBulan.Add(new dropDownListViewModel { Text = "Bulan", Value = 0 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Januari", Value = 1 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Februari", Value = 2 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Maret", Value = 3 });
            ddlBulan.Add(new dropDownListViewModel { Text = "April", Value = 4 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Mei", Value = 5 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juni", Value = 6 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Juli", Value = 7 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Agustus", Value = 8 });
            ddlBulan.Add(new dropDownListViewModel { Text = "September", Value = 9 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Oktober", Value = 10 });
            ddlBulan.Add(new dropDownListViewModel { Text = "November", Value = 11 });
            ddlBulan.Add(new dropDownListViewModel { Text = "Desember", Value = 12 });

            return ddlBulan;
        }
    }
}