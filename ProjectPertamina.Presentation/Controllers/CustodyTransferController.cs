﻿using DataTables.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Presentation.ViewModels;
using ProjectPertamina.Services;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using ProjectPertamina.UserManager.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Drawing;
//using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class CustodyTransferController : Controller
    {
        private ICustodyTransferService _custodyTransferService;
        private IPerusahaanService _perusahaanService;
        private ITipeProdukService _tipeProdukService;
        private ISGHidrogenService _sgHidrogenService;
        private IStreamService _streamService;
        private ITabel21Service _table21Service;
        private ITabel52Service _table52Service;
        private ITabel56Service _table56Service;
        private IKonfigurasiParameterService _konfigurasiParameter;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public CustodyTransferController(
            ICustodyTransferService CustodyTransferService,
            IPerusahaanService PerushaanService,
            ITipeProdukService TipeProdukService,
            ISGHidrogenService SGHidrogenService,
            IStreamService StreamService,
            ITabel21Service Tabel21Service,
            ITabel52Service Tabel52Service,
            ITabel56Service Tabel56Service,
            IKonfigurasiParameterService KonfigurasiParameterService)
        {
            _custodyTransferService = CustodyTransferService;
            _perusahaanService = PerushaanService;
            _sgHidrogenService = SGHidrogenService;
            _tipeProdukService = TipeProdukService;
            _streamService = StreamService;
            _table21Service = Tabel21Service;
            _table52Service = Tabel52Service;
            _table56Service = Tabel56Service;
            _konfigurasiParameter = KonfigurasiParameterService;
        }
        // GET: CustodyTransfer
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var TipeProduk = _tipeProdukService.GetAll().ToList();
            TipeProduk.Insert(0, new Entities.Models.WebCustodyTransfer.TipeProduk() { Id = 0, Deskripsi = "" });
            ViewBag.TipeProdukList = TipeProduk;

            var Perusahaan = UserManagementServices.getPerusahaan();
            Perusahaan.Insert(0, new UserManager.Model.Perusahaan() { Id = 0, Code = "", Description = "" });
            ViewBag.PerusahaanList = Perusahaan;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Perusahaan, int TipeProduk, string Stream, string TagNumber, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            //string kode = "";
            //if (user.Email.ToString().ToLower().Contains("@pertamina.com"))
            //    kode = ConstantPerusahaan.PeusahaanPertaminaKode;
            var Check = user.IDPerusahaan == ConstantPerusahaan.PerusahaanPertaminaKode;

            var obj = _custodyTransferService.SearchCustodyTransfer(requestModel.Start, requestModel.Length, Perusahaan, TipeProduk, Stream, TagNumber, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Tanggal = camp.Tanggal,
                TipeProduk = camp.Stream.TipeProduk.Deskripsi,
                Stream = camp.Stream.StreamName,
                MassTotalizer = camp.MT,
                SG60 = camp.SG,
                KWH = camp.Kwh,
                EditDeleteTampil = Check ? ConstantPerusahaan.PerusahaanPertaminaKode == camp.PerusahaanKode : ConstantPerusahaan.PerusahaanPertaminaKode != camp.PerusahaanKode,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexDetail(int Id = 0)
        {
            var TipeProduk = _tipeProdukService.GetAll().ToList();
            ViewBag.TipeProdukList = TipeProduk;

            if (Id == 0)
            {
                ViewBag.Title = "ADD";
                var Obj = TipeProduk.FirstOrDefault();
                var Stream = _streamService.Find(i => i.TipeProdukId == Obj.Id).ToList();
                Stream.Insert(0, new Entities.Models.WebCustodyTransfer.Stream() { StreamName = "", Id = 0 });
                ViewBag.StreamList = Stream;

                var obj = new CustodyTransfer();
                if (user.IDPerusahaan == ConstantPerusahaan.PerusahaanPertaminaKode)
                {
                    //obj.Perusahaan = _perusahaanService.Find(i => i.Kode == ConstantPerusahaan.PeusahaanPertaminaKode).FirstOrDefault();
                    obj.PerusahaanKode = ConstantPerusahaan.PerusahaanPertaminaKode;
                }
                else
                {
                    //obj.Perusahaan = _perusahaanService.GetAll().OrderBy(i => i.Id).LastOrDefault();
                    obj.PerusahaanKode = ConstantPerusahaan.PerusahaanPTSKKode;
                }
                var model = new CustodyTransferViewModel();
                model.Id = obj.Id;
                model.PerusahaanName = user.NamaPerusahaan;
                model.Stream = obj.Stream;
                model.PerusahaanKode = user.IDPerusahaan;
                model.StreamId = obj.StreamId;
                model.Tanggal = obj.Tanggal;
                model.MT = obj.MT;
                model.SG = obj.SG;
                model.Kwh = obj.Kwh;

                return View(model);
            }
            else
            {
                ViewBag.Title = "EDIT";
                var obj = _custodyTransferService.GetJoinAllDatabyId(Id);

                var Stream = _streamService.Find(i => i.TipeProdukId == obj.Stream.TipeProdukId).ToList();
                Stream.Insert(0, new Entities.Models.WebCustodyTransfer.Stream() { StreamName = "", Id = 0 });
                ViewBag.StreamList = Stream;

                var model = new CustodyTransferViewModel();
                model.Id = obj.Id;
                model.PerusahaanName = user.NamaPerusahaan;
                model.Stream = obj.Stream;
                model.PerusahaanKode = user.IDPerusahaan;
                model.StreamId = obj.StreamId;
                model.Tanggal = obj.Tanggal;
                model.MT = obj.MT;
                model.SG = obj.SG;
                model.Kwh = obj.Kwh;

                return View(model);
            }
        }

        public JsonResult getStreamDL(int TipeProdukId)
        {
            var StreamList = _streamService.Find(i => i.TipeProdukId == TipeProdukId).ToList();
            StreamList.Insert(0, new Stream() { Id = 0, StreamName = "" });
            return Json(StreamList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getStream(int StreamId)
        {
            var Stream = _streamService.Get(StreamId);
            if (Stream == null)
                Stream = new Stream() { TagNumber = "" };
            return Json(Stream, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(CustodyTransferViewModel obj)
        {
            //ModelState["Stream.TipeProdukId"].Errors.Clear();
            //ModelState["Stream.Kode"].Errors.Clear();
            //ModelState["Stream.StreamName"].Errors.Clear();
            //if (!ModelState.IsValid)
            //    return RedirectToAction("IndexDetail", model);
            try
            {
                var CT = new CustodyTransfer();
                CT.Id = obj.Id;
                //CT.Perusahaan = obj.Perusahaan;
                //CT.Stream = obj.Stream;
                CT.PerusahaanKode = obj.PerusahaanKode;
                CT.StreamId = obj.StreamId;
                CT.Tanggal = obj.Tanggal.Date;
                CT.MT = obj.MT;
                CT.SG = obj.SG;
                CT.Kwh = obj.Kwh;

                _custodyTransferService.Save(CT);
                TempData["MessageSucces"] = "Data berhasil disimpan !";
                return RedirectToAction("Index");
            }
            catch (DbUpdateException ex)
            {
                ViewBag.Title = "ADD";

                var TipeProduk = _tipeProdukService.GetAll().ToList();
                ViewBag.TipeProdukList = TipeProduk;

                var Stream = _streamService.Find(i => i.TipeProdukId == obj.Stream.TipeProdukId).ToList();
                Stream.Insert(0, new Entities.Models.WebCustodyTransfer.Stream() { StreamName = "", Id = 0 });
                ViewBag.StreamList = Stream;

                ModelState.AddModelError("Tanggal", "Kombinasi Tanggal dan Stream sudah ada");
                ModelState.AddModelError("StreamId", "Kombinasi Tanggal dan Stream sudah ada");
                return View("IndexDetail", obj);
            }
            catch (Exception ex)
            {
                var TipeProduk = _tipeProdukService.GetAll().ToList();
                ViewBag.TipeProdukList = TipeProduk;

                //var Obj = TipeProduk.FirstOrDefault();
                var Stream = _streamService.Find(i => i.TipeProdukId == obj.Stream.TipeProdukId).ToList();
                Stream.Insert(0, new Entities.Models.WebCustodyTransfer.Stream() { StreamName = "", Id = 0 });
                ViewBag.StreamList = Stream;

                ModelState.AddModelError("", ex.Message);
                return View("IndexDetail", obj);
            }
        }

        public JsonResult GetNM3(DateTime Tanggal)
        {
            return Json(CountGetNM3(Tanggal), JsonRequestBehavior.AllowGet);
        }

        public decimal CountGetNM3(DateTime Tanggal)
        {
            var SGHidrogen = _sgHidrogenService.Find(i => i.Bulan == Tanggal.Month && i.Tahun == Tanggal.Year).FirstOrDefault();

            if (SGHidrogen == null)
                return 0;
            //else
            //    return SGHidrogen.NilaiSG;

            //decimal bmh2 = (decimal)0.1178 * (decimal)28.84;
            decimal bmh2 = SGHidrogen.NilaiSG * (decimal)28.84;
            decimal berath2 = _custodyTransferService.Find(i => i.Tanggal.Month == Tanggal.Month && i.Tanggal.Year == Tanggal.Year)
                                        .Sum(i => i.MT)
                                        * 1000000;
            decimal grlh2 = berath2 / bmh2;
            decimal volh2 = grlh2 / bmh2;

            return volh2 / 1000;
        }

        public ActionResult Delete(int Id)
        {
            var custodyTransfer = _custodyTransferService.GetJoinAllDatabyId(Id);
            return PartialView("_Delete", custodyTransfer);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _custodyTransferService.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var custodyTransfer = _custodyTransferService.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", custodyTransfer);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", custodyTransfer);
            }
            catch (Exception ex)
            {
                var custodyTransfer = _custodyTransferService.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", custodyTransfer);
            }
        }

        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            var list = new List<CustodyTransfer>();
            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        var listCT = _custodyTransferService.GetAll();
                        //var UserPertamina = user.Email.ToString().ToLower().Contains("@pertamina.com");
                        var PerusahaanList = UserManagementServices.getPerusahaan();
                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "Kode Perusahaan" ||
                                workSheet.Cells[1, 2].Value.ToString() != "Kode Stream" ||
                                workSheet.Cells[1, 3].Value.ToString() != "Tanggal" ||
                                workSheet.Cells[1, 4].Value.ToString() != "MT" ||
                                workSheet.Cells[1, 5].Value.ToString() != "SG" ||
                                workSheet.Cells[1, 6].Value.ToString() != "Kwh" ||
                                noOfCol > 6)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Custody Transfer.";
                                return View("_Upload");
                            }
                            if (noOfRow == 2)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 3; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new CustodyTransfer();
                                var Perusahaan = workSheet.Cells[rowIterator, 1].Value.ToString();
                                var Stream = workSheet.Cells[rowIterator, 2].Value.ToString().Trim();
                                DateTime DocDate;
                                var CheckDate = DateTime.TryParse(workSheet.Cells[rowIterator, 3].Value.ToString(), out DocDate);
                                if (!CheckDate)
                                    obj.Tanggal = DateTime.FromOADate((double)workSheet.Cells[rowIterator, 3].Value).Date;

                                obj.MT = decimal.Parse(workSheet.Cells[rowIterator, 4].Value.ToString());
                                obj.SG = decimal.Parse(workSheet.Cells[rowIterator, 5].Value.ToString());
                                obj.Kwh = decimal.Parse(workSheet.Cells[rowIterator, 6].Value.ToString());

                                //var perusahaan = PerusahaanList.Where(i => i.Code == Perusahaan).FirstOrDefault();

                                //if (perusahaan == null)
                                //{
                                //    string Error = "";
                                //    if (rowIterator > 3)
                                //        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                //    Error += $"Perushaan pada baris ke {rowIterator} tidak ditemukan. Sampai baris ke {rowIterator - 1} sudah terinput";
                                //    ViewBag.Error = Error;

                                //    _custodyTransferService.AddRange(list);
                                //    return View("_Upload");
                                //}

                                if (user.IDPerusahaan.Trim().ToLower() != Perusahaan.Trim().ToLower())
                                {
                                    string Error = "";
                                    if (rowIterator > 3)
                                        Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>";

                                    Error += $"Perushaan pada baris ke {rowIterator} tidak sesuai dengan user Id. Sampai baris ke {rowIterator - 1} sudah terinput";

                                    ViewBag.Error = Error;
                                    _custodyTransferService.AddRange(list);
                                    return View("_Upload");
                                }

                                //if (UserPertamina)
                                //{
                                //    if (perusahaan.Kode != ConstantPerusahaan.PerusahaanPertaminaKode)
                                //    {
                                //        ViewBag.Error = $"Perushaan pada baris ke {rowIterator} tidak sesuai dengan user Id. Sampai baris ke {rowIterator - 1} sudah terinput";
                                //        _custodyTransferService.AddRange(list);
                                //        return View("_Upload");
                                //    }
                                //}
                                //else
                                //{
                                //    if (perusahaan.Kode == ConstantPerusahaan.PerusahaanPertaminaKode)
                                //    {
                                //        ViewBag.Error = $"Perushaan pada baris ke {rowIterator} tidak sesuai dengan user Id. Sampai baris ke {rowIterator - 1} sudah terinput";
                                //        _custodyTransferService.AddRange(list);
                                //        return View("_Upload");
                                //    }
                                //}

                                obj.PerusahaanKode = user.IDPerusahaan;

                                var stream = _streamService.Find(
                                    i => i.Kode == Stream).FirstOrDefault();

                                var KodeTipeProduk = _tipeProdukService.Get(stream.TipeProdukId).Kode;
                                if (KodeTipeProduk == ConstantTipeProdukKode.Oil)
                                {
                                    obj.Kwh = 0;
                                }
                                else if (KodeTipeProduk == ConstantTipeProdukKode.Hidrogen || KodeTipeProduk == ConstantTipeProdukKode.NSW)
                                {
                                    obj.SG = 0;
                                    obj.Kwh = 0;
                                }
                                else if (KodeTipeProduk == ConstantTipeProdukKode.Electricity)
                                {
                                    obj.MT = 0;
                                    obj.SG = 0;
                                }

                                if (stream == null)
                                {
                                    ViewBag.Error = $"Stream pada baris ke {rowIterator} tidak ditemukan. Sampai baris ke {rowIterator - 1} sudah terinput";
                                    _custodyTransferService.AddRange(list);
                                    return View("_Upload");
                                }

                                obj.StreamId = stream.Id;

                                var checkCustodyTrfList = list.Where(i => i.Tanggal.Date == obj.Tanggal.Date && i.StreamId == obj.StreamId && i.PerusahaanKode == obj.PerusahaanKode);
                                var checkCustodyTrfDB = listCT.Where(i => i.Tanggal.Date == obj.Tanggal.Date && i.StreamId == obj.StreamId && i.PerusahaanKode == obj.PerusahaanKode);

                                if (checkCustodyTrfDB.Count() > 0 || checkCustodyTrfList.Count() > 0)
                                {
                                    ViewBag.Error = $"Tanggal dan Stream pada baris ke {rowIterator} sudah ada. Sampai baris ke {rowIterator - 1} sudah terinput";
                                    _custodyTransferService.AddRange(list);
                                    return View("_Upload");
                                }

                                list.Add(obj);
                                //_supplyLossDischargeService.Save(obj);
                            }

                            _custodyTransferService.AddRange(list);
                        }
                    }
                }
                return Content("success");
            }
            //catch (DbEntityValidationException ex)
            //{

            //    ViewBag.Error = $"baris ke {rowIterator} :";
            //    foreach (var entityValidationErrors in ex.EntityValidationErrors)
            //    {
            //        foreach (var validationError in entityValidationErrors.ValidationErrors)
            //        {
            //            ViewBag.Error += $"<br>{validationError.ErrorMessage}";
            //            //Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
            //        }
            //    }
            //    return View("_Upload");
            //}
            //catch (DbUpdateException)
            //{
            //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
            //    return View("_Upload");
            //}
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Terdapat data kosong pada baris ke {rowIterator}";
                _custodyTransferService.AddRange(list);
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }
        }

        public ActionResult ExportExcel(DateTime TanggalDari, DateTime TanggalSampai, string Stream)
        {
            try
            {
                List<DateTime> Tanggal = new List<DateTime>();
                //var TanggalDari = new DateTime(2018, 3, 1);
                //var TanggalSampai = new DateTime(2018, 3, 31);
                for (int i = 0; i <= (TanggalSampai - TanggalDari).TotalDays; i++)
                {
                    Tanggal.Add(TanggalDari.AddDays(i));
                }

                var StreamList = _streamService.GetAllJoinData().Where(i => i.StreamName.ToLower().Contains(Stream)).OrderBy(i => i.TipeProdukId);
                var StreamHidrogenList = _streamService.GetAllJoinData().Where(i => i.TipeProduk.Kode == ConstantTipeProdukKode.Oil && i.StreamName.ToLower().Contains(Stream)).OrderBy(i => i.TipeProdukId);

                var Tabel21List = _table21Service.GetAll();
                var Tabel52List = _table52Service.GetAll();
                var Tabel56List = _table56Service.GetAll();

                List<CustodyTransferExcelViewModel> ViewModelList = new List<CustodyTransferExcelViewModel>();
                List<CustodyTransferSGViewModel> ViewModelList2 = new List<CustodyTransferSGViewModel>();
                foreach (var item in StreamList)
                {
                    var Model = new CustodyTransferExcelViewModel();
                    Model.StreamName = item.StreamName;
                    Model.KodeTipeProduk = item.TipeProduk.Kode;

                    var MTPertamina = _custodyTransferService.Find(i => i.Tanggal >= TanggalDari && i.Tanggal <= TanggalSampai && i.PerusahaanKode == ConstantPerusahaan.PerusahaanPertaminaKode && i.StreamId == item.Id).ToList();
                    var MTPTSK = _custodyTransferService.Find(i => i.Tanggal >= TanggalDari && i.Tanggal <= TanggalSampai && i.PerusahaanKode == ConstantPerusahaan.PerusahaanPTSKKode && i.StreamId == item.Id).ToList();

                    var list = (from tgl in Tanggal
                                join mtPert in MTPertamina on tgl.Date equals mtPert.Tanggal.Date into newli
                                from NewList in newli.DefaultIfEmpty()
                                select new CustodyTransferTableExcelViewModel
                                {
                                    Tanggal = tgl,
                                    MtRU2 = NewList != null ? NewList.MT : 0,
                                    CTRU2 = NewList != null ? NewList : null
                                }).ToList();

                    var CTList = (from tgl in list
                                  join mtptsk in MTPTSK on tgl.Tanggal.Date equals mtptsk.Tanggal.Date into newli
                                  from NewList in newli.DefaultIfEmpty()
                                  select new CustodyTransferTableExcelViewModel
                                  {
                                      Tanggal = tgl.Tanggal,
                                      MtRU2 = tgl.MtRU2,
                                      CTRU2 = tgl.CTRU2,
                                      MtPTSK = NewList != null ? NewList.MT : 0,
                                      CTPTSK = NewList != null ? NewList : null
                                  }).ToList();

                    foreach (var item2 in CTList)
                    {
                        item2.DensityRU2 = item2.CTRU2 != null ? HitungDensity(item2.CTRU2.SG, Tabel21List.ToList()) : 0;
                        item2.LiterRU2 = item2.CTRU2 != null ? HitungLiter15C(item2.DensityRU2, item2.MtRU2, Tabel56List.ToList()) : 0;
                        item2.BB60RU2 = item2.CTRU2 != null ? HitungBB60(item2.DensityRU2, item2.LiterRU2, Tabel52List.ToList()) : 0;

                        item2.DensityPTSK = item2.CTPTSK != null ? HitungDensity(item2.CTPTSK.SG, Tabel21List.ToList()) : 0;
                        item2.LiterPTSK = item2.CTPTSK != null ? HitungLiter15C(item2.DensityPTSK, item2.MtPTSK, Tabel56List.ToList()) : 0;
                        item2.BB60PTSK = item2.CTPTSK != null ? HitungBB60(item2.DensityPTSK, item2.LiterPTSK, Tabel52List.ToList()) : 0;
                    }

                    Model.Table = CTList;
                    ViewModelList.Add(Model);
                }

                foreach (var item in StreamHidrogenList)
                {
                    var Model2 = new CustodyTransferSGViewModel();
                    Model2.StreamName = item.StreamName;

                    var MTPertamina = _custodyTransferService.Find(i => i.Tanggal >= TanggalDari && i.Tanggal <= TanggalSampai && i.PerusahaanKode == ConstantPerusahaan.PerusahaanPertaminaKode && i.StreamId == item.Id).ToList();

                    var SGList = (from tgl in Tanggal
                                  join mtPert in MTPertamina on tgl.Date equals mtPert.Tanggal.Date into newli
                                  from NewList in newli.DefaultIfEmpty()
                                  select new CustodyTransferSGTableViewModel
                                  {
                                      Tanggal = tgl,
                                      SG = NewList != null ? NewList.SG : 0,
                                  }).ToList();


                    Model2.Table = SGList;
                    ViewModelList2.Add(Model2);

                }

                string file = Server.MapPath("~/ExcelTemplate/tes.xlsx");
                if (System.IO.File.Exists(file))
                    System.IO.File.Delete(file);

                System.IO.FileInfo newFile = new System.IO.FileInfo(file);

                using (ExcelPackage excel = new ExcelPackage(newFile))
                {
                    var ws = excel.Workbook.Worksheets.Add("Sheet 1");
                    ws.DefaultRowHeight = 12;
                    //ws.TabColor = System.Drawing.Color.Black;
                    ws.Column(1).Width = 1.57;

                    //var numStyle = excel.Workbook.Styles.CreateNamedStyle("Style");
                    //numStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //numStyle.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //numStyle.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //numStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //numStyle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //numStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    int ColumnAwal = 2;

                    foreach (var item in ViewModelList)
                    {
                        ws.Column(ColumnAwal).Width = 13;

                        if (item.KodeTipeProduk == ConstantTipeProdukKode.Oil)
                        {
                            for (int i = ColumnAwal + 1; i <= ColumnAwal + 9; i++)
                            {
                                ws.Column(i).Width = 15;
                            }

                            ws.Cells[2, ColumnAwal, 4, ColumnAwal].Merge = true;
                            ws.Cells[2, ColumnAwal + 1, 2, ColumnAwal + 9].Merge = true;
                            ws.Cells[3, ColumnAwal + 1, 3, ColumnAwal + 3].Merge = true;
                            ws.Cells[3, ColumnAwal + 4, 3, ColumnAwal + 6].Merge = true;
                            ws.Cells[3, ColumnAwal + 7, 3, ColumnAwal + 9].Merge = true;

                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Font.Bold = true;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(189, 215, 238));
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            //Header of table  
                            ws.Cells[2, ColumnAwal + 0].Value = "Date";
                            ws.Cells[2, ColumnAwal + 1].Value = item.StreamName; //nama stream
                            ws.Cells[3, ColumnAwal + 1].Value = "Mton";
                            ws.Cells[3, ColumnAwal + 4].Value = "Liter 15";
                            ws.Cells[3, ColumnAwal + 7].Value = "BB60";
                            ws.Cells[4, ColumnAwal + 1].Value = "RU II";
                            ws.Cells[4, ColumnAwal + 2].Value = "PTSK";
                            ws.Cells[4, ColumnAwal + 3].Value = "Delta";
                            ws.Cells[4, ColumnAwal + 4].Value = "RU II";
                            ws.Cells[4, ColumnAwal + 5].Value = "PTSK";
                            ws.Cells[4, ColumnAwal + 6].Value = "Delta";
                            ws.Cells[4, ColumnAwal + 7].Value = "RU II";
                            ws.Cells[4, ColumnAwal + 8].Value = "PTSK";
                            ws.Cells[4, ColumnAwal + 9].Value = "Delta";

                            var recordIndex = 5;
                            foreach (var item2 in item.Table)
                            {
                                ws.Cells[recordIndex, ColumnAwal + 0].Value = item2.Tanggal.ToString("dd MMM yyyy");
                                ws.Cells[recordIndex, ColumnAwal + 1].Value = item2.MtRU2;
                                ws.Cells[recordIndex, ColumnAwal + 2].Value = item2.MtPTSK;
                                ws.Cells[recordIndex, ColumnAwal + 3].Value = item2.MtRU2 - item2.MtPTSK;

                                ws.Cells[recordIndex, ColumnAwal + 4].Value = item2.LiterRU2;
                                ws.Cells[recordIndex, ColumnAwal + 5].Value = item2.LiterPTSK;
                                ws.Cells[recordIndex, ColumnAwal + 6].Value = item2.LiterRU2 - item2.LiterPTSK;

                                ws.Cells[recordIndex, ColumnAwal + 7].Value = item2.BB60RU2;
                                ws.Cells[recordIndex, ColumnAwal + 8].Value = item2.BB60PTSK;
                                ws.Cells[recordIndex, ColumnAwal + 9].Value = item2.BB60RU2 - item2.BB60PTSK;

                                recordIndex++;
                            }

                            ws.Cells[recordIndex, ColumnAwal].Value = "Total";
                            ws.Cells[recordIndex, ColumnAwal + 1].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 1, recordIndex - 1, ColumnAwal + 1).Address);
                            ws.Cells[recordIndex, ColumnAwal + 2].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 2, recordIndex - 1, ColumnAwal + 2).Address);
                            ws.Cells[recordIndex, ColumnAwal + 3].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 3, recordIndex - 1, ColumnAwal + 3).Address);
                            ws.Cells[recordIndex, ColumnAwal + 4].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 4, recordIndex - 1, ColumnAwal + 4).Address);
                            ws.Cells[recordIndex, ColumnAwal + 5].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 5, recordIndex - 1, ColumnAwal + 5).Address);
                            ws.Cells[recordIndex, ColumnAwal + 6].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 6, recordIndex - 1, ColumnAwal + 6).Address);
                            ws.Cells[recordIndex, ColumnAwal + 7].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 7, recordIndex - 1, ColumnAwal + 7).Address);
                            ws.Cells[recordIndex, ColumnAwal + 8].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 8, recordIndex - 1, ColumnAwal + 8).Address);
                            ws.Cells[recordIndex, ColumnAwal + 9].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 9, recordIndex - 1, ColumnAwal + 9).Address);


                            ws.Cells[recordIndex, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Font.Bold = true;

                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            ColumnAwal += 11;
                        }
                        else
                        {
                            for (int i = ColumnAwal + 1; i <= ColumnAwal + 3; i++)
                            {
                                ws.Column(i).Width = 15;
                            }

                            ws.Cells[2, ColumnAwal, 4, ColumnAwal].Merge = true;
                            ws.Cells[2, ColumnAwal + 1, 2, ColumnAwal + 3].Merge = true;
                            ws.Cells[3, ColumnAwal + 1, 3, ColumnAwal + 3].Merge = true;

                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Font.Bold = true;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(189, 215, 238));
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            ws.Cells[2, ColumnAwal, 4, ColumnAwal + 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            //Header of table  
                            ws.Cells[2, ColumnAwal + 0].Value = "Date";
                            ws.Cells[2, ColumnAwal + 1].Value = item.StreamName; //nama stream
                            ws.Cells[3, ColumnAwal + 1].Value = "Mton";
                            ws.Cells[4, ColumnAwal + 1].Value = "RU II";
                            ws.Cells[4, ColumnAwal + 2].Value = "PTSK";
                            ws.Cells[4, ColumnAwal + 3].Value = "Delta";

                            var recordIndex = 5;
                            foreach (var item2 in item.Table)
                            {
                                ws.Cells[recordIndex, ColumnAwal + 0].Value = item2.Tanggal.ToString("dd MMM yyyy");
                                ws.Cells[recordIndex, ColumnAwal + 1].Value = item2.MtRU2;
                                ws.Cells[recordIndex, ColumnAwal + 2].Value = item2.MtPTSK;
                                ws.Cells[recordIndex, ColumnAwal + 3].Value = item2.MtRU2 - item2.MtPTSK;

                                recordIndex++;
                            }

                            ws.Cells[recordIndex, ColumnAwal].Value = "Total";
                            ws.Cells[recordIndex, ColumnAwal + 1].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 1, recordIndex - 1, ColumnAwal + 1).Address);
                            ws.Cells[recordIndex, ColumnAwal + 2].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 2, recordIndex - 1, ColumnAwal + 2).Address);
                            ws.Cells[recordIndex, ColumnAwal + 3].Formula = string.Format("SUM({0})", new ExcelAddress(3, ColumnAwal + 3, recordIndex - 1, ColumnAwal + 3).Address);

                            ws.Cells[recordIndex, ColumnAwal, recordIndex, ColumnAwal + 9].Style.Font.Bold = true;

                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ws.Cells[5, ColumnAwal, recordIndex, ColumnAwal + 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            ColumnAwal += 5;
                        }
                    }


                    var ws2 = excel.Workbook.Worksheets.Add("Sheet 2");
                    ws2.DefaultRowHeight = 12;
                    //ws2.TabColor = System.Drawing.Color.Black;
                    ws2.Column(1).Width = 1.57;

                    ws2.Cells[2, 2, 2, ViewModelList2.Count() + 2].Merge = true;
                    ws2.Cells[3, 2, 3, ViewModelList2.Count() + 2].Merge = true;
                    ws2.Cells[2, 2].Value = "DataSpecific Gravity 60/60 oF";
                    ws2.Cells[3, 2].Value = $"{TanggalDari.ToString("dd MMM yyyy")} - {TanggalSampai.ToString("dd MMM yyyy")}";

                    ws2.Cells[2, 2, 3, ViewModelList2.Count() + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws2.Cells[2, 2, 5, ViewModelList2.Count() + 2].Style.Font.Bold = true;
                    ws2.Cells[5, 2, 5, ViewModelList2.Count() + 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws2.Cells[5, 2, 5, ViewModelList2.Count() + 2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(189, 215, 238));

                    var index = 6;
                    ws2.Cells[5, 2].Value = "Tanggal";
                    foreach (var item in Tanggal)
                    {
                        ws2.Cells[index, 2].Value = item.ToString("dd MMM yyyy");
                        index++;
                    }

                    var column = 3;
                    foreach (var item in ViewModelList2)
                    {
                        index = 5;
                        ws2.Cells[index, column].Value = item.StreamName;

                        foreach (var item2 in item.Table)
                        {
                            index++;
                            ws2.Cells[index, column].Value = item2.SG;
                        }

                        column++;
                    }

                    ws2.Cells[5, 2, index, column - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws2.Cells[5, 2, index, column - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws2.Cells[5, 2, index, column - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws2.Cells[5, 2, index, column - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws2.Cells[5, 2, index, column - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    for (int i = 2; i <= ViewModelList2.Count() + 2; i++)
                    {
                        ws2.Column(i).Width = 15;
                    }

                    //using (var memoryStream = new System.IO.MemoryStream())
                    //{
                    //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //    Response.AddHeader("content-disposition", "attachment; filename=" + "ReportCustodyTransfer" + ".xlsx");
                    //    excel.SaveAs(memoryStream);
                    //    memoryStream.WriteTo(Response.OutputStream);
                    //    Response.Flush();
                    //    Response.End();
                    //}

                    using (var memoryStream = new System.IO.MemoryStream())
                    {
                        string Url = @_konfigurasiParameter.Find(i => i.Key == ConstantKonfigurasiParameter.CustodyTransferReportExcel).FirstOrDefault().Value;
                        string path = System.IO.Path.Combine(Url + "ReportCustodyTransfer.xlsx");
                        //string path = Server.MapPath("~/ExcelTemplate/ReportCustodyTransfer.xlsx");
                        var stream = System.IO.File.Create(path);
                        excel.SaveAs(stream);
                        stream.Close();
                        Response.Flush();
                        Response.End();
                    }

                    //excel.SaveAs(stream);

                    //excel.Save();
                }

                return Content("ok");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpGet]
        public virtual ActionResult Download()
        {
            string Url = @_konfigurasiParameter.Find(i => i.Key == ConstantKonfigurasiParameter.CustodyTransferReportExcel).FirstOrDefault().Value;
            string fullPath = System.IO.Path.Combine(Url + "ReportCustodyTransfer.xlsx");
            //string fullPath = System.IO.Path.Combine(Server.MapPath("~/ExcelTemplate"), "ReportCustodyTransfer.xlsx");
            return File(fullPath, "application/vnd.ms-excel", "ReportCustodyTransfer.xlsx");
        }

        private decimal HitungDensity(decimal SG, List<Tabel21> Tabel21)
        {
            if (SG == 0) //Tanda Kutip yang di DS
                return 0;

            decimal sg1 = (Math.Truncate(SG * 1000) / 1000);
            decimal sg2 = sg1 + (decimal)0.001;
            Tabel21 sg3obj = Tabel21.Where(i => i.SG == sg1).FirstOrDefault();
            Tabel21 sg4obj = Tabel21.Where(i => i.SG == sg2).FirstOrDefault();

            if (sg3obj == null)// Master Ga ada
                return 0;
            if (sg4obj == null)// Master Ga ada
                return 0;

            decimal sg3 = sg3obj.Density15;
            decimal sg4 = sg4obj.Density15;

            var density = sg3 + ((SG - sg1) / (decimal)0.001 * (sg4 - sg3));

            return density;

        }
        private decimal HitungLiter15C(decimal density, decimal MT, List<Tabel56> Tabel56)
        {
            if (MT == 0) // Kutip di DS
                return 0;

            if (density == 0) // Kutip di DS
                return 0;

            decimal d1 = Math.Truncate(density * 1000) / 1000;
            decimal d2 = d1 + (decimal)0.001;
            decimal d3 = d1 + (decimal)0.001;
            decimal d4 = d2 + (decimal)0.001;
            Tabel56 d5Obj = Tabel56.Where(i => i.Density15 == d1).FirstOrDefault();
            Tabel56 d6Obj = Tabel56.Where(i => i.Density15 == d2).FirstOrDefault();
            if (d5Obj == null) // Master Ga ada
                return 0;

            if (d6Obj == null) // Master Ga ada
                return 0;

            decimal d5 = d5Obj.LiterMetricTon;
            decimal d6 = d6Obj.LiterMetricTon;

            decimal Liter15 = MT * (d5 + ((density - d3) / (decimal)0.001 * (d6 - d5)));

            return Liter15;

        }
        private decimal HitungBB60(decimal density, decimal liter15, List<Tabel52> Tabel52)
        {
            Tabel52 cf = Tabel52.Where(i => i.Density15 == density).FirstOrDefault();
            if (cf == null)
                return 0;

            var CFBarrel = cf.Barrel60F;
            var Barrel60F = (liter15 * CFBarrel) / 1000;

            return Barrel60F;
        }

        public DataTable ExecuteQuery(string query)
        {
            DataTable dt = new DataTable();
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    //sql data adapter
                    SqlDataAdapter sqlD = new SqlDataAdapter(cmd);
                    sqlD.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

        }

    }
}