﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class TipeProdukController : Controller
    {
        private ITipeProdukService _tipeProdukService;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public TipeProdukController(ITipeProdukService TipeProdukService)
        {
            _tipeProdukService = TipeProdukService;
        }
        // GET: MasterTipeProduk
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            var tipeProduk = _tipeProdukService.GetAll().ToList();
            tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
            ViewBag.TipeProduk = tipeProduk;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Kode, string Deskripsi)
        {
            var obj = _tipeProdukService.SearchTipeProduk(requestModel.Start, requestModel.Length, Kode, Deskripsi);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Kode = camp.Kode,
                Deskripsi = camp.Deskripsi
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int id = 0)
        {
            var tipeProduk = _tipeProdukService.Get(id);
            ViewBag.Title = "EDIT";
            if (tipeProduk == null)
            {
                tipeProduk = new TipeProduk();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", tipeProduk);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(TipeProduk model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                return View("_CreateEdit", model);
            }
            try
            {
                model.Kode = model.Kode.Trim();
                _tipeProdukService.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                ModelState.AddModelError("Code", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                var tipeProduk = _tipeProdukService.GetAll().ToList();
                tipeProduk.Insert(0, new TipeProduk() { Id = 0, Deskripsi = "" });
                ViewBag.TipeProduk = tipeProduk;
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        //public ActionResult Delete(int Id)
        //{
        //    var tipeProduk = _tipeProdukService.Get(Id);
        //    return PartialView("_Delete", tipeProduk);
        //}

        // POST: Admin/Role/Delete/5
        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteAction(int id)
        //{
        //    try
        //    {
        //        _tipeProdukService.Delete(id);
        //        return Content("success");
        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        var sqlException = ex.GetBaseException() as SqlException;
        //        var tipeProduk = _tipeProdukService.Get(id);

        //        if (sqlException != null)
        //        {
        //            var number = sqlException.Number;

        //            if (number == 547)
        //            {
        //                ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
        //                return View("_Delete", tipeProduk);
        //            }
        //        }
        //        ModelState.AddModelError("", ex.Message);
        //        return View("_Delete", tipeProduk);
        //    }
        //    catch (Exception ex)
        //    {
        //        var tipeProduk = _tipeProdukService.Get(id);
        //        ModelState.AddModelError("", ex.Message);
        //        return View("_Delete", tipeProduk);
        //    }
        //}
    }
}