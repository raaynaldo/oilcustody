﻿using DataTables.Mvc;
using OfficeOpenXml;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services.WebCustodyTransfer;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class Tabel57Controller : Controller
    {
        private ITabel57Service _tabel57Service;
        private CurrentUser user = CurrentUser.GetCurrentUser();
        public Tabel57Controller(ITabel57Service Tabel57Service)
        {
            _tabel57Service = Tabel57Service;
        }
        // GET: MasterTabel57
        public ActionResult Index()
        {
            ViewBag.Create = user.ActionCreate;
            ViewBag.Edit = user.ActionUpdate;
            ViewBag.Delete = user.ActionDelete;

            return View();
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, decimal Density, decimal Liter)
        {
            var obj = _tabel57Service.SearchTabel57(requestModel.Start, requestModel.Length, Density, Liter);

            var data = obj.Item1.Select(camp => new
            {
                Id = camp.Id,
                Density = camp.Density15,
                Liter = camp.LongTerm100L,
            });
            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Upload()
        {
            return PartialView("_Upload");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase FileUpload)
        {
            int rowIterator = 0;
            List<Tabel57> Tabel57List = new List<Tabel57>();

            try
            {
                if (FileUpload != null)
                {
                    if ((FileUpload != null) && (FileUpload.ContentLength > 0) && !string.IsNullOrEmpty(FileUpload.FileName))
                    {
                        string fileName = FileUpload.FileName;
                        string fileContentType = FileUpload.ContentType;
                        byte[] fileBytes = new byte[FileUpload.ContentLength];
                        var data = FileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(FileUpload.ContentLength));

                        List<Tabel57> ListTabel57 = _tabel57Service.GetAll().ToList();
                        using (var package = new ExcelPackage(FileUpload.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            if (workSheet.Cells[1, 1].Value.ToString() != "Density 15" ||
                                workSheet.Cells[1, 2].Value.ToString() != "Long Term/100L" ||
                                noOfCol > 2)
                            {
                                ViewBag.Error = $"Maaf template yang anda upload salah.<br>Mohon downlod kembali template Table 57.";
                                return View("_Upload");
                            }
                            if (noOfRow == 1)
                            {
                                ViewBag.Error = $"Harap masukan data terlebih dahulu.";
                                return View("_Upload");
                            }
                            for (rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                var obj = new Tabel57();
                                obj.Density15 = decimal.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                                if (ListTabel57.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null || Tabel57List.Where(i => i.Density15 == obj.Density15).FirstOrDefault() != null)
                                {
                                    ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Density15 baris ke {rowIterator} gagal, karena data sudah ada.";
                                    if (rowIterator == 2)
                                        ViewBag.Error = $"Density15 baris ke {rowIterator} gagal, karena data sudah ada.<br>Tidak ada data yang terupload.";

                                    _tabel57Service.AddRange(Tabel57List);

                                    return View("_Upload");
                                }

                                obj.LongTerm100L = decimal.Parse(workSheet.Cells[rowIterator, 2].Value.ToString());

                                Tabel57List.Add(obj);
                            }

                            _tabel57Service.AddRange(Tabel57List);
                        }
                    }
                }
                return Content("success");
            }
            //catch (DbUpdateException ex)
            //{
            //    ViewBag.Error = $"Code baris ke {rowIterator} sudah ada.";
            //    return View("_Upload");
            //}
            catch (NullReferenceException)
            {
                ViewBag.Error = $"Data sudah diupload sampai baris ke {rowIterator - 1} <br>Terdapat data kosong pada baris ke {rowIterator}";
                _tabel57Service.AddRange(Tabel57List);
                //ModelState.AddModelError("", $"Terdapat data kosong pada baris ke {rowIterator}");
                return View("_Upload");
            }
            catch (Exception ex)
            {
                ViewBag.Error = $"Input Gagal, tidak ada data yang diinput.";
                ModelState.AddModelError("", ex.Message);
                return View("_Upload");
            }

        }

        public ActionResult Create(int id = 0)
        {
            var tabel57 = _tabel57Service.Get(id);
            ViewBag.Title = "EDIT";
            if (tabel57 == null)
            {
                tabel57 = new Tabel57();
                ViewBag.Title = "ADD";
            }

            return PartialView("_CreateEdit", tabel57);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(Tabel57 model)
        {
            ViewBag.Title = model.Id == 0 ? "ADD" : "EDIT";
            if (!ModelState.IsValid)
            {
                return View("_CreateEdit", model);
            }
            try
            {
                _tabel57Service.Save(model);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Density15", "Data sudah ada");
                return View("_CreateEdit", model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("_CreateEdit", model);
            }
        }

        public ActionResult Delete(int Id)
        {
            var tabel57 = _tabel57Service.Get(Id);
            return PartialView("_Delete", tabel57);
        }

        // POST: Admin/Role/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            try
            {
                _tabel57Service.Delete(id);
                return Content("success");
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                var tabel57 = _tabel57Service.Get(id);

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        ModelState.AddModelError("", "Data tidak dapat dihapus karena sudah digunakan.");
                        return View("_Delete", tabel57);
                    }
                }
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel57);
            }
            catch (Exception ex)
            {
                var tabel57 = _tabel57Service.Get(id);
                ModelState.AddModelError("", ex.Message);
                return View("_Delete", tabel57);
            }
        }

        public JsonResult GetLongTonsFromTabel57(decimal mt, decimal density, decimal liter15)
        {
            decimal a = Math.Truncate(density * 1000) / 1000;
            decimal b = a + (decimal)0.001;
            decimal c = a + (decimal)0.001;
            decimal d = b + (decimal)0.001;
            Tabel57 eObj = _tabel57Service.Find(i => i.Density15 == c).FirstOrDefault();
            Tabel57 fObj = _tabel57Service.Find(i => i.Density15 == d).FirstOrDefault();
            if (eObj == null)
                return Json(new { LongTons = "Master Tabel57 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            if (fObj == null)
                return Json(new { LongTons = "Master Tabel57 tidak ditemukan", val = false }, JsonRequestBehavior.AllowGet);

            decimal e = eObj.LongTerm100L;
            decimal f = fObj.LongTerm100L;
            decimal LongTons = (liter15 * (e + ((density - a) / (decimal)0.001 * (f - e)))) / 1000;
            //LongTons = Math.Truncate(LongTons * 1000) / 1000;
            return Json(new { LongTons = LongTons, val = true }, JsonRequestBehavior.AllowGet);
        }
    }
}