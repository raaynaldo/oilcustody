﻿using DataTables.Mvc;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Presentation.Common;
using ProjectPertamina.Services;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Controllers
{
    [PertaminaAuthorize]
    public class FinanceJettyController : Controller
    {
        private CurrentUser user = CurrentUser.GetCurrentUser();
        private IFinanceJettyService _financeJettyService;
        private IFinanceJettyDetailService _financeJettyDetailService;
        private IShiftService _shiftService;
        private IPICKeuanganService _picKeuanganService;
        private IPICLoadingMasterService _picLoadingService;
        private ICargoService _cargoService;
        private ITangkiService _tangkiService;

        public FinanceJettyController(IFinanceJettyService FinanceJettyService, IFinanceJettyDetailService FinanceJettyDetailService,
            IShiftService Shift, IPICKeuanganService PICKeuangan, ICargoService Cargo, IPICLoadingMasterService PICLoading, ITangkiService TangkiService)
        {
            _financeJettyService = FinanceJettyService;
            _financeJettyDetailService = FinanceJettyDetailService;
            _shiftService = Shift;
            _picKeuanganService = PICKeuangan;
            _picLoadingService = PICLoading;
            _cargoService = Cargo;
            _tangkiService = TangkiService;
        }
        // GET: FinanceJetty
        public ActionResult Index()
        {
            //ViewBag.Create = user.ActionCreate;
            //ViewBag.Edit = user.ActionUpdate;
            //ViewBag.Delete = user.ActionDelete;

            ViewBag.Create = true;
            ViewBag.Edit = true;
            ViewBag.Delete = true;

            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "All", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "All", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            var picLoading = _picLoadingService.GetAll().ToList();
            picLoading.Insert(0, new PICLoadingMaster() { Nama = "All", Id = 0 });
            ViewBag.picLoading = picLoading;

            return View();
        }

        [HttpPost]
        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel
            , string Jetty,
            int IDShift,
            int IDPICKeuangan,
            int IDLoadingMaster
            , DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            var obj = _financeJettyService.SearchFinanceJetty(requestModel.Start, requestModel.Length,
                Jetty, IDShift, IDPICKeuangan, IDLoadingMaster, TanggalDari, TanggalSampai);

            var data = obj.Item1.Select(fj => new
            {
                Id = fj.Id,
                Tanggal = fj.Tanggal,
                Shift = fj.Shift.Description,
                PICKeuangan = fj.PICKeuangan.Nama,
                PICLoadingMaster = fj.PICLoadingMaster.Nama,
                JumlahTangki = fj.JumlahTangki,
                Jetty = fj.Jetty,
                Quantity = fj.Qty,
                EstimasiStart = fj.EstimasiStart,
                EstimasiStop = fj.EstimasiStop
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexDetail(int Id = 0)
        {
            var shift = _shiftService.GetAll().ToList();
            shift.Insert(0, new Shift() { Description = "", Id = 0 });
            ViewBag.shift = shift;

            var picKeuangan = _picKeuanganService.GetAll().ToList();
            picKeuangan.Insert(0, new PICKeuangan() { Nama = "", Id = 0 });
            ViewBag.picKeuangan = picKeuangan;

            var loading = _picLoadingService.GetAll().ToList();
            loading.Insert(0, new PICLoadingMaster() { Nama = "", Id = 0 });
            ViewBag.picLoading = loading;

            var cargo = _cargoService.GetAll().ToList();
            cargo.Insert(0, new Cargo() { Description = "", Id = 0 });
            ViewBag.cargo = cargo;

            if (Id == 0)
            {
                ViewBag.Title = "ADD";
                return View(new FinanceJetty());
            }
            else
            {
                ViewBag.Title = "EDIT";
                var financeJetty = _financeJettyService.Get(Id);

                return View(financeJetty);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEdit(FinanceJetty model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("IndexDetail", model);
            try
            {
                _financeJettyService.Save(model);
                TempData["MessageSucces"] = "Data berhasil disimpan !";

                return RedirectToAction("IndexDetail", new { Id = model.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return RedirectToAction("IndexDetail", model.Id);
            }

        }

        public ActionResult Delete(int Id)
        {
            var financeJetty = _financeJettyService.Get(Id);

            return PartialView("_Delete", financeJetty);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteAction(int id)
        {
            string message = string.Empty;
            try
            {
                var berita = _financeJettyService.Get(id);

                _financeJettyService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }


        #region detail finance jetty
        public JsonResult getDlCargo()
        {
            return Json(_cargoService.GetAll(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDlTangki()
        {
            return Json(_tangkiService.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailJetty([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int IDHeader)
        {
            var obj = _financeJettyDetailService.SearchFinanceJettyDetail(requestModel.Start, requestModel.Length,
                IDHeader);

            var data = obj.Item1.Select(fjd => new
            {
                Id = fjd.Id,
                NamaCargo = fjd.Cargo.Description,
                NomorTangki = fjd.Tangki.Code,
                IdCargo = fjd.CargoId,
                IdTangki = fjd.TangkiId
            });

            return Json(new DataTablesResponse(requestModel.Draw, data.ToList(), obj.Item3, obj.Item2), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveDetail(FinanceJettyDetail model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("IndexDetail", model);
            try
            {
                _financeJettyDetailService.Save(model);
                return Content("Success");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }

        }

        public ActionResult DeleteDetail(int Id)
        {
            var financeJetty = _financeJettyDetailService.Get(Id);

            return PartialView("_DeleteDetail", financeJetty);
        }

        [HttpPost, ActionName("DeleteDetail")]
        public ActionResult DeleteDetailAction(int id)
        {
            string message = string.Empty;
            try
            {
                _financeJettyDetailService.Delete(id);
                return Content("success");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        #endregion
    }
}