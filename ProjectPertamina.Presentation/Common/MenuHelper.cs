﻿using ProjectPertamina.UserManager;
using ProjectPertamina.UserManager.Model;
using ProjectPertamina.UserManager.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectPertamina.Presentation.Common
{
    public static class MenuHelper
    {
        #region Public Methods
        private static string namaApp = ConfigurationManager.AppSettings["AppName"];

        private static int getRoleOfUser()
        {
            int roleID = 0;
            CurrentUser user = CurrentUser.GetCurrentUser();
            //get Role
            UserLogin userRole = UserManagementServices.getUserRole(user.UserName, namaApp);
            if (userRole != null)
            {
                roleID = userRole.RoleID;
            }
            return roleID;//userGroup;
        }

        private static List<Menu> getRootMenu(string applName, int roleId)
        {
            List<Menu> menus = UserManagementServices.getMenuRoot(applName, roleId);
            //List<Menu> menus = menuService.FindRootMenuByGLAM(applName, groupName);
            return menus;//menus;
        }

        private static List<Menu> getChildMenu(int idHeader,int roleId)
        {
            List<Menu> menus = UserManagementServices.getChildMenu(idHeader, roleId);
            return menus;
        }

        /// <summary>
        /// Create Menus of SiteMapNodeCollection based on role of user.
        /// </summary>
        /// <param name="applName">Application Name.</param>
        public static SiteMapNodeCollection MenuCreate(string applName)
        {
            int roleId = getRoleOfUser();
            List<Menu> menus = getRootMenu(applName, roleId);
            //Create SiteMapNodeCollection 
            SiteMapNodeCollection nodes = new SiteMapNodeCollection();

            //Create SiteMapNode and add it to collection
            SiteMapNode tempNode;
            SiteMapNode nodeChild;
            SiteMapNodeCollection child;

            foreach (var item in menus)
            {
                tempNode = new SiteMapNode(SiteMap.Provider, item.UrlFile, item.UrlFile, item.MenuName, item.ModuleDescr);
                //Get child menu by root menu
                List<Menu> childMenus = getChildMenu((int)item.Id, roleId);
                //add child menu node
                if (childMenus.Count > 0)
                {
                    child = new SiteMapNodeCollection();
                    foreach (var childItem in childMenus)
                    {
                        nodeChild = new SiteMapNode(SiteMap.Provider, childItem.UrlFile, childItem.UrlFile, childItem.MenuName, childItem.ModuleDescr);
                        //add node as child to child collection of sitemap
                        child.Add(nodeChild);
                    }
                    tempNode.ChildNodes = child;
                }
                //add node to the collection
                nodes.Add(tempNode);
            }
            return nodes;
        }


        /// <summary>
        /// Menus the specified helper.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <returns>A string containing a list of links. The links are rendered in an HTML unordered list <ul> tag.</returns>
        /// <remarks>Extension method that extends the <see cref="HtmlHelper"/> class.</remarks>
        public static string Menu(this HtmlHelper helper, string id, string smp)
        {
            var sb = new StringBuilder();

            // Render each top level node
            //var topLevelNodes = SiteMap.Providers[smp].RootNode.ChildNodes;
            var topLevelNodes = MenuCreate(smp);

            //Create top main menu
            if (!string.IsNullOrEmpty(id) && id.Equals("navbar-nav"))
            {
                // Create opening unordered list tag
                sb.AppendLine("<ul id=\"" + id + "\" class='nav navbar-nav'>");

                // Add all nodes for main menu
                foreach (SiteMapNode mainNode in topLevelNodes)
                {
                    //if (IsAccessibleToUser(HttpContext.Current, mainNode))
                    AddChildNode(sb, mainNode, true);
                }

                // Close unordered list tag
                sb.AppendLine("</ul>");
            }            

            return sb.ToString();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Adds the child node.
        /// </summary>
        /// <param name="sb">The sb.</param>
        /// <param name="node">The node.</param>
        /// <param name="topMenu">Boolean for main menu so that all visible nodes are added to the menu</param>
        private static void AddChildNode(StringBuilder sb, SiteMapNode node, bool topMenu)
        {
            if (node.ChildNodes.Count > 0
                && (node == node.Provider.CurrentNode || IsChildSelected(node.ChildNodes) || HttpContext.Current.Request.Url.AbsoluteUri.Contains(node.Url) || topMenu))
            {

                sb.AppendLine("<li class='dropdown header-menu-dropdown'>");
                sb.AppendLine(CreateMenuItem(node));
                if (topMenu)
                    sb.AppendLine("<ul class='dropdown-menu'>");
                else
                    sb.AppendLine("<ul id=\"sidenav\">");

                foreach (SiteMapNode child in node.ChildNodes)
                    if (IsAccessibleToUser(HttpContext.Current, child))
                        AddChildNode(sb, child, topMenu);
                sb.AppendLine("</ul></li>");
            }
            else
            {
                sb.AppendLine("<li>");
                sb.AppendLine(CreateMenuItem(node));
                sb.AppendLine("</li>");
            }
        }

        /// <summary>
        /// Determines whether [is child selected] [the specified child nodes].
        /// </summary>
        /// <param name="childNodes">The child nodes.</param>
        /// <returns>
        /// 	<c>true</c> if [is child selected] [the specified child nodes]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsChildSelected(SiteMapNodeCollection childNodes)
        {
            return childNodes.Cast<SiteMapNode>().Any(childNode => childNode == childNode.Provider.CurrentNode);
        }

        private static string CreateMenuItem(SiteMapNode node)
        {
            var selected = string.Empty;
            var urlApp = "";
            //var urlApp = "/OilCustody";
            //if (ConfigurationManager.AppSettings["UrlApp"] != "")
            //    urlApp = ConfigurationManager.AppSettings["UrlApp"];
            if (node == node.Provider.CurrentNode || IsChildSelected(node.ChildNodes))
                selected = "class=\"selected\"";

            var target = node["target"];
            if (node.Key == "/Help")
                target = "blank";

            if (!string.IsNullOrEmpty(target))
            {

                if (string.IsNullOrEmpty(node.Description))
                    return string.Format("<a href=\"{0}\" {1} target=\"{2}\">{3}</a>", urlApp + node.Url, selected, target, node.Title);

                //if (string.IsNullOrEmpty(node.Description))
                //    return string.Format("<a href=\"{0}\" {1} target=\"{2}\">{3}</a>", node.Url, selected, target, node.Title);

                //return string.Format("<a href=\"{0}\" title=\"{1}\" {2} target=\"{3}\">{4}</a>", node.Url, node.Description, selected, target, node.Title);
                return string.Format("<a href=\"{0}\" title=\"{1}\" {2} target=\"{3}\">{4}</a>", urlApp + node.Url, node.Description, selected, target, node.Title);
                //return string.Format("<a href=\"{0}\" title=\"{1}\" {2} target=\"{3}\">{4}</a>", node.Url, node.Description, selected, target, node.Title);
            }

            if (string.IsNullOrEmpty(node.Description))
                return string.Format("<a href=\"{0}\" {1}>{2}</a>", urlApp + node.Url, selected, node.Title);

            //if (string.IsNullOrEmpty(node.Description))
            //    return string.Format("<a href=\"{0}\" {1}>{2}</a>", node.Url, selected, node.Title);

            return string.Format("<a href=\"{0}\" title=\"{1}\" {2}>{3}</a>", urlApp + node.Url, node.Description, selected, node.Title);

            //return string.Format("<a href=\"{0}\" title=\"{1}\" {2}>{3}</a>", node.Url, node.Description, selected, node.Title);
        }

        //Check security of the Site Map nodes
        private static bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
        {
            //Node cannot be null
            if (node == null)
                return false;

            //Context cannot be null
            if (context == null)
                return false;

            try
            {
                //Check user role with sitemap node roles
                if ((node.Roles != null) && (node.Roles.Count > 0))
                {
                    //Check each roles in the node
                    foreach (string role in node.Roles)
                    {
                        //Found a role that isn't * and that doesn't match the user role
                        if (!string.Equals(role, "*", StringComparison.InvariantCultureIgnoreCase) && ((context.User == null) || !context.User.IsInRole(role)))
                        {
                            continue;
                        }
                        //Found a match or *
                        return true;
                    }
                }

                //Find nodes with no set security - these are allowed to all
                if (node.Roles == null || node.Roles.Count == 0)
                    return true;

                //Block everything that doesn't explicitly match something above.
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}