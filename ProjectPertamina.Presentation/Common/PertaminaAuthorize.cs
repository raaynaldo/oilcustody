﻿using ProjectPertamina.UserManager.Services;
using ProjectPertamina.UserManager.Model;
using ProjectPertamina.UserManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectPertamina.Presentation.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PertaminaAuthorize : AuthorizeAttribute
    {
        private const string IS_AUTHORIZED = "isAuthorized";

        public string RedirectUrl = "/UnAuthorized";
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);

            httpContext.Items.Add(IS_AUTHORIZED, isAuthorized);

            return isAuthorized;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            var isAuthorized = filterContext.HttpContext.Items[IS_AUTHORIZED] != null
                ? Convert.ToBoolean(filterContext.HttpContext.Items[IS_AUTHORIZED])
                : false;


            if (!IsUserAuthenticated(filterContext))
            {
                //ErrorLogging errLog = new ErrorLogging();
                //errLog.WriteLog(filterContext.Controller.ToString(), filterContext.ToString());
                filterContext.RequestContext.HttpContext.Response.Redirect(RedirectUrl);
            }
        }

        private bool IsUserAuthenticated(AuthorizationContext filterContext)
        {
            string namaApp = ConfigurationManager.AppSettings["AppName"];
            int roleID = 0;
            bool result = true;
            MenuRole mnRole = null;
            CurrentUser user = CurrentUser.GetCurrentUser();
            string controller = filterContext.RequestContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RequestContext.RouteData.Values["action"].ToString();
            var area = filterContext.RequestContext.RouteData.DataTokens["area"];
            string Url = "/" + controller;
            if (area != null)
                Url = "/" + area.ToString() + Url;

            //get Role
            UserLogin userRole = UserManagementServices.getUserRole(user.UserName, namaApp);
            if (userRole != null)
            {
                roleID = userRole.RoleID;
            }
            if (roleID == 0)
            {
                user.ActionView = false;
                user.ActionCreate = false;
                user.ActionUpdate = false;
                user.ActionDelete = false;
                user.ActionApprove = false;
                user.RoleID = 0;
                user.RoleName = "";
                result = false;
            }
            else
            {
                //if (user.InModule != controller)
                //{
                mnRole = UserManagementServices.getAuthor(roleID, Url, namaApp);
                if (mnRole == null)
                {
                    user.ActionView = false;
                    user.ActionCreate = false;
                    user.ActionUpdate = false;
                    user.ActionDelete = false;
                    user.ActionApprove = false;
                    user.RoleID = 0;
                    user.RoleName = "";
                    result = false;
                }
                else
                {
                    user.ActionView = mnRole.ActionView;
                    user.ActionCreate = mnRole.ActionCreate;
                    user.ActionUpdate = mnRole.ActionUpdate;
                    user.ActionDelete = mnRole.ActionDelete;
                    user.ActionApprove = mnRole.ActionApprove;
                    user.RoleID = mnRole.RoleID;
                    user.RoleName = mnRole.Role.Description;

                    result = true;
                }
                //}
            }
            user.InModule = controller;
            return result;
        }
    }
}