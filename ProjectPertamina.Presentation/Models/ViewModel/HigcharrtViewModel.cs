﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectPertamina.Presentation.Models.ViewModel
{
    public class HigcharrtViewModel
    {
        public class SeriesModel<T>
        {
            public string name { get; set; }
            public ICollection<T> data { get; set; }
        }

        public class ChartModel<T>
        {
            public string Title { get; set; }
            public string Subtitle { get; set; }
            public string[] XAxisCategories { get; set; }
            public string XAxisTitle { get; set; }
            public string YAxisTitle { get; set; }
            public string YAxisTooltipValueSuffix { get; set; }
            public ICollection<SeriesModel<T>> Series { get; set; }
        }


        public class LineDateSeriesModel
        {
            public string name { get; set; }
            public double[][] data { get; set; }
        }
    }
}