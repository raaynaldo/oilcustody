﻿//Table Script
    var assetListVM;
    //Login
    // var edit = $('#hdActionedit').val();
    // var hapus = $('#hdActiondelete').val();
    // var visible = true;
    // if (edit == "False" && hapus == "False")
    //     visible = false;
    //Login
    $(function () {
        assetListVM = {
            dt: null,

            init: function () {
                dt = $('#Table').DataTable({
                    "serverSide": true,
                    "processing": true,
                    "searching": false,
                    "ajax": {
                        "url": "@Url.Action('Get','SupplyLoss')",
                        "data": function (data) {
                            //function search
                            data.Code = $("#Code").val();
                            data.Desc = $("#Description").val();
                        }
                    },
                    // "columnDefs": [
                    //     {
                    //         "targets": [0],
                    //         "visible": visible
                    //     },
                    // ],
                    "columns": [
                        {
                            "className": "dt-center",
                            "width": "100px",
                            "title": "<span class='glyphicon glyphicon-cog'></span>",
                            "data": "Id",
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                // var action = "";
                                // if (edit == "True") {
                                //     action = '<a class="btn btn-primary btn-sm editAsset" href="@Url.Action("Create", "Cargo")?Id=' + data + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
                                // }
                                // if (hapus == "True") {
                                //     action = action + ' <a href="@Url.Action("Delete", "Cargo")?Id=' + data + '" class="delete btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                                // }
                                // return action;
                                '<a class="btn btn-primary btn-sm editAsset" href="@Url.Action("Create", "Cargo")?Id=' + data + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
                                '<a href="@Url.Action("Delete", "Cargo")?Id=' + data + '" class="delete btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
                                
                            }
                        },
                        { "title": "Tanggal", "data": "Tanggal", "sortable": false },
                        { "title": "Loading Master", "data": "LoadingMaster", "sortable": false },
                        { "title": "Keterangan Losses", "data": "KeteranganLosses", "sortable": false },
                        { "title": "Berita Acara", "data": "BeritaAcara", "sortable": false },
                        
                    ],
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "dom": '<"top">rt<"bottom"lip><"clear">'
                });
            },

            refresh: function () {
                dt.ajax.reload();
            }
        }
        $('#btnSearch').on("click", assetListVM.refresh);

        assetListVM.init();

        $("#btnCreate").on("click", function () {

            var url = $(this).data("url");

            $.get(url, function (data) {
                $('#createContainer').html(data);

                $('#createModal').modal('show');
            });

        });

        $("#btnUploadExcel").on("click", function () {

            var url = $(this).data("url");

            $.get(url, function (data) {
                $('#createContainer').html(data);

                $('#createModal').modal('show');
            });

        });

        $('#Table').on("click", ".editAsset", function (event) {

            event.preventDefault();

            var url = $(this).attr("href");

            $.get(url, function (data) {
                $('#createContainer').html(data);

                $('#createModal').modal('show');
            });

        });

        $('#Table').on("click", ".delete", function (event) {

            event.preventDefault();

            var url = $(this).attr("href");

            $.get(url, function (data) {
                $('#createContainer').html(data);

                $('#createModal').modal('show');
            });

        });

    });