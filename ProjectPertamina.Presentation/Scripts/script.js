﻿$(document).ready(function () {
    $(function () {
        $('.pop').on('click', function () {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
            return false;
        });
    });

    $(".dropdown-large, .dropdown-menu-large").hover(function () {
        $(".dropdown-menu-large").css("display", "block");
    }, function () {
        $(".dropdown-menu-large").css("display", "none");
    });

    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 1000);
    //

    $('.datetimepicker').datetimepicker({
        //defaultDate : Date.today(),
        ignoreReadonly: true,
        format: 'DD MMM YYYY',
        useCurrent: false
    });

    $('.timepicker').datetimepicker({
        ignoreReadonly: true,
        format: 'HH:mm'
    });
});

function setFromDateTimePicker(element, val, format) {
    if (val != "") {
        $(element).datetimepicker({
            ignoreReadonly: true,
            format: format,
            maxDate: val,
            useCurrent: false
        });
    }
    else {
        $(element).datetimepicker({
            ignoreReadonly: true,
            format: format,
            useCurrent: false
        });
    }
}

function setToDateTimePicker(element, val, format) {
    if (val != "") {
        $(element).datetimepicker({
            ignoreReadonly: true,
            format: format,
            minDate: val,
            useCurrent: false
        });
    }
    else {
        $(element).datetimepicker({
            ignoreReadonly: true,
            format: format,
            useCurrent: false
        });
    }
}

//$(document).ready(function () {
//    $('.DataTable tfoot th').not(":eq(0)").each(function () {
//        var title = $(this).text();
//        $(this).html('<input type="text" placeholder="Search ' + title + '" />');
//    });

//    var table = $('.DataTable').DataTable({
//        //"searching": "false",
//        "pagingType": "full_numbers",
//        "lengthMenu": [[10, 20, 30, 40, 50, -1], [10, 20, 30, 40, 50, "All"]]
//    });

//    table.columns().every(function () {
//        var that = this;

//        $('input', this.footer()).on('keyup change', function () {
//            if (that.search() !== this.value) {
//                that
//                    .search(this.value)
//                    .draw();
//            }
//        });
//    });
//});



function getDate(date) {
    return new Date(parseInt(date.substr(6))).toString("dd MMM yyyy");
}

function CreateSuccess(data) {
    if (data != "success") {
        $('#createContainer').html(data);
        return;
    }
    Notifikasi(successMessage());
    $('#createModal').modal('hide');
    $('#createContainer').html("");
    assetListVM.refresh();

}

function CreateSuccess2(data) {
    if (data != "success") {
        $('#createContainer').html(data);
        assetListVM.refresh();
        return;
    }
    Notifikasi(successMessage());
    $('#createModal').modal('hide');
    $('#createContainer').html("");
    assetListVM.refresh();
}

function Notifikasi(data) {
    $("#NotificationBox").fadeIn(1000);
    $("#message").val(data);
    $("label[for='message']").html(data);
    $("#NotificationBox").delay(300).fadeOut(1000);

}

function successMessage() {
    return "Data berhasil disimpan !";
}

function DeleteSuccess(data) {

    if (data != "success") {
        $('#createContainer').html(data);
        return;
    }
    Notifikasi("Data berhasil dihapus !");
    $('#createModal').modal('hide');
    $('#createContainer').html("");
    assetListVM.refresh();
}

function LoadingBar() {
    swal({
        title: 'Please Wait.',
        text: '<div class="loader"></div>',
        // type: 'warning',
        animation: false,
        showConfirmButton: false,
        html: true
    });
}

function ValidationAction(element, toogle){
    if(toogle){
        element.addClass("alertValidation");
        // var text = "<p class='text-danger'>" + text + "</p>";
        // element.after(text);
    }
    else{
        element.removeClass("alertValidation");
        // console.log(element.parent());
    }
}
//CUT PARAGRAPH