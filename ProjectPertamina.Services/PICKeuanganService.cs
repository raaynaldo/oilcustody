﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IPICKeuanganService : IEntityService<PICKeuangan>
    {
        Tuple<IEnumerable<PICKeuangan>, int, int> SearchPICKeuangan(int Skip, int length, string Code, string Nama);
    }

    public class PICKeuanganService : EntityService<PICKeuangan>, IPICKeuanganService
    {
        IUnitOfWork _unitOfWork;
        IPICKeuanganRepository _picKeuanganRepository;

        public PICKeuanganService(IUnitOfWork unitOfWork, IPICKeuanganRepository PICKeuanganRepository)
            : base(unitOfWork, PICKeuanganRepository)
        {
            _unitOfWork = unitOfWork;
            _picKeuanganRepository = PICKeuanganRepository;
        }
        public Tuple<IEnumerable<PICKeuangan>, int, int> SearchPICKeuangan(int Skip, int length, string Code, string Nama)
        {
            IEnumerable<PICKeuangan> list = _picKeuanganRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Nama = Nama == null ? "" : Nama;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Nama.ToLower().Contains(Nama.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<PICKeuangan>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
