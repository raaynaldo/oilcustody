﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ICargoLossesService : IEntityService<CargoLosses>
    {
        Tuple<IEnumerable<CargoLosses>, int, int> SearchCargoLosses(int Skip, int length, string Muatan, string Asal, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        Tuple<string[], decimal[], decimal[]> getChart(string Muatan, string Asal, string NamaKapal,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        IEnumerable<CargoLosses> queryExportToExcel(string Muatan, string Asal, string NamaKapal, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        List<CargoLosses> SearchCargoLossesEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);

        IEnumerable<CargoLosses> GetAllJoinData();
        //CargoLosses GetByIdJoinData(int Id);

    }

    public class CargoLossesService : EntityService<CargoLosses>, ICargoLossesService
    {
        IUnitOfWork _unitOfWork;
        ICargoLossesRepository _CargoLossesRepository;

        public CargoLossesService(IUnitOfWork unitOfWork, ICargoLossesRepository CargoLossesRepository)
            : base(unitOfWork, CargoLossesRepository)
        {
            _unitOfWork = unitOfWork;
            _CargoLossesRepository = CargoLossesRepository;
        }

        public Tuple<IEnumerable<CargoLosses>, int, int> SearchCargoLosses(int Skip, int length, string Muatan, string Asal, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            //IEnumerable<CargoLosses> list = _CargoLossesRepository.Find(i => !i.Deleted);
            IEnumerable<CargoLosses> list = _CargoLossesRepository.GetAllJoinData().Where(x => !x.Deleted);
            var TotalCount = list.Count();

            Muatan = Muatan == null ? "" : Muatan;
            Asal = Asal == null ? "" : Asal;
            NamaKapal = NamaKapal == null ? "" : NamaKapal;
            NoBl = NoBl == null ? "" : NoBl;

            list = list.Where(i => i.NamaProduk.ToLower().Contains(Muatan.ToLower()) &&
            i.PortDescription.ToLower().Contains(Asal.ToLower()) &&
            i.TransportName.ToLower().Contains(NamaKapal.ToLower()) &&
            i.BlAsal.ToLower().Contains(NoBl.ToLower())
            ).OrderByDescending(i => i.Id);

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                list = list.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                list = list.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<CargoLosses>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public Tuple<string[], decimal[], decimal[]> getChart(string Muatan, string Asal, string NamaKapal,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var view = _CargoLossesRepository.getValueCargoLosses(Muatan, Asal, NamaKapal, TanggalBL, TanggalBLSampai, 
                TanggalBongkar, TanggalBongkarSampai);
            var TransportName = view.Select(x => x.TransportName);
            var SupplyLosses = view.Select(x => x.PercR4);
            var R4 = view.Select(x => x.R4Values);
            return new Tuple<string[], decimal[], decimal[]>(TransportName.ToArray(), SupplyLosses.ToArray(), R4.ToArray());
        }

       public IEnumerable<CargoLosses> queryExportToExcel(string Muatan, string Asal, string NamaKapal, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<CargoLosses> list = _CargoLossesRepository.GetAllJoinData().Where(x => !x.Deleted);

            Muatan = Muatan == null ? "" : Muatan;
            Asal = Asal == null ? "" : Asal;
            NamaKapal = NamaKapal == null ? "" : NamaKapal;

            list = list.Where(i => i.NamaProduk.ToLower().Contains(Muatan.ToLower()) &&
            i.PortDescription.ToLower().Contains(Asal.ToLower()) &&
            i.TransportName.ToLower().Contains(NamaKapal.ToLower())
            ).OrderByDescending(i => i.Id);

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                list = list.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                list = list.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            return list;
        }

        public List<CargoLosses> SearchCargoLossesEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return _CargoLossesRepository.SearchCargoLossesEdit(TanggalBL, TanggalBLSampai);
        }

        public IEnumerable<CargoLosses> GetAllJoinData()
        {
            return _CargoLossesRepository.GetAllJoinData();
        }

        //public CargoLosses GetByIdJoinData(int Id)
        //{
        //    return _CargoLossesRepository.GetByIdJoinData(Id);
        //}
    }
}