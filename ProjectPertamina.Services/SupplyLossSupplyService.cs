﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ISupplyLossSupplyService : IEntityService<SupplyLossSupply>
    {
        Tuple<List<SupplyLossSupply>, int, int> SearchSupplyLossSupply(int Skip, int length, string TipeTransportasi, string DischargePort, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        List<string> GetListTransportType();
        Tuple<string[], decimal[], decimal[]> getChart(string PortDescription, string NamaProduk, string TransportType,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);
        IEnumerable<SupplyLossSupply> queryExportToExcel(string AlatAngkut, string DischargePort, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);
        List<SupplyLossSupply> SearchSupplyLossSupplyEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);
        List<SupplyLossSupply> SearchSupplyLossSupplyForSupplyLoss(decimal nilai, DateTime? tanggal, DateTime? tanggalSampai, string NamaKapal);
        IEnumerable<SupplyLossSupply> GetAllJoinData();
    }

    public class SupplyLossSupplyService : EntityService<SupplyLossSupply>, ISupplyLossSupplyService
    {
        IUnitOfWork _unitOfWork;
        ISupplyLossSupplyRepository _SupplyLossSupplyRepository;

        public SupplyLossSupplyService(IUnitOfWork unitOfWork, ISupplyLossSupplyRepository SupplyLossSupplyRepository)
            : base(unitOfWork, SupplyLossSupplyRepository)
        {
            _unitOfWork = unitOfWork;
            _SupplyLossSupplyRepository = SupplyLossSupplyRepository;
        }

        public Tuple<List<SupplyLossSupply>, int, int> SearchSupplyLossSupply(int Skip, int length, string TipeTransportasi, string DischargePort, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            // var list = _SupplyLossSupplyRepository.Find(i => !i.Deleted).ToList();
            var list = _SupplyLossSupplyRepository.GetAllJoinData().Where(x => !x.Deleted);
            var TotalCount = list.Count();

            TipeTransportasi = TipeTransportasi == null ? "" : TipeTransportasi;
            DischargePort = DischargePort == null ? "" : DischargePort;
            NamaKapal = NamaKapal == null ? "" : NamaKapal;
            NoBl = NoBl == null ? "" : NoBl;

            var newList = list.Where(i => i.TransportType.ToLower().Contains(TipeTransportasi.ToLower().Trim()) &&
            i.DischargePortDesc.ToLower().Contains(DischargePort.ToLower().Trim()) &&
            i.TransportName.ToLower().Contains(NamaKapal.ToLower().Trim()) &&
            i.Roas.BlAsal.ToLower().Contains(NoBl.ToLower().Trim())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                newList = newList.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                newList = newList.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalBongkar != null)
                newList = newList.Where(i => i.TanggalBongkar >= TanggalBongkar).ToList();

            if (TanggalBongkarSampai != null)
                newList = newList.Where(i => i.TanggalBongkar <= TanggalBongkarSampai).ToList();

            var filterCount = newList.Count();

            return new Tuple<List<SupplyLossSupply>, int, int>(newList.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }

        public List<string> GetListTransportType()
        {
            return _SupplyLossSupplyRepository.GetListTrasportType();
        }

        public Tuple<string[], decimal[], decimal[]> getChart(string PortDescription, string NamaProduk, string TransportType,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var view = _SupplyLossSupplyRepository.getValueSupplyLossSupply(PortDescription, NamaProduk, TransportType,
                TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);
            var TransportName = view.Select(x => x.TransportName);
            var R1 = view.Select(x => x.R1);
            var PercR1 = view.Select(x => x.PercR1);
            return new Tuple<string[], decimal[], decimal[]>(TransportName.ToArray(), R1.ToArray(), PercR1.ToArray());
        }

        public IEnumerable<SupplyLossSupply> queryExportToExcel(string AlatAngkut, string DischargePort, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var list = _SupplyLossSupplyRepository.Find(i => !i.Deleted).ToList();
            var TotalCount = list.Count();

            AlatAngkut = AlatAngkut == null ? "" : AlatAngkut;
            DischargePort = DischargePort == null ? "" : DischargePort;
            Material = Material == null ? "" : Material;


            var newList = list.Where(i => i.TransportType.ToLower().Contains(AlatAngkut.ToLower()) &&
            i.DischargePortDesc.ToLower().Contains(DischargePort.ToLower()) &&
            i.NamaProduk.ToLower().Contains(Material.ToLower())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                newList = newList.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                newList = newList.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalBongkar != null)
                newList = newList.Where(i => i.TanggalBongkar >= TanggalBongkar).ToList();

            if (TanggalBongkarSampai != null)
                newList = newList.Where(i => i.TanggalBongkar <= TanggalBongkarSampai).ToList();

            return newList;
        }

        public List<SupplyLossSupply> SearchSupplyLossSupplyEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return _SupplyLossSupplyRepository.SearchSupplyLossSupplyEdit(TanggalBL, TanggalBLSampai);
        }

        public IEnumerable<SupplyLossSupply> GetAllJoinData()
        {
            return _SupplyLossSupplyRepository.GetAllJoinData();
        }

        public List<SupplyLossSupply> SearchSupplyLossSupplyForSupplyLoss(decimal nilai, DateTime ? tanggal, DateTime ? tanggalSampai, string NamaKapal)
        {
            return _SupplyLossSupplyRepository.SearchSupplyLossSupplyForSupplyLoss(nilai, tanggal, tanggalSampai, NamaKapal);
        }
    }
}
