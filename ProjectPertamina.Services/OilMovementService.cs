﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IOilMovementService : IEntityService<OilMovement>
    {
        Tuple<IEnumerable<OilMovement>, int, int> SearchOilMovement(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, string Tujuan);

        Tuple<IEnumerable<OilMovement>, int, int> SearchOilMovementReport(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai,
            string Tujuan, string Nopol);
        List<OilMovement> reportJurnal(DateTime? TanggalDari, DateTime? TanggalSampai,
            string Tujuan, string Nopol);
    }

    public class OilMovementService : EntityService<OilMovement>, IOilMovementService
    {
        IUnitOfWork _unitOfWork;
        IOilMovementRepository _OilMovementRepository;

        public OilMovementService(IUnitOfWork unitOfWork, IOilMovementRepository OilMovementRepository)
            : base(unitOfWork, OilMovementRepository)
        {
            _unitOfWork = unitOfWork;
            _OilMovementRepository = OilMovementRepository;
        }
        public Tuple<IEnumerable<OilMovement>, int, int> SearchOilMovement(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, string Tujuan)
        {
            IEnumerable<OilMovement> list = _OilMovementRepository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            Tujuan = Tujuan == null ? "" : Tujuan;

            list = list.Where(i => i.Tujuan.ToLower().Contains(Tujuan.ToLower())).OrderByDescending(i => i.Id);

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<OilMovement>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public Tuple<IEnumerable<OilMovement>, int, int> SearchOilMovementReport(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, 
            string Tujuan, string Nopol)
        {
            IEnumerable<OilMovement> list = _OilMovementRepository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            Tujuan = Tujuan == null ? "" : Tujuan;

            list = list.Where(i => i.Tujuan.ToLower().Contains(Tujuan.ToLower()) && i.NoPolisi.ToLower().Contains(Nopol.ToLower()))
                .OrderByDescending(i => i.Id);

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<OilMovement>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public List<OilMovement> reportJurnal(DateTime? TanggalDari, DateTime? TanggalSampai, string Tujuan, string Nopol)
        {
            IEnumerable<OilMovement> list = _OilMovementRepository.GetAll();
            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);
            list = list.Where(i => i.Tujuan.ToLower().Contains(Tujuan.ToLower()) && i.NoPolisi.ToLower().Contains(Nopol.ToLower()));
            return list.ToList();
        }
    }
}
