﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinancePipaDetailService : IEntityService<FinancePipaDetail>
    {
        Tuple<IEnumerable<FinancePipaDetail>, int, int> SearchFinancePipaDetail(int Skip, int length, string Code, string Desc);
    }

    public class FinancePipaDetailService : EntityService<FinancePipaDetail>, IFinancePipaDetailService
    {
        IUnitOfWork _unitOfWork;
        IFinancePipaDetailRepository _financePipaDetailRepository;

        public FinancePipaDetailService(IUnitOfWork unitOfWork, IFinancePipaDetailRepository FinancePipaDetailRepository)
            : base(unitOfWork, FinancePipaDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _financePipaDetailRepository = FinancePipaDetailRepository;
        }
        public Tuple<IEnumerable<FinancePipaDetail>, int, int> SearchFinancePipaDetail(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<FinancePipaDetail> list = _financePipaDetailRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            //list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<FinancePipaDetail>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
