﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinancePipaService : IEntityService<FinancePipa>
    {
        Tuple<IEnumerable<FinancePipa>, int, int> SearchFinancePipa(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, int PICKeuanganId = 0, int ShiftId = 0);
    }

    public class FinancePipaService : EntityService<FinancePipa>, IFinancePipaService
    {
        IUnitOfWork _unitOfWork;
        IFinancePipaRepository _financePipaRepository;

        public FinancePipaService(IUnitOfWork unitOfWork, IFinancePipaRepository FinancePipaRepository)
            : base(unitOfWork, FinancePipaRepository)
        {
            _unitOfWork = unitOfWork;
            _financePipaRepository = FinancePipaRepository;
        }
        public Tuple<IEnumerable<FinancePipa>, int, int> SearchFinancePipa(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, int PICKeuanganId = 0, int ShiftId = 0)
        {
            IEnumerable<FinancePipa> list = _financePipaRepository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (PICKeuanganId != 0)
                list = list.Where(i => i.PICKeuanganId == PICKeuanganId);

            if (ShiftId != 0)
                list = list.Where(i => i.ShiftId == ShiftId);

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<FinancePipa>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
