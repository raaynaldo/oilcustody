﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.ViewModel;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IKKSupplyLossService : IEntityService<KKSupplyLoss>
    {
        Tuple<List<KKSupplyLoss>, int, int> SearchKertasKerjaSupplyLoss(int Skip, int length, string TipeTransportasi, string LoadingPort, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai);

        List<string> GetListTransportType();
        Tuple<List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>> getChart(string LoadingPort, string Produk, string Vessel,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai);

        //Tuple<DateTime[], string[], decimal[], decimal[], decimal[], decimal[], decimal[]> getChart(string LoadingPort, string Produk, string Vessel,
        //   DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai);

        IEnumerable<KKSupplyLoss> queryExportToExcel(string LoadingPort, string Produk, string Vessel, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        List<KKSupplyLoss> SearchKKSupplyLossEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);

        Tuple<List<KKSupplyLoss>, int, int> SearchKertasKerjaSupplyLossReport(int Skip, int length, string LoadingPort, string Produk, string Vessel,
        DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai);
    }

    public class KKSupplyLossService : EntityService<KKSupplyLoss>, IKKSupplyLossService
    {
        IUnitOfWork _unitOfWork;
        IKKSupplyLossRepository _kkSupplyLossRepository;

        public KKSupplyLossService(IUnitOfWork unitOfWork, IKKSupplyLossRepository KKSupplyLossRepository)
            : base(unitOfWork, KKSupplyLossRepository)
        {
            _unitOfWork = unitOfWork;
            _kkSupplyLossRepository = KKSupplyLossRepository;
        }

        public Tuple<List<KKSupplyLoss>, int, int> SearchKertasKerjaSupplyLoss(int Skip, int length, string TipeTransportasi, string LoadingPort, string NamaKapal, string NoBl,
        DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            List<KKSupplyLoss> list = _kkSupplyLossRepository.GetAllJoinData().ToList();
            var TotalCount = list.Count();

            TipeTransportasi = TipeTransportasi == null ? "" : TipeTransportasi;
            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            NamaKapal = NamaKapal == null ? "" : NamaKapal;
            NoBl = NoBl == null ? "" : NoBl;

            list = list.Where(i => i.Roas.TransportType.ToLower().Contains(TipeTransportasi.ToLower().Trim()) &&
            i.LoadingPort.ToLower().Contains(LoadingPort.ToLower().Trim()) &&
            i.TransportName.ToLower().Contains(NamaKapal.ToLower().Trim()) &&
            i.Roas.BlAsal.ToLower().Contains(NoBl.ToLower().Trim())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalCQD != null)
                list = list.Where(i => i.DocDate >= TanggalCQD).ToList();

            if (TanggalCQDSampai != null)
                list = list.Where(i => i.DocDate <= TanggalCQDSampai).ToList();

            var filterCount = list.Count();

            return new Tuple<List<KKSupplyLoss>, int, int>(list.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }

        public List<string> GetListTransportType()
        {
            return _kkSupplyLossRepository.GetListTrasportType();
        }

        public Tuple<List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>> getChart(string LoadingPort, string Produk, string Vessel,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            var view = _kkSupplyLossRepository.getValueKKSupplyLoss(LoadingPort, Produk, Vessel,
              TanggalBL, TanggalBLSampai, TanggalCQD, TanggalCQDSampai);

            //var BlDate_Pipa = view.Item1.Select(x => x.BlDate);
            //var TransportName_Pipa = view.Item1.Select(x => x.TransportName);
            //var D1QualityBl = view.Item1.Select(x => x.D1QualityBl);
            //var D1QualitiTanki = view.Item1.Select(x => x.D1QualitiTanki);

            //var BlDate_Vessel = view.Item2.Select(x => x.BlDate);
            //var TransportName_Vessel = view.Item2.Select(x => x.TransportName);
            //var D2ObqAwal = view.Item2.Select(x => x.D2ObqAwal);
            //var D2ObqAkhir = view.Item2.Select(x => x.D2ObqAkhir);
            //var D2BarrelsRob = view.Item2.Select(x => x.D2BarrelsRob);

            return new Tuple<List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>>(view.Item1, view.Item2);
        }

        public IEnumerable<KKSupplyLoss> queryExportToExcel(string LoadingPort, string Produk, string Vessel, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            IEnumerable<KKSupplyLoss> list = _kkSupplyLossRepository.GetAllJoinData().Where(i => !i.Deleted);
            var TotalCount = list.Count();

            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            Produk = Produk == null ? "" : Produk;
            Vessel = Vessel == null ? "" : Vessel;

            list = list.Where(i => i.LoadingPort.ToLower().Contains(LoadingPort.ToLower()) &&
            i.NamaProduk.ToLower().Contains(Produk.ToLower()) &&
            i.TransportName.ToLower().Contains(Vessel.ToLower())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalCQD != null)
                list = list.Where(i => i.DocDate >= TanggalCQD).ToList();

            if (TanggalCQDSampai != null)
                list = list.Where(i => i.DocDate <= TanggalCQDSampai).ToList();

            return list;
        }

        public List<KKSupplyLoss> SearchKKSupplyLossEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return _kkSupplyLossRepository.SearchKKSupplyLossEdit(TanggalBL, TanggalBLSampai);
        }

        public Tuple<List<KKSupplyLoss>, int, int> SearchKertasKerjaSupplyLossReport(int Skip, int length, string LoadingPort, string Produk, string Vessel,
         DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            List<KKSupplyLoss> list = _kkSupplyLossRepository.GetAllJoinData().ToList();
            var TotalCount = list.Count();

            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            Produk = Produk == null ? "" : Produk;
            Vessel = Vessel == null ? "" : Vessel;
            

            list = list.Where(i => i.NamaProduk.ToLower().Contains(Produk.ToLower()) &&
            i.LoadingPort.ToLower().Contains(LoadingPort.ToLower()) &&
            i.TransportName.ToLower().Contains(Vessel.ToLower())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalCQD != null)
                list = list.Where(i => i.DocDate >= TanggalCQD).ToList();

            if (TanggalCQDSampai != null)
                list = list.Where(i => i.DocDate <= TanggalCQDSampai).ToList();

            var filterCount = list.Count();

            return new Tuple<List<KKSupplyLoss>, int, int>(list.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }
    }
}
