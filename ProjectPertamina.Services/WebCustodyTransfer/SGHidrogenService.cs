﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ISGHidrogenService : IEntityService<SGHidrogen>
    {
        Tuple<IEnumerable<SGHidrogen>, int, int> SearchSGHidrogen(int Skip, int length, int Bulan, int Tahun);
        decimal getSGHidrogen(DateTime Tanggal);
    }

    public class SGHidrogenService : EntityService<SGHidrogen>, ISGHidrogenService
    {
        IUnitOfWork _unitOfWork;
        ISGHidrogenRepository _SGHidrogenRepository;

        public SGHidrogenService(IUnitOfWork unitOfWork, ISGHidrogenRepository SGHidrogenRepository)
            : base(unitOfWork, SGHidrogenRepository)
        {
            _unitOfWork = unitOfWork;
            _SGHidrogenRepository = SGHidrogenRepository;
        }

        public Tuple<IEnumerable<SGHidrogen>, int, int> SearchSGHidrogen(int Skip, int length, int Bulan, int Tahun)
        {
            IEnumerable<SGHidrogen> list = _SGHidrogenRepository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (Bulan != 0)
                list = list.Where(i => i.Bulan == Bulan);

            if (Tahun != 0)
                list = list.Where(i => i.Tahun == Tahun);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<SGHidrogen>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public decimal getSGHidrogen(DateTime Tanggal)
        {
            var bulan = Tanggal.Month;
            var tahun = Tanggal.Year;
            var SGHidrogen = _SGHidrogenRepository.Find(i => i.Bulan == bulan && i.Tahun == tahun).FirstOrDefault();

            if (SGHidrogen == null)
                return 0;
            else
                return SGHidrogen.NilaiSG;
        }
    }
}
