﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ITipeProdukService : IEntityService<TipeProduk>
    {
        Tuple<IEnumerable<TipeProduk>, int, int> SearchTipeProduk(int Skip, int length, string Kode, string Deskripsi);
    }

    public class TipeProdukService : EntityService<TipeProduk>, ITipeProdukService
    {
        IUnitOfWork _unitOfWork;
        ITipeProdukRepository _TipeProdukRepository;

        public TipeProdukService(IUnitOfWork unitOfWork, ITipeProdukRepository TipeProdukRepository)
            : base(unitOfWork, TipeProdukRepository)
        {
            _unitOfWork = unitOfWork;
            _TipeProdukRepository = TipeProdukRepository;
        }

        public Tuple<IEnumerable<TipeProduk>, int, int> SearchTipeProduk(int Skip, int length, string Kode, string Deskripsi)
        {
            IEnumerable<TipeProduk> list = _TipeProdukRepository.GetAll();
            var TotalCount = list.Count();

            Kode = Kode == null ? "" : Kode;
            Deskripsi = Deskripsi == null ? "" : Deskripsi;

            list = list.Where(i => i.Kode.ToLower().Contains(Kode.ToLower()) &&
                    i.Deskripsi.ToLower().Contains(Deskripsi.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<TipeProduk>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
