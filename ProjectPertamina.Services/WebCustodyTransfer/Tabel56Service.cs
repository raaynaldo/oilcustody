﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ITabel56Service : IEntityService<Tabel56>
    {
        Tuple<IEnumerable<Tabel56>, int, int> SearchTabel56(int Skip, int length, decimal Density, decimal Liter);

        decimal GetLiter15(decimal mt, decimal density);
    }

    public class Tabel56Service : EntityService<Tabel56>, ITabel56Service
    {
        IUnitOfWork _unitOfWork;
        ITabel56Repository _Tabel56Repository;

        public Tabel56Service(IUnitOfWork unitOfWork, ITabel56Repository Tabel56Repository)
            : base(unitOfWork, Tabel56Repository)
        {
            _unitOfWork = unitOfWork;
            _Tabel56Repository = Tabel56Repository;
        }

        public Tuple<IEnumerable<Tabel56>, int, int> SearchTabel56(int Skip, int length, decimal Density, decimal Liter)
        {
            IEnumerable<Tabel56> list = _Tabel56Repository.GetAll().OrderByDescending(i => i.Id);
                var TotalCount = list.Count();

            if (Density != 0)
                list = list.Where(i => i.Density15 == Density);

            if (Liter != 0)
                list = list.Where(i => i.LiterMetricTon == Liter);


            var filterCount = list.Count();

            return new Tuple<IEnumerable<Tabel56>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public decimal GetLiter15(decimal mt, decimal density)
        {
            if (mt == 0) // Kutip di DS
                return 0;

            if (density == 0) // Kutip di DS
                return 0;

            decimal d1 = Math.Truncate(density * 1000) / 1000;
            decimal d2 = d1 + (decimal)0.001;
            decimal d3 = d1 + (decimal)0.001;
            decimal d4 = d2 + (decimal)0.001;
            Tabel56 d5Obj = _Tabel56Repository.Find(i => i.Density15 == d1).FirstOrDefault();
            Tabel56 d6Obj = _Tabel56Repository.Find(i => i.Density15 == d2).FirstOrDefault();
            if (d5Obj == null) // Master Ga ada
                return 0;

            if (d6Obj == null) // Master Ga ada
                return 0;

            decimal d5 = d5Obj.LiterMetricTon;
            decimal d6 = d6Obj.LiterMetricTon;

            decimal Liter15 = mt * (d5 + ((density - d3) / (decimal)0.001 * (d6 - d5)));

            return Liter15;
        }
    }
}
