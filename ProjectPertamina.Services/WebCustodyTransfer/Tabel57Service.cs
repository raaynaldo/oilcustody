﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ITabel57Service : IEntityService<Tabel57>
    {
        Tuple<IEnumerable<Tabel57>, int, int> SearchTabel57(int Skip, int length, decimal Density, decimal Liter);

        decimal GetLongTons(decimal mt, decimal density, decimal liter15);
    }

    public class Tabel57Service : EntityService<Tabel57>, ITabel57Service
    {
        IUnitOfWork _unitOfWork;
        ITabel57Repository _Tabel57Repository;

        public Tabel57Service(IUnitOfWork unitOfWork, ITabel57Repository Tabel57Repository)
            : base(unitOfWork, Tabel57Repository)
        {
            _unitOfWork = unitOfWork;
            _Tabel57Repository = Tabel57Repository;
        }

        public Tuple<IEnumerable<Tabel57>, int, int> SearchTabel57(int Skip, int length, decimal Density, decimal Liter)
        {
            IEnumerable<Tabel57> list = _Tabel57Repository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (Density != 0)
                list = list.Where(i => i.Density15 == Density);

            if (Liter != 0)
                list = list.Where(i => i.LongTerm100L == Liter);


            var filterCount = list.Count();

            return new Tuple<IEnumerable<Tabel57>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public decimal GetLongTons(decimal mt, decimal density, decimal liter15)
        {
            if (mt == 0) // Kutip di DS
                return 0;

            if (density == 0) // Kutip di DS
                return 0;

            decimal a = Math.Truncate(density * 1000) / 1000;
            decimal b = a + (decimal)0.001;
            decimal c = a + (decimal)0.001;
            decimal d = b + (decimal)0.001;
            Tabel57 eObj = _Tabel57Repository.Find(i => i.Density15 == c).FirstOrDefault();
            Tabel57 fObj = _Tabel57Repository.Find(i => i.Density15 == d).FirstOrDefault();

            if (eObj == null) // Master tidak ada
                return 0;

            if (fObj == null) // Master tidak ada
                return 0;

            decimal e = eObj.LongTerm100L;
            decimal f = fObj.LongTerm100L;
            decimal LongTons = (liter15 * (e + ((density - a) / (decimal)0.001 * (f - e)))) / 1000;

            return LongTons;
        }
    }
}
