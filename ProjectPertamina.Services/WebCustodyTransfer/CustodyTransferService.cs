﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ICustodyTransferService : IEntityService<CustodyTransfer>
    {
        Tuple<IEnumerable<CustodyTransfer>, int, int> SearchCustodyTransfer(int Skip, int length, string Perusahaan, int TipeProduk, string Stream, string TagNumber, DateTime? TanggalDari, DateTime? TanggalSampai);
        IEnumerable<CustodyTransfer> SearchCustodyTransferForReportBeritaAcara(int bulan, int tahun);

        decimal GetNM3(DateTime Tanggal);

        CustodyTransfer GetJoinAllDatabyId(int id);
    }

    public class CustodyTransferService : EntityService<CustodyTransfer>, ICustodyTransferService
    {
        IUnitOfWork _unitOfWork;
        ICustodyTransferRepository _CustodyTransferRepository;

        public CustodyTransferService(IUnitOfWork unitOfWork, ICustodyTransferRepository CustodyTransferRepository)
            : base(unitOfWork, CustodyTransferRepository)
        {
            _unitOfWork = unitOfWork;
            _CustodyTransferRepository = CustodyTransferRepository;
        }

        public Tuple<IEnumerable<CustodyTransfer>, int, int> SearchCustodyTransfer(int Skip, int length, string Perusahaan, int TipeProduk, string Stream, string TagNumber, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            IEnumerable<CustodyTransfer> list = _CustodyTransferRepository.GetAllJoinData();
            var TotalCount = list.Count();

            Stream = Stream == null ? "" : Stream;
            TagNumber = TagNumber == null ? "" : TagNumber;

            list = list.Where(i => i.Stream.StreamName.ToLower().Contains(Stream.ToLower()) && i.Stream.TagNumber.ToLower().Contains(TagNumber.ToLower())).OrderByDescending(i => i.Id);

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);

            if (Perusahaan != "")
                list = list.Where(i => i.PerusahaanKode == Perusahaan);

            if (TipeProduk != 0)
                list = list.Where(i => i.Stream.TipeProdukId == TipeProduk);

            var filterCount = list.Count();
            return new Tuple<IEnumerable<CustodyTransfer>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public CustodyTransfer GetJoinAllDatabyId(int id)
        {
            return _CustodyTransferRepository.GetJoinAllDatabyId(id);
        }

        public IEnumerable<CustodyTransfer> SearchCustodyTransferForReportBeritaAcara(int bulan, int tahun)
        {
            return _CustodyTransferRepository.GetAllJoinData(bulan, tahun);
        }

        public decimal GetNM3(DateTime Tanggal)
        {
            decimal bmh2 = (decimal)0.1178 * (decimal)28.84;
            decimal berath2 = _CustodyTransferRepository.Find(i => i.Tanggal.Month == Tanggal.Month && i.Tanggal.Year == Tanggal.Year)
                                        .Sum(i => i.MT)
                                        * 1000000;
            decimal grlh2 = berath2 / bmh2;
            decimal volh2 = grlh2 / bmh2;

            var NM3 = volh2 / 1000;

            return NM3;
        }

        //public Tuple<IEnumerable<CustodyTransfer>, int, int> SearchCustodyTransfer(int Skip, int length, string Code, string Desc)
        //{
        //    IEnumerable<CustodyTransfer> list = _CustodyTransferRepository.GetAll();
        //    var TotalCount = list.Count();

        //    Code = Code == null ? "" : Code;
        //    Desc = Desc == null ? "" : Desc;

        //    list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

        //    var filterCount = list.Count();

        //    return new Tuple<IEnumerable<CustodyTransfer>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        //}
    }
}
