﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface IStreamService : IEntityService<Stream>
    {
        Tuple<IEnumerable<Stream>, int, int> SearchStream(int Skip, int length, string Stream, string TagNumber, int TipeProduk);
        List<Stream> GetAllJoinData();
    }

    public class StreamService : EntityService<Stream>, IStreamService
    {
        IUnitOfWork _unitOfWork;
        IStreamRepository _StreamRepository;

        public StreamService(IUnitOfWork unitOfWork, IStreamRepository StreamRepository)
            : base(unitOfWork, StreamRepository)
        {
            _unitOfWork = unitOfWork;
            _StreamRepository = StreamRepository;
        }

        public Tuple<IEnumerable<Stream>, int, int> SearchStream(int Skip, int length, string Stream, string TagNumber, int TipeProduk)
        {
            IEnumerable<Stream> list = _StreamRepository.GetAllJoinData();
            var TotalCount = list.Count();

            Stream = Stream == null ? "" : Stream;
            TagNumber = TagNumber == null ? "" : TagNumber;

            list = list.Where(i => i.StreamName.ToLower().Contains(Stream.ToLower()) &&
                    i.TagNumber.ToLower().Contains(TagNumber.ToLower())).OrderByDescending(i => i.Id);

            if (TipeProduk != 0)
                list = list.Where(i => i.TipeProdukId == TipeProduk);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Stream>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public List<Stream> GetAllJoinData()
        {
            return _StreamRepository.GetAllJoinData().ToList();
        }
    }
}
