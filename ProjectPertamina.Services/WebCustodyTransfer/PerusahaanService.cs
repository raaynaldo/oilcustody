﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface IPerusahaanService : IEntityService<Perusahaan>
    {
        //Tuple<IEnumerable<Perusahaan>, int, int> SearchPerusahaan(int Skip, int length, string Code, string Desc);
    }

    public class PerusahaanService : EntityService<Perusahaan>, IPerusahaanService
    {
        IUnitOfWork _unitOfWork;
        IPerusahaanRepository _PerusahaanRepository;

        public PerusahaanService(IUnitOfWork unitOfWork, IPerusahaanRepository PerusahaanRepository)
            : base(unitOfWork, PerusahaanRepository)
        {
            _unitOfWork = unitOfWork;
            _PerusahaanRepository = PerusahaanRepository;
        }
        //public Tuple<IEnumerable<Perusahaan>, int, int> SearchPerusahaan(int Skip, int length, string Code, string Desc)
        //{
        //    IEnumerable<Perusahaan> list = _PerusahaanRepository.GetAll();
        //    var TotalCount = list.Count();

        //    Code = Code == null ? "" : Code;
        //    Desc = Desc == null ? "" : Desc;

        //    list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

        //    var filterCount = list.Count();

        //    return new Tuple<IEnumerable<Perusahaan>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        //}
    }
}
