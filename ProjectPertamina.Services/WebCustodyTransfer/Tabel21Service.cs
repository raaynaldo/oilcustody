﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ITabel21Service : IEntityService<Tabel21>
    {
        Tuple<IEnumerable<Tabel21>, int, int> SearchTabel21(int Skip, int length, decimal SG, decimal Desnsity15);
        decimal GetDensity(decimal sg);
    }

    public class Tabel21Service : EntityService<Tabel21>, ITabel21Service
    {
        IUnitOfWork _unitOfWork;
        ITabel21Repository _Tabel21Repository;

        public Tabel21Service(IUnitOfWork unitOfWork, ITabel21Repository Tabel21Repository)
            : base(unitOfWork, Tabel21Repository)
        {
            _unitOfWork = unitOfWork;
            _Tabel21Repository = Tabel21Repository;
        }

        public Tuple<IEnumerable<Tabel21>, int, int> SearchTabel21(int Skip, int length, decimal SG, decimal Density15)
        {
            IEnumerable<Tabel21> list = _Tabel21Repository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (SG != 0)
                list = list.Where(i => i.SG == SG);

            if (Density15 != 0)
                list = list.Where(i => i.Density15 == Density15);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Tabel21>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public decimal GetDensity(decimal sg)
        {
            if (sg == 0) //Tanda Kutip yang di DS
                return 0;

            decimal sg1 = (Math.Truncate(sg * 1000) / 1000);
            decimal sg2 = sg1 + (decimal)0.001;
            Tabel21 sg3obj = _Tabel21Repository.Find(i => i.SG == sg1).FirstOrDefault();
            Tabel21 sg4obj = _Tabel21Repository.Find(i => i.SG == sg2).FirstOrDefault();

            if (sg3obj == null)// Master Ga ada
                return 0; 
            if (sg4obj == null)// Master Ga ada
                return 0;

            decimal sg3 = sg3obj.Density15;
            decimal sg4 = sg4obj.Density15;

            var density = sg3 + ((sg - sg1) / (decimal)0.001 * (sg4 - sg3));

            return density;
        }
    }
}
