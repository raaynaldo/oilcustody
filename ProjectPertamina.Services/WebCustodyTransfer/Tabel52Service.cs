﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface ITabel52Service : IEntityService<Tabel52>
    {
        Tuple<IEnumerable<Tabel52>, int, int> SearchTabel52(int Skip, int length, decimal Density, decimal USGallon, decimal Barrel);
        decimal GetCFBarrel(decimal sg, decimal density);
    }

    public class Tabel52Service : EntityService<Tabel52>, ITabel52Service
    {
        IUnitOfWork _unitOfWork;
        ITabel52Repository _Tabel52Repository;

        public Tabel52Service(IUnitOfWork unitOfWork, ITabel52Repository Tabel52Repository)
            : base(unitOfWork, Tabel52Repository)
        {
            _unitOfWork = unitOfWork;
            _Tabel52Repository = Tabel52Repository;
        }

        public Tuple<IEnumerable<Tabel52>, int, int> SearchTabel52(int Skip, int length, decimal Density, decimal USGallon, decimal Barrel)
        {
            IEnumerable<Tabel52> list = _Tabel52Repository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (Density != 0)
                list = list.Where(i => i.Density15 == Density);

            if (USGallon != 0)
                list = list.Where(i => i.UsGallon60F == USGallon);

            if (Barrel != 0)
                list = list.Where(i => i.Barrel60F == Barrel);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Tabel52>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public decimal GetCFBarrel(decimal sg, decimal density)
        {
            Tabel52 cf = _Tabel52Repository.Find(i => i.Density15 == density).FirstOrDefault();
            if (cf == null)
                return 0;

            var CFBarrel = cf.Barrel60F;

            return CFBarrel;
        }
    }
}
