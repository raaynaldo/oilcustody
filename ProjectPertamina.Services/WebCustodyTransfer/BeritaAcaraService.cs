﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.WebCustodyTransfer
{
    public interface IBeritaAcaraService : IEntityService<BeritaAcara>
    {
        Tuple<IEnumerable<BeritaAcara>, int, int> SearchBeritaAcara(int Skip, int length, int Bulan, int Tahun);
    }

    public class BeritaAcaraService : EntityService<BeritaAcara>, IBeritaAcaraService
    {
        IUnitOfWork _unitOfWork;
        IBeritaAcaraRepository _BeritaAcaraRepository;

        public BeritaAcaraService(IUnitOfWork unitOfWork, IBeritaAcaraRepository BeritaAcaraRepository)
            : base(unitOfWork, BeritaAcaraRepository)
        {
            _unitOfWork = unitOfWork;
            _BeritaAcaraRepository = BeritaAcaraRepository;
        }
        public Tuple<IEnumerable<BeritaAcara>, int, int> SearchBeritaAcara(int Skip, int length, int Bulan, int Tahun)
        {
            IEnumerable<BeritaAcara> list = _BeritaAcaraRepository.GetAll();
            var TotalCount = list.Count();

            if (Bulan != 0)
            {
                list = list.Where(i => i.Bulan == Bulan);
            }

            if (Tahun != 0)
            {
                list = list.Where(i => i.Tahun == Tahun);
            }

            var filterCount = list.Count();

            return new Tuple<IEnumerable<BeritaAcara>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
