﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services.BaseServices
{
    public class EntityService<T> : IEntityService<T> where T : BaseEntities
    {
        IUnitOfWork _unitOfWork;
        IGenericRepository<T> _repository;

        public EntityService(IUnitOfWork unitOfWork, IGenericRepository<T> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public void Save(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("Entity parameter cannot be null");
            }
            //If the entity has an ID, we assume it exists in the DB
            if (entity.Id != 0)
            {
                DoUpdate(entity);

            }
            else
            {
                DoInsert(entity);
            }

        }

        //public void Add(T entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException("entity");
        //    }
        //    _repository.Add(entity);
        //}

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return _repository.Find(predicate);
        }

        public T Get(object Id)
        {
            return _repository.Get(Id);
        }

        protected virtual void DoInsert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.Add(entity);
            _unitOfWork.Commit();
        }

        public void AddRange(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.AddRange(entities);
            _unitOfWork.Commit();
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.Remove(entity);
            _unitOfWork.Commit();
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.RemoveRange(entities);
            _unitOfWork.Commit();

        }

        public void Delete(int Id)
        {
            if (Id == 0)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.Remove(Id);
            _unitOfWork.Commit();
        }

        protected virtual void DoUpdate(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _repository.Update(entity);
            _unitOfWork.Commit();
        }

        public void UpdateRange(IEnumerable<T> entity)
        {
            foreach (T item in entity)
            {
                _repository.Update(item);
            }
            _unitOfWork.Commit();
        }
    }
}
