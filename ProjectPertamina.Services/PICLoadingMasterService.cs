﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IPICLoadingMasterService : IEntityService<PICLoadingMaster>
    {
        Tuple<IEnumerable<PICLoadingMaster>, int, int> SearchPICLoadingMaster(int Skip, int length, string Code, string Nama);
    }

    public class PICLoadingMasterService : EntityService<PICLoadingMaster>, IPICLoadingMasterService
    {
        IUnitOfWork _unitOfWork;
        IPICLoadingMasterRepository _picLoadingMasterRepository;

        public PICLoadingMasterService(IUnitOfWork unitOfWork, IPICLoadingMasterRepository PICLoadingMasterRepository)
            : base(unitOfWork, PICLoadingMasterRepository)
        {
            _unitOfWork = unitOfWork;
            _picLoadingMasterRepository = PICLoadingMasterRepository;
        }
        public Tuple<IEnumerable<PICLoadingMaster>, int, int> SearchPICLoadingMaster(int Skip, int length, string Code, string Nama)
        {
            IEnumerable<PICLoadingMaster> list = _picLoadingMasterRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Nama = Nama == null ? "" : Nama;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Nama.ToLower().Contains(Nama.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<PICLoadingMaster>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
