﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IRoasService : IEntityService<Roas>
    {
        Tuple<IEnumerable<Roas>, int, int> SearchRoas(int Skip, int length, string KodeOpr, string TipeTransport,
            string TransportName, string AsalTujuan, string NamaProduk, DateTime? TanggalDari, DateTime? TanggalSampai);

        List<Roas> GetRoasForCargoLosses();
        List<Roas> GetRoasForSupplyLossSupply();
        List<Roas> GetRoasForSupplyLossDischarge();
        List<Roas> GetRoasForKKSupplyLoss();
        Roas GetByIdJoinData(int Id);

    }

    public class RoasService : EntityService<Roas>, IRoasService
    {
        IUnitOfWork _unitOfWork;
        IRoasRepository _RoasRepository;

        public RoasService(IUnitOfWork unitOfWork, IRoasRepository RoasRepository)
            : base(unitOfWork, RoasRepository)
        {
            _unitOfWork = unitOfWork;
            _RoasRepository = RoasRepository;
        }
        public Tuple<IEnumerable<Roas>, int, int> SearchRoas(int Skip, int length, string KodeOpr, string TipeTransport,
            string TransportName, string AsalTujuan, string NamaProduk, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            IEnumerable<Roas> list = _RoasRepository.GetAll();
            var TotalCount = list.Count();

            KodeOpr = KodeOpr == null ? "" : KodeOpr;
            TipeTransport = TipeTransport == null ? "" : TipeTransport;
            TransportName = TransportName == null ? "" : TransportName;
            AsalTujuan = AsalTujuan == null ? "" : AsalTujuan;
            NamaProduk = NamaProduk == null ? "" : NamaProduk;


            list = list.Where(i => i.KodeOpr.ToLower().Contains(KodeOpr.ToLower()) &&
            i.TransportType.ToLower().Contains(TipeTransport.ToLower()) &&
            i.TransportName.ToLower().Contains(TransportName.ToLower()) &&
            i.AsalTujuan.ToLower().Contains(AsalTujuan.ToLower()) &&
            i.NamaProduk.ToLower().Contains(NamaProduk.ToLower())
            ).OrderByDescending(i => i.Id);

            if (TanggalDari != null)
                list = list.Where(i => i.DocDate >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.DocDate <= TanggalSampai);


            var filterCount = list.Count();

            return new Tuple<IEnumerable<Roas>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }

        public List<Roas> GetRoasForCargoLosses()
        {
            return _RoasRepository.GetRoasForCargoLosses();
        }

        public List<Roas> GetRoasForSupplyLossSupply()
        {
            return _RoasRepository.GetRoasForSupplyLossSupply();
        }
        public List<Roas> GetRoasForSupplyLossDischarge()
        {
            return _RoasRepository.GetRoasForSupplyLossDischarge();
        }

        public List<Roas> GetRoasForKKSupplyLoss()
        {
            return _RoasRepository.GetRoasForKKSupplyLoss();
        }

        public Roas GetByIdJoinData(int Id)
        {
            return _RoasRepository.GetByIdJoinData(Id);
        }
    }
}
