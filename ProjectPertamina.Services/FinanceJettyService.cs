﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinanceJettyService : IEntityService<FinanceJetty>
    {
        Tuple<IEnumerable<FinanceJetty>, int, int> SearchFinanceJetty(int Skip, int length,
            string Jetty, int IDShift, int IDPICKeuangan, int IDLoadingMaster, DateTime? TanggalDari, DateTime? TanggalSampai);
    }

    public class FinanceJettyService : EntityService<FinanceJetty>, IFinanceJettyService
    {
        IUnitOfWork _unitOfWork;
        IFinanceJettyRepository _financeJettyRepository;

        public FinanceJettyService(IUnitOfWork unitOfWork, IFinanceJettyRepository FinanceJettyRepository)
            : base(unitOfWork, FinanceJettyRepository)
        {
            _unitOfWork = unitOfWork;
            _financeJettyRepository = FinanceJettyRepository;
        }

        public Tuple<IEnumerable<FinanceJetty>, int, int> SearchFinanceJetty(int Skip, int length, string Jetty,
            int IDShift, int IDPICKeuangan, int IDLoadingMaster, DateTime? TanggalDari, DateTime? TanggalSampai)
        {
            IEnumerable<FinanceJetty> list = _financeJettyRepository.GetAll();
            var TotalCount = list.Count();
            list = list.Where(i => i.Jetty.ToLower().Contains(Jetty.ToLower())).OrderByDescending(i => i.Id);
            if (IDShift > 0)
                list = list.Where(x => x.ShiftId == IDShift);
            if (IDPICKeuangan > 0)
                list = list.Where(x => x.PICKeuanganId == IDPICKeuangan);
            if (IDLoadingMaster > 0)
                list = list.Where(x => x.PICLoadingMasterId == IDLoadingMaster);
            if (TanggalDari != null)
                list = list.Where(x => x.Tanggal >= TanggalDari);
            if (TanggalSampai != null)
                list = list.Where(x => x.Tanggal <= TanggalSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<FinanceJetty>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);

        }
    }
}
