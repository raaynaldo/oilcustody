﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ISupplyLossService : IEntityService<SupplyLoss>
    {
        Tuple<List<SupplyLoss>, int, int> SearchSupplyLoss(int Skip, int length, string Keterangan, DateTime? TanggalDari, DateTime? TanggalSampai, int Loading);
        IEnumerable<SupplyLoss> SearchByDocDate(DateTime? Tanggal, DateTime? TanggalSampai, string NamaKapal);
        SupplyLoss GetBySupplyLossDischarge(int SupplyLossDischargeId);
        SupplyLoss GetBySupplyLossSupply(int SupplyLossSupplyId);
    }

    public class SupplyLossService : EntityService<SupplyLoss>, ISupplyLossService
    {
        IUnitOfWork _unitOfWork;
        ISupplyLossRepository _SupplyLossRepository;

        public SupplyLossService(IUnitOfWork unitOfWork, ISupplyLossRepository SupplyLossRepository)
            : base(unitOfWork, SupplyLossRepository)
        {
            _unitOfWork = unitOfWork;
            _SupplyLossRepository = SupplyLossRepository;
        }

        public Tuple<List<SupplyLoss>, int, int> SearchSupplyLoss(int Skip, int length, string Keterangan, DateTime? TanggalDari, DateTime? TanggalSampai, int Loading = 0)
        {
            List<SupplyLoss> list = _SupplyLossRepository.GetAllJoinData().ToList();
            var TotalCount = list.Count();

            Keterangan = Keterangan == null ? "" : Keterangan;

            list = list.Where(i => i.Keterangan.ToLower().Contains(Keterangan.ToLower())).OrderByDescending(i => i.Id).ToList();

            if (Loading != 0)
                list = list.Where(i => i.LoadingId == Loading).ToList();

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari).ToList();

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai).ToList();

            var filterCount = list.Count();

            return new Tuple<List<SupplyLoss>, int, int>(list.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }

        public IEnumerable<SupplyLoss> SearchByDocDate(DateTime? Tanggal, DateTime? TanggalSampai, string NamaKapal)
        {
            return _SupplyLossRepository.SearchByDocDate(Tanggal, TanggalSampai, NamaKapal);
        }

        public SupplyLoss GetBySupplyLossSupply(int SupplyLossSupplyId)
        {
            return _SupplyLossRepository.GetBySupplyLossSupply(SupplyLossSupplyId);
        }

        public SupplyLoss GetBySupplyLossDischarge(int SupplyLossDischargeId)
        {
            return _SupplyLossRepository.GetBySupplyLossDischarge(SupplyLossDischargeId);
        }
    }
}
