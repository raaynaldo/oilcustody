﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IPortService : IEntityService<Port>
    {
        Tuple<IEnumerable<Port>, int, int> SearchPort(int Skip, int length, string Code, string Desc);
    }

    public class PortService : EntityService<Port>, IPortService
    {
        IUnitOfWork _unitOfWork;
        IPortRepository _PortRepository;

        public PortService(IUnitOfWork unitOfWork, IPortRepository PortRepository)
            : base(unitOfWork, PortRepository)
        {
            _unitOfWork = unitOfWork;
            _PortRepository = PortRepository;
        }
        public Tuple<IEnumerable<Port>, int, int> SearchPort(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Port> list = _PortRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Port>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
