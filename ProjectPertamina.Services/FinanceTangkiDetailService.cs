﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinanceTangkiDetailService : IEntityService<FinanceTangkiDetail>
    {
        Tuple<IEnumerable<FinanceTangkiDetail>, int, int> SearchFinanceTangkiDetail(int Skip, int length, int Id);
    }

    public class FinanceTangkiDetailService : EntityService<FinanceTangkiDetail>, IFinanceTangkiDetailService
    {
        IUnitOfWork _unitOfWork;
        IFinanceTangkiDetailRepository _FinanceTangkiDetailRepository;

        public FinanceTangkiDetailService(IUnitOfWork unitOfWork, IFinanceTangkiDetailRepository FinanceTangkiDetailRepository)
            : base(unitOfWork, FinanceTangkiDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _FinanceTangkiDetailRepository = FinanceTangkiDetailRepository;
        }
        public Tuple<IEnumerable<FinanceTangkiDetail>, int, int> SearchFinanceTangkiDetail(int Skip, int length, int Id)
        {
            IEnumerable<FinanceTangkiDetail> list = _FinanceTangkiDetailRepository.Find(i => i.FinanceTangkiId == Id);
            var TotalCount = list.Count();

            var filterCount = list.Count();
            return new Tuple<IEnumerable<FinanceTangkiDetail>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
