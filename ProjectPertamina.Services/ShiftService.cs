﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IShiftService : IEntityService<Shift>
    {
        Tuple<IEnumerable<Shift>, int, int> SearchShift(int Skip, int length, string Code, string Desc);
    }

    public class ShiftService : EntityService<Shift>, IShiftService
    {
        IUnitOfWork _unitOfWork;
        IShiftRepository _ShiftRepository;

        public ShiftService(IUnitOfWork unitOfWork, IShiftRepository ShiftRepository)
            : base(unitOfWork, ShiftRepository)
        {
            _unitOfWork = unitOfWork;
            _ShiftRepository = ShiftRepository;
        }
        public Tuple<IEnumerable<Shift>, int, int> SearchShift(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Shift> list = _ShiftRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Shift>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
