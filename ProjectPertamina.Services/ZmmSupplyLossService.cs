﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;

namespace ProjectPertamina.Services
{
    public interface IZmmSupplyLossService : IEntityService<ZmmSupplyLoss>
    {

    }
    public class ZmmSupplyLossService : EntityService<ZmmSupplyLoss>, IZmmSupplyLossService
    {
        IUnitOfWork _unitOfWork;
        IZmmSupplyLossRepository _zmmSupplyLossRepository;
        public ZmmSupplyLossService(IUnitOfWork UnitOfWork, IZmmSupplyLossRepository ZmmSupplyLossRepository)
            :base(UnitOfWork, ZmmSupplyLossRepository)
        {
            _unitOfWork = UnitOfWork;
            _zmmSupplyLossRepository = ZmmSupplyLossRepository;
        }
    }
}
