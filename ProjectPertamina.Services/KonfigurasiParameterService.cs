﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IKonfigurasiParameterService : IEntityService<KonfigurasiParameter>
    {
    }

    public class KonfigurasiParameterService : EntityService<KonfigurasiParameter>, IKonfigurasiParameterService
    {
        IUnitOfWork _unitOfWork;
        IKonfigurasiParameterRepository _KonfigurasiParameterRepository;

        public KonfigurasiParameterService(IUnitOfWork unitOfWork, IKonfigurasiParameterRepository KonfigurasiParameterRepository)
            : base(unitOfWork, KonfigurasiParameterRepository)
        {
            _unitOfWork = unitOfWork;
            _KonfigurasiParameterRepository = KonfigurasiParameterRepository;
        }
    }
}
