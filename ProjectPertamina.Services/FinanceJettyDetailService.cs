﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinanceJettyDetailService : IEntityService<FinanceJettyDetail>
    {
        Tuple<IEnumerable<FinanceJettyDetail>, int, int> SearchFinanceJettyDetail(int Skip, int length, int IDHeader);
    }

    public class FinanceJettyDetailService : EntityService<FinanceJettyDetail>, IFinanceJettyDetailService
    {
        IUnitOfWork _unitOfWork;
        IFinanceJettyDetailRepository _financeJettyDetailRepository;

        public FinanceJettyDetailService(IUnitOfWork unitOfWork, IFinanceJettyDetailRepository FinanceJettyDetailRepository)
            : base(unitOfWork, FinanceJettyDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _financeJettyDetailRepository = FinanceJettyDetailRepository;
        }

        public Tuple<IEnumerable<FinanceJettyDetail>, int, int> SearchFinanceJettyDetail(int Skip, int length, int IDHeader)
        {
            IEnumerable<FinanceJettyDetail> list = _financeJettyDetailRepository.Find(x=>x.FinanceJettyId == IDHeader);
            var TotalCount = list.Count();
            var filterCount = list.Count();

            return new Tuple<IEnumerable<FinanceJettyDetail>, int, int>(list.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }
    }
}
