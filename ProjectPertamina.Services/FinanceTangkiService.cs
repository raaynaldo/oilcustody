﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface IFinanceTangkiService : IEntityService<FinanceTangki>
    {
        Tuple<IEnumerable<FinanceTangki>, int, int> SearchFinanceTangki(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, int PICKeuanganId = 0, int ShiftId = 0, int TangkiId = 0, int CargoId = 0 );
    }

    public class FinanceTangkiService : EntityService<FinanceTangki>, IFinanceTangkiService
    {
        IUnitOfWork _unitOfWork;
        IFinanceTangkiRepository _FinanceTangkiRepository;

        public FinanceTangkiService(IUnitOfWork unitOfWork, IFinanceTangkiRepository FinanceTangkiRepository)
            : base(unitOfWork, FinanceTangkiRepository)
        {
            _unitOfWork = unitOfWork;
            _FinanceTangkiRepository = FinanceTangkiRepository;
        }
        public Tuple<IEnumerable<FinanceTangki>, int, int> SearchFinanceTangki(int Skip, int length, DateTime? TanggalDari, DateTime? TanggalSampai, int PICKeuanganId = 0, int ShiftId = 0, int TangkiId = 0, int CargoId = 0)
        {
            IEnumerable<FinanceTangki> list = _FinanceTangkiRepository.GetAll().OrderByDescending(i => i.Id);
            var TotalCount = list.Count();

            if (PICKeuanganId != 0)
                list = list.Where(i => i.PICKeuanganId == PICKeuanganId);

            if (ShiftId != 0)
                list = list.Where(i => i.ShiftId == ShiftId);

            if (TangkiId != 0)
                list = list.Where(i => i.TangkiId == TangkiId);

            if (CargoId != 0)
                list = list.Where(i => i.CargoId == CargoId);

            if (TanggalDari != null)
                list = list.Where(i => i.Tanggal >= TanggalDari);

            if (TanggalSampai != null)
                list = list.Where(i => i.Tanggal <= TanggalSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<FinanceTangki>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
