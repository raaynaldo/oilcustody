﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ILoadingService : IEntityService<Loading>
    {
        Tuple<IEnumerable<Loading>, int, int> SearchLoading(int Skip, int length, string Code, string Desc);
    }

    public class LoadingService : EntityService<Loading>, ILoadingService
    {
        IUnitOfWork _unitOfWork;
        ILoadingRepository _loadingRepository;

        public LoadingService(IUnitOfWork unitOfWork, ILoadingRepository LoadingRepository)
            : base(unitOfWork, LoadingRepository)
        {
            _unitOfWork = unitOfWork;
            _loadingRepository = LoadingRepository;
        }
        public Tuple<IEnumerable<Loading>, int, int> SearchLoading(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Loading> list = _loadingRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Loading>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
