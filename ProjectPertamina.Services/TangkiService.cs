﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ITangkiService : IEntityService<Tangki>
    {
        Tuple<IEnumerable<Tangki>, int, int> SearchTangki(int Skip, int length, string Code, string Desc);
    }

    public class TangkiService : EntityService<Tangki>, ITangkiService
    {
        IUnitOfWork _unitOfWork;
        ITangkiRepository _tangkiRepository;

        public TangkiService(IUnitOfWork unitOfWork, ITangkiRepository TangkiRepository)
            : base(unitOfWork, TangkiRepository)
        {
            _unitOfWork = unitOfWork;
            _tangkiRepository = TangkiRepository;
        }
        public Tuple<IEnumerable<Tangki>, int, int> SearchTangki(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Tangki> list = _tangkiRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Tangki>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
