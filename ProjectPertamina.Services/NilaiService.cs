﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface INilaiService : IEntityService<Nilai>
    {
        Tuple<IEnumerable<Nilai>, int, int> SearchNilai(int Skip, int length, string Code, string Desc);
    }

    public class NilaiService : EntityService<Nilai>, INilaiService
    {
        IUnitOfWork _unitOfWork;
        INilaiRepository _nilaiRepository;

        public NilaiService(IUnitOfWork unitOfWork, INilaiRepository NilaiRepository)
            : base(unitOfWork, NilaiRepository)
        {
            _unitOfWork = unitOfWork;
            _nilaiRepository = NilaiRepository;
        }
        public Tuple<IEnumerable<Nilai>, int, int> SearchNilai(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Nilai> list = _nilaiRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Nilai>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
