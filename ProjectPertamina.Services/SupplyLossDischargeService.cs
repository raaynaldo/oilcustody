﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ISupplyLossDischargeService : IEntityService<SupplyLossDischarge>
    {
        Tuple<List<SupplyLossDischarge>, int, int> SearchSupplyLossDischarge(int Skip, int length, string TipeTransportasi, string LoadingPort, string NamaKapal, string NoBl,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        List<string> GetListTransportType();

        Tuple<string[], decimal[], decimal[]> getChart(string AlatAngkut, string LoadingPort, string Material,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);
        
        IEnumerable<SupplyLossDischarge> queryExportToExcel(string AlatAngkut, string LoadingPort, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        Tuple<IEnumerable<SupplyLossDischarge>, int, int> SearchSupplyLossDischargeReport(int Skip, int length, string AlatAngkut, string LoadingPort, string Material, string NoBl,
        DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai);

        IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai);
        IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeForSupplyLoss(decimal nilai, DateTime? tanggal, DateTime? tanggalSampai, string NamaKapal);

        IEnumerable<SupplyLossDischarge> GetAllJoinData();
    }
    public class SupplyLossDischargeService : EntityService<SupplyLossDischarge>, ISupplyLossDischargeService
    {
        IUnitOfWork _unitOfWork;
        ISupplyLossDischargeRepository _supplyLossDischargeRepository;

        public SupplyLossDischargeService(IUnitOfWork unitOfWork, ISupplyLossDischargeRepository SupplyLossDischargeRepository)
            : base(unitOfWork, SupplyLossDischargeRepository)
        {
            _unitOfWork = unitOfWork;
            _supplyLossDischargeRepository = SupplyLossDischargeRepository;
        }

        public Tuple<List<SupplyLossDischarge>, int, int> SearchSupplyLossDischarge(int Skip, int length, string TipeTransportasi, string LoadingPort, string NamaKapal, string NoBl,
    DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            //IEnumerable<SupplyLossDischarge> list = _supplyLossDischargeRepository.Find(i => !i.Deleted);
            IEnumerable<SupplyLossDischarge> list = _supplyLossDischargeRepository.GetAllJoinData().Where(x => !x.Deleted);
            var TotalCount = list.Count();

            TipeTransportasi = TipeTransportasi == null ? "" : TipeTransportasi;
            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            NamaKapal = NamaKapal == null ? "" : NamaKapal;
            NoBl = NoBl == null ? "" : NoBl;

            list = list.Where(i => i.TransportType.ToLower().Contains(TipeTransportasi.ToLower().Trim()) &&
            i.DischargePortDesc.ToLower().Contains(LoadingPort.ToLower().Trim()) &&
            i.TransportName.ToLower().Contains(NamaKapal.ToLower().Trim()) &&
            i.Roas.BlAsal.ToLower().Contains(NoBl.ToLower().Trim())
            ).OrderByDescending(i => i.Id).ToList();

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL).ToList();

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai).ToList();

            if (TanggalBongkar != null)
                list = list.Where(i => i.TanggalBongkar >= TanggalBongkar).ToList();

            if (TanggalBongkarSampai != null)
                list = list.Where(i => i.TanggalBongkar <= TanggalBongkarSampai).ToList();

            var filterCount = list.Count();

            return new Tuple<List<SupplyLossDischarge>, int, int>(list.Skip(Skip).Take(length).ToList(), TotalCount, filterCount);
        }

        public List<string> GetListTransportType()
        {
            return _supplyLossDischargeRepository.GetListTrasportType();
        }

       
        public Tuple<string[], decimal[], decimal[]> getChart(string AlatAngkut, string LoadingPort, string Material,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            var view = _supplyLossDischargeRepository.getValueSupplyLossDischarge(AlatAngkut, LoadingPort, Material,
               TanggalBL, TanggalBLSampai, TanggalBongkar, TanggalBongkarSampai);
            var TransportName = view.Select(x => x.TransportName);
            var R4Nett = view.Select(x => x.R4Nett);
            var PercNett = view.Select(x => x.PercNett);
            return new Tuple<string[], decimal[], decimal[]>(TransportName.ToArray(), R4Nett.ToArray(), PercNett.ToArray());
        }

        public IEnumerable<SupplyLossDischarge> queryExportToExcel(string AlatAngkut, string LoadingPort, string Material, DateTime? TanggalBL, DateTime? TanggalBLSampai,
           DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<SupplyLossDischarge> list = _supplyLossDischargeRepository.GetAllJoinData().Where(x => !x.Deleted);

            AlatAngkut = AlatAngkut == null ? "" : AlatAngkut;
            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            Material = Material == null ? "" : Material;

            list = list.Where(i => i.NamaProduk.ToLower().Contains(Material.ToLower()) &&
            i.LoadingPortDesc.ToLower().Contains(LoadingPort.ToLower()) &&
            i.TransportType.ToLower().Contains(AlatAngkut.ToLower())
            ).OrderByDescending(i => i.Id);

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                list = list.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                list = list.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            return list;
        }

        public Tuple<IEnumerable<SupplyLossDischarge>, int, int> SearchSupplyLossDischargeReport(int Skip, int length, string AlatAngkut, string LoadingPort, string Material, string NoBl,
        DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<SupplyLossDischarge> list = _supplyLossDischargeRepository.GetAllJoinData().Where(x => !x.Deleted);
            var TotalCount = list.Count();

            AlatAngkut = AlatAngkut == null ? "" : AlatAngkut;
            LoadingPort = LoadingPort == null ? "" : LoadingPort;
            Material = Material == null ? "" : Material;
            NoBl = NoBl == null ? "" : NoBl;

            list = list.Where(i => i.NamaProduk.ToLower().Contains(Material.ToLower()) &&
            i.LoadingPortDesc.ToLower().Contains(LoadingPort.ToLower()) &&
            i.TransportType.ToLower().Contains(AlatAngkut.ToLower())
            ).OrderByDescending(i => i.Id);

            if (TanggalBL != null)
                list = list.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                list = list.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                list = list.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                list = list.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<SupplyLossDischarge>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);

        }

        public IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return _supplyLossDischargeRepository.SearchSupplyLossDischargeEdit(TanggalBL, TanggalBLSampai);
            //return _supplyLossDischargeRepository.GetAllJoinData().Where(i => !i.Deleted && i.BlDate >= TanggalBL && i.BlDate <= TanggalBLSampai);
        }

        public IEnumerable<SupplyLossDischarge> GetAllJoinData()
        {
            return _supplyLossDischargeRepository.GetAllJoinData();
        }

        public IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeForSupplyLoss(decimal nilai, DateTime? tanggal, DateTime? tanggalSampai, string NamaKapal)
        {
            return _supplyLossDischargeRepository.SearchSupplyLossDischargeForSupplyLoss(nilai, tanggal, tanggalSampai, NamaKapal);
        }
    }
}
