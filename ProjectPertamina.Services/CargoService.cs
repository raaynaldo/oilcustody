﻿using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Services.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Services
{
    public interface ICargoService : IEntityService<Cargo>
    {
        Tuple<IEnumerable<Cargo>, int, int> SearchCargo(int Skip, int length, string Code, string Desc);
    }

    public class CargoService : EntityService<Cargo>, ICargoService
    {
        IUnitOfWork _unitOfWork;
        ICargoRepository _CargoRepository;

        public CargoService(IUnitOfWork unitOfWork, ICargoRepository CargoRepository)
            : base(unitOfWork, CargoRepository)
        {
            _unitOfWork = unitOfWork;
            _CargoRepository = CargoRepository;
        }
        public Tuple<IEnumerable<Cargo>, int, int> SearchCargo(int Skip, int length, string Code, string Desc)
        {
            IEnumerable<Cargo> list = _CargoRepository.GetAll();
            var TotalCount = list.Count();

            Code = Code == null ? "" : Code;
            Desc = Desc == null ? "" : Desc;

            list = list.Where(i => i.Code.ToLower().Contains(Code.ToLower()) && i.Description.ToLower().Contains(Desc.ToLower())).OrderByDescending(i => i.Id);

            var filterCount = list.Count();

            return new Tuple<IEnumerable<Cargo>, int, int>(list.Skip(Skip).Take(length), TotalCount, filterCount);
        }
    }
}
