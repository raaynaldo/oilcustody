﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories
{
    public class RoasRepository : GenericRepository<Roas>, IRoasRepository
    {
        public RoasRepository(DbContext context)
            : base(context)
        {

        }

        public List<Roas> GetRoasForCargoLosses()
        {
            using (BaseContext context = new BaseContext())
            {
                var model = context.Roas.Where(i => i.TransportType == "VESSEL" && i.KodeOpr == "D").Include("Port").ToList();
                var cargolosses = context.CargoLosses.ToList();

                var list = (from roas in model
                            join cargo in cargolosses
                            on roas.Id equals cargo.ROASId into listbaru
                            from cargoKosong in listbaru.DefaultIfEmpty()
                            where cargoKosong == null
                            select roas
                            ).ToList();

                return list;
            }
        }

        public List<Roas> GetRoasForSupplyLossSupply()
        {
            using (BaseContext context = new BaseContext())
            {
                var model = context.Roas.Where(i => i.KodeOpr == "S").Include("Port").ToList();
                var supplyLossSupply = context.SupplyLossSupply.ToList();

                var list = (from roas in model
                            join SLS in supplyLossSupply
                            on roas.Id equals SLS.ROASId into listbaru
                            from cargoKosong in listbaru.DefaultIfEmpty()
                            where cargoKosong == null
                            select roas
                            ).ToList();

                return list;
            }
        }

        public List<Roas> GetRoasForSupplyLossDischarge()
        {
            using (BaseContext context = new BaseContext())
            {
                var model = context.Roas.Where(i => (i.TransportType == "VESSEL" || i.TransportType == "PIPA") && i.KodeOpr == "D").Include("Port").ToList();
                var supplyLossDischarge = context.SupplyLossDischarge.ToList();

                var list = (from roas in model
                            join SLS in supplyLossDischarge
                            on roas.Id equals SLS.ROASId into listbaru
                            from cargoKosong in listbaru.DefaultIfEmpty()
                            where cargoKosong == null
                            select roas
                            ).ToList();

                return list;
            }
        }

        public List<Roas> GetRoasForKKSupplyLoss()
        {
            using (BaseContext context = new BaseContext())
            {
                var model = context.Roas.Where(i => i.KodeOpr == "D").Include("Port").ToList();
                var kkSupplyLoss = context.KKSupplyLoss.ToList();

                var list = (from roas in model
                            join SLS in kkSupplyLoss
                            on roas.Id equals SLS.ROASId into listbaru
                            from cargoKosong in listbaru.DefaultIfEmpty()
                            where cargoKosong == null
                            select roas
                            ).ToList();

                return list;
            }
        }

        public Roas GetByIdJoinData(int Id)
        {
            using (BaseContext context = new BaseContext())
            {
                return context.Roas.Include("Port").Where(i => i.Id == Id).FirstOrDefault();
            }
        }
    }
}
