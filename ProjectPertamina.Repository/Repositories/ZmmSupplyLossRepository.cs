﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using System.Data.Entity;

namespace ProjectPertamina.Repository.Repositories
{
    public class ZmmSupplyLossRepository : GenericRepository<ZmmSupplyLoss>, IZmmSupplyLossRepository
    {
        public ZmmSupplyLossRepository(DbContext context)
            :base(context)
        {

        }
    }
}
