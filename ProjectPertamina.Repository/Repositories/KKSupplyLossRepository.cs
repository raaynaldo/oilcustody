﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities.Models.ViewModel;

namespace ProjectPertamina.Repository.Repositories
{
    public class KKSupplyLossRepository : GenericRepository<KKSupplyLoss>, IKKSupplyLossRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public KKSupplyLossRepository(DbContext context)
            : base(context)
        {

        }

        public List<string> GetListTrasportType()
        {
            using (BaseContext context = new BaseContext())
            {
                return context.KKSupplyLoss.Select(i => i.Roas.TransportType).Distinct().ToList();
            }
        }

        public Tuple< List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>> getValueKKSupplyLoss(string LoadingPort, string Produk, string Vessel,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalCQD, DateTime? TanggalCQDSampai)
        {
            IEnumerable<KKSupplyLoss> KKsupplylosess = GetAllJoinData().Where(i => !i.Deleted);
            KKsupplylosess = KKsupplylosess.Where(y => y.LoadingPort.ToLower().Contains(LoadingPort.ToLower()) &&
            y.NamaProduk.ToLower().Contains(Produk.ToLower()) &&
            y.TransportName.ToLower().Contains(Vessel.ToLower()));
            if (TanggalBL != null)
                KKsupplylosess = KKsupplylosess.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                KKsupplylosess = KKsupplylosess.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalCQD != null)
                KKsupplylosess = KKsupplylosess.Where(i => i.DocDate >= TanggalCQD);

            if (TanggalCQDSampai != null)
                KKsupplylosess = KKsupplylosess.Where(i => i.DocDate <= TanggalCQDSampai);

            var KKsupplylosess_Pipa = KKsupplylosess.Where(x => x.Roas.TransportType.Contains("PIPA"));
            KKsupplylosess_Pipa = KKsupplylosess_Pipa.OrderByDescending(x => x.PercNett);

            var KKsupplylosess_Vessel = KKsupplylosess.Where(x => x.Roas.TransportType.Contains("VESSEL"));
            KKsupplylosess_Vessel = KKsupplylosess_Vessel.OrderByDescending(x => x.PercNett);

            //KKsupplylosess = KKsupplylosess.OrderByDescending(x => x.PercNett);

            var chartSupplyPipa = KKsupplylosess_Pipa.Select(
                 y => new VMKKSupplyLossPipa
                 {
                     BlDate = y.BlDate,
                     TransportName = y.TransportName,
                     D1QualityBl = y.QualityBl,
                     D1QualitiTanki = y.QualityTanki

                 }).Take(10).ToList();

            var chartSupplyVessel = KKsupplylosess_Vessel.Select(
                 y => new VMKKSupplyLossVessel
                 {
                     BlDate = y.BlDate,
                     TransportName = y.TransportName,
                     D2ObqAwal = y.QualityBl,
                     D2ObqAkhir = y.QualityTanki,
                     D2BarrelsRob = y.BarrelsRob
                 }).Take(10).ToList();
            return new Tuple<List<VMKKSupplyLossPipa>, List<VMKKSupplyLossVessel>>(chartSupplyPipa, chartSupplyVessel);
        }

        public IEnumerable<KKSupplyLoss> GetAllJoinData()
        {
            var query = dbCtx.KKSupplyLoss.Where(i => !i.Deleted).Include(m => m.Roas);
            return query;
        }

        public List<KKSupplyLoss> SearchKKSupplyLossEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return dbCtx.KKSupplyLoss.Where(i => i.BlDate >= TanggalBL && i.BlDate <= TanggalBLSampai && !i.Deleted).Include("Roas").ToList();
        }

    }
}
