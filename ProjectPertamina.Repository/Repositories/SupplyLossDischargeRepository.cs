﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities.Models.ViewModel;

namespace ProjectPertamina.Repository.Repositories
{
    public class SupplyLossDischargeRepository : GenericRepository<SupplyLossDischarge>, ISupplyLossDischargeRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public SupplyLossDischargeRepository(DbContext context)
            : base(context)
        {

        }

        public List<string> GetListTrasportType()
        {
            using (BaseContext context = new BaseContext())
            {
                return context.SupplyLossDischarge.Select(i => i.TransportType).Distinct().ToList();
            }
        }


        public List<VMSupplyLossDischarge> getValueSupplyLossDischarge(string AlatAngkut, string LoadingPort, string Material,
           DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<SupplyLossDischarge> supplylosess = Find(i => !i.Deleted);
            supplylosess = supplylosess.Where(y => y.TransportType.ToLower().Contains(AlatAngkut.ToLower()) &&
            y.NamaProduk.ToLower().Contains(Material.ToLower()) &&
            y.LoadingPortDesc.ToLower().Contains(LoadingPort.ToLower()));
            if (TanggalBL != null)
                supplylosess = supplylosess.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                supplylosess = supplylosess.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                supplylosess = supplylosess.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                supplylosess = supplylosess.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            supplylosess = supplylosess.OrderByDescending(x => x.PercNett);

            var chartSupply = supplylosess.Select(
                y => new VMSupplyLossDischarge
                {
                    TransportName = y.TransportName,
                    R4Nett = y.R4Nett,
                    PercNett = y.PercNett
                }).Take(10);
            return chartSupply.ToList();
        }

        public IEnumerable<SupplyLossDischarge> GetAllJoinData()
        {
            var query = dbCtx.SupplyLossDischarge.Where(i => !i.Deleted).Include(m => m.Roas);
            return query;
        }

        public IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return dbCtx.SupplyLossDischarge.Where(i => !i.Deleted && i.BlDate >= TanggalBL && i.BlDate <= TanggalBLSampai).Include("Roas");
        }

        public IEnumerable<SupplyLossDischarge> SearchSupplyLossDischargeForSupplyLoss(decimal nilai, DateTime? Tanggal, DateTime? TanggalSampai, string NamaKapal)
        {
            //var listSupplyLoss = dbCtx.SupplyLoss.ToList();
            //return dbCtx.SupplyLossDischarge.Where(i=> !listSupplyLoss.Any(x=> x.SupplyLossDischargeId == i.Id) && !i.Deleted && i.R4Nett > nilai && i.DocDate >= Tanggal && i.DocDate <= TanggalSampai && i.TransportName.ToLower().Trim() == NamaKapal.ToLower().Trim()).Include("Roas");

            var listSupplyLoss = dbCtx.SupplyLoss.Where(i => i.SupplyLossSupplyId != null).ToList();
            var listSupplyLossDischarge = dbCtx.SupplyLossDischarge.Where(i => (i.PercNett > nilai || i.PercNett < -nilai) && !i.Deleted  && i.DocDate >= Tanggal && i.DocDate <= TanggalSampai && i.TransportName.ToLower().Trim() == NamaKapal.ToLower().Trim()).Include("Roas").ToList();

            return listSupplyLossDischarge.Where(i => !listSupplyLoss.Any(x => x.SupplyLossDischargeId == i.Id)).ToList();
        }
    }
}
