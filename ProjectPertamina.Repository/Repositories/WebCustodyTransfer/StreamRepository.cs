﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories.WebCustodyTransfer
{
    public class StreamRepository : GenericRepository<Stream>, IStreamRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public StreamRepository(DbContext context)
            : base(context)
        {

        }

        public IEnumerable<Stream> GetAllJoinData()
        {
            return dbCtx.Stream.Include("TipeProduk");
        }

        public IEnumerable<Stream> GetAllJoinData(Expression<Func<Stream, bool>> predicate)
        {
            return dbCtx.Stream.Where(predicate).Include("TipeProduk");
        }
    }
}
