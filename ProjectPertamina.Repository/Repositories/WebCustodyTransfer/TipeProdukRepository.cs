﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories.WebCustodyTransfer
{
    public class TipeProdukRepository : GenericRepository<TipeProduk>, ITipeProdukRepository
    {
        BaseContext Dbctx = new BaseContext();
        public TipeProdukRepository(DbContext context)
            : base(context)
        {

        }

    }
}
