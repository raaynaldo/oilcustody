﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Common.Interfaces.RepoInterfaces.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories.WebCustodyTransfer
{
    public class CustodyTransferRepository : GenericRepository<CustodyTransfer>, ICustodyTransferRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public CustodyTransferRepository(DbContext context)
            : base(context)
        {

        }

        public IEnumerable<CustodyTransfer> GetAllJoinData()
        {
            return dbCtx.CustodyTransfer.Include("Stream.TipeProduk");
        }

        public IEnumerable<CustodyTransfer> GetAllJoinData(int bulan, int tahun)
        {
            return dbCtx.CustodyTransfer.Where(i => i.Tanggal.Month == bulan && i.Tanggal.Year == tahun)
                //.Include("Perusahaan")
                .Include("Stream.TipeProduk");
        }

        public CustodyTransfer GetJoinAllDatabyId(int id)
        {
            return dbCtx.CustodyTransfer.Where(i => i.Id == id)
                //.Include("Perusahaan")
                .Include("Stream.TipeProduk")
                .FirstOrDefault();
        }
    }
}
