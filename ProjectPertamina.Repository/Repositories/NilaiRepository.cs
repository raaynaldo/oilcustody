﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories
{
    public class NilaiRepository : GenericRepository<Nilai>, INilaiRepository
    {
        public NilaiRepository(DbContext context)
            : base(context)
        {

        }
    }
}
