﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Repository.Repositories
{
    public class SupplyLossRepository : GenericRepository<SupplyLoss>, ISupplyLossRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public SupplyLossRepository(DbContext context)
            : base(context)
        {

        }

        public SupplyLoss GetBySupplyLossDischarge(int supplyLossDischargeId)
        {
            return dbCtx.SupplyLoss.Where(i => i.SupplyLossDischargeId == supplyLossDischargeId).FirstOrDefault();
        }

        public SupplyLoss GetBySupplyLossSupply(int supplyLossSupplyId)
        {
            return dbCtx.SupplyLoss.Where(i => i.SupplyLossSupplyId == supplyLossSupplyId).FirstOrDefault();
        }

        public IEnumerable<SupplyLoss> GetAllJoinData()
        {
            return dbCtx.SupplyLoss.Include("Loading");
        }

        public IEnumerable<SupplyLoss> SearchByDocDate(DateTime? tanggal, DateTime? tanggalSampai, string NamaKapal)
        {
            return dbCtx.SupplyLoss.Where(i => i.Tanggal >= tanggal && i.Tanggal <= tanggalSampai && i.NamaKapal.ToLower().Trim() == NamaKapal.ToLower().Trim());
        }
    }
}
