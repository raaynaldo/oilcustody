﻿using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Entities.Models.ViewModel;

namespace ProjectPertamina.Repository.Repositories
{
    public class SupplyLossSupplyRepository : GenericRepository<SupplyLossSupply>, ISupplyLossSupplyRepository
    {
        private BaseContext dbCtx = new BaseContext();

        public SupplyLossSupplyRepository(DbContext context)
            : base(context)
        {

        }
        public List<string> GetListTrasportType()
        {
            using (BaseContext context = new BaseContext())
            {
                return context.SupplyLossSupply.Select(i => i.TransportType).Distinct().ToList();
            }
        }

        public List<VMSupplyLossesSupply> getValueSupplyLossSupply(string PortDescription, string NamaProduk, string TransportType,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<SupplyLossSupply> supplylosess = Find(i => !i.Deleted);
            supplylosess = supplylosess.Where(y => y.TransportType.ToLower().Contains(TransportType.ToLower()) &&
            y.NamaProduk.ToLower().Contains(NamaProduk.ToLower()) &&
            y.DischargePortDesc.ToLower().Contains(PortDescription.ToLower()));
            if (TanggalBL != null)
                supplylosess = supplylosess.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                supplylosess = supplylosess.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                supplylosess = supplylosess.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                supplylosess = supplylosess.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);

            supplylosess = supplylosess.OrderByDescending(x => x.PercR1);

            var chartSupply = supplylosess.Select(
                y => new VMSupplyLossesSupply
                {
                    TransportName = y.TransportName,
                    R1 = y.R1,
                    PercR1 = y.PercR1
                }).Take(10);
            return chartSupply.ToList();
        }

        public IEnumerable<SupplyLossSupply> GetAllJoinData()
        {
            var query = dbCtx.SupplyLossSupply.Where(i => !i.Deleted).Include(m => m.Roas);
            return query;
        }

        public List<SupplyLossSupply> SearchSupplyLossSupplyEdit(DateTime? TanggalBL, DateTime? TanggalBLSampai)
        {
            return dbCtx.SupplyLossSupply.Where(i => !i.Deleted && i.TanggalBongkar >= TanggalBL && i.TanggalBongkar <= TanggalBLSampai).Include("Roas").ToList();
        }

        public List<SupplyLossSupply> SearchSupplyLossSupplyForSupplyLoss(decimal nilai, DateTime? tanggal, DateTime? tanggalSampai, string NamaKapal)
        {
            var listSupplyLoss = dbCtx.SupplyLoss.Where(i => i.SupplyLossSupplyId != null).ToList();
            var listSupplyLossSupply = dbCtx.SupplyLossSupply.Where(i => (i.PercR1 > nilai || i.PercR1 < -nilai) && !i.Deleted && i.DocDate >= tanggal && i.DocDate <= tanggalSampai && i.TransportName.ToLower().Trim() == NamaKapal.ToLower().Trim()).Include("Roas").ToList();

            return listSupplyLossSupply.Where(i => !listSupplyLoss.Any(x => x.SupplyLossSupplyId == i.Id)).ToList();
        }
    }
}
