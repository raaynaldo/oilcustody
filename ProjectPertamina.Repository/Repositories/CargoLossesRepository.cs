﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Common.Interfaces.RepoInterfaces;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities.Models.ViewModel;
using ProjectPertamina.Repository.BaseRepository;

namespace ProjectPertamina.Repository.Repositories
{
    public class CargoLossesRepository : GenericRepository<CargoLosses>, ICargoLossesRepository
    {
        private BaseContext dbCtx = new BaseContext();

        private readonly BaseContext _dbContext;
        public CargoLossesRepository(DbContext context)
            : base(context)
        {
            _dbContext = (_dbContext ?? (BaseContext)context);
        }

        public List<VMChartCargoLosses> getValueCargoLosses(string Muatan, string Asal, string NamaKapal,
            DateTime? TanggalBL, DateTime? TanggalBLSampai, DateTime? TanggalBongkar, DateTime? TanggalBongkarSampai)
        {
            IEnumerable<CargoLosses> cargolosess = Find(i => !i.Deleted);
            cargolosess = cargolosess.Where(y => y.NamaProduk.ToLower().Contains(Muatan.ToLower()) &&
            y.NamaProduk.ToLower().Contains(Asal.ToLower()) && y.TransportName.ToLower().Contains(NamaKapal.ToLower()));
            if (TanggalBL != null)
                cargolosess = cargolosess.Where(i => i.BlDate >= TanggalBL);

            if (TanggalBLSampai != null)
                cargolosess = cargolosess.Where(i => i.BlDate <= TanggalBLSampai);

            if (TanggalBongkar != null)
                cargolosess = cargolosess.Where(i => i.TanggalBongkar >= TanggalBongkar);

            if (TanggalBongkarSampai != null)
                cargolosess = cargolosess.Where(i => i.TanggalBongkar <= TanggalBongkarSampai);
            cargolosess = cargolosess.OrderByDescending(x => x.PercR4);

            var chartCargo = cargolosess.Select(
                y => new VMChartCargoLosses
                {
                    TransportName = y.TransportName,
                    PercR4 = y.PercR4,
                    R4Values = y.R4
                }).Take(10);
            return chartCargo.ToList();
        }

        public IEnumerable<CargoLosses> GetAllJoinData()
        {
            var query = dbCtx.CargoLosses.Where(i => !i.Deleted).Include(m => m.Roas);
            return query;
        }

        public List<CargoLosses> SearchCargoLossesEdit(DateTime? tanggalBL, DateTime? TanggalBLSampai)
        {
            return dbCtx.CargoLosses.Where(i => i.BlDate >= tanggalBL && i.BlDate <= TanggalBLSampai && !i.Deleted).Include("Roas").ToList();
        }
        //public CargoLosses GetByIdJoinData(int Id)
        //{
        //    return dbCtx.CargoLosses.Include(m => m.Roas).Where(i => i.Id == Id).FirstOrDefault();
        //}
    }
}
