﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectPertamina.Common.Interfaces.BaseInterfaces;
using ProjectPertamina.Entities;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ProjectPertamina.Repository.BaseRepository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntities
    {
        protected readonly DbContext db;

        public GenericRepository(DbContext _db)
        {
            db = _db;
        }

        public IEnumerable<T> GetAll()
        {
            return db.Set<T>().ToList();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>().Where(predicate);
        }

        public T Get(object Id)
        {
            return db.Set<T>().Find(Id);
        }

        public void Add(T entity)
        {
            db.Set<T>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            db.Set<T>().AddRange(entities);
        }

        //public virtual void SaveMany(IEnumerable<T> entities)
        //{
        //    foreach (var item in entities)
        //    {
        //        db.Set<T>().Add(item);
        //    }
        //}

        //public void Remove(T entity)
        //{
        //    entity.StatusData = false;
        //    entity.DeletedDate = DateTime.Now;
        //    db.Entry<T>(entity).State = EntityState.Modified;
        //}

        public void RemoveRange(IEnumerable<T> entities)
        {
            db.Set<T>().RemoveRange(entities);
        }

        //public virtual void Remove(object Id)
        //{
        //    T entity = db.Set<T>().Find(Id);
        //    entity.StatusData = false;
        //    entity.DeletedDate = DateTime.Now;
        //    db.Entry<T>(entity).State = EntityState.Modified;
        //}

        public virtual void Remove(object Id)
        {
            T entity = db.Set<T>().Find(Id);
            db.Set<T>().Remove(entity);
        }

        public virtual void Update(T entity)
        {
            db.Entry<T>(entity).State = EntityState.Modified;
        }
    }
}
