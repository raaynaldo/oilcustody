﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ProjectPertamina.Entities.Models.WebCustodyTransfer;
using ProjectPertamina.Entities.Models;
using ProjectPertamina.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using ProjectPertamina.UserManager;

namespace ProjectPertamina.Repository.BaseRepository
{
    public class BaseContext : DbContext
    {
        public BaseContext() : base(getConnectionString())
        {
            Database.SetInitializer<BaseContext>(new CreateDatabaseIfNotExists<BaseContext>());
            //Configuration.LazyLoadingEnabled = true;
        }

        private static string getConnectionString()
        {
            string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            return conn;
        }

        #region DBSetOilCustody
        public DbSet<Cargo> Cargo { get; set; }
        public DbSet<CargoLosses> CargoLosses { get; set; }
        public DbSet<FinanceJetty> FinanceJetty { get; set; }
        public DbSet<FinanceJettyDetail> FinanceJettyDetail { get; set; }
        public DbSet<FinancePipa> FinancePipa { get; set; }
        public DbSet<FinancePipaDetail> FinancePipaDetail { get; set; }
        public DbSet<FinanceTangki> FinanceTangki { get; set; }
        public DbSet<FinanceTangkiDetail> FinanceTangkiDetail { get; set; }
        public DbSet<KKSupplyLoss> KKSupplyLoss { get; set; }
        public DbSet<KonfigurasiParameter> KonfigurasiParameter { get; set; }
        public DbSet<Loading> Loading { get; set; }
        public DbSet<Nilai> Nilai { get; set; }
        public DbSet<OilMovement> OilMovement { get; set; }
        public DbSet<PICKeuangan> PICKeuangan { get; set; }
        public DbSet<PICLoadingMaster> PICLoadingMaster { get; set; }
        public DbSet<Port> Port { get; set; }
        public DbSet<Roas> Roas { get; set; }
        public DbSet<Shift> Shift { get; set; }
        public DbSet<SupplyLoss> SupplyLoss { get; set; }
        public DbSet<SupplyLossDischarge> SupplyLossDischarge { get; set; }
        public DbSet<SupplyLossSupply> SupplyLossSupply { get; set; }
        public DbSet<Tangki> Tangki { get; set; }
        #endregion

        #region DBSetCustodyTransfer
        public DbSet<CustodyTransfer> CustodyTransfer { get; set; }
        public DbSet<Perusahaan> Perusahaan { get; set; }
        public DbSet<Tabel21> Tabel21 { get; set; }
        public DbSet<Tabel52> Tabel52 { get; set; }
        public DbSet<Tabel56> Tabel56 { get; set; }
        public DbSet<Tabel57> Tabel57 { get; set; }
        public DbSet<TipeProduk> TipeProduk { get; set; }
        public DbSet<SGHidrogen> SGHidrogen { get; set; }
        public DbSet<Stream> Stream { get; set; }
        public DbSet<ZmmSupplyLoss> ZmmSupplyLoss { get; set; }
        public DbSet<BeritaAcara> BeritaAcara { get; set; }
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<string>().Configure(c => c.HasColumnType("varchar"));

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<FinancePipa>()
                .HasMany(e => e.FinancePipaDetails)
                .WithRequired(e => e.FinancePipa)
                .HasForeignKey(e => e.FinancePipaId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<FinanceTangki>()
                .HasMany(e => e.FinanceTangkiDetails)
                .WithRequired(e => e.FinanceTangki)
                .HasForeignKey(e => e.FinanceTangkiId)
                .WillCascadeOnDelete(true);

            #region Precision
            modelBuilder.Properties<decimal>().Configure(c => c.HasPrecision(18, 3));

            modelBuilder.Entity<OilMovement>().Property(p => p.MeterAwal).HasPrecision(18, 2);
            modelBuilder.Entity<OilMovement>().Property(p => p.MeterAkhir).HasPrecision(18, 2);
            modelBuilder.Entity<OilMovement>().Property(p => p.KapBridger).HasPrecision(18, 2);

            modelBuilder.Entity<Nilai>().Property(p => p.Value).HasPrecision(18, 2);

            modelBuilder.Entity<FinanceJetty>().Property(p => p.PumpRate).HasPrecision(18, 2);
            modelBuilder.Entity<FinanceJetty>().Property(p => p.Qty).HasPrecision(18, 2);

            modelBuilder.Entity<FinancePipa>().Property(p => p.PumpRate).HasPrecision(18, 2);
            modelBuilder.Entity<FinancePipa>().Property(p => p.Qty).HasPrecision(18, 2);

            modelBuilder.Entity<FinanceTangki>().Property(p => p.Qty).HasPrecision(18, 2);
            modelBuilder.Entity<FinanceTangkiDetail>().Property(p => p.JumlahKl).HasPrecision(18, 2);

            modelBuilder.Entity<CustodyTransfer>().Property(p => p.MT).HasPrecision(18, 5);
            modelBuilder.Entity<CustodyTransfer>().Property(p => p.SG).HasPrecision(18, 15);
            modelBuilder.Entity<CustodyTransfer>().Property(p => p.Kwh).HasPrecision(18, 4);

            modelBuilder.Entity<Tabel21>().Property(p => p.Density15).HasPrecision(18, 4);

            modelBuilder.Entity<Tabel52>().Property(p => p.UsGallon60F).HasPrecision(18, 5);
            modelBuilder.Entity<Tabel52>().Property(p => p.Barrel60F).HasPrecision(18, 4);

            modelBuilder.Entity<Tabel56>().Property(p => p.LiterMetricTon).HasPrecision(18, 1);

            modelBuilder.Entity<Tabel57>().Property(p => p.LongTerm100L).HasPrecision(18, 4);

            modelBuilder.Entity<SGHidrogen>().Property(p => p.NilaiSG).HasPrecision(18, 4);
            #endregion


        }

        public override int SaveChanges()
        {
            CurrentUser user = CurrentUser.GetCurrentUser();
            var Entities = ChangeTracker.Entries<BaseEntities>();
            foreach (var item in Entities)
            {
                if (item.State == System.Data.Entity.EntityState.Added)
                {
                    item.Entity.CreatedBy = user.UserName;
                    item.Entity.CreatedDate = DateTime.Now;
                }
                else
                {
                    base.Entry(item.Entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(item.Entity).Property(x => x.CreatedDate).IsModified = false;
                }

                item.Entity.UpdatedBy = user.UserName;
                item.Entity.UpdatedDate = DateTime.Now;

            }
            return base.SaveChanges();
        }
    }
}
