﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class UserLogin
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public bool ActionView { get; set; }
        public bool ActionCreate { get; set; }
        public bool ActionUpdate { get; set; }
        public bool ActionDelete { get; set; }
        public bool ActionApprove { get; set; }
        public string InModule { get; set; }
    }
}
