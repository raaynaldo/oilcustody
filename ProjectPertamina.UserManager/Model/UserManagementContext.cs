﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class UserManagementContext : DbContext
    {
        public UserManagementContext(): base(getConnectionString())
        {
            Database.SetInitializer<UserManagementContext>(new CreateDatabaseIfNotExists<UserManagementContext>());
            Configuration.LazyLoadingEnabled = false;
        }

        private static string getConnectionString()
        {
            string conn = ConfigurationManager.ConnectionStrings["UserManagerConnectionString"].ConnectionString;
            return conn;
        }

        #region dbset
        public DbSet<Role> Role { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<MenuRole> MenuRole { get; set; }
        public DbSet<Application> Application { get; set; }
        public DbSet<Perusahaan> Perusahaan { get; set; }
        public DbSet<Jabatan> Jabatan { get; set; }
        public DbSet<User> User { get; set; }
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<string>().Configure(c => c.HasColumnType("varchar"));
        }
    }
}
