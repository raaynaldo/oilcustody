﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class User
    {
        public int Id { get; set; }
        public string UID { get; set; }
        public string NoPek { get; set; }
        public string NamaPekerja { get; set; }
        public int JabatanID { get; set; }
        public virtual Jabatan Jabatan { get; set; }
        public int PerusahaanID { get; set; }
        public virtual Perusahaan Perusahaan { get; set; }

    }
}
