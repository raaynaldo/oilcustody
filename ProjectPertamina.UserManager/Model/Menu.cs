﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class Menu
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public int? Parent { get; set; }
        public string UrlFile { get; set; }
        public int? Order { get; set; }
        public string ModuleDescr { get; set; }
        public int ApplicationID { get; set; }
        public virtual Application Application { get; set; }
        public bool Status { get; set; }
        //public bool StatusData { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public string UpdatedBy { get; set; }
        //public DateTime? UpdatedDate { get; set; }
        //public string DeletedBy { get; set; }
        //public DateTime? DeletedDate { get; set; }
    }
}
