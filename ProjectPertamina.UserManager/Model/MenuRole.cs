﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class MenuRole
    {
        public int Id { get; set; }
        public int RoleID { get; set; }
        public virtual Role Role { get; set; }
        public int MenuID { get; set; }
        public virtual Menu Menu { get; set; }
        public bool ActionView { get; set; }
        public bool ActionCreate { get; set; }
        public bool ActionUpdate { get; set; }
        public bool ActionDelete { get; set; }
        public bool ActionApprove { get; set; }
        public bool Status { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public string UpdatedBy { get; set; }
        //public DateTime? UpdatedDate { get; set; }
        //public bool StatusData { get; set; }
        //public string DeletedBy { get; set; }
        //public DateTime? DeletedDate { get; set; }
    }
}
