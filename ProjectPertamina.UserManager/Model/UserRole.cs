﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Model
{
    public class UserRole
    {
        public int Id { get; set; }
        public string UID { get; set; }        
        public string Nama { get; set; }
        public string Email { get; set; }
        public int RoleID { get; set; }
        public virtual Role Role { get; set; }
        //public bool Status { get; set; }
    }
}
