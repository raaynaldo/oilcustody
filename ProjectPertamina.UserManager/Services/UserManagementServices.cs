﻿using ProjectPertamina.UserManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.UserManager.Services
{
    public class UserManagementServices
    {
        public static UserRole getUser(string Userid)
        {
            UserRole usr = null;
            using (UserManagementContext ctx = new UserManagementContext())
            {                
                usr = ctx.UserRole.Where(x => x.UID == Userid )//&&  x.Status == true)
                    .FirstOrDefault();
            }
            return usr;
        }
        public static UserLogin getUserRole(string Userid, string AppName)
        {
            UserLogin usr = null;
            using (UserManagementContext ctx = new UserManagementContext())
            {
                List<int> Idrole = ctx.MenuRole.Where(x => x.Menu.Application.ApplicationName == AppName
                && x.Status == true)
                .GroupBy(z => z.RoleID).Select(y => y.Key).ToList();
                IEnumerable<UserLogin> usrRole = ctx.UserRole.Include("Role").Where(x => Idrole.Contains(x.RoleID) && x.UID == Userid
                      && x.Role.Status == true).OrderBy(y => y.RoleID).Select(y => new UserLogin
                      {
                          UserName = y.UID,
                          RoleID = y.RoleID,
                          RoleName = y.Role.Description,
                          DisplayName = y.Nama
                      });
                usr = usrRole.FirstOrDefault();
            }

            return usr;
        }

        public static MenuRole getAuthor(int RoleID, string MenuURL, string AppName)
        {
            MenuRole data = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                data = context.MenuRole.Include("Role")
                .Where(x => x.ActionView == true &&
                x.Menu.UrlFile == MenuURL &&
                x.RoleID == RoleID && x.Menu.Application.ApplicationName == AppName && x.Status == true)// && x.StatusData == true)
                .FirstOrDefault();
                return data;
            }
        }

        public static List<Menu> getMenuRoot(string applName, int roleId)
        {
            List<Menu> menu = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                List<int> IdMenu = context.MenuRole.Where(x => x.Status == true && x.RoleID == roleId) //&& x.StatusData == true )
                    .GroupBy(z => z.MenuID).Select(y => y.Key).ToList();
                menu = context.Menu.Include("Application")
                    .Where(x => x.Application.ApplicationName == applName &&
                    IdMenu.Contains(x.Id) && x.Status == true && ((x.Parent == 0) || (x.Parent == null)))//&& x.StatusData == true)
                    .OrderBy(y => y.Order).ToList();
            }
            return menu;
        }

        public static List<Menu> getChildMenu(int idHeader, int RoleId)
        {
            List<Menu> menu = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                List<int> IdMenu = context.MenuRole.Include("Menu").Where(x => x.Status == true
                                 && x.Menu.Parent == idHeader && x.RoleID == RoleId && x.ActionView == true)
                                 .GroupBy(z => z.MenuID).Select(y => y.Key).ToList();
                menu = context.Menu.Where(x => IdMenu.Contains(x.Id) && x.Status == true).OrderBy(y => y.Order).ThenBy(y => y.MenuName).ToList();
            }
            return menu;
        }


        public static User getUserJabatan(string loginName)
        {
            User user = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                user = context.User.Include("Jabatan").Include("Perusahaan").Where(x => x.UID == loginName).FirstOrDefault();
            }
            return user;
        }

        public static List<Perusahaan> getPerusahaan()
        {
            List<Perusahaan> Perusahaan = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                Perusahaan = context.Perusahaan.ToList();
            }
            return Perusahaan;
        }

        public static List<User> getUserNopek()
        {
            List<User> User = null;
            using (UserManagementContext context = new UserManagementContext())
            {
                User = context.User.ToList();
            }
            return User;
        }
    }
}
