﻿using ProjectPertamina.UserManager.Model;
using ProjectPertamina.UserManager.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ProjectPertamina.UserManager
{
    public class CurrentUser : GenericIdentity
    {
        #region "Properties"
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public bool ActionView { get; set; }
        public bool ActionCreate { get; set; }
        public bool ActionUpdate { get; set; }
        public bool ActionDelete { get; set; }
        public bool ActionApprove { get; set; }
        public string InModule { get; set; }
        public string Nopek { get; set; }
        public string NamaPek { get; set; }
        public string IDJabatan { get; set; }
        public string NamaJabatan { get; set; }
        public string IDPerusahaan { get; set; }
        public string NamaPerusahaan { get; set; }
        public CurrentUser _userRoleService { get; set; }
        #endregion

        protected CurrentUser()
            : this(HttpContext.Current.User.Identity.Name)
        {

        }

        public CurrentUser(string logonIDorCAI)
            : base(logonIDorCAI, "Windows")
        {

            if ((logonIDorCAI == null) || (logonIDorCAI.Length == 0))
            {
                throw (new ArgumentException("Logon ID of the current user is empty. Make sure to enable Windows authentication on the website."));
            }
            PopulateUser(logonIDorCAI);

        }

        /// <summary>
        /// Populate User Attribute by CAI or Network ID
        /// </summary>
        /// <param name="logonIDorCAI">CAI or Network ID</param>
        private void PopulateUser(string logonId)
        {
            //string namaApp = ConfigurationManager.AppSettings["AppName"];
            if (logonId.Contains("\\"))
            {
                logonId = logonId.Split('\\')[1];
            }
            UserRole userManager = UserManagementServices.getUser(logonId);//_userRoleService.Get(logonId, namaApp);
            if (userManager == null)
            {
                UserName = logonId;
                DisplayName = "Anonymous";
                Email = "";
            }
            else
            {
                UserName = userManager.UID;
                DisplayName = userManager.Nama;
                Email = userManager.Email;
                User userPejabat = UserManagementServices.getUserJabatan(userManager.UID);
                if (userPejabat != null)
                {
                    Nopek = userPejabat.NoPek;
                    NamaPek = userPejabat.NamaPekerja;
                    IDJabatan = userPejabat.JabatanID.ToString();
                    NamaJabatan = userPejabat.Jabatan.Description;
                    IDPerusahaan = userPejabat.Perusahaan.Code;
                    NamaPerusahaan = userPejabat.Perusahaan.Description;
                }
                //RoleID = userManager.RoleID;
                //RoleName = userManager.Role.Description;
            }
        }

        /// Get Logon Current User
        /// </summary>
        public static CurrentUser GetCurrentUser()
        {
            CurrentUser retVal;

            Object cachedObject = HttpContext.Current.Cache[HttpContext.Current.User.Identity.Name];

            //Checks if user is already stored in cache
            if (cachedObject == null)
            {
                retVal = new CurrentUser();
                HttpContext.Current.Cache.Insert(HttpContext.Current.User.Identity.Name, retVal, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(GetPersonCacheTime()));
            }
            else
            {
                retVal = (CurrentUser)cachedObject;
            }

            return retVal;
        }

        /// Gets cache information expiration time previously set in web.config
        /// </summary>
        private static int GetPersonCacheTime()
        {
            AppSettingsReader configReader = new AppSettingsReader();

            int cacheSeconds = (int)configReader.GetValue("PersonCacheSeconds", typeof(int));

            return cacheSeconds;
        }
    }
}
