﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities
{
    public abstract class BaseEntities
    {
        public virtual int Id { get; set; }

        [StringLength(100)]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(100)]
        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }

        [Display(Name = "Updated Date")]
        public DateTime? UpdatedDate { get; set; }

        //[StringLength(100)]
        //[Display(Name = "Deleted By")]
        //public string DeletedBy { get; set; }

        //[Display(Name = "Deleted Date")]
        //public DateTime? DeletedDate { get; set; }

        //public bool StatusData { get; set; } = true;
    }
}
