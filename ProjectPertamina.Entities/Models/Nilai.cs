﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class Nilai : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Kode"), Required(ErrorMessage = "Kode harus diisi")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode adalah 10 Karakter")]
        public string Code { get; set; }

        [Display(Name = "Deskripsi"), Required(ErrorMessage = "Deskripsi harus diisi")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Deskripsi adalah 250 Karakter")]
        public string Description { get; set; }

        [Range(0.01, double.MaxValue, ErrorMessage = "Batas Toleransi harus lebih besar dari 0")]
        [Display(Name = "Batas Toleransi"), Required(ErrorMessage = "Batas Toleransi harus diisi")]
        public decimal Value { get; set; }
    }
}
