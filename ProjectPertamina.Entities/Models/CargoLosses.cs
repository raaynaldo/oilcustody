﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class CargoLosses : BaseEntities
    {
        public Roas Roas { get; set; }

        [Display(Name = "ROASId"), Required]
        public int ROASId { get; set; }

        [Display(Name = "Tanggal BL")]
        public DateTime BlDate { get; set; }

        [Display(Name = "Tanggal Bongkar")]
        public DateTime TanggalBongkar { get; set; }

        [Display(Name = "KursBeli")]
        public decimal KursBeli { get; set; }

        [Display(Name = "PriceRef")]
        public decimal PriceRef { get; set; }

        [StringLength(250)]
        [Display(Name = "Nama Kapal")]
        public string TransportName { get; set; }

        [Display(Name = "Nama Produk")]
        [StringLength(250)]
        public string NamaProduk { get; set; }

        [StringLength(250)]
        public string PortDescription { get; set; }

        [StringLength(50)]
        public string BlAsal { get; set; }
        public decimal BarrelsPtm { get; set; }
        public decimal BarrelsAl { get; set; }
        public decimal BarrelsBd { get; set; }
        public decimal Barrels { get; set; }
        public decimal R1 { get; set; }
        public decimal PercR1 { get; set; }

        [Column("[R1>0.3]")]
        [Display(Name = "[R1>0.3]")]
        [StringLength(50)]
        public string R103{ get; set; }

        [Display(Name = "[R2]")]
        public decimal R2 { get; set; }

        [Display(Name = "[PercR2]")]
        public decimal PercR2 { get; set; }

        [Column("[R2>0.15]")]
        [Display(Name = "[R2>0.15]")]
        [StringLength(50)]
        public string R2015 { get; set; }

        [Display(Name = "[R3]")]
        public decimal R3 { get; set; }

        [Display(Name = "[PercR3]")]
        public decimal PercR3 { get; set; }

        [Column("[R3>0.3]")]
        [Display(Name = "[R3>0.3]")]
        [StringLength(50)]
        public string R303 { get; set; }

        [Display(Name = "[R4]")]
        public decimal R4 { get; set; }

        [Display(Name = "[PercR4]")]
        public decimal PercR4 { get; set; }

        [Column("[R4>0.3]")]
        [Display(Name = "[R4>0.3]")]
        [StringLength(50)]
        public string R403 { get; set; }

        public decimal SupplyLossesValue { get; set; }
        public bool Deleted { get; set; }
    }
}
