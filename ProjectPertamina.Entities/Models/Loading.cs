﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class Loading : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Plant Code"), Required(ErrorMessage = "Plant Code harus di isi")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode adalah 10 Karakter")]
        public string Code { get; set; }

        [Display(Name = "Deskripsi"), Required(ErrorMessage = "Deskripsi harus di isi")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Deskripsi adalah 250 Karakter")]
        public string Description { get; set; }
    }
}
