﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class KonfigurasiParameter : BaseEntities
    {
        [Required]
        [Display(Name = "Key"), MaxLength(250)]
        public string Key { get; set; }

        [Required]
        [Display(Name = "Value"), MaxLength(250)]
        public string Value { get; set; }
    }
}
