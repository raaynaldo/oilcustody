﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class Roas : BaseEntities
    {

        public int PortId { get; set; }

        public Port Port { get; set; }

        [Display(Name = "No")]
        [Range(0, int.MaxValue, ErrorMessage = "No tidak boleh minus")]
        public int? No { get; set; }

        [Display(Name = "Doc Date"), Required(ErrorMessage = "Doc Date harus diisi")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Doc No"), Required(ErrorMessage = "Doc No harus diisi")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Doc No adalah 50 Karakter")]
        public string DocNo { get; set; }

        [Display(Name = "Bl Asal")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Bl Asal adalah 50 Karakter")]
        public string BlAsal { get; set; }

        [Display(Name = "Bl Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime BlDate { get; set; }

        [Display(Name = "Plant")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Plant adalah 10 Karakter")]
        public string Plant { get; set; }

        [Display(Name = "Refinery Name")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Refinery Name adalah 50 Karakter")]
        public string RefineryName { get; set; }

        [Display(Name = "Kode Produk")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode Produk adalah 10 Karakter")]
        public string KodeProduk { get; set; }

        [Display(Name = "Nama Produk")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Nama Produk adalah 250 Karakter")]
        public string NamaProduk { get; set; }

        [Display(Name = "Kd Material")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Kd Material adalah 50 Karakter")]
        public string KdMaterial { get; set; }

        [Display(Name = "Kode Fml Produk")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Kode Fml Material adalah 50 Karakter")]
        public string KodeFmlProduk { get; set; }

        [Display(Name = "Nama Fml Produk")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Nama Fml Material adalah 250 Karakter")]
        public string NamaFmlProduk { get; set; }

        [Display(Name = "Kode Opr")]
        [StringLength(1, ErrorMessage = "Batas Maksimal input Nama Fml Material adalah 1 Karakter")]
        public string KodeOpr { get; set; }

        [Display(Name = "Transport Type")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Transport Type adalah 50 Karakter")]
        public string TransportType { get; set; }

        [Display(Name = "Transport Name")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Transport Name adalah 250 Karakter")]
        public string TransportName { get; set; }

        [Display(Name = "Asal Tujuan")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Asal Tujuan adalah 250 Karakter")]
        public string AsalTujuan { get; set; }

        [Display(Name = "Pau Info")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Pau Info adalah 50 Karakter")]
        public string PauInfo { get; set; }

        [Display(Name = "Liter Obs")]
        public decimal LiterObs { get; set; }

        [Display(Name = "Liter 15")]
        public decimal Liter15 { get; set; }

        [Display(Name = "Mton")]
        public decimal Mton { get; set; }

        [Display(Name = "Barrels")]
        public decimal Barrels { get; set; }

        [Display(Name = "Lton")]
        public decimal Lton { get; set; }

        [Display(Name = "Liter Obs Ptm")]
        public decimal LiterObsPtm { get; set; }

        [Display(Name = "Liter 15 Ptm")]
        public decimal Liter15Ptm { get; set; }

        [Display(Name = "Mton Ptm")]
        public decimal MtonPtm { get; set; }

        [Display(Name = "Barrels Ptm")]
        public decimal BarrelsPtm { get; set; }

        [Display(Name = "Lton Ptm")]
        public decimal LtonPtm { get; set; }

        [Display(Name = "Liter Obs Al")]
        public decimal LiterObsAl { get; set; }

        [Display(Name = "Liter 15 Al")]
        public decimal Liter15Al { get; set; }

        [Display(Name = "Mton Al")]
        public decimal MtonAl { get; set; }

        [Display(Name = "Barrels Al")]
        public decimal BarrelsAl { get; set; }

        [Display(Name = "Lton Al")]
        public decimal LtonAl { get; set; }

        [Display(Name = "Liter Obs Bd")]
        public decimal LiterObsBd { get; set; }

        [Display(Name = "Liter 15 Bd")]
        public decimal Liter15Bd { get; set; }

        [Display(Name = "Mton Bd")]
        public decimal MtonBd { get; set; }

        [Display(Name = "Barrels Bd")]
        public decimal BarrelsBd { get; set; }

        [Display(Name = "Lton Bd")]
        public decimal LtonBd { get; set; }

        [Display(Name = "Liter Obs Rob")]
        public decimal LiterObsRob { get; set; }

        [Display(Name = "Liter 15 Rob")]
        public decimal Liter15Rob { get; set; }

        [Display(Name = "Mton Rob")]
        public decimal MtonRob { get; set; }

        [Display(Name = "Barrels Rob")]
        public decimal BarrelsRob { get; set; }

        [Display(Name = "Lton Rob")]
        public decimal LtonRob { get; set; }

        [Display(Name = "PO Number")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input PO Number adalah 50 Karakter")]
        public string PoNumber { get; set; }

        [Display(Name = "Del Order Number")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Del Order Number adalah 50 Karakter")]
        public string DelOrderNumber { get; set; }

        [Display(Name = "Shipment Number")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Shipment Number adalah 50 Karakter")]
        public string ShipmentNumber { get; set; }

        [Display(Name = "Peb Number")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Peb Number adalah 50 Karakter")]
        public string PebNumber { get; set; }

        [Display(Name = "SAP Material Doc")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input SAP Material Doc adalah 50 Karakter")]
        public string SAPMaterialDoc { get; set; }

        [Display(Name = "Interface")]
        [StringLength(1, ErrorMessage = "Batas Maksimal input Interface adalah 1 Karakter")]
        public string Interface { get; set; }

        [Display(Name = "Roas Created By")]
        [StringLength(50, ErrorMessage = "Batas Maksimal input Interface adalah 1 Karakter")]
        public string RoasCreatedBy { get; set; }

        [Display(Name = "Roas Created Date")]
        public DateTime? RoasCreatedOn { get; set; }

        [StringLength(100)]
        [Display(Name = "Roas Updated By")]
        public string RoasUpdatedBy { get; set; }

        [Display(Name = "Roas Updated On")]
        public DateTime? RoasUpdatedOn { get; set; }
    }
}
