﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinanceJettyDetail : BaseEntities
    {
        public virtual FinanceJetty FinanceJetty { get; set; }
        [Display(Name = "Finance Jetty"), Required]
        public int FinanceJettyId { get; set; }

        public virtual Tangki Tangki { get; set; }
        [Display(Name = "Tangki"), Required]
        public int TangkiId { get; set; }

        public virtual Cargo Cargo { get; set; }
        [Display(Name = "Cargo"), Required]
        public int CargoId { get; set; }
    }
}
