﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.ViewModel
{
    public class VMKKSupplyLossPipa
    {
        public DateTime BlDate { get; set; }
        public string TransportName { get; set; }
        public decimal D1QualityBl { get; set; }
        public decimal D1QualitiTanki { get; set; }
    }
       

    public class VMKKSupplyLossVessel
    {
        public DateTime BlDate { get; set; }
        public string TransportName { get; set; }
        public decimal D2ObqAwal { get; set; }
        public decimal D2ObqAkhir { get; set; }
        public decimal D2BarrelsRob { get; set; }
    }
}
