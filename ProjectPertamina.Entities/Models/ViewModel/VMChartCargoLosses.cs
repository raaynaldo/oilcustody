﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.ViewModel
{
    public class VMChartCargoLosses
    {
        public decimal PercR4 { get; set; }
        public string TransportName { get; set; }
        public decimal R4Values { get; set; }
    }
}
