﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.ViewModel
{
    public class VMSupplyLossesSupply
    {
        public decimal R1 { get; set; }
        public string TransportName { get; set; }
        public decimal PercR1 { get; set; }
    }
}
