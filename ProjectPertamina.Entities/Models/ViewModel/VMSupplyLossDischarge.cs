﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.ViewModel
{
    public class VMSupplyLossDischarge
    {
        public string TransportName { get; set; }
        public decimal R4Nett { get; set; }
        public decimal PercNett { get; set; }
    }
}
