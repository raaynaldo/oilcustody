﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class SupplyLossSupply : BaseEntities
    {
        public Roas Roas { get; set; }

        [Display(Name = "ROASId"), Required]
        public int ROASId { get; set; }

        [Display(Name = "Transport Type"), Required]
        [StringLength(50)]
        public string  TransportType { get; set; }

        [Display(Name = "Loading Port Code")]
        [StringLength(10)]
        public string LoadingPortCode { get; set; }

        [Display(Name = "Loading Port Desc")]
        [StringLength(250)]
        public string LoadingPortDesc { get; set; }

        [Display(Name = "Discharge Port Code")]
        [StringLength(10)]
        public string DischargePortCode { get; set; }

        [Display(Name = "Discharge Port Desc")]
        [StringLength(250)]
        public string DischargePortDesc { get; set; }

        [Display(Name = "Tanggal Bongkar")]
        public DateTime TanggalBongkar { get; set; }

        [Display(Name = "Doc Date")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Kode Material")]
        public string KdMaterial { get; set; }

        [Display(Name = "Nama Produk")]
        [StringLength(50)]
        public string NamaProduk { get; set; }

        [Display(Name = "Uom"), StringLength(20)]
        public string Uom { get; set; }

        [Display(Name = "Doc No"), StringLength(50)]
        public string DocNo { get; set; }

        [Display(Name = "Tanggal BL")]
        public DateTime BlDate { get; set; }

        [Display(Name = "BarrelsPtm")]
        public decimal BarrelsPtm { get; set; }

        [Display(Name = "SFAL")]
        public decimal SFAL { get; set; }

        [Display(Name = "R1")]
        public decimal R1 { get; set; }

        [Display(Name = "PercR1")]
        public decimal PercR1 { get; set; }

        [Display(Name = "Shipment Number"), StringLength(50)]
        public string ShipmentNumber { get; set; }

        [Display(Name = "TranportName")]
        public string TransportName { get; set; }

        public bool Deleted { get; set; }
    }
}
