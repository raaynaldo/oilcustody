﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinanceTangki : BaseEntities
    {
        public virtual Shift Shift { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Shift harus diisi")]
        [Display(Name = "Shift Id"), Required(ErrorMessage = "Shift harus diisi")]
        public int ShiftId { get; set; }

        public virtual PICKeuangan PICKeuangan { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "PIC Keuangan harus diisi")]
        [Display(Name = "PIC Keuangan Id"), Required(ErrorMessage = "PIC Keuangan harus diisi")]
        public int PICKeuanganId { get; set; }

        public virtual Cargo Cargo { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Cargo harus diisi")]
        [Display(Name = "Cargo Id")]
        public int CargoId { get; set; }

        public virtual Tangki Tangki { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "No Tangki harus diisi")]
        [Display(Name = "Tangki Id")]
        public int TangkiId { get; set; }

        [Display(Name = "No PO"), Required(ErrorMessage = "No PO harus diisi")]
        [StringLength(20, ErrorMessage = "Batas Maksimal input No PO adalah 20 Karakter")]
        public string NoPO { get; set; }

        [Required(ErrorMessage = "Tanggal harus diisi"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        [Display(Name = "Quantity"), Required(ErrorMessage = "Quantity harus diisi")]
        [DisplayFormat(DataFormatString = "{0:#.####}")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Quantity harus lebih besar dari 0")]
        public decimal Qty { get; set; }

        [Display(Name = "From/To"), Required(ErrorMessage = "From/To harus diisi")]
        [StringLength(100, ErrorMessage = "Batas Maksimal input From/To adalah 100 Karakter")]
        public string FromTo { get; set; }

        [Display(Name = "Keterangan per mobil tangki"), Required(ErrorMessage = "Keterangan per mobil tangki harus diisi")]
        public string keterangan { get; set; }

        [Display(Name = "Cat. Follow Up"), Required(ErrorMessage = "Cat. Follow Up harus diisi")]
        public string Catatan { get; set; }

        public virtual ICollection<FinanceTangkiDetail> FinanceTangkiDetails { get; set; }

        public FinanceTangki()
        {
            this.FinanceTangkiDetails = new HashSet<FinanceTangkiDetail>();
        }
    }
}
