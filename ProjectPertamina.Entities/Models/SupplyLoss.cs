﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class SupplyLoss : BaseEntities
    {
        public SupplyLossSupply SupplyLossSupply { get; set; }
        public int? SupplyLossSupplyId { get; set; }
        public SupplyLossDischarge SupplyLossDischarge { get; set; }
        public int? SupplyLossDischargeId { get; set; }
        [Display(Name = "Loading Master")]
        public Loading Loading { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Loading Master harus diisi")]
        public int LoadingId { get; set; }
        [Required]
        public DateTime Tanggal { get; set; }

        [StringLength(250), Required]
        public string NamaKapal { get; set; }

        public string Keterangan { get; set; }
        [Display(Name = "Berita Acara")]
        public string BeritaAcara { get; set; }
    }
}
