﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class CustodyTransfer : BaseEntities
    {
        //public Perusahaan Perusahaan { get; set; }
        public Stream Stream { get; set; }

        //[Display(Name = "Perusahaan")]
        [Index("IX_StreamDanTanggal", 3, IsUnique = true)]
        [StringLength(50)]
        public string PerusahaanKode { get; set; }

        //[Display(Name = "Stream")]
        //[Range(1, int.MaxValue, ErrorMessage = "Harap Masukan Stream")]
        [Index("IX_StreamDanTanggal", 1, IsUnique = true)]
        public int StreamId { get; set; }

        //[Display(Name = "Tanggal"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        [Index("IX_StreamDanTanggal", 2, IsUnique = true)]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        //[Display(Name = "MT")]
        //[Range(1, int.MaxValue, ErrorMessage = "Harap Masukan MT")]
        //[Required(ErrorMessage = "Harap Masukan MT")]
        public decimal MT { get; set; }

        //[Display(Name = "Specific Gravity (SG)")]
        //[Range(1, int.MaxValue, ErrorMessage = "Harap Masukan Specific Gravity (SG)")]
        //[Required(ErrorMessage = "Harap Masukan Specific Gravity (SG)")]
        public decimal SG { get; set; }

        //[Display(Name = "Kilowatt Hour (Kwh)")]
        //[Range(1, int.MaxValue, ErrorMessage = "Harap Masukan Kilowatt Hour (Kwh)")]
        //[Required(ErrorMessage = "Harap Masukan Kilowatt Hour (Kwh)")]
        public decimal Kwh { get; set; }

    }
}
