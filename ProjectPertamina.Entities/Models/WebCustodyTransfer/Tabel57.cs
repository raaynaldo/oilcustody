﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class Tabel57 : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Density 15"), Required(ErrorMessage = "Density 15 harus diisi")]
        public decimal Density15 { get; set; }

        [Display(Name = "Longton/1000L"), Required(ErrorMessage = "Longton/1000L Ton harus diisi")]
        public decimal LongTerm100L { get; set; }
    }
}
