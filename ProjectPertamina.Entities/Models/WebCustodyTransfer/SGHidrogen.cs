﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class SGHidrogen : BaseEntities
    {
        [Display(Name = "Bulan")]
        [Index("IX_BulanDanTahun", 1, IsUnique = true)]
        [Range(1, 12, ErrorMessage = "Bulan harus diisi")]
        public int Bulan { get; set; } = 0;

        [Display(Name = "Tahun"), Required(ErrorMessage = "Tahun harus diisi")]
        [Index("IX_BulanDanTahun", 2, IsUnique = true)]
        [Range(1900, 9999, ErrorMessage = "Range tahun dimulai dari 1900 sampai 9999")]
        public int Tahun { get; set; }

        [Display(Name = "Nilai SG"), Required(ErrorMessage = "Nilai SG harus diisi")]
        public decimal NilaiSG { get; set; }
    }
}
