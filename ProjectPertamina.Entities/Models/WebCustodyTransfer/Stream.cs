﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class Stream : BaseEntities
    {
        [Range(1, int.MaxValue, ErrorMessage = "Tipe Produk harus diisi")]
        public int TipeProdukId { get; set; }

        [Display(Name = "Tipe Produk")]
        public TipeProduk TipeProduk { get; set; }

        [Index(IsUnique = true)]
        [Display(Name = "Kode Stream"), Required(ErrorMessage = "Kode harus diisi")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode adalah 10 Karakter")]
        public string Kode { get; set; }

        [Display(Name = "Stream"), Required(ErrorMessage = "Stream harus diisi")]
        [StringLength(100, ErrorMessage = "Batas Maksimal input Stream adalah 100 Karakter")]
        public string StreamName { get; set; }

        [Display(Name = "Tag Number")]
        [StringLength(100, ErrorMessage = "Batas Maksimal input TagNumber adalah 100 Karakter")]
        public string TagNumber { get; set; }
    }
}
