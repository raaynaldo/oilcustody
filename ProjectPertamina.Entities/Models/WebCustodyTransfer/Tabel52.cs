﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class Tabel52 : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Density 15"), Required(ErrorMessage = "Density 15 harus diisi")]
        public decimal Density15 { get; set; }
        [Display(Name = "US Gallon 60F"), Required(ErrorMessage = "Us Gallon 60F harus diisi")]
        public decimal UsGallon60F { get; set; }
        [Display(Name = "Barrel 60F"), Required(ErrorMessage = "Barrel 60F harus diisi")]
        public decimal Barrel60F { get; set; }
    }
}
