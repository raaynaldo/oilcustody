﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class Perusahaan:BaseEntities
    {
        [StringLength(10)]
        public string Kode { get; set; }

        [StringLength(100)]
        public string Deskripsi { get; set; }
    }
}
