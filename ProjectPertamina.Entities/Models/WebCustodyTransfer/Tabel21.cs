﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class Tabel21 : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Specific Gravity"), Required(ErrorMessage = "Specific Gravity harus diisi")]
        public decimal SG { get; set; }
        [Display(Name = "Density 15"), Required(ErrorMessage = "Density 15 harus diisi")]
        public decimal Density15 { get; set; }
    }
}
