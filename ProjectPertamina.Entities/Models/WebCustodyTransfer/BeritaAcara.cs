﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class BeritaAcara : BaseEntities
    {
        public int Bulan { get; set; }
        public int Tahun { get; set; }
        public string Dokumen { get; set; }
    }
}
