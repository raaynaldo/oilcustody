﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models.WebCustodyTransfer
{
    public class TipeProduk : BaseEntities
    {
        [Display(Name = "Kode"), Required(ErrorMessage = "Kode harus diisi")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode adalah 10 Karakter")]
        public string Kode { get; set; }

        [Display(Name = "Deskripsi"), Required(ErrorMessage = "Deskripsi harus diisi")]
        [StringLength(100, ErrorMessage = "Batas Maksimal input Stream adalah 100 Karakter")]
        public string Deskripsi { get; set; }
    }
}
