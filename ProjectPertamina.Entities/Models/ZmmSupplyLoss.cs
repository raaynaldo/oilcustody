﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class ZmmSupplyLoss : BaseEntities
    {
        [Display(Name = "Shipment No"), StringLength(50), Required]
        public string ShipmentNo { get; set; }

        [Display(Name = "Shipment Status"), StringLength(50)]
        public string ShipmentStatus { get; set; }

        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Display(Name = "Loading Port"), StringLength(50)]
        public string LoadingPort { get; set; }

        [Display(Name = "Loading Port Dec")]
        public string LoadingPortDesc { get; set; }

        [Display(Name = "Loading Date")]
        public DateTime LoadingDate { get; set; }

        [Display(Name = "Discharge Port"), StringLength(50)]
        public string DischargePort { get; set; }

        [Display(Name = "Discharge Port Dec")]
        public string DischargePortDesc { get; set; }

        [Display(Name = "Discharge Date")]
        public DateTime DischargeDate { get; set; }

        [Display(Name = "Type"), StringLength(50)]
        public string Type { get; set; }

        [Display(Name = "Vessel"), StringLength(50)]
        public string Vessel { get; set; }

        [Display(Name = "Vessel Name"), StringLength(50)]
        public string VesselName { get; set; }

        [Display(Name = "BI Number"), StringLength(50)]
        public string BINumber { get; set; }

        [Display(Name = "Material No"), StringLength(50)]
        public string MaterialNo { get; set; }

        [Display(Name = "Material Name")]
        public string MaterialName { get; set; }

        [Display(Name = "Uom"), StringLength(20)]
        public string Uom { get; set; }

        [Display(Name = "Tipe"), StringLength(50)]
        public string Tipe { get; set; }

        [Display(Name = "Bl Gross")]
        public decimal BlGross { get; set; }

        [Display(Name = "Bl")]
        public decimal Bl { get; set; }

        [Display(Name = "Sfal")]
        public decimal Sfal { get; set; }

        [Display(Name = "Sfbd")]
        public decimal Sfbd { get; set; }

        [Display(Name = "Ar Gross")]
        public decimal ArGross { get; set; }

        [Display(Name = "Ar")]
        public decimal Ar { get; set; }

        [Display(Name = "New Bl")]
        public decimal NewBl { get; set; }

        [Display(Name = "Sfad")]
        public decimal Sfad { get; set; }

        [Display(Name = "R1")]
        public decimal R1 { get; set; }

        [Display(Name = "R2")]
        public decimal R2 { get; set; }

        [Display(Name = "R3")]
        public decimal R3 { get; set; }

        [Display(Name = "R4")]
        public decimal R4 { get; set; }

        [Display(Name = "R1Perc")]
        public decimal R1Perc { get; set; }

        [Display(Name = "R2Perc")]
        public decimal R2Perc { get; set; }

        [Display(Name = "R3Perc")]
        public decimal R3Perc { get; set; }

        [Display(Name = "R4PercGross")]
        public decimal R4PercGross { get; set; }

        [Display(Name = "R4Perc")]
        public decimal R4Perc { get; set; }

        [Display(Name = "DocType"), StringLength(50)]
        public string DocType { get; set; }

        [Display(Name = "R4Gross")]
        public decimal R4Gross { get; set; }
    }
}
