﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class SupplyLossDischarge : BaseEntities
    {
        public Roas Roas { get; set; }

        [Display(Name = "ROASId"), Required]
        public int ROASId { get; set; }

        [Display(Name = "Transport Type"), StringLength(50)]
        public string TransportType { get; set; }

        [Display(Name = "Loading Port Code"), StringLength(10)]
        public string LoadingPortCode { get; set; }

        [Display(Name = "Loading Port Desc"), StringLength(250)]
        public string LoadingPortDesc { get; set; }

        [Display(Name = "Discharge Port Code"), StringLength(10)]
        public string DischargePortCode { get; set; }

        [Display(Name = "Discharge Port Desc"), StringLength(250)]
        public string DischargePortDesc { get; set; }

        [Display(Name = "Tanggal Bongkar")]
        public DateTime TanggalBongkar { get; set; }

        [Display(Name = "Doc Date")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Kode Material")]
        public string KdMaterial { get; set; }

        [Display(Name = "Nama Produk")]
        [StringLength(50)]
        public string NamaProduk { get; set; }

        [Display(Name = "Uom"), StringLength(20)]
        public string Uom { get; set; }

        [Display(Name = "Doc No"), StringLength(50)]
        public string DocNo { get; set; }

        [Display(Name = "Tanggal BL")]
        public DateTime BlDate { get; set; }

        [Display(Name = "Bl Gross")]
        public decimal BlGross { get; set; }

        [Display(Name = "BarrelsPtm")]
        public decimal BarrelsPtm { get; set; }

        [Display(Name = "BarrelsAl")]
        public decimal BarrelsAl { get; set; }

        [Display(Name = "R1")]
        public decimal R1 { get; set; }

        [Display(Name = "PercR1")]
        public decimal PercR1 { get; set; }
        [Display(Name = "BarrelsBd")]
        public decimal BarrelsBd { get; set; }

        [Display(Name = "R2")]
        public decimal R2 { get; set; }

        [Display(Name = "PercR2")]
        public decimal PercR2 { get; set; }

        [Display(Name = "ARGross")]
        public decimal ARGross { get; set; }

        [Display(Name = "Barrels")]
        public decimal Barrels { get; set; }

        [Display(Name = "R3")]
        public decimal R3 { get; set; }

        [Display(Name = "PercR3")]
        public decimal PercR3 { get; set; }

        [Display(Name = "R4Gross")]
        public decimal R4Gross { get; set; }

        [Display(Name = "PercGross")]
        public decimal PercGross { get; set; }

        [Display(Name = "R4Nett")]
        public decimal R4Nett { get; set; }

        [Display(Name = "PercNett")]
        public decimal PercNett { get; set; }

        [Display(Name = "Sfad")]
        public decimal Sfad { get; set; }

        [Display(Name = "Shipment Number"), StringLength(50)]
        public string ShipmentNumber { get; set; }

        [Display(Name = "Transport Name")]
        public string TransportName { get; set; }

        [Display(Name = "Sap Material Doc")]
        public string SapMaterialDoc { get; set; }

        [Display(Name = "AccountingDoc"), StringLength(250)]
        public string AccountingDoc { get; set; } = "";

        [Display(Name = "Amount1")]
        public decimal Amount1 { get; set; }

        [Display(Name = "Amount2")]
        public decimal Amount2 { get; set; }

        public bool Deleted { get; set; }

        [Display(Name = "AccPostedBy"), StringLength(100)]
        public string AccPostedBy { get; set; }

        public DateTime AccPostedDate { get; set; } = DateTime.Now;

    }
}
