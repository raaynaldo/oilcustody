﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class KKSupplyLoss : BaseEntities
    {
        public Roas Roas { get; set; }

        [Display(Name = "ROASId"), Required]
        public int ROASId { get; set; }

        [Display(Name = "Nama Produk")]
        [StringLength(50)]
        public string NamaProduk { get; set; }

        [Display(Name = "TranportName")]
        public string TransportName { get; set; }

        [Display(Name = "Bl Date")]
        public DateTime BlDate { get; set; }

        [Display(Name = "Doc Date")]
        public DateTime DocDate { get; set; }

        [Display(Name = "Loading Port"), StringLength(250)]
        public string LoadingPort { get; set; }

        [Display(Name = "Loading Port Desc"), StringLength(250)]
        public string DischargePort { get; set; }

        [Display(Name = "PONumber"), StringLength(50)]
        public string PONumber { get; set; }

        [Display(Name = "DelOrderNumber"), StringLength(50)]
        public string DelOrderNumber { get; set; }

        [Display(Name = "ShipmentNumber"), StringLength(50)]
        public string ShipmentNumber { get; set; }

        [Display(Name = "BlGross")]
        public decimal BlGross { get; set; }

        [Display(Name = "BarrelsPtm")]
        public decimal BarrelsPtm { get; set; }

        [Display(Name = "BarrelsAl")]
        public decimal BarrelsAl { get; set; }

        [Display(Name = "BarrelsBd")]
        public decimal BarrelsBd { get; set; }

        [Display(Name = "ArGross")]
        public decimal ArGross { get; set; }

        [Display(Name = "Barrels")]
        public decimal Barrels { get; set; }

        public decimal R1 { get; set; }

        public decimal PercR1 { get; set; }

        public decimal R2 { get; set; }

        public decimal PercR2 { get; set; }

        public decimal R3 { get; set; }

        public decimal PercR3 { get; set; }

        public decimal R4Gross { get; set; }

        public decimal PercGross { get; set; }

        public decimal R4Nett { get; set; }

        public decimal PercNett { get; set; }

        public decimal ObqAwal { get; set; }

        public decimal ObqAkhir { get; set; }

        public decimal BarrelsRob { get; set; }

        public decimal QualityBl { get; set; }

        public decimal QualitySample { get; set; }

        public decimal QualityAft { get; set; }

        public decimal QualityBef { get; set; }

        public decimal QualityTanki { get; set; }

        public decimal FreeWaterSfal { get; set; }

        public decimal FreeWaterSfbd { get; set; }

        public decimal FreeWaterAr { get; set; }

        public bool Deleted { get; set; }
    }
}
