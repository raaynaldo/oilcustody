﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinanceJetty : BaseEntities
    {
        public virtual Shift Shift { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Shift harus diisi")]
        [Display(Name = "Shift Id"), Required(ErrorMessage = "Shift harus diisi")]
        public int ShiftId { get; set; }

        public virtual PICKeuangan PICKeuangan { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "PIC Keuangan harus diisi")]
        [Display(Name = "PIC Keuangan Id"), Required(ErrorMessage = "PIC Keuangan harus diisi")]
        public int PICKeuanganId { get; set; }

        public virtual PICLoadingMaster PICLoadingMaster { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "PIC Loading Master harus diisi")]
        [Display(Name = "PIC Loading Master Id"), Required(ErrorMessage = "PIC Loading Master harus diisi")]
        public int PICLoadingMasterId { get; set; }

        [Display(Name = "No PO"), StringLength(20), Required(ErrorMessage = "No PO harus diisi")]
        public string NoPo { get; set; }

        [Required(ErrorMessage = "Tanggal harus diisi"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        [Display(Name = "Jetty"), StringLength(100), Required(ErrorMessage = "Jetty harus diisi")]
        public string Jetty { get; set; }

        [Display(Name = "Jumlah Tangki"), Required(ErrorMessage = "Jumlah Tangki harus diisi")]
        public int JumlahTangki { get; set; }

        [Range(0.01, double.MaxValue, ErrorMessage = "Pump Rate harus lebih besar dari 0")]
        [Display(Name = "Pump Rate"), Required(ErrorMessage = "Pump Rate harus diisi")]
        public decimal PumpRate { get; set; }

        [Range(0.01, double.MaxValue, ErrorMessage = "Quantity harus lebih besar dari 0")]
        [Display(Name = "Quantity"), Required(ErrorMessage = "Quantity harus diisi")]
        public decimal Qty { get; set; }

        [Display(Name = "Estimasi Start"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? EstimasiStart { get; set; }
        [Display(Name = "Estimasi Stop"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? EstimasiStop { get; set; }
        [Display(Name = "Realisasi Start"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? RealisasiStart { get; set; }
        [Display(Name = "Realisasi Stop"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? RealisasiStop { get; set; }


        [Display(Name = "Keterangan"), Required(ErrorMessage = "Keterangan harus diisi")]
        public string Keterangan { get; set; }
        [Display(Name = "Cat. Follow Up"), Required(ErrorMessage = "Cat. Follow Up harus diisi")]
        public string Catatan { get; set; }
    }
}
