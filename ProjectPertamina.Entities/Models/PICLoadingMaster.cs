﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class PICLoadingMaster : BaseEntities
    {
        [Index(IsUnique = true)]
        [Display(Name = "Kode"), Required(ErrorMessage = "Kode harus diisi")]
        [StringLength(10, ErrorMessage = "Batas Maksimal input Kode adalah 10 Karakter")]
        public string Code { get; set; }

        [Display(Name = "Nama"), Required(ErrorMessage = "Nama harus diisi")]
        [StringLength(250, ErrorMessage = "Batas Maksimal input Nama adalah 250 Karakter")]
        public string Nama { get; set; }
    }
}
