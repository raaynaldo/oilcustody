﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinancePipa : BaseEntities
    {
        public virtual Shift Shift { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Shift harus diisi")]
        [Display(Name = "Shift Id"), Required(ErrorMessage = "Shift harus diisi")]
        public int ShiftId { get; set; }

        public virtual PICKeuangan PICKeuangan { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "PIC Keuangan harus diisi")]
        [Display(Name = "PIC Keuangan Id"), Required(ErrorMessage = "PIC Keuangan harus diisi")]
        public int PICKeuanganId { get; set; }

        public virtual Cargo Cargo { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Cargo Tangki harus diisi")]
        [Display(Name = "Cargo Id")]
        public int CargoId { get; set; }

        [Display(Name = "No PO"), Required(ErrorMessage = "No PO harus diisi")]
        [StringLength(20, ErrorMessage = "Batas Maksimal input Deskripsi adalah 20 Karakter")]
        public string NoPO { get; set; }

        [Required(ErrorMessage = "Tanggal harus diisi"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        [Display(Name = "Jumlah Tangki"), Required(ErrorMessage = "Jumlah Tangki harus diisi")]
        public int JumlahTangki { get; set; } = 1;

        [Range(0.01 , double.MaxValue, ErrorMessage = "Pump Rate harus lebih besar dari 0")]
        [Display(Name = "Pump Rate"), Required(ErrorMessage = "Pump Rate harus diisi")]
        public decimal PumpRate { get; set; }

        [Range(0.01 , double.MaxValue, ErrorMessage = "Quantity harus lebih besar dari 0")]
        [DisplayFormat(DataFormatString = "{0:N1}")]
        [Display(Name = "Quantity"), Required(ErrorMessage = "Quantity harus diisi")]
        public decimal Qty { get; set; }

        [Display(Name = "Estimasi Start"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? EstimasiStart { get; set; }
        [Display(Name = "Estimasi Stop"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? EstimasiStop { get; set; }
        [Display(Name = "Realisasi Start"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? RealisasiStart { get; set; }
        [Display(Name = "Realisasi Stop"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime? RealisasiStop { get; set; }

        [Display(Name = "From/To"), StringLength(100, ErrorMessage = "Batas Maksimal input Deskripsi adalah 100 Karakter"), Required(ErrorMessage = "From/To harus diisi")]
        public string FromTo { get; set; }
        [Display(Name = "Cat. Follow Up"), Required(ErrorMessage = "Cat. Follow Up harus diisi")]
        public string Catatan { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Nomor Tangki harus diisi")]
        [NoTangkiValidation]
        public ICollection<int> FinancePipaDetailSelect { get; set; }
        public virtual ICollection<FinancePipaDetail> FinancePipaDetails { get; set; }

        public FinancePipa()
        {
            this.FinancePipaDetails = new HashSet<FinancePipaDetail>();
        }
    }

    public class NoTangkiValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var FinancePipa = (FinancePipa)validationContext.ObjectInstance;

            if (FinancePipa.FinancePipaDetailSelect.Count == 0)
            {
                return new ValidationResult("Nomor Tangki harus diisi");
            }

            return FinancePipa.FinancePipaDetailSelect.Count < FinancePipa.JumlahTangki
            ? new ValidationResult("Nomor tangki kurang dari jumlah tangki yang sudah dipilih")
            : ValidationResult.Success;
        }
    }
}
