﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class OilMovement : BaseEntities
    {
        [Required(ErrorMessage = "Tanggal harus diisi"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Tanggal { get; set; } = DateTime.Now;

        [Display(Name = "No PO"), StringLength(20), Required(ErrorMessage = "No PO Tangki harus diisi")]
        public string NoPO { get; set; }

        [Display(Name = "Jam Mulai"), Required(ErrorMessage = "Jam Mulai harus diisi")]
        public TimeSpan JamMulai { get; set; }

        //[JamSelesaiValidation]
        [Display(Name = "Jam Selesai"), Required(ErrorMessage = "Jam Selesai harus diisi")]
        public TimeSpan JamSelesai { get; set; }

        [Display(Name = "No Polisi"), StringLength(50), Required(ErrorMessage = "No Polisi Tangki harus diisi")]
        public string NoPolisi { get; set; }

        [StringLength(100), Required(ErrorMessage = "Tujuan Tangki harus diisi")]
        public string Tujuan { get; set; }

        [Display(Name = "Meter Awal"), Required(ErrorMessage = "Meter Awal Tangki harus diisi")]
        [Range(0.01, double.MaxValue, ErrorMessage = "MeterAwal harus lebih besar dari 0")]
        public decimal MeterAwal { get; set; }

        [Display(Name = "Meter Akhir"), Required(ErrorMessage = "Meter Akhir Tangki harus diisi")]
        [Range(0.01, double.MaxValue, ErrorMessage = "MeterAkhir harus lebih besar dari 0")]
        public decimal MeterAkhir { get; set; }

        [Display(Name = "Kap Bridger"), Required(ErrorMessage = "Kap Bridger Tangki harus diisi")]
        [Range(0.01, double.MaxValue, ErrorMessage = "KapBridger harus lebih besar dari 0")]
        public decimal KapBridger { get; set; }
    }

    //public class JamSelesaiValidation : ValidationAttribute
    //{
    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        var OilMovement = (OilMovement)validationContext.ObjectInstance;

    //        return OilMovement.JamMulai > OilMovement.JamSelesai
    //        ? new ValidationResult("Jam Selesai lebih kecil dari Jam Mulai")
    //        : ValidationResult.Success;
    //    }
    //}
}
