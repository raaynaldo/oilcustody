﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinanceTangkiDetail : BaseEntities
    {
        public FinanceTangki FinanceTangki { get; set; }
        [Display(Name = "Finance Id"), Required]
        public int FinanceTangkiId { get; set; }

        [Display(Name = "No Polisi"), Required, StringLength(50)]
        public string NoPolisi { get; set; }

        [Display(Name = "Jumlah Kl"), Required]
        public decimal JumlahKl { get; set; }

        [Display(Name = "No Do"), Required, StringLength(50)]
        public string NoDo { get; set; }

        [Display(Name = "No Shipment"), Required, StringLength(50)]
        public string NoShipment { get; set; }

    }
}
