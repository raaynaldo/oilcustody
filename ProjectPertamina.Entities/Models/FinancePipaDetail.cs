﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPertamina.Entities.Models
{
    public class FinancePipaDetail : BaseEntities
    {
        public FinancePipa FinancePipa { get; set; }
        [Display(Name = "Finance Pipa Id"), Required]
        public int FinancePipaId { get; set; }

        public Tangki Tangki { get; set; }
        [Display(Name = "Tangki"), Required]
        public int TangkiId { get; set; }
    }
}
